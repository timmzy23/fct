#!/usr/bin/env node
const ARGV = require('yargs').argv;
const fs = require('fs');
const path = require('path');

function assertArgument(name, defaultValue) {
  if (ARGV[name] === undefined && defaultValue === undefined) {
    throw new Error(`Missing argument "${name}"`);
  } else {
    return ARGV[name] === undefined ? defaultValue : ARGV[name];
  }
}

const APP_NAME = assertArgument('name');
const VERSION = assertArgument('appVersion', '0.0.0');
const LICENSE = assertArgument('license', 'UNLICENSED');
const packageJson = require(`../package.json`);

const DEPS = assertArgument('deps', '')
    .split('\n')
    .map((d) => d.trim())
    .filter((d) => !!packageJson.dependencies[d])
    .map((d) => `"${d}": "${packageJson.dependencies[d]}"`);

const packageJsonFile = `{
  "name": "${APP_NAME}",
  "version": "${VERSION}",
  "license": "${LICENSE}",
  "private": true,
  "dependencies": {
  ${DEPS.join(',\n\t\t')}
  }
}`;

const packageJsonFilePath = 
  path.join(__dirname, '..', `./dist/apps/${APP_NAME}`, 'package.json');

fs.writeFile(packageJsonFilePath, packageJsonFile, (err) => {
  if (err) {
    console.error(`Cannot write into ${packageJsonFilePath}`, err);
    process.exit(1);
  }
  console.log(packageJsonFile);
  process.exit(0);
});
