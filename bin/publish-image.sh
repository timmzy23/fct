# !bin/bash

IMAGE=${1};
TAG=${2:-latest};
DOMAIN="4ctplatform";
CONFIGURATION=${3:-production};
DIST="./dist/apps"

function buildDockerImage() {
  local IMAGE_NAME=$1;
  local IMAGE_PATH=${2:-"./dist/apps/"};

  echo "Build Docker image ${DOMAIN}/${IMAGE_NAME}:${TAG} from ${IMAGE_PATH}";

  docker build -t $IMAGE_NAME:$TAG -f "${IMAGE_PATH}${IMAGE_NAME}/Dockerfile" "${IMAGE_PATH}${IMAGE_NAME}";
}

function buildNxApp() {
  local APP_NAME=$1;

  echo "Build Nx App ${APP_NAME}:${TAG} with configuration ${CONFIGURATION}";

  # creates build folder in dist
  npx ng build ${APP_NAME} --configuration=$CONFIGURATION --no-sourceMap;

  # copy dockerfile
  cp "./apps/${APP_NAME}/Dockerfile" "./dist/apps/${APP_NAME}/Dockerfile";
}

function buildServer() {
  local SERVER_NAME=$1;
  local DEPS=$2;

  buildNxApp $SERVER_NAME

  # create package.json for docker
  ./bin/create-package-json.js  \
      --name=${SERVER_NAME} \
      --deps="${DEPS}";

  buildDockerImage $SERVER_NAME;
}

function buildClient() {
  local CLIENT_NAME=$1;

  buildNxApp $CLIENT_NAME;

  buildDockerImage $CLIENT_NAME;
}

echo "Publish IMAGE ${DOMAIN}/${IMAGE}:${TAG} with config: ${CONFIGURATION}.";

# Build image resources if needed

# Build image
case $IMAGE in 
  "platform-deployer")
    buildDockerImage "platform-deployer" "./apps/"
  ;;
  "platform-proxy")
    buildDockerImage "platform-proxy" "./apps/"
  ;;
  "asap-browser")
    buildClient "asap-browser";
  ;;
  "asap-server")
    buildServer "asap-server" "
          @nest-modules/mailer
          @nestjs/common
          @nestjs/core
          @nestjs/cqrs
          @nestjs/mongoose
          @nestjs/platform-express
          class-transformer
          class-validator
          jsonwebtoken
          mongoose
          reflect-metadata
          dotenv
          rxjs";
  ;;
  "auth-server")
    buildServer "auth-server" "
          @nest-modules/mailer
          @nestjs/common
          @nestjs/core
          @nestjs/cqrs
          @nestjs/mongoose
          @nestjs/platform-express
          bcrypt
          class-transformer
          class-validator
          node-rsa
          jsonwebtoken
          mongoose
          reflect-metadata
          dotenv
          rxjs";
  ;;
  "auth-browser")
    buildClient "auth-browser";
  ;;
esac;

# Publish created image
docker tag $IMAGE:$TAG ${DOMAIN}/${IMAGE}:$TAG

docker push ${DOMAIN}/${IMAGE}:$TAG