import { getGreeting } from '../support/app.po';

describe('asap-browser', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to asap-browser!');
  });
});
