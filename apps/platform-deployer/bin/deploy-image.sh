#!/bin/sh

IMAGE="${1}";
TAG="${2:-latest}";
DOMAIN="4ctplatform";
CONTAINER_NAME=${IMAGE};
INTERNAL_NETWORK="platformnetwork"

# CONFIG
AUTH_SERVER_API_PORT=8080;
ASAP_SERVER_API_PORT=8081;

echo "Deploy ${DOMAIN}/${IMAGE}:${TAG}"

echo "Stop running container: ${CONTAINER_NAME}"
docker kill $CONTAINER_NAME &>/dev/null

echo "Remove old container: ${CONTAINER_NAME}"
docker container rm $CONTAINER_NAME &>/dev/null

echo "Pull new docker image: ${CONTAINER_NAME}"
docker pull "${DOMAIN}/${IMAGE}"

echo "Create internal network: ${INTERNAL_NETWORK}"
docker network create $INTERNAL_NETWORK &>/dev/null

case ${IMAGE} in
  "auth-browser")
    echo "Copy auth browser content into platform revers proxy."
    docker run -d \
        --name="${CONTAINER_NAME}" \
        --volume="/usr/html" \
        ${DOMAIN}/${IMAGE}:${TAG}

    docker cp $CONTAINER_NAME:/usr/html "./${CONTAINER_NAME}.pGlwFknjlF"
    docker exec platform-proxy rm -rf /usr/share/nginx/html/auth
    docker cp "./${CONTAINER_NAME}.pGlwFknjlF" platform-proxy:/usr/share/nginx/html/auth
    rm -rf "./${CONTAINER_NAME}.pGlwFknjlF";
  ;;
  "asap-browser")
    echo "Copy asap browser content into platform revers proxy."
    docker run -d \
        --name="${CONTAINER_NAME}" \
        --volume="/usr/html" \
        ${DOMAIN}/${IMAGE}:${TAG}

    docker cp $CONTAINER_NAME:/usr/html "./${CONTAINER_NAME}.pGlwFknjlF"
    docker exec platform-proxy rm -rf /usr/share/nginx/html/asap
    sed -i "s/\${AUTH_HOSTNAME}/${AUTH_HOSTNAME}/g" "./${CONTAINER_NAME}.pGlwFknjlF/config/app.config.json"
    docker cp "./${CONTAINER_NAME}.pGlwFknjlF" platform-proxy:/usr/share/nginx/html/asap
    
    rm -rf "./${CONTAINER_NAME}.pGlwFknjlF";
  ;;
  "auth-server")
      echo "Expose Auth Server on port: ${AUTH_SERVER_API_PORT}"

      docker run -d \
          --name="${CONTAINER_NAME}" \
          -p 127.0.0.1:$AUTH_SERVER_API_PORT:3333 \
          --hostname="${CONTAINER_NAME}" \
          --network $INTERNAL_NETWORK \
          --env AUTH_DB_CONNECTION_URL=$AUTH_DB_CONNECTION_URL \
          --env AUTH_EMAIL_PASSWORD=$AUTH_EMAIL_PASSWORD \
          --env AUTH_EMAIL_USER=$AUTH_EMAIL_USER \
          --env AUTH_EMAIL_PORT=${AUTH_EMAIL_PORT} \
          --env AUTH_EMAIL_HOST=${AUTH_EMAIL_HOST} \
          --env AUTH_EMAIL_FROM=${AUTH_EMAIL_FROM} \
          --env AUTH_CLIENT_PORT=${AUTH_CLIENT_PORT} \
          --env AUTH_SERVER_PORT=${AUTH_SERVER_PORT} \
          --env AUTH_ALLOWED_ORIGINS=${AUTH_ALLOWED_ORIGINS} \
          --env AUTH_HOSTNAME=${AUTH_HOSTNAME} \
          ${DOMAIN}/${IMAGE}:${TAG}
  ;;
  "asap-server")
      echo "Expose Auth Server on port: ${ASAP_SERVER_API_PORT}"
      docker run -d \
          --name="${CONTAINER_NAME}" \
          -p 127.0.0.1:${ASAP_SERVER_API_PORT}:3334 \
          --hostname="${CONTAINER_NAME}" \
          --network="${INTERNAL_NETWORK}" \
          --env ASAP_DB_CONNECTION_URL=$ASAP_DB_CONNECTION_URL \
          --env AUTH_URL=$AUTH_URL \
          --env AUTH_RSA_PUBLIC_URL=$AUTH_RSA_PUBLIC_URL \
          --env AUTH_RSA_INTERNAL_URL=$AUTH_RSA_INTERNAL_URL \
          --env ASAP_SERVER_PORT=$ASAP_SERVER_PORT \
          --env AUTH_HOSTNAME=${AUTH_HOSTNAME} \
          --env ASAP_HOSTNAME=${ASAP_HOSTNAME} \
          ${DOMAIN}/${IMAGE}:${TAG}
  ;;
  "platform-proxy")
    echo "Start Platform reverse proxy."

    docker run -d \
        --name="${CONTAINER_NAME}" \
        -p 0.0.0.0:80:80 \
        --network="${INTERNAL_NETWORK}" \
        --env ASAP_HOSTNAME=$ASAP_HOSTNAME \
        --env AUTH_HOSTNAME=$AUTH_HOSTNAME \
        ${DOMAIN}/${IMAGE}:${TAG}
  ;;
esac
