import {
  MiddlewareConsumer,
  Module,
  NestModule
  } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SdkNestModule } from 'fct/sdk-nest';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthAuthenticationMiddleware } from './auth/auth.authentication.middleware';
import { AuthModule } from './auth/auth.module';
import { ProjectController } from './project/project.controller';
import { ProjectModule } from './project/project.module';

const AUTH_CONFIG: AuthModule.Init = {
  authTokenAccessUrl: process.env.AUTH_URL,
  authRsaPublicKeyUrl: process.env.AUTH_RSA_PUBLIC_URL
}

@Module({
  imports: [
    AuthModule.forRoot(AUTH_CONFIG),
    MongooseModule.forRoot(process.env.ASAP_DB_CONNECTION_URL),
    ProjectModule,
    SdkNestModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  constructor() {}

  public configure(consumer: MiddlewareConsumer): MiddlewareConsumer {
    return consumer
        .apply(AuthAuthenticationMiddleware)
        .forRoutes(ProjectController);
  }
}
