import * as mongoose from 'mongoose';

function getProjectNodeContextDef(type: any, required: boolean): mongoose.SchemaDefinition {
  return {
    context: getProjectNodeContextValueDef(type, required),
    value: { type, required }
  }
}

function getProjectNodeContextValueDef(type: any, required: boolean): mongoose.SchemaDefinition {
  return {
    S: { type, required: false },
    M: { type, required: false },
    L: { type, required: false },
  }
}

const ProjectEditorDef: mongoose.SchemaDefinition = {
  name: { type: String },
  createdAt: { type: Date, default: () => new Date()},
}

const ProjectIndicatorDef: mongoose.SchemaDefinition = {
  author: ProjectEditorDef,
  id: String,
  name: String,
  notes: String,
  units: String,
  accessibility: String,
  source: String,
  uap: Boolean,
  values: getProjectNodeContextValueDef(String, false)
}

const ProjectNodeDef: mongoose.SchemaDefinition = {
  contentId: { type: Number, required: true },
  createdBy: { type: ProjectEditorDef },
  verifiedBy: { type: ProjectEditorDef },
  critical: { type: Boolean, required: false },
  xPath: { type: String, required: true },
  name: { type: String, required: true },
  fullName: { type: String, required: true },
  recommendation: { type: String, required: false },
  summary: { type: String, required: false },
  comment: getProjectNodeContextDef(String, false),
  score: getProjectNodeContextDef(Number, false),
  tendency: getProjectNodeContextDef(Number, false),
  weight: getProjectNodeContextDef(Number, false),
  indicators: [ ProjectIndicatorDef ],
}

const ProjectUserDef: mongoose.SchemaDefinition = {
  name: { type: String, required: true }
}

const ProjectDef: mongoose.SchemaDefinition = {
  projectName: { type: String, required: true },
  projectId: { type: String, required: true, index: { unique: true }},
  clientName: { type: String, required: false},
  projectVersion: { type: String, required: false},
  projectState: { type: String, required: false},
  projectScenario: { type: String, required: false},
  createdAt: { type: Date, required: true },
  modifiedAt: { type: Date, required: false },
  createdBy: ProjectUserDef,
  nodesCount: { type: Number, required: true},
  nodes: [ProjectNodeDef]
}

export const ProjectSchema = new mongoose.Schema(ProjectDef);
