import * as mongoose from 'mongoose';
import { ProjectEditorDef } from './project-editor.schema';

export const ProjectIndicatorDef: mongoose.SchemaDefinition = {
  id: { type: String, required: true, index: { unique: true }},
  author: { type: ProjectEditorDef, required: false },
}