import * as mongoose from 'mongoose';

export const ProjectEditorDef: mongoose.SchemaDefinition = {
  name: { type: String, required: true, index: { unique: true }},
  createdAt: { type: Date, required: true }
}

export const ProjectEditorModel = new mongoose.Schema(ProjectEditorDef);