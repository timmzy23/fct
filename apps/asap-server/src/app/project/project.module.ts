import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth';
import { ProjectSchema } from './project-schema';
import { ProjectController } from './project.controller';
import { ProjectApi } from './project.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'project', schema: ProjectSchema },
    ])
  ],
  controllers: [
    ProjectController
  ],
  providers: [
    ProjectApi,
  ]
})
export class ProjectModule {}