import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Req
  } from '@nestjs/common';
import {
  map,
  tap
  } from 'rxjs/operators';
import { AuthAuthenticationMiddleware } from '../auth/auth.authentication.middleware';
import { CreateProjectDto } from './project-dto';
import { UpdateProjectDto } from './project-dto/update-project.dto';
import { ProjectApi as ProjectService } from './project.service';

@Controller('project')
export class ProjectController {

  constructor(private projectService: ProjectService) {}

  @Post()
  public createProject(
    @Body() payload: CreateProjectDto, 
    @Req() req: AuthAuthenticationMiddleware.Request
  ) {
    return this.projectService
        .createProject(payload, req.user)
        .pipe(
          map((response: any) => ({ 
            id: response['_id'],
            projectName: response.projectName 
          })))
        .toPromise();
  }

  @Put()
  public updateProject(
    @Body() payload: UpdateProjectDto,
    @Req() req: AuthAuthenticationMiddleware.Request
  ) {
    return this.projectService
        .updateProject(payload, req.user)
        .pipe(
          map((response: any) => ({ 
            id: response['_id'],
            projectName: response.projectName 
          })))
        .toPromise();
  }

  @Get('headers')
  public getProjectHeaders() {
    return this.projectService
        .getProjectsHeaders()
        .pipe(map((projects) => ({ items: projects })))
        .toPromise();
  }

  @Get(':projectId')
  public getProject(@Param('projectId') projectId) {
    return this.projectService
        .getProject(projectId)
        .toPromise();
  }
}