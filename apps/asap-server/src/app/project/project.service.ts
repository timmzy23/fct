import {
  ProjectHeaders,
  ProjectInfo
  } from '@fct/asap-interfaces';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from } from 'rxjs';
import { CreateProjectDto } from './project-dto';
import { UpdateProjectDto } from './project-dto/update-project.dto';

@Injectable()
export class ProjectApi {
  constructor(
    @InjectModel('project') 
    private readonly projectModel: Model<any>
  ) {}

  public createProject(payload: CreateProjectDto, user: any) {
    return from(this.projectModel.create({
      ...payload.projectInfo,
      createdAt: new Date(),
      nodesCount: payload.nodes.length,
      createdBy: { name: `${user.identity.firstName} ${user.identity.lastName}` },
      nodes: payload.nodes
    }));
  }

  public updateProject(payload: UpdateProjectDto, user: any) {
    return from(this.projectModel.findByIdAndUpdate(payload.projectId, {
      ...payload.projectInfo,
      updatedAt: new Date(),
      nodesCount: payload.nodes.length,
      updatedBy: { name: `${user.identity.firstName} ${user.identity.lastName}` },
      nodes: payload.nodes
    }));
  }

  public getProject(projectId: string) {
    return from(this.projectModel.findById(projectId).exec());
  }

  public getProjectsHeaders() {
    return from(this.projectModel
        .find({}, { 
          "projectName": 1, 
          "projectId": 1, 
          "clientName": 1,
          "projectVersion": 1,
          "projectState": 1,
          "projectScenario": 1,
          "createdAt": 1,
          "createdBy": 1,
          "nodesCount": 1
        })
        .exec());
  }
}