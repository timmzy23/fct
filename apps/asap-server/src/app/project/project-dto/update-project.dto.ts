import {
  IsArray,
  IsNotEmpty
  } from 'class-validator';
import { ProjectInfoDto } from './project-info.dto';

export class UpdateProjectDto {
  @IsNotEmpty()
  projectId: string;

  @IsNotEmpty()
  projectInfo: ProjectInfoDto;

  @IsArray()
  nodes: any[];
}