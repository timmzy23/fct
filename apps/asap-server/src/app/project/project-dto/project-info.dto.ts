import {
  IsNotEmpty,
  IsNumber,
  IsString
  } from 'class-validator';

export class ProjectInfoDto {
  @IsNotEmpty()
  @IsString()
  projectName: string;

  @IsNotEmpty()
  @IsNumber()
  projectId: string;

  @IsString()
  clientName?: string;
}