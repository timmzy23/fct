import {
  IsArray,
  IsNotEmpty
  } from 'class-validator';
import { ProjectInfoDto } from './project-info.dto';

export class CreateProjectDto {
  @IsNotEmpty()
  projectInfo: ProjectInfoDto;

  @IsArray()
  nodes: any[];
}