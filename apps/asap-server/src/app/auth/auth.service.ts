import { ErrnoError } from '@fct/sdk';
import {
  HttpService,
  Inject,
  Injectable,
  OnModuleInit
  } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import {
  from,
  Observable,
  of,
  throwError
  } from 'rxjs';
import {
  catchError,
  map,
  tap
  } from 'rxjs/operators';
import { ERR_UNKNOWN } from '../main';
import { ERR_AUTH_SERVER_NOT_AVAILABLE } from './auth.errors';
import {
  AUTH_CONFIG_TOKEN,
  AuthInit
  } from './auth.types';

@Injectable()
export class AuthService implements OnModuleInit {
  private publicKey: string;

  constructor(
    @Inject(AUTH_CONFIG_TOKEN) private config: AuthInit,
    private httpService: HttpService
  ) {}

  public onModuleInit() {
    return this.httpService
      .get(this.config.authRsaPublicKeyUrl)
      .pipe(
        map((response) => response.data),
        tap((publicKey) => this.publicKey = publicKey),
        catchError((err: NodeJS.ErrnoException) => {
          console.log(`Request to: ${this.config.authRsaPublicKeyUrl} failed!`);
          console.error(err);
          if (err.code === 'ECONNREFUSED') {
            return throwError(new ErrnoError(
              ERR_AUTH_SERVER_NOT_AVAILABLE,
              'Auth public cannot be retrieved! ' + 
              `Authentication server at "${this.config.authRsaPublicKeyUrl}" is not available!`
            ));
          } else {
            return throwError(new ErrnoError(ERR_UNKNOWN));
          }
        }))
      .toPromise();
  }

  public fetchAccessToken(code: string) {
    return this.httpService
      .get(this.config.authTokenAccessUrl, {
        params: { code }
      })
      .pipe(
        map((response) => response.data),
        catchError((err) => {
          console.log(err);
          return of(err);
        }));
  }

  public refresh(prevAccessToken: string) {
    return this.httpService
      .get(this.config.authTokenAccessUrl, {
        params: { code: prevAccessToken }
      })
      .pipe(
        map((response) => response.data),
        catchError((err) => {
          console.log(err);
          return of(err);
        }));
  }

  public verify(accessToken: string): Observable<any> {
    return from(new Promise((resolve, reject) => {
      jwt.verify(
        accessToken, 
        this.publicKey,
        { algorithms: ['RS256'] },
        (err, payload) => {
          err ? reject(err) : resolve(payload);
        })
    }));
  }
}
