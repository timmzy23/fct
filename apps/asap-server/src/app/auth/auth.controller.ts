import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Options,
  Query,
  Req
  } from '@nestjs/common';
import { Request } from 'express';
import { throwError } from 'rxjs';
import {
  catchError,
  map
  } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

  constructor(
    private authService: AuthService
  ) {}

  @Get('access-token')
  public getAccessToken(@Query('code') code: string) {
    return this.authService.fetchAccessToken(code)
        .pipe(map((accessToken) => ({ accessToken })))
        .toPromise();
  }

  @Options('verify')
  public verify(@Req() req: Request) {
    const accessToken = (req.headers['authorization'] as string || '').split(' ')[1];

    return this.authService.verify(accessToken)
        .pipe(catchError((err) => {
          console.log(err);
          return throwError(new HttpException({}, HttpStatus.UNAUTHORIZED));
        }));
  }

  @Get('refresh')
  public getRefreshToken(@Req() req: Request) {
    const prevAccessToken = (req.headers['authorization'] as string || '').split(' ')[1];

    return this.authService.refresh(prevAccessToken);
  }
}
