import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware
  } from '@nestjs/common';
import {
  NextFunction,
  Request as ExpressRequest,
  Response
  } from 'express';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

export declare namespace AuthAuthenticationMiddleware {
  interface Request extends ExpressRequest {
    user: any;
  }
}

@Injectable()
export class AuthAuthenticationMiddleware implements NestMiddleware {

  constructor(private authService: AuthService) {}

  public use(req: AuthAuthenticationMiddleware.Request, res: Response, next: NextFunction) {
    const accessToken = (req.headers['authorization'] as string || '').split(' ')[1];

    this.authService.verify(accessToken).subscribe({
      next: (user) => {
        req.user = user;
        next();
      },
      error: (err) => {
        console.error(err);
        next(new HttpException({}, HttpStatus.UNAUTHORIZED));
      }
    });
  }
}
