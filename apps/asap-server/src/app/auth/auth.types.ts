export const AUTH_CONFIG_TOKEN = Symbol('auth.config');

export interface AuthInit {
  authTokenAccessUrl: string;
  authRsaPublicKeyUrl: string;
}
