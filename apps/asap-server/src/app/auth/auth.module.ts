import {
  DynamicModule,
  HttpModule,
  Module
  } from '@nestjs/common';
import { AuthAuthenticationMiddleware } from './auth.authentication.middleware';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import {
  AUTH_CONFIG_TOKEN,
  AuthInit
  } from './auth.types';

export declare namespace AuthModule {
  type Init = AuthInit;
}

@Module({
  imports: [HttpModule],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {
  public static forRoot(init: AuthModule.Init): DynamicModule {
    return {
      module: AuthModule,
      providers: [
        { provide: AUTH_CONFIG_TOKEN, useValue: init },
        AuthService,
        AuthAuthenticationMiddleware
      ]
    }
  }
}
