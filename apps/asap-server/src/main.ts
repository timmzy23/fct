import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import { AppModule } from './app/app.module';

dotenv.config();

console.log(process.env);
/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
async function bootstrap() {
  const globalPrefix = 'v1/api';
  const port = process.env.ASAP_SERVER_PORT;
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix(globalPrefix);
  app.useGlobalPipes(new ValidationPipe());
  app.use(bodyParser.json({limit: '50mb'}));

  app
    .listen(port, () => 
      console.log(
          'Listening at http://localhost:' + port + '/' + globalPrefix))
    .catch((error) => 
      console.error(error));
}

bootstrap();
    