module.exports = {
  name: 'asap-server',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/asap-server'
};
