module.exports = {
  name: 'auth-server',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/auth-server'
};
