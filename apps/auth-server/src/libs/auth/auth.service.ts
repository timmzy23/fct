import {
  assert,
  ErrnoError
  } from '@fct/sdk';
import {
  Inject,
  Injectable
  } from '@nestjs/common';
import {
  from,
  Observable,
  of,
  throwError
  } from 'rxjs';
import {
  catchError,
  mapTo,
  switchMap
  } from 'rxjs/operators';
import { AccessTokenService } from '../access-token';
import { PasswordService } from '../password';
import {
  UserDocument,
  UsersService
  } from '../users';
import {
  ERR_ACCOUNT_LOCKED_DOWN,
  ERR_UNKNOWN_IDENTIFIER
  } from './auth.errors';
import { AUTH_SERVICE_CONFIG } from './auth.interfaces';

export declare namespace AuthService {
  interface Config {
    maxAttemptsCount: number,
    lockDownDurationMs: number
  }
}

@Injectable()
export class AuthService {

  constructor(
    private passwords: PasswordService,
    private users: UsersService,
    private accessTokens: AccessTokenService,
    @Inject(AUTH_SERVICE_CONFIG)
    private config: AuthService.Config
  ) {}

  public authenticate(
    identifier: string, password: string, issuer: string
  ): Observable<any> {
    return this.users.findUserByIdentifier(identifier).pipe(
      switchMap((user) => {
        if (!user) {
          return throwError(new ErrnoError(ERR_UNKNOWN_IDENTIFIER));
        } else if (this.isLockedDown(user)) {
          return throwError(new ErrnoError(ERR_ACCOUNT_LOCKED_DOWN));
        } else {
          return this.passwords.verify(password, user.id).pipe(
            mapTo(user),
            switchMap((u) => this.handleSuccessLogin(u)),
            switchMap((u) => this.accessTokens.generateAccessToken({
              identity: {
                firstName: u.identity.firstName,
                identifier: u.identity.identifier,
                lastName: u.identity.lastName
              },
              issuer,
              userId: u.id,
            })),
            catchError((err) => this.handleFailedLoginAttempt(err, user))
          );
        }
      }));
  }

  private isLockedDown(user: UserDocument): boolean {
    const failedAttemptCount = user.metadata.failedAttemptCount || 0;
    const now = Date.now();
    const lastLoginAttempt = user.metadata.lastLoginAttempt;

    return failedAttemptCount >= this.config.maxAttemptsCount &&
      lastLoginAttempt && 
      (now - lastLoginAttempt.getTime() < this.config.lockDownDurationMs)
  }

  private handleSuccessLogin(user: UserDocument) {
    user.set('metadata.lastLoginAttempt', new Date());
    user.set('metadata.failedAttemptCount', 0);

    return from(user.save());
  }

  private handleFailedLoginAttempt(err: any, user?: UserDocument) {
    if (user) {
      user.set('metadata.lastLoginAttempt', new Date());
      user.set('metadata.failedAttemptCount', (user.metadata.failedAttemptCount || 0) + 1);

      return from(user.save()).pipe(switchMap(() => throwError(err)));
    } else {
      return throwError(err);
    }
  }
}