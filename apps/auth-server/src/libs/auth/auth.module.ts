import {
  DynamicModule,
  Global,
  Module
  } from '@nestjs/common';
import { AccessTokenModule } from '../access-token';
import { PasswordModule } from '../password';
import { UserModule } from '../users';
import { AUTH_SERVICE_CONFIG } from './auth.interfaces';
import { AuthService } from './auth.service';

export declare namespace AuthModule {
  interface Config {
    auth: AuthService.Config;
  }
}

@Global()
@Module({})
export class AuthModule {
  public static forRoot(init: AuthModule.Config): DynamicModule {
    return {
      module: AuthModule,
      imports: [
        PasswordModule,
        UserModule,
        AccessTokenModule
      ],
      providers: [
        AuthService,
        { provide: AUTH_SERVICE_CONFIG, useValue: init.auth }
      ],
      exports: [
        AuthService
      ]
    }
  }
}