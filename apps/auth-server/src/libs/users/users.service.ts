import {
  assert,
  assertPromise,
  switchPromise
  } from '@fct/sdk';
import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  from,
  Observable,
  of
  } from 'rxjs';
import {
  map,
  mapTo,
  switchMap,
  tap
  } from 'rxjs/operators';
import { SendEmailCommand } from '../emails';
import { PasswordService } from '../password';
import {
  TemporaryAccessToken,
  TemporaryAccessTokenService
  } from '../temporary-access-token';
import {
  ERR_TMP_USER_NOT_FOUND,
  ERR_USER_NOT_UNIQUE
  } from './users.errors';
import {
  User,
  UserDocument,
  UserIdentity,
  UserMetadata
  } from './users.interfaces';

export declare namespace UsersService {
  interface RequestPasswordInit {
    emailInit: PasswordService.RecoveryPasswordRequestEmailInit;
    buildReplyUrl: PasswordService.BuildReplyUrl;
    identifier: string;
  }

  interface CreateUserInit {
    emailInit: {
      from: string;
      subject: string;
      template: string;
    },
    userData: {
      identifier: string;
      password: string;
      firstName: string;
      lastName: string;
    },
    buildUrl: PasswordService.BuildReplyUrl 
  }
}

@Injectable()
export class UsersService {

  private confirmUserScopeToken = 'confirmUser';

  constructor(
    @InjectModel('user_tmp') 
    private readonly userTmpModel: Model<any>,
    @InjectModel('user') 
    private readonly userModel: Model<UserDocument>,
    private passwordService: PasswordService,
    private temporaryAccessTokens: TemporaryAccessTokenService,
    private commands$: CommandBus
  ) {}

  /**
   * Creates new user into userTmp collection where it will wait
   * until approved and gets back user id
   */
  public createUser(init: UsersService.CreateUserInit): Observable<UserDocument> {
    const userIdenity: UserIdentity = { 
      identifier: init.userData.identifier,
      firstName: init.userData.firstName,
      lastName: init.userData.lastName,
    };
    const userMetadata: UserMetadata = {
      createdAt: new Date(),
      loginCount: 0
    };
    const userInitData: Partial<User> = {
      identity: userIdenity,
      metadata: userMetadata,
    }

    return of(userInitData).pipe(
      // Check the unicity (no other active user with the same identifer)
      assertPromise((userData) => this.userModel
          .exists({ 'identity.identifier': userData.identity.identifier })
          .then((exists) => !exists),
        ERR_USER_NOT_UNIQUE),
      // Check whether such user exists already in tmp state
      assertPromise(
        (userData) => this.userTmpModel
          .exists({ 'identity.identifier': userData.identity.identifier })
          .then((exists) => !exists),
        ERR_USER_NOT_UNIQUE),
      // Hash email
      switchMap((userData) => from(this.passwordService.hashString(init.userData.identifier))
          .pipe(map((identityHash) => ({ ...userData, identityHash })))),
      // Store user in tmp until it is verified
      switchPromise((userData: User) => this.userTmpModel
          .create(userData)
          .then((result) => ({ ...userData, id: result.id }))),
      switchMap((user) => 
          from(this.passwordService.createPassword(init.userData.password, user.id)).pipe(
            mapTo(user))),
      switchMap((user: UserDocument) => this.createConfirmAccessToken(user.id).pipe(
          map((temporaryAccessToken: TemporaryAccessToken ) => ({ user, temporaryAccessToken })))),
      tap(({ user, temporaryAccessToken }) => this.commands$.execute(new SendEmailCommand({
        to: user.identity.identifier,
        from: init.emailInit.from,
        subject: init.emailInit.subject,
        template: init.emailInit.template,
        context: {
          accessToken: temporaryAccessToken.id,
          identifer: user.identity.identifier,
          firstName: user.identity.firstName,
          lastName: user.identity.lastName,
          url: init.buildUrl(temporaryAccessToken.id)
        }
      }))),
      map(({ user }) => user));
  }

  public confirmUser(tokenId: string) {
    return this.temporaryAccessTokens.getToken(tokenId, this.confirmUserScopeToken).pipe(
      switchMap((accessToken) => 
          from(this.userTmpModel.findById(accessToken.userId).exec()).pipe(
            assert((tmpUser) => !!tmpUser, ERR_TMP_USER_NOT_FOUND),
            map((tmpUser) => ({ accessToken, tmpUser })))),
        switchMap(({ accessToken, tmpUser }) => 
          from(this.userModel.create({ 
              identity: tmpUser.identity,
              identityHash: tmpUser.identityHash,
              metadata: {
                ...tmpUser.metadata,
                confirmedAt: new Date()
              }
          })).pipe(
            map((user) => ({ accessToken, tmpUser, user })))),
        switchMap((data) => this.passwordService
            .updatePasswordOwner(data.tmpUser.id, data.user.id)
            .pipe(mapTo(data))),
        switchMap(({ accessToken, tmpUser, user }) => 
            from((tmpUser as UserDocument).remove())
                .pipe(mapTo({ accessToken, user }))),
        switchMap(({ accessToken, user}) => 
            this.temporaryAccessTokens.removeToken(accessToken.id)
                .pipe(mapTo({ accessToken, user }))));
  }

  public createConfirmAccessToken(userId: string): Observable<TemporaryAccessToken> {
    return this.temporaryAccessTokens.createToken(
        userId, 
        [this.confirmUserScopeToken]);
  }

  public findUserByIdentifier(identifier: string): Observable<UserDocument> {
    return from(this.userModel
        .findOne({ 'identity.identifier': identifier })
        .exec());
  }

  public requestPasswordRecovery(init: UsersService.RequestPasswordInit) {
    return this.findUserByIdentifier(init.identifier).pipe(
      switchMap((user: UserDocument) => user ?
          this.passwordService.requestPasswordRecovery({
            emailInit: init.emailInit,
            buildReplayUrl: init.buildReplyUrl,
            userData: {
              firstName: user.identity.firstName,
              lastName: user.identity.lastName,
              identifier: init.identifier,
              userId: user.id
            },
          }) :
          of({})),
      mapTo({}));
  }
}