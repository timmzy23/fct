import * as mongoose from 'mongoose';

export interface UserIdentity {
  identifier: string;
  firstName: string;
  lastName: string;
  phoneNumber?: number;
}

export interface UserMetadata {
  createdAt: Date;
  updatedAt?: Date;
  confirmedAt?: Date;
  anonymisedAt?: Date;
  lastLoginAttempt?: Date;
  failedAttemptCount?: number;
  loginCount?: number;
}

export interface User {
  id?: string;
  identityHash: string;
  identity: UserIdentity;
  metadata: UserMetadata;
}

export interface UserDocument extends mongoose.Document {
  id: string;
  identityHash: string;
  identity: UserIdentity;
  metadata: UserMetadata;
}
