import * as mongoose from 'mongoose';

const UserIdentitySchemaDef: mongoose.SchemaDefinition = {
  identifier: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  phoneNumber: { type: String, required: false }
}

const UserMetadataDef: mongoose.SchemaDefinition = {
  createdAt: { type: Date, required: true },
  updatedAt: { type: Date, required: false },
  confirmedAt: { type: Date, required: false },
  anonymisedAt: { type: Date, required: false },
  lastLoginAttempt: { type: Date, required: false },
  failedAttemptCount: { type: Number, required: false },
  loginCount: { type: Number, required: false }
}

const UserSchemaDef: mongoose.SchemaDefinition = {
  identityHash: { type: String, required: true },
  identity: UserIdentitySchemaDef,
  metadata: UserMetadataDef,
}

const UserTmpSchemaDef: mongoose.SchemaDefinition = {
  identityHash: { type: String, required: true },
  identity: UserIdentitySchemaDef,
  metadata: {
    ...UserMetadataDef,
    createdAt: { type: Date, required: true, index: { expires: '4h' } }
  },
}

export const UserTmpSchema = new mongoose.Schema(UserTmpSchemaDef);

export const UserSchema = new mongoose.Schema(UserSchemaDef);
