import {
  Global,
  Module
  } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { PasswordModule } from '../password';
import { TemporaryAccessTokenModule } from '../temporary-access-token';
import {
  UserSchema,
  UserTmpSchema
  } from './users.schema';
import { UsersService } from './users.service';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'user', schema: UserSchema },
      { name: 'user_tmp', schema: UserTmpSchema },
    ]),
    CqrsModule,
    PasswordModule,
    TemporaryAccessTokenModule
  ],
  providers: [
    UsersService,
  ],
  exports: [
    UsersService
  ]
})
export class UserModule {}