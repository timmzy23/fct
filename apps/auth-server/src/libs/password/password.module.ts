import {
  Global,
  Module
  } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { AccessTokenModule } from '../access-token';
import { TemporaryAccessTokenModule } from '../temporary-access-token';
import { PasswordSchema } from './password.schema';
import { PasswordService } from './password.service';

@Global()
@Module({
  imports: [
    TemporaryAccessTokenModule,
    AccessTokenModule,
    CqrsModule,
    MongooseModule.forFeature([
      { name: 'password', schema: PasswordSchema },
    ]),
  ],
  providers: [
    PasswordService
  ],
  exports: [
    PasswordService
  ]
})
export class PasswordModule {

}