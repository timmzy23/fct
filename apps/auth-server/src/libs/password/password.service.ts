import { assert } from '@fct/sdk';
import { ISendMailOptions } from '@nest-modules/mailer';
import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';
import {
  from,
  Observable
  } from 'rxjs';
import {
  map,
  mapTo,
  switchMap,
  tap
  } from 'rxjs/operators';
import { AccessTokenService } from '../access-token';
import { SendEmailCommand } from '../emails';
import { TemporaryAccessTokenService } from '../temporary-access-token';
import {
  ERR_PASSWORD_NOT_FOUND,
  ERR_PASSWORD_NOT_MATCH
  } from './password.errors';
import {
  Password,
  PasswordDocument
  } from './password.interfaces';

export declare namespace PasswordService {
  interface RecoveryPasswordRequestEmailInit {
    to: string;
    from: string;
    subject: string;
    template: string;
  }

  type BuildReplyUrl = (accessToken: string) => string;

  interface RecoveryPasswordRequestInit {
    emailInit: RecoveryPasswordRequestEmailInit;
    buildReplayUrl: BuildReplyUrl;
    userData: {
      userId: string;
      identifier: string;
      firstName: string;
      lastName: string;
    }
  }
}

@Injectable()
export class PasswordService {
  public init = {
    saltRounds: 12
  }

  private passwordUpdateTokenScope = 'passwordUpdate';

  constructor(
    private temporaryAccessToken: TemporaryAccessTokenService,
    private accessTokenService: AccessTokenService,
    @InjectModel('password')
    private passwordModel: Model<PasswordDocument>,
    private commands$: CommandBus
  ) {}

  public encryptPassword(passwordStr: string): Observable<string> {
    return this.hashString(passwordStr, this.init.saltRounds);
  }

  public comparePassword(
    passwordStr: string, 
    passwordHash: string
  ): Observable<boolean> {
    return from(bcrypt.compare(passwordStr, passwordHash));
  }

  public createPasswordUpdateToken(userId: string) {
    return this.temporaryAccessToken.createToken(userId, [this.passwordUpdateTokenScope]);
  }

  public createPassword(passwordStr: string, userId: string): Observable<any> {
    return this.encryptPassword(passwordStr).pipe(
      map((passwordHash: string): Password => ({
        createdAt: new Date(),
        passwordHash,
        userId,
      })),
      switchMap((password: Password) => 
          from(this.passwordModel.create(password))),
      // Do not publish password hash outside. No reason for.
      map(() => ({})));
  }

  public updatePasswordOwner(prevUserId: string, newUserId: string) {
    return from(this.passwordModel
      .findOneAndUpdate(
        { 'userId': prevUserId }, 
        { 'userId': newUserId})
      .exec());
  }

  public verify(passwordStr: string, userId: string): Observable<boolean> {
    return from(this.passwordModel.findOne({ 'userId': userId })).pipe(
      assert((password: Password) => !!password, ERR_PASSWORD_NOT_FOUND),
      switchMap((password: Password) => 
          this.comparePassword(passwordStr, password.passwordHash)),
      assert((isPasswordMatch) => !!isPasswordMatch, ERR_PASSWORD_NOT_MATCH));
  }

  public updatePassword(userId: string, passwordStr: string) {
    return from(this.passwordModel.findOne({ 'userId': userId })).pipe(
      assert((password) => !!password, ERR_PASSWORD_NOT_FOUND),
      switchMap((password: PasswordDocument) => this.encryptPassword(passwordStr).pipe(
        map((newPasswordHash) => {
          password.set('passwordHash', newPasswordHash);
          password.set('modifiedAt', new Date());
          return password;
        }))),
      switchMap((password: PasswordDocument) => from(password.save())),
      map(() => ({})));
  }

  public updatePasswordByToken(tokenId: string, newPasswordStr: string)  {
    return this.temporaryAccessToken
      .getToken(tokenId, this.passwordUpdateTokenScope)
      .pipe(
        switchMap((token) => this.updatePassword(token.userId, newPasswordStr)
            .pipe(mapTo(token))),
        switchMap((token) => this.temporaryAccessToken.removeToken(token.id)));
  }

  public updatePasswordByJwt(bearer: string, newPasswordStr: string) {
    return this.accessTokenService.verifyAccessToken(bearer).pipe(
      switchMap((token) => this.updatePassword(token.subject, newPasswordStr))
    );
  }

  public deletePassword(userId: string) {
    from(this.passwordModel.deleteOne({ 'userId': userId }))
  }

  public requestPasswordRecovery(init: PasswordService.RecoveryPasswordRequestInit) {
    return from(this.createPasswordUpdateToken(init.userData.userId)).pipe(
      tap((accessToken) => {
        const emailInit: ISendMailOptions = {
          ...init.emailInit,
          context: {
            ...init.userData,
            accessToken,
            url: init.buildReplayUrl(accessToken.id)
          },
        };

        this.commands$.execute(new SendEmailCommand(emailInit));
      }));
  }

  public hashString(
    string: string, 
    saltRounds = this.init.saltRounds
  ): Observable<string> {
    return from(bcrypt.hash(string, saltRounds));
  }
}
