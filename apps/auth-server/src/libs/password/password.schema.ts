import * as mongoose from 'mongoose';
import { Password } from './password.interfaces';

const PasswordDef: mongoose.SchemaDefinition = {
  passwordHash: { 
    type: String, 
    required: true 
  },
  userId: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  modifiedAt: {
    type: Date,
  }
};

export const PasswordSchema = new mongoose.Schema<Password>(PasswordDef);
