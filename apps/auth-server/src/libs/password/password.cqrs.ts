import {
  EventsHandler,
  IEventHandler
  } from '@nestjs/cqrs';
import { PasswordService } from './password.service';

export declare namespace PasswordUpdatedEvent {

  type Payload = any;

  interface Meta {
    hostname: string;
    port: string;
    protocol: string;
    pathname: string;
  }
}

export class PasswordUpdatedEvent {
  public readonly payload: PasswordUpdatedEvent.Payload;
  public readonly meta: PasswordUpdatedEvent.Meta;
}

@EventsHandler(PasswordUpdatedEvent)
export class PasswordUpdatedHandler implements IEventHandler<PasswordUpdatedEvent> {

  constructor(private passwordService: PasswordService) {}

  handle(event: PasswordUpdatedEvent) {
    console.log('IMPLEMENTS EMAIL SENDING TO CONFIRM PASSWORD UPDATE');
    // this.passwordService...
  }
}
