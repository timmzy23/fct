import * as mongoose from 'mongoose';

export interface Password {
  userId: string;
  passwordHash: string;
  createdAt: Date;
  modifiedAt?: Date;
}

export interface PasswordDocument extends mongoose.Document {
  userId: string;
  passwordHash: string;
  createdAt: Date;
  modifiedAt?: Date;
}
