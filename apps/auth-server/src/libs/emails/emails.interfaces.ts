export interface EmailsWhitelistRegExpMap {
  [email: string]: RegExp;
}

export interface EmailsConfig {
  /**
   * List of addresses available to send mail for (use only in DEV mode!!!)
   */
  whitelist?: string[];

  /**
   * If entry match to given pattern it will be send
   * to given email.
   */
  whitelistRegExp?: EmailsWhitelistRegExpMap;
}

export const EMAIL_WHITELIST = Symbol('email-whitelist');

export const EMAIL_WHITELIST_REGEXP = Symbol('email-whitelist-regexp');
