import { ErrnoError } from '@fct/sdk';
import {
  ISendMailOptions,
  MailerService
  } from '@nest-modules/mailer';
import {
  Inject,
  Injectable,
  Optional
  } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ERR_EMAIL_NOT_WHITELISTED } from './emails.errors';
import {
  EMAIL_WHITELIST,
  EMAIL_WHITELIST_REGEXP,
  EmailsWhitelistRegExpMap
  } from './emails.interfaces';

@Injectable()
export class EmailService {
  constructor(
    private mailerService: MailerService,
    @InjectModel('emails_log')
    private emailLog: Model<any>,
    @Inject(EMAIL_WHITELIST) 
    @Optional()
    private whitelist: string[],
    @Inject(EMAIL_WHITELIST_REGEXP) 
    @Optional()
    private whitelistRegExp: EmailsWhitelistRegExpMap
  ) {}

  public sendEmail(options: ISendMailOptions): Promise<any> {
    const handledOptions = options;

    if (this.whitelistRegExp) {
      const whitelistedEmail: string = Reflect
          .ownKeys(this.whitelistRegExp)
          .find((email: string) => {
            const regExp = this.whitelistRegExp[email];

            return regExp.test(options.to);
          }) as string;
      if (!whitelistedEmail) {
        return Promise.reject(new ErrnoError(
          ERR_EMAIL_NOT_WHITELISTED,
          `Email "${options.to}" is not on whitelist. Consider to add it!`,
          { email: options.to }));
      } else {
        options.to = whitelistedEmail;
      }
    }

    return this.mailerService.sendMail(handledOptions)
        .then(() => {
          return this.emailLog.create({
            ...options,
            createdAt: Date.now()
          });
        })
  }
}