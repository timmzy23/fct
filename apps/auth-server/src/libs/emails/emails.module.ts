import { MailerModule } from '@nest-modules/mailer';
import {
  DynamicModule,
  Global,
  Module
  } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { SendEmailCommandHandler } from './emails.cqrs';
import {
  EMAIL_WHITELIST,
  EMAIL_WHITELIST_REGEXP,
  EmailsConfig
  } from './emails.interfaces';
import { EmailLogSchema } from './emails.schemas';
import { EmailService } from './emails.service';

export declare namespace EmailsModule {
  type Config = EmailsConfig;
}

@Global()
@Module({
  imports: [
    MailerModule,
    CqrsModule,
    MongooseModule.forFeature([
      { name: 'emails_log', schema: EmailLogSchema }
    ])
  ],
  exports: [
    EmailService
  ],
})
export class EmailsModule {
  public static forRoot(config: EmailsConfig): DynamicModule  {
    return {
      module: EmailsModule,
      providers: [
        { provide: EMAIL_WHITELIST, useValue: config.whitelist },
        { provide: EMAIL_WHITELIST_REGEXP, useValue: config.whitelistRegExp },
        EmailService,
        SendEmailCommandHandler
      ]
    }
  }
}
