import { ISendMailOptions } from '@nest-modules/mailer';
import {
  CommandHandler,
  ICommandHandler
  } from '@nestjs/cqrs';
import { EmailService } from './emails.service';

export declare namespace SendEmailCommand {
  type Payload = ISendMailOptions;
}

export class SendEmailCommand {
  constructor(public readonly payload: SendEmailCommand.Payload) {}
}

@CommandHandler(SendEmailCommand)
export class SendEmailCommandHandler implements ICommandHandler {

  constructor(private emailService: EmailService) {};

  public execute(command: SendEmailCommand): Promise<any> {
    return this.emailService.sendEmail(command.payload)
        .catch((err) => console.error(err));
  }
}
