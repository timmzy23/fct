import * as mongoose from 'mongoose';

const EmailLogDef: mongoose.SchemaDefinition = {
  to: { type: String, required: true },
  from: { type: String, required: true },
  subject: { type: String, required: true },
  html: { type: String },
  template: { type: String },
  context: { type: Object, required: true },
  createdAt: { type: Date, default: Date.now, index: { expires: '48h' }},
}

export const EmailLogSchema = new mongoose.Schema(EmailLogDef);
