import {
  Global,
  Module
  } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TemporaryAccessTokenSchema } from './tat.schemas';
import { TemporaryAccessTokenService } from './tat.service';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'temporary_access_token', schema: TemporaryAccessTokenSchema },
    ]),
  ],
  providers: [
    TemporaryAccessTokenService
  ],
  exports: [
    TemporaryAccessTokenService
  ]
})
export class TemporaryAccessTokenModule {}
