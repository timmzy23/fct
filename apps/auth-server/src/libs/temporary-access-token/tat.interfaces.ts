import * as mongoose from 'mongoose';

export interface TemporaryAccessToken {
  id: string;
  createdAt: Date;
  /**
   * Token is always related to single user and allows to perform
   * one time specific operation over that user informations
   * without being otherwise authenticated.
   */
  userId: string;
  /**
   * Defines purposes which token is created for to prevent
   * usage of the same token to gain access to unwanted
   * actions.
   */
  scope: string[];
}

export interface TemporaryAccessTokenDocument extends mongoose.Document {
  id: string;
  createdAt: Date;
  /**
   * Token is always related to single user and allows to perform
   * one time specific operation over that user informations
   * without being otherwise authenticated.
   */
  userId: string;
  /**
   * Defines purposes which token is created for to prevent
   * usage of the same token to gain access to unwanted
   * actions.
   */
  scope: string[];
}