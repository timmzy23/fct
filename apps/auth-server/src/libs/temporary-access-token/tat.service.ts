import { ErrnoError } from '@fct/sdk';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  from,
  Observable,
  of,
  throwError
  } from 'rxjs';
import {
  switchMap,
  tap
  } from 'rxjs/operators';
import {
  TEMPORARY_ACCESS_TOKEN_INVALID_SCOPE,
  TEMPORARY_ACCESS_TOKEN_NOT_FOUND
  } from './tat.errors';
import {
  TemporaryAccessToken,
  TemporaryAccessTokenDocument
  } from './tat.interfaces';

@Injectable()
export class TemporaryAccessTokenService {

  constructor(
    @InjectModel('temporary_access_token')
    private temporaryAccessToken: Model<TemporaryAccessTokenDocument>
  ) {}

  public createToken(userId: string, scope: string[]): Observable<TemporaryAccessToken> {
    const tokenInit: Partial<TemporaryAccessToken> = {
      createdAt: new Date(),
      userId,
      scope,
    }

    return from(this.temporaryAccessToken.create(tokenInit));
  }

  public getToken(tokenId: string, scope?: string): Observable<TemporaryAccessToken> {
    return from(this.temporaryAccessToken.findById(tokenId).exec()).pipe(
      switchMap((token) => !!token ? 
        of(token) : 
        throwError(new ErrnoError(TEMPORARY_ACCESS_TOKEN_NOT_FOUND))),
      switchMap((token) => !scope || token.scope.includes(scope) ?
          of(token) :
          throwError(new ErrnoError(TEMPORARY_ACCESS_TOKEN_INVALID_SCOPE)))
    );
  }

  public removeToken(tokenId: string) {
    return from(this.temporaryAccessToken.findByIdAndRemove(tokenId));
  }
}