import * as mongoose from 'mongoose';

const TemporaryAccessTokenDef: mongoose.SchemaDefinition = {
  createdAt: { 
    type: Date, 
    required: true, 
    default: Date.now,
    index: { expires: '15m' } 
  },
  userId: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: true,
  },
  scope: { 
    type: [String], 
    required: true 
  }
};

export const TemporaryAccessTokenSchema = 
    new mongoose.Schema(TemporaryAccessTokenDef);