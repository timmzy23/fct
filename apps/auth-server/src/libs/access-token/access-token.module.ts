import {
  DynamicModule,
  Global,
  Module,
  Provider
  } from '@nestjs/common';
import {
  ACCESS_TOKEN_CONFIG,
  ACCESS_TOKEN_RSA_CONFIG
  } from './access-token.interfaces';
import { AccessTokenRsaService } from './access-token.rsa.service';
import { AccessTokenService } from './access-token.service';

export declare namespace AccessTokenModule {
  interface Config {
    rsa: AccessTokenRsaService.Config;
    accessToken: AccessTokenService.Config;
  }
}

@Global()
@Module({})
export class AccessTokenModule {

  private static rootProviders: Provider[] = []

  public static forRoot(config: AccessTokenModule.Config): DynamicModule {
    AccessTokenModule.rootProviders = [
      { provide: ACCESS_TOKEN_RSA_CONFIG, useValue: config.rsa },
      { provide: ACCESS_TOKEN_CONFIG, useValue: config.accessToken },
      AccessTokenRsaService,
      AccessTokenService
    ];

    return {
      module: AccessTokenModule,
      providers: AccessTokenModule.rootProviders,
      exports: [
        AccessTokenService
      ]
    }
  }

  public static forFeature() {
    return {
      module: AccessTokenModule,
      providers: AccessTokenModule.rootProviders,
      exports: [
        AccessTokenService
      ]
    }
  }
}