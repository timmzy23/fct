import {
  Inject,
  Injectable,
  OnModuleInit
  } from '@nestjs/common';
import * as fs from 'fs';
import * as NodeRSA from 'node-rsa';
import * as path from 'path';
import { ACCESS_TOKEN_RSA_CONFIG } from './access-token.interfaces';

export declare namespace AccessTokenRsaService {
  interface Config {
    rsaKeyName: string;
    pathToRsaKeyFolder: string;
  }
}

/**
 * Service manages rsa private/public key designed for use
 * to sign JWT
 */
@Injectable()
export class AccessTokenRsaService implements OnModuleInit {

  private privateKey: string;
  private publicKey: string;

  constructor(
    @Inject(ACCESS_TOKEN_RSA_CONFIG) private config: AccessTokenRsaService.Config
  ) {}

  public onModuleInit() {
    // Check for rsa and rsa.pub keys and if they do not
    // exist, generate it.
    return Promise.resolve()
        .then(() => this.createRsaDirIfNotExists())
        .then(() => this.createRsaKeysIfNotExists())
        .then((pair) => {
          this.publicKey = pair.public;
          this.privateKey = pair.private;
        });
  }

  public getPrivateKey(): string {
    return this.privateKey;
  }

  public getPublicKey(): string {
    return this.publicKey;
  }

  private loadRsaKeys(): Promise<{ public: string, private: string }> {
    const keyName = this.config.rsaKeyName;

    return Promise
      .all([
        this.readRsaKey(keyName)
            .then((key) => ({ private: key })),
        this.readRsaKey(`${keyName}.pub`)
            .then((key) => ({ public: key })),
      ])
      .then((result: any[]) => 
        result.reduce((acc, fragment) => ({...acc, ...fragment }), {}))
  }

  private createRsaDirIfNotExists() {
    return this.rsaDirExists()
        .then<any>((exists) => exists || this.createRsaDir());
  }

  private createRsaKeysIfNotExists(): Promise<{ public: string, private: string }> {
    return this.rsaKeyExists().then((exists) => exists ? 
        this.loadRsaKeys() : 
        this.createRsaKeys());
  }

  private createRsaKeys() {
    const key = new NodeRSA().generateKeyPair();
    const privateKey = key.exportKey('pkcs1-private');
    const publicKey = key.exportKey('pkcs1-public');
    const keyName = this.config.rsaKeyName;

    return Promise.all([
      this.writeRsaKey(keyName, privateKey),
      this.writeRsaKey(`${keyName}.pub`, publicKey)
    ]).then(() => ({ 'public': publicKey, 'private': privateKey }));
  }

  private writeRsaKey(keyName, key: string) {
    const keyPath = path.join(this.config.pathToRsaKeyFolder, keyName);
    return new Promise((resolve, reject) => {
      fs.writeFile(
        keyPath, key, 
        { encoding: 'utf-8' }, 
        (err) => !err ? resolve() : reject(err));
    });
  }

  private readRsaKey(keyName: string) {
    const keyPath = path.join(this.config.pathToRsaKeyFolder, keyName);

    return new Promise((resolve, reject) => {
      fs.readFile(
        keyPath, 
        { encoding: 'utf-8' }, 
        (err, content) => !err ? resolve(content) : reject(err));
    });
  }

  private createRsaDir(): Promise<void> {
    return new Promise((resolve, reject) => {
      fs.mkdir(this.config.pathToRsaKeyFolder, (err) => {
        if (err) { reject(err) }
        else { resolve() }
      });
    });
  }

  private rsaDirExists(): Promise<boolean> {
    return new Promise((resolve) => {
      fs.stat(this.config.pathToRsaKeyFolder, (err, stats) => 
          err || !stats.isDirectory() ? 
            resolve(false) : resolve(true))
    });
  }

  private rsaKeyExists(): Promise<boolean> {
    return new Promise((resolve) => {
      const keyPath = path.join(this.config.pathToRsaKeyFolder, this.config.rsaKeyName);

      fs.stat(keyPath, (err, stats) => {
        err || !stats.isFile() ? 
          resolve(false) : resolve(true);
      });
    })
  }
}
