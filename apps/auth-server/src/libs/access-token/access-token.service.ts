import {
  assert,
  ErrnoError
  } from '@fct/sdk';
import {
  Inject,
  Injectable
  } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import {
  from,
  Observable,
  throwError
  } from 'rxjs';
import {
  catchError,
  switchMap
  } from 'rxjs/operators';
import {
  ERR_TOKEN_EXPIRED,
  ERR_TOKEN_MALLFORMATED
  } from './access-token.errors';
import { ACCESS_TOKEN_CONFIG } from './access-token.interfaces';
import { AccessTokenRsaService } from './access-token.rsa.service';

export declare namespace AccessTokenService {
  interface Config {
    jwtShortTtlSec: number;
    jwtLongTtlSec: number;
  }

  interface AccessTokenInit {
    issuer: string;
    userId: string;
    identity: {
      firstName: string;
      lastName: string;
      identifier: string;
    }
  }
}

@Injectable()
export class AccessTokenService {

  constructor(
    @Inject(ACCESS_TOKEN_CONFIG) 
    private config: AccessTokenService.Config,
    private rsaService: AccessTokenRsaService
  ) {}

  public generateAccessToken(init: AccessTokenService.AccessTokenInit, ttl?: number) {
    return from(new Promise<string>((resolve, reject) => {
      jwt.sign(
        {
          identity: init.identity, 
          ttl: ttl || (Date.now() / 1000) + this.config.jwtLongTtlSec,
          pathToRefresh: '/v1/api/auth/refresh',
        },
        this.rsaService.getPrivateKey(),
        { 
          algorithm: 'RS256', 
          expiresIn:  this.config.jwtShortTtlSec,
          subject: init.issuer,
          issuer: init.issuer
        },
        (err, token: string) => err ? reject(err) : resolve(token)
      )
    }))
  }

  public verifyAccessToken(bearer: string) {
    return this.fromVerifyPromise(bearer).pipe(
      assert(
        (tokenPayload: any) => tokenPayload.ttl > (Date.now() / 1000),
        ERR_TOKEN_EXPIRED));
  }

  public refreshAccessToken(prevBearer: string) {
    return this.verifyAccessToken(prevBearer).pipe(
      switchMap((tokenPayload) => this.generateAccessToken(
          {
            identity: tokenPayload.identity,
            issuer: tokenPayload.iss,
            userId: tokenPayload.sub
          }, 
          tokenPayload.ttl)),
      catchError((err) => {
        if (!(err instanceof jwt.JsonWebTokenError)) {
          return throwError(err);
        } else {
          if (err.message === 'jwt malformed') {
            return throwError(new ErrnoError(ERR_TOKEN_MALLFORMATED));
          } else {
            return throwError(err);
          }
        }
      }));
  }

  public getPublicRsa() {
    return this.rsaService.getPublicKey();
  }

  private fromVerifyPromise(accessToken: string): Observable<any> {
    return from(new Promise((resolve, reject) => {
      jwt.verify(
        accessToken, 
        this.rsaService.getPublicKey(),
        { algorithms: ['RS256'] },
        (err, payload) => err ? 
            reject(err) : resolve(payload))
    }));
  }
}