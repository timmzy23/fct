export const environment = {
  production: true,
  AUTH_SERVER_PORT: 3333,
  AUTH_ALLOWED_ORIGINS: [
    'http://localhost:4201'
  ],
  APP_ENV: 'prod',
  AUTH_EMAIL_WHITELIST: {
    'iskilber@gmail.com': /^(iskilber|kilbergr).*/,
    'ctibor@4ct.eu': /^ctibor.*/,
    'sedlacek@4ctplatform.eu': /^sedlacek.*/,
    'myska@4ctplatform.eu': /^myska.*/,
    'hornikova@4ctplatform.eu': /^hornikova.*/,
    'kostin@4ctplatform.eu': /^kostin.*/
  }
};
