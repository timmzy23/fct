export interface AuthSeverEnv {
  DB_CONNECTION_URL?: string;
  AUTH_EMAIL_USER?: string;
  AUTH_EMAIL_PASSWORD?: string;
  AUTH_EMAIL_PORT?: number;
  AUTH_EMAIL_HOST?: string;
  AUTH_EMAIL_FROM?: string;
  AUTH_EMAIL_REDIRECT_TO?: string;
  AUTH_CLIENT_PORT?: number;
  AUTH_URL?: string;
  AUTH_RSA_PUBLIC_URL?: string; 
  AUTH_SERVER_PORT?: number;
  AUTH_ALLOWED_ORIGINS?: string[];
  APP_ENV: string;
  AUTH_EMAIL_WHITELIST?: {
    [email: string]: RegExp
  }
}