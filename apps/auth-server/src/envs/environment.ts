export const environment = {
  production: false,
  passwordSaltRounds: 12,
  AUTH_EMAIL_PORT: 465,
  AUTH_EMAIL_HOST: 'smtp.gmail.com',
  AUTH_EMAIL_FROM: 'auth@4ctplatform.eu',
  AUTH_EMAIL_REDIRECT_TO: 'iskilber@gmail.com',
  AUTH_CLIENT_PORT: 4200,
  AUTH_URL: 'http://localhost:3333/v1/api/auth/access-token',
  AUTH_RSA_PUBLIC_URL: 'http://localhost:3333/v1/api/auth/rsa-public-key',
  AUTH_SERVER_PORT: 3333,
  AUTH_ALLOWED_ORIGINS: ['http://localhost:4201'],
  APP_ENV: 'dev',
  AUTH_EMAIL_WHITELIST: {
    'iskilber@gmail.com': /.*/
  }
};
