import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as dotenv from 'dotenv';
import { AppModule } from './app/app.module';

dotenv.config();

function bootstrap() {
  NestFactory.create(AppModule).then((app) => {
    const globalPrefix = 'v1/api';
    app.setGlobalPrefix(globalPrefix);
    app.useGlobalPipes(new ValidationPipe());
    const port = process.env.AUTH_SERVER_PORT || 3333;

    app.listen(port, () => {
      console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    });
  })
}

bootstrap();
    