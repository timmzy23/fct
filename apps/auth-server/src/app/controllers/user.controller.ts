import {
  ERR_DUPLICATED_ENTRY,
  ERR_UNKNOW_SERVER_ERROR
  } from '@fct/auth-common';
import { ErrnoError } from '@fct/sdk';
import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Query,
  Req
  } from '@nestjs/common';
import { Request } from 'express';
import { throwError } from 'rxjs';
import {
  catchError,
  map,
  mapTo
  } from 'rxjs/operators';
import {
  ERR_TOKEN_NOT_FOUND,
  ERR_USER_NOT_UNIQUE,
  PasswordService,
  UsersService
  } from '../../libs';
import {
  ConfirmUserDto,
  CreateUserDto,
  PasswordRecoveryRequestDto,
  UpdatePasswordDto
  } from '../app.dto';

@Controller('user')
export class UserController {
  constructor(
    private users: UsersService,
    private passwords: PasswordService
  ) {}

  @Post()
  public create(
    @Body() requestDto: CreateUserDto,
    @Req() request: Request
  ) {      
    const createUserInit: UsersService.CreateUserInit = {
      userData: {
        firstName: requestDto.firstName,
        lastName: requestDto.lastName,
        identifier: requestDto.identifier,
        password: requestDto.password
      },
      buildUrl: (accessToken: string) => this.buildAddress(
        accessToken, 
        requestDto.identifier, 
        request.protocol, 
        // request.hostname cannot be used because auth server can
        // run inside a docker container, where host name may point
        // virtual network between containers.
        process.env.AUTH_HOSTNAME,
        '/public/signup/confirm'
      ),
      emailInit: {
        from: 'noreply.auth@4ctplatform.eu',
        subject: '[4ct auth] registration confirmation',
        template: 'registration-confirm.email.hbs',
      }
    }

    return this.users.createUser(createUserInit).pipe(
        map((user) => ({
          identifier: user.identity.identifier,
          id: user.id
        })),
        catchError((err) => {
          let exception: HttpException;
          if (err instanceof ErrnoError && err.errno === ERR_USER_NOT_UNIQUE) {
            exception = new HttpException({
                error: {
                  message: 'Duplicated identifier!',
                  errno: ERR_DUPLICATED_ENTRY,
                  params: { identifier: requestDto.identifier }
                }
            }, HttpStatus.BAD_REQUEST);
          } else if (err instanceof ErrnoError && err.errno === ERR_TOKEN_NOT_FOUND) {
            exception = new HttpException({
              error: {
                message: 'Token already used!',
                errno: ERR_TOKEN_NOT_FOUND
              }
            }, HttpStatus.BAD_REQUEST);
          } else {
            console.error(err);
            exception = new HttpException({
              error: {
                message: 'Server Internal Error',
                errno: ERR_UNKNOW_SERVER_ERROR
              }
            }, HttpStatus.INTERNAL_SERVER_ERROR);
          }
          return throwError(exception);
        }))
      .toPromise();
  }

  @Post('confirm')
  public confirm(@Body() confirmDto: ConfirmUserDto) {
    return this.users.confirmUser(confirmDto.token).pipe(
        map(({ user }) => ({ identifier: user.identity.identifier })));
  }

  @Post('password/request')
  public requestPasswordRecovery(
    @Body() requestDto: PasswordRecoveryRequestDto,
    @Req() req: Request
  ) {
    const recoveryPasswordInit: UsersService.RequestPasswordInit = {
      identifier: requestDto.identifier,
      emailInit: {
        to: requestDto.identifier,
        from: 'noreply.auth@4ctplatform.eu',
        subject: '[4ct auth] password recovery request',
        template: 'password-recovery-request.email.hbs'
      },
      buildReplyUrl: (accessToken: string) => this.buildAddress(
        accessToken, 
        requestDto.identifier, 
        req.protocol, 
        req.hostname,
        '/public/password/reset'),
    };

    return this.users.requestPasswordRecovery(recoveryPasswordInit);
  }

  @Post('password/reset')
  public resetPassword(
    @Body() updatePasswordDto: UpdatePasswordDto,
    @Req() req: Request,
    @Query('token') tokenId: string
  ) {
    const authorization = req.headers['authorization'] as string;
    if (authorization) {
      const bearerToken = authorization.split(' ')[1];

      return this.passwords
          .updatePasswordByJwt(bearerToken, updatePasswordDto.newPassword)
          .pipe(
            mapTo({}),
            catchError((err) => {
              console.error(err);
              return throwError(new HttpException({} , HttpStatus.UNAUTHORIZED));
            }));
    } else if (tokenId) {
      return this.passwords
        .updatePasswordByToken(tokenId, updatePasswordDto.newPassword)
        .pipe(
          mapTo({}),
          catchError((err) => {
            console.error(err);
            return throwError(new HttpException({} , HttpStatus.UNAUTHORIZED));
          }));
    } else {
      return Promise.reject(new HttpException({} , HttpStatus.UNAUTHORIZED));
    }
  }

  private buildAddress(
    accessTokenStr: string, 
    identifier: string, 
    protocol: string, 
    hostname: string,
    pathname: string
  ) {
    const port = process.env.AUTH_CLIENT_PORT;

    return `${protocol}://${hostname}:${port}${pathname}?token=${accessTokenStr}&identifier=${identifier}`;
  }
}