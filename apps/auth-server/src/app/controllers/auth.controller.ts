import {
  ERR_UNIDENTIFIED,
  ERR_UNKNOW_SERVER_ERROR
  } from '@fct/auth-common';
import { ErrnoError } from '@fct/sdk';
import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Options,
  Post,
  Req
  } from '@nestjs/common';
import { Request } from 'express';
import { throwError } from 'rxjs';
import {
  catchError,
  map
  } from 'rxjs/operators';
import {
  AccessTokenService,
  AuthService,
  ERR_ACCOUNT_LOCKED_DOWN,
  ERR_INVALID_PASSWORD,
  ERR_TOKEN_EXPIRED,
  ERR_TOKEN_MALLFORMATED,
  ERR_UNKNOWN_IDENTIFIER
  } from '../../libs';
import {
  IdentifyDto,
  LoginDto
  } from '../app.dto';

@Controller('auth')
export class AuthController {

  constructor(
    private authService: AuthService,
    private accessTokens: AccessTokenService
    ) {}

  /**
   * Checks which authority is going to handle authentication process.
   * (Ours or some third party service like microsoft or goolge)
   */
  @Post('identify')
  public identify(@Body() identifyDto: IdentifyDto) {
    return {
      status: 'accepted'
    }
  }

  @Post('login')
  public login(@Body() loginDto: LoginDto, @Req() req: Request) {
    const issuer = `${req.protocol}://${req.hostname}:${process.env.AUTH_SERVER_PORT}`;

    return this.authService
      .authenticate(loginDto.identifier, loginDto.password, issuer)
      .pipe(
        map((accessToken) => ({ accessToken })),
        catchError((err: any) => {
          console.error(err);
          let exception: HttpException;
          if (err instanceof ErrnoError && ([
            ERR_UNKNOWN_IDENTIFIER, 
            ERR_ACCOUNT_LOCKED_DOWN, 
            ERR_INVALID_PASSWORD
          ].includes(err.errno))) {
            exception = new HttpException({
              message: 'Login failed',
              errno: ERR_UNIDENTIFIED
            }, HttpStatus.UNAUTHORIZED);
          } else {
            exception = new HttpException({
              message: 'Server Internal Error',
              errno: ERR_UNKNOW_SERVER_ERROR
            }, HttpStatus.INTERNAL_SERVER_ERROR);
          }
          return throwError(exception);
      }));
  }

  /**
   * Verify and decode provided access token
   */
  @Options('verify')
  public verify(@Req() req: Request) {
    const authorization = req.headers['authorization'] as string;

    if (!authorization) {
      return Promise.reject(new HttpException({} , HttpStatus.UNAUTHORIZED));
    } 
    const bearerToken = authorization.split(' ')[1];

    return this.accessTokens.verifyAccessToken(bearerToken).pipe(
        catchError((err) => {
          console.log(err);
          return throwError(new HttpException({ }, HttpStatus.UNAUTHORIZED))})
      );
  }

  @Get('rsa-public-key')
  public getRsaPublicKey() {
    return Promise.resolve(this.accessTokens.getPublicRsa());
  }

  @Get('refresh')
  public getRefreshToken(@Req() req: Request) {
    const prevAccessToken = (req.headers['authorization'] as string || '').split(' ')[1];

    return this.accessTokens.refreshAccessToken(prevAccessToken).pipe(
        map((newAccessToken) => ({ accessToken: newAccessToken })),
        catchError((err: any) => {
          if (err instanceof ErrnoError && [
            ERR_TOKEN_EXPIRED,
            ERR_TOKEN_MALLFORMATED
          ].includes(err.errno)) {
            return throwError(new HttpException({}, HttpStatus.UNAUTHORIZED))
          } else {
            return throwError(new HttpException({}, HttpStatus.INTERNAL_SERVER_ERROR));
          }
        }));
  }

  /**
   * Gets back list of allowed origins
   */
  @Get('allowed-origins')
  public getAllowedOrigins() {
    const origins = process.env.AUTH_ALLOWED_ORIGINS.split(';');

    return { origins };
  }
}