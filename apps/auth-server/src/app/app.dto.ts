import {
  IsEmail,
  IsString
  } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  public readonly identifier: string;

  public readonly password: string;

  @IsString()
  public readonly firstName: string;

  @IsString()
  public readonly lastName: string;
}

export class ConfirmUserDto {
  @IsString()
  public readonly token: string;
}

export class PasswordRecoveryRequestDto {
  @IsEmail()
  public readonly identifier: string;
}

export class UpdatePasswordDto {
  public readonly newPassword: string
}

export class IdentifyDto {
  @IsEmail()
  public readonly identifier: string;
}

export class LoginDto {
  @IsString()
  public readonly identifier: string;

  @IsString()
  public readonly password: string;
}
