import {
  HandlebarsAdapter,
  MailerModule
  } from '@nest-modules/mailer';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { SdkNestModule } from 'fct/sdk-nest';
import * as path from 'path';
import { environment } from '../envs/environment';
import {
  AccessTokenModule,
  AuthModule,
  EmailsModule,
  PasswordModule,
  UserModule
  } from '../libs';
import {
  AuthController,
  UserController
  } from './controllers';

const NODEMAILER_SETTINGS = {
  transport: {
    auth: {
      pass: process.env.AUTH_EMAIL_PASSWORD,
      user: process.env.AUTH_EMAIL_USER,
    },
    host: process.env.AUTH_EMAIL_HOST,
    port: process.env.AUTH_EMAIL_PORT,
    secure: true,
  },
  defaults: {
    from:`"4ct auth" <${process.env.AUTH_EMAIL_FROM}>`,
  },
  template: {
    dir: __dirname + '/assets/email-templates',
    adapter: new HandlebarsAdapter(),
    options: {
      strict: true,
    },
  },
};

const AUTH_CONFIG: AuthModule.Config = {
  auth: {
    maxAttemptsCount: 5,
    lockDownDurationMs: 1000 * 60 * 15
  }
}

const EMAIL_CONFIG: EmailsModule.Config = {
  whitelistRegExp: environment.AUTH_EMAIL_WHITELIST,
}

const ACCESS_TOKEN_CONFIG: AccessTokenModule.Config = {
  rsa: {
    pathToRsaKeyFolder: path.join(__dirname, './rsa'),
    rsaKeyName: 'auth.rsa',
  },
  accessToken: {
    jwtLongTtlSec:  60 * 60 * 24,
    jwtShortTtlSec: 60 * 3,
  }
}

@Module({
  imports: [
    MongooseModule.forRoot(process.env.AUTH_DB_CONNECTION_URL),
    CqrsModule,
    SdkNestModule,
    AccessTokenModule.forRoot(ACCESS_TOKEN_CONFIG),
    MailerModule.forRoot(NODEMAILER_SETTINGS),
    UserModule,
    PasswordModule,
    EmailsModule.forRoot(EMAIL_CONFIG),
    AuthModule.forRoot(AUTH_CONFIG),
  ],
  controllers: [
    UserController,
    AuthController
  ],
})
export class AppModule {

}
