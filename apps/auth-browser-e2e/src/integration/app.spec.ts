import { getGreeting } from '../support/app.po';

describe('auth-browser', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to auth-browser!');
  });
});
