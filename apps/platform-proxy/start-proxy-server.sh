#!/bin/sh

echo "STARTING..."

sed -i "s/\${AUTH_HOSTNAME}/${AUTH_HOSTNAME}/g" /etc/nginx/conf.d/auth.4ctasap.conf
sed -i "s/\${ASAP_HOSTNAME}/${ASAP_HOSTNAME}/g" /etc/nginx/conf.d/4ctasap.conf

cat /etc/nginx/conf.d/auth.4ctasap.conf

cat /etc/nginx/conf.d/4ctasap.conf

nginx -g "daemon off;"
