import { FluxStandardAction } from '@fct/sdk-react';

enum SsoMessageTypes {
  READY = 'SSO_READY',
  NOT_AUTHENTICATED = 'NOT_AUTHENTICATED',
  AUTH_TOKEN = 'AUTH_TOKEN',
  REQUEST_TOKEN = 'REQUEST_TOKEN',
  REGISTER = 'REGISTER'
}

export declare namespace SsoNotAuthenticatedMessage {
  type ACTION_TYPE = SsoMessageTypes.NOT_AUTHENTICATED;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    url: string;
  }
}

export class SsoNotAuthenticatedMessage {
  public static ACTION_TYPE: SsoNotAuthenticatedMessage.ACTION_TYPE = 
    SsoMessageTypes.NOT_AUTHENTICATED;

  public static create(payload: SsoNotAuthenticatedMessage.Payload): SsoNotAuthenticatedMessage.Action {
    return FluxStandardAction.create(SsoNotAuthenticatedMessage.ACTION_TYPE, payload);
  }
}

export declare namespace SsoAuthTokenMessage {
  type ACTION_TYPE = SsoMessageTypes.AUTH_TOKEN;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    authToken: string;
    identity: any;
  }
}

export class SsoAuthTokenMessage {
  public static ACTION_TYPE: SsoAuthTokenMessage.ACTION_TYPE = 
    SsoMessageTypes.AUTH_TOKEN;

  public static create(payload: SsoAuthTokenMessage.Payload): SsoAuthTokenMessage.Action {
    return FluxStandardAction.create(SsoAuthTokenMessage.ACTION_TYPE, payload);
  }
}

export declare namespace SsoRegisterMessage {
  type ACTION_TYPE = SsoMessageTypes.REGISTER;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    authCode: string;
    redirectPath: string;
  }
}

export class SsoRegisterMessage {
  public static ACTION_TYPE: SsoRegisterMessage.ACTION_TYPE = 
    SsoMessageTypes.REGISTER;

  public static create(payload: SsoRegisterMessage.Payload): SsoRegisterMessage.Action {
    return FluxStandardAction.create(SsoRegisterMessage.ACTION_TYPE, payload);
  }
}

export declare namespace SsoReadyMessage {
  type ACTION_TYPE = SsoMessageTypes.READY;
  type Action = FluxStandardAction.Action<ACTION_TYPE>
}

export class SsoReadyMessage {
  public static ACTION_TYPE: SsoReadyMessage.ACTION_TYPE = 
    SsoMessageTypes.READY;

  public static create(): SsoReadyMessage.Action {
    return FluxStandardAction.create(SsoReadyMessage.ACTION_TYPE);
  }
}