import {
  filterAsync,
  ReflectionHelper
  } from '@fct/sdk';
import { FluxStandardAction } from '@fct/sdk-react';
import {
  fromEvent,
  Subject
  } from 'rxjs';
import {
  filter,
  share,
  takeUntil,
  tap
  } from 'rxjs/operators';
import { AuthClientService } from '../auth-client';
import {
  SsoAuthTokenMessage,
  SsoNotAuthenticatedMessage,
  SsoReadyMessage,
  SsoRegisterMessage
  } from './sso.messages';

export class SsoService {

  private stopper$ = new Subject();

  private incommingMessages$ = fromEvent(window, 'message').pipe(
    takeUntil(this.stopper$),
    tap((msg) => console.log('MSG IN IFRAME', msg)),
    filterAsync((message: MessageEvent) => 
        this.authClient.isOriginAllowed(message.origin)),
    filter((message: MessageEvent) => ReflectionHelper.isObject(message.data)),
    share()
  );

  constructor(private authClient: AuthClientService) {}

  public onInit() {
    this.handleRegisterMesssage();
    this.dispatchReadyMessage();
  }

  public onDistroy() {
    this.stopper$.next();
  }

  private handleRegisterMesssage() {
    this.incommingMessages$
        .pipe(filter((message) => message.data.type === SsoRegisterMessage.ACTION_TYPE))
        .subscribe((message) => 
            this.handleRegisterMessage(message.data, message.origin))
  }

  private handleRegisterMessage = (action: SsoRegisterMessage.Action, origin: string) => {
    const authCode = action.payload.authCode;
    const redirectPath = action.payload.redirectPath;

    const currAuthCode = this.authClient.getAuthorizationCode();

    if (authCode !== currAuthCode) {
      this.dispatchUnauthenticated(redirectPath, origin);
    } else {
      this.authClient.verify(AuthClientService.VERIFY_SILENT).toPromise()
          .then((authTokenDecoded: any) => { 
            this.dispatchAuthToken(
              this.authClient.getAccessToken(), 
              authTokenDecoded.identity, 
              origin);
            return authTokenDecoded;
          })
          .then((authTokenDecoded) => {
            this.authClient.pollToken({
              next: () => {
                this.dispatchAuthToken(
                  this.authClient.getAccessToken(),
                  authTokenDecoded.identity,
                  origin);
              },
              error: (err) => {
                this.dispatchUnauthenticated(redirectPath, origin);    
              }
            });
          })
          .catch((err) => {
            this.dispatchUnauthenticated(redirectPath, origin);
          });
    }
  }

  private dispatchAuthToken(authToken: string, identity: any, origin: string) {
    this.dispatchMessage(SsoAuthTokenMessage.create({
      authToken,
      identity
    }), origin);
  }

  private dispatchReadyMessage() {
    this.dispatchMessage(SsoReadyMessage.create(), '*');
  }

  private dispatchUnauthenticated(redirectPath: string, origin: string) {
    const continueParam = `${origin}${redirectPath}`;

    this.dispatchMessage(SsoNotAuthenticatedMessage.create({
      url: `${location.protocol}//${location.hostname}:${location.port}/public?continue=${continueParam}`
    }),
    origin);
  }

  private dispatchMessage(message: FluxStandardAction, origin: string) {
    window.parent.postMessage(message, origin);
  }
}
