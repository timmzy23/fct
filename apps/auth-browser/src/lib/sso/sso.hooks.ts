import { AuthClientService } from '../auth-client';
import { SsoService } from './sso.service';
import {
  FactoryProvider,
  useInjector,
  } from '@fct/sdk-react';

export function useSsoService(): SsoService {
  const injector = useInjector();

  if (!injector.hasProvider(SsoService)) {
    injector.use([
      new FactoryProvider(
        SsoService, 
        (authClient: AuthClientService) => new SsoService(authClient),
        [ AuthClientService ])
    ]);
  }

  return injector.inject<SsoService>(SsoService) as SsoService;
}