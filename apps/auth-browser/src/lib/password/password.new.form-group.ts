import {
  FormControl,
  FormGroup,
  Validators
  } from '@fct/ui-react';
import { PasswordValidators } from './password.validators';

export declare namespace NewPasswordFormGroup {
  interface Config {
    validators: PasswordValidators,
    authMethod: 'none' | 'accessToken' | 'currPassword'
  }
}

export enum NewPasswordControlNames {
  NEW_PASSWORD = 'newPassword',
  CONFIRM_PASSWORD = 'confirmPassword'
}

export class NewPasswordFormGroup extends FormGroup {

  public static create(name: string, validators: PasswordValidators) {

    const newPasswordControl = new FormControl('NewPassword', '', [
      Validators.required,
      validators.minLength,
      validators.numeric,
      validators.upperCase
    ]);
    const comparePasswordControl = new FormControl('ComparePassword', '', [
      validators.isEqual(newPasswordControl)
    ]);

    newPasswordControl.valueChanges.subscribe(
        () => comparePasswordControl.updateValueAndValidity());

    return new FormGroup(name, {
      [NewPasswordControlNames.NEW_PASSWORD]: newPasswordControl,
      [NewPasswordControlNames.CONFIRM_PASSWORD]: comparePasswordControl
    });
  }
}
