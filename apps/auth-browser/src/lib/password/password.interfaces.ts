export interface PasswordConfig {
  policyUseUpperCase: boolean;
  policyUseNumeric: boolean;
  policyMinLength: number;
}
