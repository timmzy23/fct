import { ErrnoError } from '@fct/sdk';
import { useRefByCallback } from '@fct/sdk-react';
import {
  FormBasic,
  FormControl,
  FormFieldViewStyle,
  FormGroup,
  Validators
  } from '@fct/ui-react';
import * as React from 'react';
import { useAuthClient } from '../../../auth-client';
import {
  MSG_PASSWORD_AUTH_RESET,
  MSG_PASSWORD_AUTH_SUBMIT,
  MSG_PASSWORD_LABEL_PASSWORD
  } from '../../password.messages';
import { PasswordField } from '../password.password-field/password.pasword-field.component';

export declare namespace PasswordAuthForm {
  interface Props {
    identifier: string;
    onSuccess: (authCode: string) => void;
    onCancel: () => void;
  }
  type Component = React.FunctionComponent<Props>;

  interface FormPayload {
    password: string;
    identifier: string;
  }
}

export const PasswordAuthForm: PasswordAuthForm.Component = (props) => {
  const authClient = useAuthClient();
  const form = useRefByCallback<FormGroup<PasswordAuthForm.FormPayload>>(() => {
    return new FormGroup('auth-form', {
      identifier: new FormControl('identifier', props.identifier, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('password', '', [
        Validators.required
      ])
    })
  }).current;

  const handleSubmit = React.useCallback((payload: PasswordAuthForm.FormPayload) => {
    const passwordControl = form.get('password') as FormControl;

    authClient.login(payload.identifier, payload.password).toPromise()
        .then(({ authorizationCode }) => props.onSuccess(authorizationCode))
        .catch((err: ErrnoError) => passwordControl.setErrors({
          [err.errno]: err
        }));
  }, [authClient]);

  return (
    <FormBasic 
      form={form} 
      onSubmit={handleSubmit}
      onReset={props.onCancel}
      resetBtn={MSG_PASSWORD_AUTH_RESET}
      submitBtn={MSG_PASSWORD_AUTH_SUBMIT}>
      <input type="hidden" value={props.identifier} name="identidier" />
      <PasswordField 
          control={form.get('password') as FormControl}
          viewStyle={FormFieldViewStyle.CONDENSED}
          labelIntl={MSG_PASSWORD_LABEL_PASSWORD}
          autoComplete="current-password"/>
    </FormBasic>
  );
};

PasswordAuthForm.displayName = 'PasswordAuthForm';

PasswordAuthForm.defaultProps = {};
