import { useRefByCallback } from '@fct/sdk-react';
import {
  FormBasic,
  FormControl,
  FormFieldViewStyle,
  FormGroup,
  ValidationErrors
  } from '@fct/ui-react';
import * as React from 'react';
import { ERR_AUTH_NOT_AUTHENTICATED } from '../../../auth-client';
import { usePasswordValidator } from '../../password.hooks';
import {
  MSG_PASSWORD_ERR_INVALID,
  MSG_PASSWORD_LABEL_CONFIRM_PASSWORD,
  MSG_PASSWORD_LABEL_NEW_PASSWORD,
  MSG_PASSWORD_RESET_SUBMIT_BTN
  } from '../../password.messages';
import { NewPasswordFormGroup } from '../../password.new.form-group';
import { PasswordField } from '../password.password-field/password.pasword-field.component';

const styles = require('./password.reset-form.component.scss');

export declare namespace PasswordResetForm {
  interface Props {
    identifier?: string;
    accessToken: string;
    onSubmit: (newPassword: string) => void;
    onCancel: () => void;
    errors?: ValidationErrors;
  }
  type Component = React.FunctionComponent<Props>;

  interface FormPayload {
    newPassword: string;
    confirmPassword: string;
  }
}

const ERROR_MESSAGES = {
  [ERR_AUTH_NOT_AUTHENTICATED]: MSG_PASSWORD_ERR_INVALID
}

export const PasswordResetForm: PasswordResetForm.Component = (props) => {
  const passwordValidator = usePasswordValidator();
  const form = useRefByCallback<FormGroup<PasswordResetForm.FormPayload>>(() => {
    return NewPasswordFormGroup.create('passwords', passwordValidator)
  }).current;

  const handleSubmit = React.useCallback((payload: PasswordResetForm.FormPayload) => {
    props.onSubmit(payload.newPassword)
  }, [props.onSubmit]);

  React.useEffect(() => {
    if (!props.errors) { return; }
    const control = form.get('newPassword');
    const updateErrors = Reflect.ownKeys(props.errors)
        .some((errno: string) => !control.hasError(errno));
    if (updateErrors) {
      control.setErrors({
        ...(control.errors || {}),
        ...props.errors
      });
    }
  }, [props.errors]);

  return (
    <FormBasic
      form={form}
      onSubmit={handleSubmit}
      onReset={props.onCancel}
      submitBtn={MSG_PASSWORD_RESET_SUBMIT_BTN}
      formContentClassName={styles.host}
    >
      <input type="hidden" value={props.identifier} name="username" />
      <PasswordField 
          control={form.get('newPassword') as FormControl}
          labelIntl={MSG_PASSWORD_LABEL_NEW_PASSWORD}
          viewStyle={FormFieldViewStyle.CONDENSED}
          errorMessages={ERROR_MESSAGES}
          autoComplete="new-password"/>
      <PasswordField 
          control={form.get('confirmPassword') as FormControl}
          labelIntl={MSG_PASSWORD_LABEL_CONFIRM_PASSWORD}
          viewStyle={FormFieldViewStyle.CONDENSED}
          autoComplete="confirm-password"/>
    </FormBasic>
  );
};

PasswordResetForm.displayName = 'PasswordResetForm';

PasswordResetForm.defaultProps = {};
