import { InputField } from '@fct/ui-react';
import * as React from 'react';
import {
  ERR_AUTH_NOT_AUTHENTICATED,
  ERR_AUTH_UNKNOWN
  } from '../../../auth-client';
import {
  ERR_PASSWORD_EQUALITY,
  ERR_PASSWORD_POLICY_MIN_LENGTH,
  ERR_PASSWORD_POLICY_NUMERIC,
  ERR_PASSWORD_POLICY_UPPER_CASE
  } from '../../password.errors';
import {
  MSG_PASSWORD_ERR_EQUALITY,
  MSG_PASSWORD_ERR_INVALID,
  MSG_PASSWORD_ERR_POLICY_LENGTH,
  MSG_PASSWORD_ERR_POLICY_NUMERIC,
  MSG_PASSWORD_ERR_POLICY_UPPERCASE
  } from '../../password.messages';

export declare namespace PasswordField {
  type Props = InputField.Props;
  
  type Component = React.FunctionComponent<Props>;
}

const DEFAULT_ERROR_MESSAGES = {
  [ERR_PASSWORD_POLICY_MIN_LENGTH]: MSG_PASSWORD_ERR_POLICY_LENGTH,
  [ERR_PASSWORD_POLICY_UPPER_CASE]: MSG_PASSWORD_ERR_POLICY_UPPERCASE,
  [ERR_PASSWORD_POLICY_NUMERIC]: MSG_PASSWORD_ERR_POLICY_NUMERIC,
  [ERR_PASSWORD_EQUALITY]: MSG_PASSWORD_ERR_EQUALITY,
  [ERR_AUTH_UNKNOWN]: MSG_PASSWORD_ERR_INVALID,
  [ERR_AUTH_NOT_AUTHENTICATED]: MSG_PASSWORD_ERR_INVALID,
}

export const PasswordField: PasswordField.Component = (props) => {
  const { 
    errorMessages,
    ...inputFieldProps 
  } = props;

  const errMessages = {
    ...DEFAULT_ERROR_MESSAGES,
    ...errorMessages
  };

  return (
    <InputField 
      {...inputFieldProps} 
      errorMessages={errMessages}
      type="password" />
  );
};

PasswordField.displayName = 'PasswordField';

PasswordField.defaultProps = {};
