export const MSG_PASSWORD_ERR_POLICY_UPPERCASE =
  { id: 'auth.password.err.policyUpperCase' };
export const MSG_PASSWORD_ERR_POLICY_NUMERIC =
  { id: 'auth.password.err.policyNumeric'};
export const MSG_PASSWORD_ERR_POLICY_LENGTH =
  { id: 'auth.password.err.policyLength'};
export const MSG_PASSWORD_ERR_EQUALITY =
  { id: 'auth.password.err.equality'};
export const MSG_PASSWORD_ERR_INVALID =
  { id: 'auth.password.err.invalid'};
export const MSG_PASSWORD_AUTH_SUBMIT =
  { id: 'auth.password.authForm.submitBtn' };
export const MSG_PASSWORD_AUTH_RESET =
  { id: 'auth.password.authForm.resetButton' };
export const MSG_PASSWORD_LABEL_PASSWORD =
  { id: 'auth.password.label.password' };
export const MSG_PASSWORD_LABEL_NEW_PASSWORD =
  { id: 'auth.password.label.newPassword' };
export const MSG_PASSWORD_LABEL_CONFIRM_PASSWORD =
  { id: 'auth.password.label.confirmPassword' };
export const MSG_PASSWORD_RESET_SUBMIT_BTN =
  { id: 'auth.password.reset.submitBtn' };
