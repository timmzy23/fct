import {
  FactoryProvider,
  useInject,
  useProvide,
  useRefByCallback
  } from '@fct/sdk-react';
import { PasswordConfig } from './password.interfaces';
import { PasswordValidators } from './password.validators';

export function usePasswordInit(config: PasswordConfig) {
  const providers = useRefByCallback(() => [
    new FactoryProvider(
        PasswordValidators,
        () => new PasswordValidators(config)),
  ]).current;

  useProvide(providers);
}

export function usePasswordValidator() {
  const { validator } = useInject({ validator: PasswordValidators });

  return validator;
}
