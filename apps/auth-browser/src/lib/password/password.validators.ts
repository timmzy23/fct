import { ErrnoError } from '@fct/sdk';
import {
  AbstractControl,
  ValidationErrors
  } from '@fct/ui-react';
import {
  ERR_PASSWORD_EQUALITY,
  ERR_PASSWORD_POLICY_MIN_LENGTH,
  ERR_PASSWORD_POLICY_NUMERIC,
  ERR_PASSWORD_POLICY_UPPER_CASE
  } from './password.errors';
import { PasswordConfig } from './password.interfaces';

export class PasswordValidators {
  constructor(private config: PasswordConfig) {}

  public minLength = (control: AbstractControl): ValidationErrors => {
    const minLength = this.config.policyMinLength;
    const currLength = (control.value || '').length;
    const useValidation = control.value && minLength;

    return useValidation && minLength > currLength ?
      { 
        [ERR_PASSWORD_POLICY_MIN_LENGTH]: new ErrnoError(
          ERR_PASSWORD_POLICY_MIN_LENGTH,
          `Password too short! Min length: (${minLength}), curr length: (${currLength})!`,
          { minLength, currLength }
        )
      } :
      null;
  }

  public upperCase = (control: AbstractControl): ValidationErrors => {
    const useValidation = control.value && this.config.policyUseUpperCase;
    return useValidation && !/.*[A-Z].*/.test(control.value) ?
      {
        [ERR_PASSWORD_POLICY_UPPER_CASE]: new ErrnoError(
          ERR_PASSWORD_POLICY_UPPER_CASE,
          `Password requires at least one uppercase character!`
        )
      } :
      null
  }

  public numeric = (control: AbstractControl): ValidationErrors => {
    const useValidation = control.value && this.config.policyUseUpperCase;
    return useValidation && !/.*[0-9].*/.test(control.value) ?
      {
        [ERR_PASSWORD_POLICY_NUMERIC]: new ErrnoError(
          ERR_PASSWORD_POLICY_NUMERIC,
          `Password requires at least one numeric character!`
        )
      } :
      null
  }

  public isEqual(compareControl: AbstractControl) {
    return (control: AbstractControl) => {
      return control.value && control.value !== compareControl.value ?
        {
          [ERR_PASSWORD_EQUALITY]: new ErrnoError(ERR_PASSWORD_EQUALITY)
        } :
        null
    }
  }
}