import {
  FactoryProvider,
  HttpClient,
  useInject,
  useProvide,
  useRefByCallback
  } from '@fct/sdk-react';
import { AuthClientConfig } from './auth-client.config';
import { AuthClientService } from './auth-client.service';

export function useAuthClientInit(config: AuthClientConfig.Config) {
  const providers = useRefByCallback(() => [
    new FactoryProvider(AuthClientConfig, () => new AuthClientConfig(config)),
    new FactoryProvider(
      AuthClientService, 
      (http: HttpClient, authClientConfig: AuthClientConfig) => 
          new AuthClientService(http, authClientConfig),
      [HttpClient, AuthClientConfig])
  ]);

  useProvide(providers.current);
}

export function useAuthClientConfig(): AuthClientConfig {
  const { authClientConfig } = useInject({ authClientConfig: AuthClientConfig });

  return authClientConfig;
}

export function useAuthClient(): AuthClientService {
  const { authClient } = useInject({ authClient: AuthClientService });

  return authClient;
}