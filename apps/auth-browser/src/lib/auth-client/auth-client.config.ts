import { Configuration } from '@fct/sdk';

export declare namespace AuthClientConfig {
  interface Config {
    pathIdentify: string;
    pathLogin: string;
    pathVerify: string;
    pathRefresh: string;
    pathAllowedOrigins: string;
    refreshIntervalMs: number;
    authAuthorityUrl: string;
  }
}

export class AuthClientConfig extends Configuration<AuthClientConfig.Config> {}


