import {
  FactoryProvider,
  HttpClient,
  useInject,
  useProvide,
  useRefByCallback
  } from '@fct/sdk-react';
import { UserService } from './users.service';

export function useUsersInit() {
  const providers = useRefByCallback(() => [
    new FactoryProvider(
      UserService,
      (http: HttpClient) => new UserService(http),
      [HttpClient])
  ]);

  useProvide(providers.current);
}

export function useUserService(): UserService {
  const { service } = useInject({ service: UserService });

  return service;
}
