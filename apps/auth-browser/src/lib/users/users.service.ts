import {
  ErrnoError,
  HttpStatusCode
  } from '@fct/sdk';
import { HttpClient } from '@fct/sdk-react';
import {
  from,
  throwError
  } from 'rxjs';
import {
  catchError,
  switchMap
  } from 'rxjs/operators';
import {
  ERR_AUTH_NOT_AUTHENTICATED,
  ERR_AUTH_SERVER_UNAVAILABLE
  } from '../auth-client';
import {
  ERR_SERVER_TEMPORARY_UNAVAILABLE,
  ERR_UNKNOWN
  } from '../common';

export class UserService {
  constructor(private http: HttpClient) {}

  public createUser(payload) {
    const request: HttpClient.DefaultInit = { data: payload };

    return this.http.post('/v1/api/user', request)
        .pipe(catchError((response: Response) => {
          if (response.status === 504) {
            return throwError(new ErrnoError(ERR_SERVER_TEMPORARY_UNAVAILABLE));
          } else if (response.status >= 500) {
            return throwError(new ErrnoError(ERR_AUTH_SERVER_UNAVAILABLE));
          } else {
            return this.handleCommonErrors(response);
          }
        }));
  }

  public confirmUser(confirmToken: string) {
    const request: HttpClient.DefaultInit = { data: { token: confirmToken } };

    return this.http.post('/v1/api/user/confirm', request)
      .pipe(catchError((response: Response) => {
        return this.handleCommonErrors(response);
      }));
  }

  public requestPasswordRecovery(identifier: string) {
    const request: HttpClient.DefaultInit = { data: { identifier } };

    return this.http.post('/v1/api/user/password/request', request)
        .pipe(catchError((response: Response) => {
          return this.handleCommonErrors(response);
        }));
  }

  public resetPassword(newPassword: string, tmpToken: string) {
    const request: HttpClient.DefaultInit = { data: { newPassword } };

    return this.http.post(`/v1/api/user/password/reset?token=${tmpToken}`, request)
        .pipe(catchError((response: Response) => {
          return this.handleCommonErrors(response);
        }));
  }

  private handleCommonErrors(response: Response) {
    if (response.status === HttpStatusCode.UNAUTHORIZED) {
      return throwError(new ErrnoError((ERR_AUTH_NOT_AUTHENTICATED)));
    } else if (response.status === HttpStatusCode.BAD_REQUEST) {
      return from(response.json()).pipe(
          switchMap((res: any) => {
            if (res.error && res.error.errno) {
              return throwError(new ErrnoError(
                res.error.errno, 
                res.error.message, 
                res.error.params));
            } else {
              return throwError(new ErrnoError(ERR_UNKNOWN));
            }
          }));
    } else {
      return throwError(new ErrnoError(ERR_UNKNOWN));
    }
  }
}
