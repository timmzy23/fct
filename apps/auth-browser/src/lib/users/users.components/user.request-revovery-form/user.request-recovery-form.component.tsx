import { ErrnoError } from '@fct/sdk';
import { useRefByCallback } from '@fct/sdk-react';
import {
  EmailField,
  FormBasic,
  FormControl,
  FormFieldViewStyle,
  FormGroup,
  Validators
  } from '@fct/ui-react';
import * as React from 'react';
import { useUserService } from '../../users.hooks';
import {
  MSG_USER_LABEL_IDENTIFIER,
  MSG_USER_REQUEST_RECOVERY_SUBMIT
  } from '../../users.messages';
import { UserIdentityForm } from '../user.identity-form/user.identity-form.component';

export declare namespace UserRequestRecoveryForm {
  interface Props {
    identifier?: string;
    onCancel: (identifier: string) => void;
    onSuccess: (identifier: string) => void;
  }
  type Component = React.FunctionComponent<Props>;
}

export const UserRequestRecoveryForm: UserRequestRecoveryForm.Component = (props) => {
  const users = useUserService();
  const form = useRefByCallback<FormGroup<UserIdentityForm.FormPayload>>(() => {
    return new FormGroup('requestRecovery', {
      identifier: new FormControl('identifier', props.identifier || '', [
        Validators.required,
        Validators.email
      ])
    })
  }).current;

  const handleSubmit = React.useCallback((payload: UserIdentityForm.FormPayload) => {
    const identifierControl = form.get('identifier') as FormControl;

    users.requestPasswordRecovery(payload.identifier).toPromise()
        .then(() => props.onSuccess(payload.identifier))
        .catch((err: ErrnoError) => identifierControl.setErrors({
          [err.errno]: err
        }));
  }, [users, props.onSuccess]);

  const handleCancel = React.useCallback((payload: UserIdentityForm.FormPayload) => {
    props.onCancel(payload.identifier);
  }, [props.onCancel]);

  return (
    <FormBasic
    form={form}
    onSubmit={handleSubmit}
    onReset={handleCancel}
    submitBtn={MSG_USER_REQUEST_RECOVERY_SUBMIT}
    >
    <EmailField 
        control={form.get('identifier') as any}
        autoComplete="username"
        labelIntl={MSG_USER_LABEL_IDENTIFIER}
        viewStyle={FormFieldViewStyle.CONDENSED}/>
  </FormBasic>
  );
};

UserRequestRecoveryForm.displayName = 'UserRequestRecoveryForm';

UserRequestRecoveryForm.defaultProps = {};
