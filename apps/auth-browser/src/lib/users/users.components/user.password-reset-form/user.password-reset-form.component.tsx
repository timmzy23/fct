import { ErrnoError } from '@fct/sdk';
import * as React from 'react';
import { PasswordResetForm } from '../../../password';
import { useUserService } from '../../users.hooks';

export declare namespace UserPasswordResetForm {
  interface Props {
    identifier: string;
    accessToken: string;
    onSuccess: (identifier: string) => void;
    onReset: (identifier: string) => void;
  }
  type Component = React.FunctionComponent<Props>;
}

export const UserPasswordResetForm: UserPasswordResetForm.Component = (props) => {
  const users = useUserService();
  const [ errors, setErrors ] = React.useState();

  const handleSubmit = React.useCallback((newPassword: string) => {
    users.resetPassword(newPassword, props.accessToken)
        .toPromise()
        .then(() => props.onSuccess(props.identifier))
        .catch((err: ErrnoError) => setErrors({ [err.errno]: err }));
  }, [props.onSuccess]);

  const handleReset = React.useCallback(() => {
    props.onReset(props.identifier);
  }, [props.onSuccess]);

  return (<PasswordResetForm 
      accessToken={props.accessToken}
      identifier={props.identifier}
      onSubmit={handleSubmit}
      onCancel={handleReset}
      errors={errors}/>);
};

UserPasswordResetForm.displayName = 'UserPasswordResetForm';

UserPasswordResetForm.defaultProps = {};
