import { ERR_DUPLICATED_ENTRY } from '@fct/auth-interfaces';
import { ErrnoError } from '@fct/sdk';
import { useRefByCallback } from '@fct/sdk-react';
import {
  EmailField,
  FormBasic,
  FormControl,
  FormFieldViewStyle,
  FormGroup,
  InputField,
  Validators
  } from '@fct/ui-react';
import * as React from 'react';
import {
  NewPasswordFormGroup,
  usePasswordValidator
  } from '../../../password';
import {
  MSG_PASSWORD_LABEL_CONFIRM_PASSWORD,
  MSG_PASSWORD_LABEL_NEW_PASSWORD,
  PasswordField
  } from '../../../password/';
import { useUserService } from '../../users.hooks';
import {
  MSG_USER_CREATE_FORM_RESET,
  MSG_USER_CREATE_FORM_SUBMIT,
  MSG_USER_LABEL_FIRST_NAME,
  MSG_USER_LABEL_IDENTIFIER,
  MSG_USER_LABEL_LAST_NAME
  } from '../../users.messages';

const styles = require('./user.create-form.component.scss');

export declare namespace UserCreateForm {
  interface Props {
    identifier?: string;
    onSuccess: (identifier: string) => void;
    onCancel: () => void;
  }
  type Component = React.FunctionComponent<Props>;

  interface FormPayload {
    identifier: string;
    firstName: string;
    lastName: string;
    passwords: {
      newPassword: string;
      confirmPassword: string;
    };
  }
}

const EMAIL_FIELD_ERRORS = {
  [ERR_DUPLICATED_ENTRY]: { id: 'auth.users.err.duplicate.text' }
}

export const UserCreateForm: UserCreateForm.Component = (props) => {
  const users = useUserService();
  const passwordValidator = usePasswordValidator();

  const form = useRefByCallback<FormGroup<UserCreateForm.FormPayload>>(() => {
    return new FormGroup('new-user-form', {
      identifier: new FormControl('identifier', props.identifier || '', [
        Validators.required,
        Validators.email
      ]),
      firstName: new FormControl('firstName', '', [ Validators.required ]),
      lastName: new FormControl('lastName', '', [ Validators.required ]),
      passwords: NewPasswordFormGroup.create('passwords', passwordValidator),
    })
  }).current;

  const handleSubmit = React.useCallback((payload: UserCreateForm.FormPayload) => {
    const identifierControl = form.get('identifier') as FormControl;

    const userPayload: any = {
      identifier: payload.identifier,
      firstName: payload.firstName,
      lastName: payload.lastName,
      password: payload.passwords.newPassword
    };

    users.createUser(userPayload)
        .toPromise()
        .then(() => props.onSuccess(userPayload.identifier))
        .catch((err: ErrnoError) => identifierControl.setErrors({
          [err.errno]: err
        }));
  }, [users]);

  return (
    <FormBasic
      form={form}
      onSubmit={handleSubmit}
      onReset={props.onCancel}
      resetBtn={MSG_USER_CREATE_FORM_RESET}
      submitBtn={MSG_USER_CREATE_FORM_SUBMIT}
      formContentClassName={styles.host}
    >
      <EmailField 
          control={form.get('identifier') as FormControl}
          labelIntl={MSG_USER_LABEL_IDENTIFIER}
          viewStyle={FormFieldViewStyle.CONDENSED}
          errorMessages={EMAIL_FIELD_ERRORS}/>
      <PasswordField 
          control={form.get('passwords.newPassword') as FormControl}
          labelIntl={MSG_PASSWORD_LABEL_NEW_PASSWORD}
          viewStyle={FormFieldViewStyle.CONDENSED}
          autoComplete="new-password"/>
      <PasswordField 
          control={form.get('passwords.confirmPassword') as FormControl}
          labelIntl={MSG_PASSWORD_LABEL_CONFIRM_PASSWORD}
          viewStyle={FormFieldViewStyle.CONDENSED}
          autoComplete="confirm-password"/>
      <InputField 
          control={form.get('firstName') as FormControl}
          labelIntl={MSG_USER_LABEL_FIRST_NAME}
          viewStyle={FormFieldViewStyle.CONDENSED}
          autoComplete="first-name" />
      <InputField 
          control={form.get('lastName') as FormControl}
          labelIntl={MSG_USER_LABEL_LAST_NAME}
          viewStyle={FormFieldViewStyle.CONDENSED}
          autoComplete="family-name" />
    </FormBasic>
  );
};

UserCreateForm.displayName = 'UserCreateForm';

UserCreateForm.defaultProps = {};
