import { ErrnoError } from '@fct/sdk';
import { useRefByCallback } from '@fct/sdk-react';
import {
  EmailField,
  FormBasic,
  FormControl,
  FormFieldViewStyle,
  FormGroup,
  Validators
  } from '@fct/ui-react';
import * as React from 'react';
import { useAuthClient } from '../../../auth-client';
import {
  MSG_USER_IDENTITY_FORM_RESET,
  MSG_USER_IDENTITY_FORM_SUBMIT,
  MSG_USER_LABEL_IDENTIFIER
  } from '../../users.messages';

export declare namespace UserIdentityForm {
  interface Props {
    identifier?: string;
    onCancel: () => void;
    onSuccess: (identifier: string) => void;
  }
  type Component = React.FunctionComponent<Props>;

  interface FormPayload {
    identifier: string;
  }
}

export const UserIdentityForm: UserIdentityForm.Component = (props) => {

  const form = useRefByCallback<FormGroup<UserIdentityForm.FormPayload>>(() => {
    return new FormGroup('user-identity', {
      identifier: new FormControl('identifier', props.identifier || '', [
        Validators.required,
        Validators.email
      ])
    })
  }).current;

  const authClient = useAuthClient();

  const handleSubmit = React.useCallback((payload: UserIdentityForm.FormPayload) => {
    const identifierControl = form.get('identifier') as FormControl;

    authClient.identify(payload.identifier).toPromise()
        .then(() => props.onSuccess(payload.identifier))
        .catch((err: ErrnoError) => identifierControl.setErrors({
          [err.errno]: err
        }));
  }, [authClient]);

  return (
    <FormBasic 
      form={form}
      onSubmit={handleSubmit}
      onReset={props.onCancel}
      submitBtn={MSG_USER_IDENTITY_FORM_SUBMIT}
      resetBtn={MSG_USER_IDENTITY_FORM_RESET}
      >
      <EmailField 
          control={form.get('identifier') as any}
          autoComplete="username"
          labelIntl={MSG_USER_LABEL_IDENTIFIER}
          viewStyle={FormFieldViewStyle.CONDENSED}/>
    </FormBasic>
  );
};

UserIdentityForm.displayName = 'UserIdentityForm';

UserIdentityForm.defaultProps = {};
