import { ReflectionHelper } from '@fct/sdk';
import {
  actionOf,
  useCommands,
  useDispatchCommand,
  useEarlySubscription,
  useRefByCallback
  } from '@fct/sdk-react';
import * as React from 'react';
import { useHistory } from 'react-router';
import { map } from 'rxjs/operators';
import { NavigateToCommand } from './common.actions';

export function useNavigateToObserverEffect() {
  const commands$ = useCommands();
  const history = useHistory();

  useEarlySubscription(
    () => commands$
      .pipe(
          actionOf(NavigateToCommand),
          map((command: NavigateToCommand.Action) => command.payload))
      .subscribe((payload: NavigateToCommand.Payload) => history.push({
        pathname: payload.path,
        state: payload.opts ? payload.opts.state : undefined,
        search: payload.opts && payload.opts.queryParams ? 
            (new URLSearchParams(
                ReflectionHelper.removeUndefinedProps(payload.opts.queryParams))
            ).toString() : 
            undefined
      })));
}

export function useURLSearchParams(): URLSearchParams {
  return useRefByCallback<URLSearchParams>(() => 
      new URLSearchParams(location.search)).current;
}

export function useGoToMessageScreen() {
  const dispatch = useDispatchCommand();

  return React.useCallback(
    (message: NavigateToCommand.MessageState) => 
        dispatch(NavigateToCommand.goToMessageScreen(message)), 
    [dispatch]);
}

export function useGoToSignInIdentifierScreen() {
  const dispatch = useDispatchCommand();

  return React.useCallback(
    (identifier?: string) => 
        dispatch(NavigateToCommand.goToSignInIdentifierScreen(identifier)), 
    [dispatch]);
}
