import { FluxStandardAction } from '@fct/sdk-react';
import { IntlMessage } from '@fct/ui-react';
export enum COMMON_ACTION_TYPES {
  NAVIGATE_TO_CMD = 'app.navigateTo.cdm'
}


export declare namespace NavigateToCommand {
  type ACTION_TYPE = COMMON_ACTION_TYPES.NAVIGATE_TO_CMD;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    path: string,
    opts?: Opts
  }

  interface Opts  {
    queryParams?: any,
    state?: any
  }

  interface MessageState {
    title?: IntlMessage.Message;
    subtitle?: IntlMessage.Message;
    text?: IntlMessage.Message;
  }
}

export class NavigateToCommand {
  public static ACTION_TYPE: NavigateToCommand.ACTION_TYPE = 
      COMMON_ACTION_TYPES.NAVIGATE_TO_CMD;

  public static create(path: string, opts?: NavigateToCommand.Opts): NavigateToCommand.Action {
    return FluxStandardAction.create(NavigateToCommand.ACTION_TYPE, { path, opts });
  }

  public static goToPasswordRequest(identifier: string) {
    return NavigateToCommand.create('/public/password/request', {
      queryParams: { identifier }
    });
  }

  public static goToSignInIdentifierScreen(identifier?: string) {
    return NavigateToCommand.create('/public', {
      queryParams: { identifier }
    });
  }

  public static goToProfile() {
    return NavigateToCommand.create('/profile');
  }

  public static goToSignUp() {
    return NavigateToCommand.create('/public/signup/create');
  }

  public static goToSignInPassword(identifier: string, continueUrl?: string) {
    return NavigateToCommand.create('/public/signin/password', {
      queryParams: { identifier, continue: continueUrl }
    });
  }

  public static goToMessageScreen(msgState: NavigateToCommand.MessageState) {
    return NavigateToCommand.create('/public/message', {
      state: msgState
    });
  }
}
