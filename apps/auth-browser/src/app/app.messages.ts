export const MSG_SIGN_IN_IDENTIFIER_TITLE = 
  { id: 'auth.signInIdentifier.title'};
export const MSG_SIGN_IN_IDENTIFIER_SUBTITLE = 
  { id: 'auth.signInIdentifier.subtitle' };
export const MSG_SING_IN_PASSWORD_TITLE = 
  { id: 'auth.signInPassword.title'};
export const MSG_SING_IN_PASSWORD_SUBTITLE = 
  { id: 'auth.signInPassword.subtitle'};
export const MSG_SIGN_UP_TITLE =
  { id: 'auth.signUp.title' };
export const MSG_SIGN_UP_SUBTITLE =
  { id: 'auth.signUp.subtitle' };
export const MSG_PASSWORD_RECOVERY_TITLE =
  { id: 'auth.passwordRecovery.title' };
export const MSG_PASSWORD_RECOVERY_SUBTITLE =
  { id: 'auth.passwordRecovery.subtitle' };
export const MSG_PASSWORD_RESET_TITLE =
  { id: 'auth.passwordReset.title' };
export const MSG_PASSWORD_RESET_SUBTITLE =
  { id: 'auth.passwordReset.subtitle' };