import {
  ApplicationState,
  HttpClient,
  HttpHeader,
  HttpRequest,
  InMemoryState,
  useActions,
  useActionsFeature,
  useHttpClientInit,
  useInject,
  useRefByCallback,
  useStores,
  useSubscription
  } from '@fct/sdk-react';
import {
  IntlDynamicProvider,
  useIntlInit,
  useThemes
  } from '@fct/ui-react';
import * as React from 'react';
import {
  PasswordConfig,
  useNavigateToObserverEffect,
  usePasswordInit,
  useUsersInit
  } from '../../lib';
import {
  AuthClientConfig,
  useAuthClientInit
  } from '../../lib/auth-client';
import { AppView } from './app-view.component';

export declare namespace AppContainer {
  interface Props {
    env: any;
    intlMessages: any;
  }

  type Component = React.FunctionComponent<Props>;
}

export const AppContainer: AppContainer.Component = (props) => {
  const INTL_CONIG = React.useRef({
    defaultLocal: props.env.defaultLocal,
    enabledLocals: props.env.locals,
    messages: props.intlMessages
  }).current;

  const THEME_CONFIG = React.useRef({
    defaultThemeName: props.env.defaultThemeName,
    themeNames: props.env.themeNames
  }).current;

  const AUTH_CONFIG: AuthClientConfig.Config = React.useRef({
    pathIdentify: '/identify',
    pathLogin: '/login',
    pathVerify: '/verify',
    pathRefresh: '/refresh',
    authAuthorityUrl: '/v1/api/auth',
    pathAllowedOrigins: '/allowed-origins',
    refreshIntervalMs: 1000 * 10 * 60
  }).current;

  const HTTP_CONFIG: HttpClient.Init = useRefByCallback(() => ({
    base: location.href,
    requestInterceptors: [
      HttpRequest.getRequestHeaderDecorator(
          HttpHeader.CONTENT_TYPE, 'application/json')
    ]
  })).current;

  const PASSWORD_CONFIG: PasswordConfig = React.useRef({
    policyUseUpperCase: true,
    policyUseNumeric: true,
    policyMinLength: 8,
  }).current;

  useActionsFeature();
  useStores(new InMemoryState());
  useThemes(THEME_CONFIG);
  useIntlInit(INTL_CONIG);
  useHttpClientInit(HTTP_CONFIG);
  useAuthClientInit(AUTH_CONFIG);
  usePasswordInit(PASSWORD_CONFIG);
  useNavigateToObserverEffect();
  useUsersInit();

  const { appState } = useInject({ appState: ApplicationState });
  window['appState'] = appState;
  const actions = useActions();
  useSubscription(() => actions.subscribe({ next: (a) => console.debug(a) }), []);

  return (
    <IntlDynamicProvider>
      <AppView />
    </IntlDynamicProvider>
  );
}

AppContainer.displayName = 'AppContainer';
