import * as React from 'react';
import { useSsoService } from '../../../lib';

export declare namespace AuthSsoScreen {
  type Component = React.FunctionComponent<any>;
}

export const AuthSsoScreen: AuthSsoScreen.Component = (props) => {
  const ssoService = useSsoService();
  
  React.useEffect(() => {
    ssoService.onInit();

    return () => ssoService.onDistroy();
  }, []);
  return (<div></div>);
};

AuthSsoScreen.displayName = 'AuthSsoScreen';

AuthSsoScreen.defaultProps = {};
