import {
  dispatchChangeTheme,
  useCurrentThemeState
  } from '@fct/ui-react';
import * as React from 'react';
import {
  Redirect,
  Route,
  Switch
  } from 'react-router-dom';
import { useAuthClient } from '../../lib';
import { ProfileScreen } from './profile';
import { PublicScreen } from './public';
import { AuthSsoScreen } from './sso';

const styles = require('./app-view.component.scss');

export const AppView = () => {
  const currentTheme = useCurrentThemeState();
  const auth = useAuthClient();

  const className = React.useMemo(
      () => `${styles.host} themed ${currentTheme}`,
      [currentTheme]);

  return (
      <div className={className}>
        <Switch>
          <Route path="/public" component={PublicScreen} />
          <Route path="/profile" component={ProfileScreen} />
          <Route path="/sso" component={AuthSsoScreen} />
          <Redirect from="/" to={auth.isAuthenticated() ? '/profile' : '/public'}/>
        </Switch>
      </div>
  );
}

AppView.displayName = 'AppView';