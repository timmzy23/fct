import * as React from 'react';
import {
  RouteComponentProps,
  useHistory
  } from 'react-router';
import { useAuthClient } from '../../../../lib';

export declare namespace ProfileScreen {
  interface Props extends RouteComponentProps {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProfileScreen: ProfileScreen.Component = (props) => {
  const auth = useAuthClient();
  const history = useHistory();

  if (!auth.isAuthenticated()) {
    history.replace('/public');
  }

  return (
    <div>
      Profile...
    </div>);
};

ProfileScreen.displayName = 'ProfileScreen';

ProfileScreen.defaultProps = {};
