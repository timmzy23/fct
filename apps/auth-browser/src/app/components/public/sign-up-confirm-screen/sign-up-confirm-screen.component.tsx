import {
  useSubscription,
  useUrlSearchParams
  } from '@fct/sdk-react';
import * as React from 'react';
import { first } from 'rxjs/operators';
import {
  MSG_USER_CREATE_CONFIRM_ERR_SUBTITLE,
  MSG_USER_CREATE_CONFIRM_ERR_TEXT,
  MSG_USER_CREATE_CONFIRM_ERR_TITLE,
  MSG_USER_CREATE_CONFIRM_TEXT,
  MSG_USER_CREATE_CONFIRM_TITLE,
  useGoToMessageScreen,
  useUserService
  } from '../../../../lib';

// const styles = require('./sign-up-confirm-screen.component.scss');

export declare namespace SignUpConfirmScreen {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

const ERROR_MESSAGE_PAYLOAD = {
  subtitle: MSG_USER_CREATE_CONFIRM_ERR_SUBTITLE,
  title: MSG_USER_CREATE_CONFIRM_ERR_TITLE,
  text: MSG_USER_CREATE_CONFIRM_ERR_TEXT
}

export const SignUpConfirmScreen: SignUpConfirmScreen.Component = (props) => {
  const urlSearch = useUrlSearchParams();
  const identifier = urlSearch.get('identifier');
  const token = urlSearch.get('token');
  const users = useUserService();
  const goToMessageScreen = useGoToMessageScreen();

  if (!token || !identifier) {
    goToMessageScreen({
      subtitle: MSG_USER_CREATE_CONFIRM_ERR_SUBTITLE,
      title: MSG_USER_CREATE_CONFIRM_ERR_TITLE,
      text: MSG_USER_CREATE_CONFIRM_ERR_TEXT
    });
  }

  React.useEffect(() => {
    if (!token || !identifier) {
      goToMessageScreen(ERROR_MESSAGE_PAYLOAD);
    } else {
      users.confirmUser(token).pipe(first()).subscribe({
        next: () => goToMessageScreen({
          subtitle: identifier,
          title: MSG_USER_CREATE_CONFIRM_TITLE,
          text: {
            ...MSG_USER_CREATE_CONFIRM_TEXT,
            values: { identifier }
          }
        }),
        error: () => goToMessageScreen(ERROR_MESSAGE_PAYLOAD)
      })
    }
  }, 
  []);
  
  return null;
};

SignUpConfirmScreen.displayName = 'SignUpConfirmScreen';

SignUpConfirmScreen.defaultProps = {};
