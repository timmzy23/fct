import * as React from 'react';
import {
  Redirect,
  Route,
  RouteComponentProps,
  Switch
  } from 'react-router';
import { MessageScreen } from '../message-screen/message-screen.component';
import { PasswordRecoveryRequestScreen } from '../password-recovery-request-screen/password-recovery-request-screen.component';
import { PasswordResetScreen } from '../password-reset-screen/password-reset-screen.component';
import { SignInIdentifierScreen } from '../sign-in-identifier-screen/sign-in-identifier.screen.component';
import { SignInPasswordScreen } from '../sign-in-password-screen/sign-in-password-screen.component';
import { SignUpConfirmScreen } from '../sign-up-confirm-screen/sign-up-confirm-screen.component';
import { SignUpScreen } from '../sign-up-screen/sign-up-screen.component';

export declare namespace PublicScreen {
  type Props = RouteComponentProps;
  type Component = React.FunctionComponent<Props>;
}

export const PublicScreen: PublicScreen.Component = (props) => {
  return (
    <Switch>
      <Route 
        path={`${props.match.path}/signin/identifier`} 
        component={SignInIdentifierScreen} />
      <Route 
        path={`${props.match.path}/signin/password`} 
        component={SignInPasswordScreen} />
      <Route 
        path={`${props.match.path}/signup/create`} 
        component={SignUpScreen} />
      <Route 
        path={`${props.match.path}/signup/confirm`}
        component={SignUpConfirmScreen} />
      <Route 
        path={`${props.match.path}/message`} 
        component={MessageScreen} />
      <Route 
        path={`${props.match.path}/password/request`} 
        component={PasswordRecoveryRequestScreen} />
      <Route 
        path={`${props.match.path}/password/reset`}
        component={PasswordResetScreen}/>
      <Redirect 
        from={props.match.path} 
        to={{
          pathname: `${props.match.path}/signin/identifier`,
          search: props.location.search
        }}
        exact/>
    </Switch>);
};

PublicScreen.displayName = 'PublicScreen';

PublicScreen.defaultProps = {};
