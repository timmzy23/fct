import {
  Button,
  ButtonViewStyle,
  FctCenteredCard,
  IntlMessage,
  Text,
  TextAlign,
  TextBlock,
  TextColor,
  TextFontSize,
  TextFontStyle,
  useTranslate
  } from '@fct/ui-react';
import * as React from 'react';
import {
  RouteComponentProps,
  useLocation
  } from 'react-router';
import {
  NavigateToCommand,
  useGoToSignInIdentifierScreen
  } from '../../../../lib';

const styles = require('./message-screen.component.scss');

export declare namespace MessageScreen {
  type Props = RouteComponentProps;
  type Component = React.FunctionComponent<Props>;
}

export const MessageScreen: MessageScreen.Component = (props) => {
  const loc = useLocation();
  const msgState: NavigateToCommand.MessageState = loc.state;
  const translate = useTranslate();
  const goToSignInIdentifierScreen = useGoToSignInIdentifierScreen();
  const handleClick = React.useCallback(() => goToSignInIdentifierScreen(), []);
  
  return msgState && (
    <FctCenteredCard
      titleIntl={msgState.title}
      subtitleIntl={msgState.subtitle}
    >
      <div className={styles.host}>
        <TextBlock 
          as="p"
          textAlign={TextAlign.CENTER} 
          fontStyle={TextFontStyle.MEDIUM_ITALIC}
        >
          <IntlMessage message={msgState.text}/>
        </TextBlock>
        <div>
          <Button onClick={handleClick}
              viewStyle={ButtonViewStyle.SECONDARY}>
            <Text 
              fontStyle={TextFontStyle.SEMIBOLD}
              fontSize={TextFontSize.MEDIUM}
              textColor={TextColor.PRIMARY}>
              {translate({ id: 'auth.signInIdentifier.title' })}
            </Text>
          </Button>
        </div>
      </div>
    </FctCenteredCard>);
};

MessageScreen.displayName = 'MessageScreen';

MessageScreen.defaultProps = {};
