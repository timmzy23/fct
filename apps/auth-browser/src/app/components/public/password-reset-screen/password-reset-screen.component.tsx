import { useDispatchCommand } from '@fct/sdk-react';
import { FctCenteredCard } from '@fct/ui-react';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import {
  NavigateToCommand,
  UserPasswordResetForm,
  useURLSearchParams
  } from '../../../../lib';
import {
  MSG_PASSWORD_RESET_SUBTITLE,
  MSG_PASSWORD_RESET_TITLE
  } from '../../../app.messages';

export declare namespace PasswordResetScreen {
  type Props = RouteComponentProps;
  type Component = React.FunctionComponent<Props>;
}

export const PasswordResetScreen: PasswordResetScreen.Component = (props) => {
  const query = useURLSearchParams();
  const dispatch = useDispatchCommand();

  const handleSuccess = React.useCallback((identifier: string) => {
    dispatch(NavigateToCommand.goToSignInIdentifierScreen(identifier));
  }, []);

  const handleCancel = React.useCallback((identifier: string) => {
    dispatch(NavigateToCommand.goToSignInIdentifierScreen(identifier));
  }, []);

  return (
    <FctCenteredCard
        titleIntl={MSG_PASSWORD_RESET_TITLE}
        subtitleIntl={MSG_PASSWORD_RESET_SUBTITLE}>
      <UserPasswordResetForm 
        accessToken={query.get('token')}
        identifier={query.get('identifier')}
        onSuccess={handleSuccess}
        onReset={handleCancel}
      />
    </FctCenteredCard>
  );
};

PasswordResetScreen.displayName = 'PasswordResetScreen';

PasswordResetScreen.defaultProps = {};
