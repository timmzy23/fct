import { useDispatchCommand } from '@fct/sdk-react';
import { FctCenteredCard } from '@fct/ui-react';
import * as React from 'react';
import { RouteProps } from 'react-router';
import {
  NavigateToCommand,
  PasswordAuthForm,
  useAuthClient,
  useURLSearchParams
  } from '../../../../lib';
import { MSG_SING_IN_PASSWORD_SUBTITLE } from '../../../app.messages';

export declare namespace SignInPasswordScreen {
  type Props = RouteProps;
  type Component = React.FunctionComponent<Props>;
}

export const SignInPasswordScreen: SignInPasswordScreen.Component = (props) => {
  const query = useURLSearchParams();
  const dispatch = useDispatchCommand();
  const authClient = useAuthClient();

  const identifier = query.get('identifier');

  const handleSuccess = React.useCallback((authCode: string) => {
    const continueParam = query.get('continue');
    if (continueParam) {
      const url = new URL(continueParam);
      url.searchParams.set('code', authCode);
      authClient.trustedRedirect(url);
    } else {
      dispatch(NavigateToCommand.goToProfile());
    }
  }, []);

  const handleCancel = React.useCallback(() => {
    dispatch(NavigateToCommand.goToPasswordRequest(identifier));
  }, []);

  return (
    <FctCenteredCard 
      titleIntl={identifier}
      subtitleIntl={MSG_SING_IN_PASSWORD_SUBTITLE}>
      <PasswordAuthForm 
          identifier={identifier}
          onSuccess={handleSuccess}
          onCancel={handleCancel}/>
    </FctCenteredCard>
  );
};

SignInPasswordScreen.displayName = 'SignInPasswordScreen';

SignInPasswordScreen.defaultProps = {};
