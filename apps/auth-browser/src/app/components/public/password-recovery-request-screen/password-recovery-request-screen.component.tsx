import { FctCenteredCard } from '@fct/ui-react';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import {
  MSG_USER_RECOVER_REQUEST_TEXT,
  MSG_USER_RECOVER_REQUEST_TITLE,
  useGoToMessageScreen,
  useGoToSignInIdentifierScreen,
  UserRequestRecoveryForm,
  useURLSearchParams
  } from '../../../../lib';
import {
  MSG_PASSWORD_RECOVERY_SUBTITLE,
  MSG_PASSWORD_RECOVERY_TITLE
  } from '../../../app.messages';

export declare namespace PasswordRecoveryRequestScreen {
  type Props = RouteComponentProps;
  type Component = React.FunctionComponent<Props>;
}

export const PasswordRecoveryRequestScreen: PasswordRecoveryRequestScreen.Component = 
  (props) => {
    const query = useURLSearchParams();
    const goToMessageScreen = useGoToMessageScreen();
    const goToSignInIdentifierScreen = useGoToSignInIdentifierScreen();

    const handleSuccess = React.useCallback((identifier: string) => {
      goToMessageScreen({
        title: MSG_USER_RECOVER_REQUEST_TITLE,
        subtitle: identifier,
        text: MSG_USER_RECOVER_REQUEST_TEXT
      });
    }, []);

    return (
      <FctCenteredCard
        titleIntl={MSG_PASSWORD_RECOVERY_TITLE}
        subtitleIntl={MSG_PASSWORD_RECOVERY_SUBTITLE}
      >
        <UserRequestRecoveryForm 
          identifier={query.get('identifier')}
          onSuccess={handleSuccess}
          onCancel={goToSignInIdentifierScreen}/>
      </FctCenteredCard>
    );
  };

PasswordRecoveryRequestScreen.displayName = 'PasswordRecoveryRequestScreen';

PasswordRecoveryRequestScreen.defaultProps = {};
