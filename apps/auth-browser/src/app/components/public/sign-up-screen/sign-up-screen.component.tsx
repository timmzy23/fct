import { FctCenteredCard } from '@fct/ui-react';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import {
  MSG_USER_CREATED_SUBTITLE,
  MSG_USER_CREATED_TEXT,
  MSG_USER_CREATED_TITLE,
  useGoToMessageScreen,
  useGoToSignInIdentifierScreen,
  UserCreateForm,
  useURLSearchParams
  } from '../../../../lib';
import {
  MSG_SIGN_UP_SUBTITLE,
  MSG_SIGN_UP_TITLE
  } from '../../../app.messages';

export declare namespace SignUpScreen {
  type Props = RouteComponentProps;
  type Component = React.FunctionComponent<Props>;
}

export const SignUpScreen: SignUpScreen.Component = (props) => {
  const query = useURLSearchParams();
  const identifier = query.get('identifier');
  const goToMessageScreen = useGoToMessageScreen();
  const goToSignInIdentifierScreen = useGoToSignInIdentifierScreen();
  const handleSuccess = React.useCallback((newIdentifier: string) => {
    goToMessageScreen({
      title: MSG_USER_CREATED_TITLE,
      subtitle: {
        ...MSG_USER_CREATED_SUBTITLE,
        values: { identifier: newIdentifier }
      },
      text: MSG_USER_CREATED_TEXT
      });
  }, []);

  const handleCancel = React.useCallback(
    () => goToSignInIdentifierScreen(identifier || ''), 
    []);

  return (
    <FctCenteredCard
      titleIntl={MSG_SIGN_UP_TITLE}
      subtitleIntl={MSG_SIGN_UP_SUBTITLE}
    >
      <UserCreateForm 
        identifier={identifier}
        onSuccess={handleSuccess}
        onCancel={handleCancel}/>
    </FctCenteredCard>
  );
};

SignUpScreen.displayName = 'SignUpScreen';

SignUpScreen.defaultProps = {};
