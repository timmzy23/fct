import { useDispatchCommand } from '@fct/sdk-react';
import { FctCenteredCard } from '@fct/ui-react';
import * as React from 'react';
import { RouteProps } from 'react-router';
import {
  NavigateToCommand,
  UserIdentityForm,
  useURLSearchParams
  } from '../../../../lib';
import {
  MSG_SIGN_IN_IDENTIFIER_SUBTITLE,
  MSG_SIGN_IN_IDENTIFIER_TITLE
  } from '../../../app.messages';

export declare namespace SignInIdentifierScreen {
  type Props = RouteProps;
  type Component = React.FunctionComponent<Props>;
}

export const SignInIdentifierScreen: SignInIdentifierScreen.Component = (props) => {
  const query = useURLSearchParams();
  const dispatch = useDispatchCommand();

  const handleSuccess = React.useCallback((identifier: string) => {
    dispatch(NavigateToCommand.goToSignInPassword(
        identifier, 
        query.get('continue') || undefined));
  }, []);

  const handleCancel = React.useCallback(() => {
    dispatch(NavigateToCommand.goToSignUp());
  }, []);

  return (
    <FctCenteredCard 
      titleIntl={MSG_SIGN_IN_IDENTIFIER_TITLE}
      subtitleIntl={MSG_SIGN_IN_IDENTIFIER_SUBTITLE}>
      <UserIdentityForm 
          identifier={query.get('identifier')}
          onSuccess={handleSuccess}
          onCancel={handleCancel}/>
    </FctCenteredCard>
  );
};

SignInIdentifierScreen.displayName = 'SignInIdentifierScreen';

SignInIdentifierScreen.defaultProps = {};
