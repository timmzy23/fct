import {
  InjectorProvider,
  Provider
  } from '@fct/sdk-react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AppContainer } from './app-container.component';

const PROVIDERS: Provider[] = [

]

export declare namespace AuthApp {
  interface Props {
    appEnv: string;
    env: any;
    intlMessages: any;
  }

  type Component = React.FunctionComponent<Props>;
}

export const AuthApp: AuthApp.Component = (props) => {

  return (
    <InjectorProvider providers={PROVIDERS}>
      <BrowserRouter>
        <AppContainer 
          env={props.env}
          intlMessages={props.intlMessages} />
      </BrowserRouter>
    </InjectorProvider>
  );
};

AuthApp.displayName = 'AuthApp';