import React from 'react';
import ReactDOM from 'react-dom';
import { AuthApp } from './app/components';
import { environment } from './environments/environment';

const appEnv = environment.production ? 'prod' : 'dev';

const intlMessages = environment.locals.reduce((acc, locale) => {
  acc[locale] = require(`./local/${locale}.local.yaml`);

  return acc;
}, {});

setTimeout(() => {
  ReactDOM.render(
    <AuthApp 
      appEnv={appEnv} 
      env={environment}
      intlMessages={intlMessages}
    />, 
    document.getElementById('root'));
}, 60);
