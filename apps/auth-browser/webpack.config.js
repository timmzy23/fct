const combineLoaders = require('webpack-combine-loaders');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = (config) => {
  // Skip bable pre-preprocesing. It is broken now.
  config.resolve.extensions = [
    ...config.resolve.extensions,
    '.scss'
  ];

  // console.log(config.module.rules);

  config.module.rules = [
    {
      test: /\.(j|t)sx?$/,
      loader: 'ts-loader',
      options: {
        configFile: __dirname + '/tsconfig.app.json',
        transpileOnly: true,
        experimentalWatchApi: true,
        compilerOptions: null
      }
    },
    // Load modular css
    {
      test: /\.component\.s(a|c)ss$/,
      loader: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: true,
            import: true,
            sourceMap: true
          }
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            includePaths: [
              'libs/ui-theme/src',
              'libs/ui-react/src/lib/typography',
            ]
          }
        }
      ]
    },
    // load global css
    {
      test: /\.(global|theme)\.s(a|c)ss$/,
      loader: [
        'style-loader',
        'css-loader',
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            includePaths: [
              'libs/ui-theme/src',
              'libs/ui-react/src/lib/typography',
            ]
          }
        }
      ]
    },
    //...config.module.rules
  ];

  config.module.rules.push({
    test: /\.yaml$/,
    use: 'js-yaml-loader',
  });

  config.plugins = [
    ...(config.plugins || []),
    /*new BundleAnalyzerPlugin({
      analyzerPort: 8875
    })*/
  ];

  return config;
}
