const combineLoaders = require('webpack-combine-loaders');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');

function loadConfig() {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path.join(__dirname, 'src', 'config', 'app.config.json'),
      (err, data) => {
        if (err) { reject(err); }
        else { resolve(data) }
      })
  });
}

/**
 * Replace config placeholders by env variables in runtime.
 */
function parseConfig(req, resp, next) {
  dotenv.config({ path: path.join(__dirname, '../../.env')});
  console.log(process.cwd(),  path.join(__dirname, '../../.env'));

  console.log(process.env);


  loadConfig()
      .then((configBuffer) => {

        const dataStr = Reflect.ownKeys(process.env).reduce(
            (config, ENV) => {
              return config.replace('${'+ ENV+ '}', process.env[ENV]);
            },
            configBuffer.toString());

        resp.json(JSON.parse(dataStr));
      })
      .catch((err) => {
        console.error(err);
        resp.json({ error: 'sorry' });
      })
}

module.exports = (config) => {
  // Skip bable pre-preprocesing. It is broken now.
  config.resolve.extensions = [
    ...config.resolve.extensions,
    '.scss'
  ];

  config.module.rules = [
    {
      test: /\.(j|t)sx?$/,
      loader: 'ts-loader',
      options: {
        configFile: __dirname + '/tsconfig.app.json',
        transpileOnly: true,
        experimentalWatchApi: true,
        compilerOptions: null
      }
    },
    // Load modular css
    {
      test: /\.component\.s(a|c)ss$/,
      loader: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            modules: true,
            import: true,
            sourceMap: true
          }
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            includePaths: [
              'libs/ui-theme/src',
              'libs/ui-react/src/lib/typography',
            ]
          }
        }
      ]
    },
    // load global css
    {
      test: /\.(global|theme)\.s(a|c)ss$/,
      loader: [
        'style-loader',
        'css-loader',
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            includePaths: [
              'libs/ui-theme/src',
              'libs/ui-react/src/lib/typography',
            ]
          }
        }
      ]
    }
    //...config.module.rules
  ];

  config.module.rules

  config.module.rules.push({
    test: /\.yaml$/,
    use: 'js-yaml-loader',
  });
  config.devServer = {
    ...config.devServer,
    before: function(app, server, compiler) {
      app.get('/config/app.config.json', parseConfig);
    }
  }

  return config;
}
