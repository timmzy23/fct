const PROXY_CONFIG = {
  "/v1/api": {
    "target": "http://localhost:3334",
    "secure": false
  },
}

module.exports = PROXY_CONFIG;
