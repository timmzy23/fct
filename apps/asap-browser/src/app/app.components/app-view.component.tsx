import {
  AuthSso,
  useAuthStore,
  useIsAuthenticated
  } from '@fct/auth-sso-react';
import { useSubscription } from '@fct/sdk-react';
import {
  dispatchChangeTheme,
  useCurrentThemeState
  } from '@fct/ui-react';
import * as React from 'react';
import {
  Redirect,
  Route,
  Switch
  } from 'react-router-dom';
import { EditorScreenComponent } from '../../lib';
import { AppSplashScreen } from './screens/app.splash-screen.component';

const styles = require('./app-view.component.scss');

export declare namespace AppView {
  interface Props {
    ssoServiceUrl: string
  }

  type Component = React.FunctionComponent<Props>;
}

export const AppView: AppView.Component = (props) => {
  const currentTheme = useCurrentThemeState();

  const className = React.useMemo(
      () => `${styles.host} themed ${currentTheme}`,
      [currentTheme]);
  const isAuthenticated = useIsAuthenticated();
  
  return (
      <div className={className}>
        <AuthSso ssoServiceUrl={props.ssoServiceUrl}/>
        {!isAuthenticated && <AppSplashScreen />}
        {isAuthenticated &&
          <Switch>
            <Route path="/editor" component={EditorScreenComponent}/>
            <Redirect 
              from="/"
              to="/editor"
              exact/>
          </Switch>
        }
      </div>
  );
}

AppView.displayName = 'AppView';