import {
  AsapSpinner,
  CenteredCard,
  Heading,
  Subheading,
  TextBlock,
  TextFontSize,
  TextFontStyle
  } from '@fct/ui-react';
import * as React from 'react';

const styles = require('./app.splash-screen.component.scss');

export declare namespace AppSplashScreen {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const AppSplashScreen: AppSplashScreen.Component = (props) => {
  return (
    <CenteredCard className={styles.host}>
      <AsapSpinner 
          width="200px" 
          height="200px" 
          label="ASaP"
          labelSize={TextFontSize.EXTRA_EXTRA_LARGE}
          labelStyle={TextFontStyle.BOLD}/>
    </CenteredCard>
  );
};

AppSplashScreen.displayName = 'AppSplashScreen';

AppSplashScreen.defaultProps = {};
