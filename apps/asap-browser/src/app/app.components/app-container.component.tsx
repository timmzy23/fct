import { useAuthSsoInit } from '@fct/auth-sso-react';
import {
  ApplicationState,
  HttpClient,
  HttpHeader,
  HttpRequest,
  InMemoryState,
  useActions,
  useActionsFeature,
  useHttpClientInit,
  useInject,
  useRefByCallback,
  useStores,
  useSubscription
  } from '@fct/sdk-react';
import {
  IntlDynamicProvider,
  useIntlInit,
  useKeyboard,
  useThemes
  } from '@fct/ui-react';
import * as React from 'react';
import { useNavigateToObserverEffect } from '../../lib/common';
import { useRuntimeConfig } from '../app.hooks';
import { AppView } from './app-view.component';

export declare namespace AppContainer {
  interface Props {
    env: any;
    intlMessages: any;
  }

  type Component = React.FunctionComponent<Props>;
}

export const AppContainer: AppContainer.Component = (props) => {
  const INTL_CONIG = React.useRef({
    defaultLocal: props.env.defaultLocal,
    enabledLocals: props.env.locals,
    messages: props.intlMessages
  }).current;

  const THEME_CONFIG = React.useRef({
    defaultThemeName: props.env.defaultThemeName,
    themeNames: props.env.themeNames
  }).current;

  const HTTP_CONFIG: HttpClient.Init = useRefByCallback(() => ({
    base: location.href,
    requestInterceptors: [
      HttpRequest.getRequestHeaderDecorator(
          HttpHeader.CONTENT_TYPE, 'application/json')
    ]
  })).current;

  useActionsFeature();
  useStores(new InMemoryState());
  useThemes(THEME_CONFIG);
  useIntlInit(INTL_CONIG);
  useHttpClientInit(HTTP_CONFIG);
  useNavigateToObserverEffect();
  useAuthSsoInit();
  useKeyboard();

  const config = useRuntimeConfig('/config/app.config.json');
  const { appState } = useInject({ appState: ApplicationState });
  window['appState'] = appState;
  const actions = useActions();
  useSubscription(() => actions.subscribe({ next: (a) => console.debug(a) }), []);

  return config && (
    <IntlDynamicProvider>
      <AppView ssoServiceUrl={config.ssoServiceUrl}/>
    </IntlDynamicProvider>
  );
}

AppContainer.displayName = 'AppContainer';
