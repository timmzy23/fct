import {
  useHttp,
  useInjector,
  ValueProvider
  } from '@fct/sdk-react';
import * as React from 'react';

export const RUNTIME_CONFIG_TOKEN = Symbol('RUNTIME_CONFIG_TOKEN');

export function useRuntimeConfig(configUrl: string) {
  const http = useHttp();
  const injector = useInjector();
  const [config, setConfig] = React.useState(null);

  if (!injector.hasProvider(RUNTIME_CONFIG_TOKEN)) {
    injector.use(new ValueProvider(RUNTIME_CONFIG_TOKEN, new Map()));
  }

  const configMap: Map<string, any> = 
      injector.inject(RUNTIME_CONFIG_TOKEN)as Map<string, any>;

  React.useEffect(
    () => {
      if (!configMap.has(configUrl)) {
        http.fetch(configUrl).subscribe((r) => {
          configMap.set(configUrl, r);
          setConfig(r);
        });
      } else {
        setConfig(configMap.get(configUrl));
      }
    }, 
    []);

  return config;
}