export const environment = {
  production: true,
  locals: [
    'cs', 'en'
  ],
  defaultLocal: 'en',
  defaultThemeName: 'light-theme',
  themeNames: ['light-theme', 'dark-theme'],
};
