// This file can be replaced during build by using the `fileReplacements` array.
// When building for production, this file is replaced with `environment.prod.ts`.

export const environment = {
  production: false,
  locals: [
    'cs', 'en'
  ],
  defaultLocal: 'en',
  defaultThemeName: 'light-theme',
  themeNames: ['light-theme', 'dark-theme'],
  ssoServiceUrl: "http://localhost:4200/sso"
};
