import { useOnKeyboardOpenEvent } from '@fct/ui-react';
import * as React from 'react';
import {
  Route,
  RouteComponentProps,
  Switch
  } from 'react-router';
import { mapTo } from 'rxjs/operators';
import {
  ProjectMainScreen,
  ProjectModalExplorer
  } from '../../project';
import { ProjectNavigateToCommand } from '../../project/project.actions';
import { ProjectCreateScreen } from '../../project/project.components/create';
import { useOpenProjectExplorerShortcut } from '../../project/project.hooks';
import { EditorNewProjectCommand } from '../editor.actions';
import { EditorFooter } from './editor.footer.component';
import { EditorSidebar } from './editor.sidebar.component';
import { EditorSplashScreen } from './editor.splash-screen.component';
import {
  actionOf,
  useCommandEffect,
  } from '@fct/sdk-react';

const styles = require('./editor.screen.component.scss');

export declare namespace EditorScreenComponent {
  type Props = RouteComponentProps;
  type Component = React.FunctionComponent<Props>;
}

export const EditorScreenComponent: EditorScreenComponent.Component = (props) => {
  useCommandEffect((commands$) => commands$.pipe(
    actionOf(EditorNewProjectCommand),
    mapTo(ProjectNavigateToCommand.goToNewProject())
  ));
  useOpenProjectExplorerShortcut();

  return (
    <>
    <article className={styles.host}>
      <section grid-area="sidebar">
        <EditorSidebar />
      </section>
      <section grid-area="content">
        <Switch>
          <Route 
            path={`${props.match.path}/project/new`} 
            component={ProjectCreateScreen}/>
          <Route 
            path={`${props.match.path}/project/:projectId`} 
            component={ProjectMainScreen} />
          <Route 
            path={`${props.match.path}/`} 
            component={EditorSplashScreen}/>
        </Switch>
        
      </section>
      <footer grid-area="footer">
        <EditorFooter />
      </footer>
    </article>
    <ProjectModalExplorer />
    </>
  );
};

EditorScreenComponent.displayName = 'EditorScreenComponent';

EditorScreenComponent.defaultProps = {};
