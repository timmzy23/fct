import * as React from 'react';

const styles = require('./editor.sidebar-link.component.scss');

export declare namespace EditorSidebarLink {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const EditorSidebarLink: EditorSidebarLink.Component = (props) => {
  return (<div>My component: EditorSidebarLink</div>);
};

EditorSidebarLink.displayName = 'EditorSidebarLink';

EditorSidebarLink.defaultProps = {};
