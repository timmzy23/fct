import {
  useAuthIdentity,
  useAuthStore
  } from '@fct/auth-sso-react';
import {
  IconColor,
  IntlSwitch,
  Text,
  TextColor,
  TextFontSize,
  TextFontStyle,
  ThemeSwitch
  } from '@fct/ui-react';
import * as React from 'react';

const styles = require('./editor.footer.component.scss');

export declare namespace EditorFooter {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const EditorFooter: EditorFooter.Component = (props) => {
  const identity = useAuthIdentity() as any;

  return (
    <div className={styles.host}>
      <div grid-area="left">
        {identity && 
        <Text fontSize={TextFontSize.SMALL} textColor={TextColor.SECONDARY}>
          {identity.firstName} {identity.lastName} ({identity.identifier})
        </Text>}
      </div>
      <div grid-area="right">
        <IntlSwitch />
        <ThemeSwitch iconColor={IconColor.SECONDARY}  />
      </div>
    </div>
  );
};

EditorFooter.displayName = 'EditorFooter';

EditorFooter.defaultProps = {};
