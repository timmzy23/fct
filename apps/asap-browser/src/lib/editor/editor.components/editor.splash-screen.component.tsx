import { useDispatchCommand } from '@fct/sdk-react';
import {
  AsapLogo,
  CenteredCard,
  IconColor,
  TextBlock,
  TextColor,
  TextLineHeight
  } from '@fct/ui-react';
import * as React from 'react';
import { ProjectExplorerOpenCommand } from '../../project/project.actions';
import { EditorNewProjectCommand } from '../editor.actions';

const styles = require('./editor.splash-screen.component.scss');

export declare namespace EditorSplashScreen {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const EditorSplashScreen: EditorSplashScreen.Component = (props) => {
  const dispatchCommand = useDispatchCommand();

  const handleNewProject = React.useCallback(() => {
    dispatchCommand(EditorNewProjectCommand.create());
  }, [dispatchCommand]);

  const handleOpenProject = React.useCallback(() => {
    dispatchCommand(ProjectExplorerOpenCommand.create());
  }, [dispatchCommand]);

  return (
    <CenteredCard>
      <article className={styles.host}>
        <AsapLogo 
            logoColor={IconColor.MARGINAL}
            width="300px"
            height="300px"
            className={styles.logo}/>
        <TextBlock 
            className={styles.link} 
            lineHeight={TextLineHeight.EXTRA_LARGE}
            textColor={TextColor.SECONDARY}
            onClick={handleNewProject}> 
          Start new project   
        </TextBlock>
        <TextBlock 
            className={styles.link} 
            lineHeight={TextLineHeight.EXTRA_LARGE}
            textColor={TextColor.SECONDARY}
            onClick={handleOpenProject}> 
          Open existing project
        </TextBlock>
      </article>
    </CenteredCard>
  );
};

EditorSplashScreen.displayName = 'EditorSplashScreen';

EditorSplashScreen.defaultProps = {};
