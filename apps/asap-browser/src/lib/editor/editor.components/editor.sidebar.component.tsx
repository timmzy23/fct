import { useDispatchCommand } from '@fct/sdk-react';
import {
  AddListIcon,
  IconColor,
  IconSize,
  ProductDocumentsIcon
  } from '@fct/ui-react';
import * as React from 'react';
import { ProjectExplorerOpenCommand } from '../../project/project.actions';
import { EditorNewProjectCommand } from '../editor.actions';

const styles = require('./editor.sidebar.component.scss');

export declare namespace EditorSidebar {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const EditorSidebar: EditorSidebar.Component = (props) => {
  const dispatchCommand = useDispatchCommand();

  const handleNewProject = React.useCallback(() => {
    dispatchCommand(EditorNewProjectCommand.create());
  }, [dispatchCommand]);
  
  const handleOpenProject = React.useCallback(() => {
    dispatchCommand(ProjectExplorerOpenCommand.create());
  }, [dispatchCommand]);

  return (
    <div className={styles.host}>
      <div className={styles.sidebarLink} onClick={handleNewProject}>
        <AddListIcon 
          size={IconSize.EXTRA_LARGE} 
          iconColor={IconColor.SECONDARY}/>
      </div>
      <div className={styles.sidebarLink} onClick={handleOpenProject}>
        <ProductDocumentsIcon 
          size={IconSize.EXTRA_LARGE} 
          iconColor={IconColor.SECONDARY}/>
      </div>
    </div>);
};

EditorSidebar.displayName = 'EditorSidebar';

EditorSidebar.defaultProps = {};
