import { FluxStandardAction } from '@fct/sdk-react';
enum EDITOR_ACTION_TYPES {
  NEW_PROJECT = 'asap.editor.newProject',
  OPEN_EXPLORER = 'asap.editor.openExplorer'
}

export declare namespace EditorNewProjectCommand {
  type ACTION_TYPE = EDITOR_ACTION_TYPES.NEW_PROJECT;
  type Action = FluxStandardAction.Action<ACTION_TYPE>;
}

export class EditorNewProjectCommand {
  public static ACTION_TYPE: EditorNewProjectCommand.ACTION_TYPE = 
      EDITOR_ACTION_TYPES.NEW_PROJECT;

  public static create(): EditorNewProjectCommand.Action {
    return FluxStandardAction.create(EditorNewProjectCommand.ACTION_TYPE);
  }
}

export declare namespace EditorExplorerOpenCommand {
  type ACTION_TYPE = EDITOR_ACTION_TYPES.OPEN_EXPLORER;
  type Action = FluxStandardAction.Action<ACTION_TYPE>;
}

export class EditorExplorerOpenCommand {
  public static ACTION_TYPE: EditorExplorerOpenCommand.ACTION_TYPE = 
      EDITOR_ACTION_TYPES.OPEN_EXPLORER;

  public static create(): EditorExplorerOpenCommand.Action {
    return FluxStandardAction.create(EditorExplorerOpenCommand.ACTION_TYPE);
  }
}
