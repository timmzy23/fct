import { ProjectXmlParserService } from '../project.services';

export const PROJECT_XML_SCHEMA: ProjectXmlParserService.Schema = {
  clientName: { type: String, required: true },
  commentL: { type: String, required: false },
  commentM: { type: String, required: false },
  commentS: { type: String, required: false },
  contentId: { type: Number, required: true },
  createdBy: { type: String, required: false },
  dataAccessibility: { type: String, required: false },
  dataSource: { type: String, required: false },
  humanID: { type: String, required: true },
  indAuthor: { type: String, required: false },
  indID: { type: String, required: false },
  indName: { type: String, required: false },
  indNotes: { type: String, required: false },
  indValueL: { type: String, required: false },
  indValueM: { type: String, required: false },
  indUAP: { type: Boolean, required: false, parse: (value: string) => {
    return Number.parseInt(value, 10) === 1 
  }},
  indValueS: { type: String, required: false },
  level: { type: Number, required: true },
  name: { type: String, required: true },
  parameterFullName: { type: String, required: true },
  parentId: { type: Number, required: true },
  projectId: { type: Number, required: true },
  projectName: { type: String, required: false },
  projectScenario: { type: String, required: false},
  projectVersion: { type: String, required: false},
  projectState: { type: String, required: false},
  recommendation: { type: String, required: false },
  score: { type: Number, required: true },
  scoreL: { type: Number, required: false },
  scoreM: { type: Number, required: false },
  scoreS: { type: Number, required: false },
  summary: { type: String, required: false },
  tendency: { type: Number, required: true },
  tendencyL: { type: Number, required: false },
  tendencyM: { type: Number, required: false },
  tendencyS: { type: Number, required: false },
  indUnits: { type: String, required: false },
  verifiedBy: { type: String, required: false },
  weight: { type: Number, required: true },
  weightL: { type: Number, required: false },
  weightM: { type: Number, required: false },
  weightS: { type: Number, required: false }
}
