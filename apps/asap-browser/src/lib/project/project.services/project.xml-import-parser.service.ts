import {
  ProjectDocument,
  ProjectDocumentNode
  } from '@fct/asap-interfaces';
import {
  ErrnoError,
  ReflectionHelper
  } from '@fct/sdk';
import {
  ERR_INVALID_PROJECT_PROP_TYPE,
  ERR_MISSING_REQUIRED_PROP,
  ERR_UNKNOWN_PROJECT_PROP_TYPE,
  ERR_UNKNOWN_PROJECT_PROPERTY
  } from '../project.errors';

export declare namespace ProjectXmlParserService {
  interface SchemaType<T = any> {
    type: any;
    required: boolean;
    parse?: (raw: any) => T;
  }

  interface Schema {
    [nodeName: string]: SchemaType;
  }

  interface XMLNodeRaw {
    clientName?: string;
    comment?: string;
    commentL?: string;
    commentM?: string;
    commentS?: string;
    contentId: number;
    createdBy?: string;
    critical?: boolean;
    dataAccessibility?: string;
    dataSource?: string;
    humanID: string;
    indAuthor?: string;
    indID?: string;
    indName?: string;
    indNotes?: string;
    indValueL?: number;
    indValueM?: number;
    indValueS?: number;
    indUAP?: boolean;
    level: number;
    name: string;
    parameterFullName?: string;
    parentId?: number;
    projectId: number;
    projectName?: string;
    projectVersion?: string;
    projectScenario?: string;
    projectState?: string;
    recommendation?: string;
    score: number;
    scoreL?: number;
    scoreM?: number;
    scoreS?: number;
    summary?: string;
    tendency: number;
    tendencyL?: number;
    tendencyM?: number;
    tendencyS?: number;
    indUnits?: string;
    verifiedBy?: string;
    weight: number;
    weightL?: number;
    weightM?: number;
    weightS?: number;
  }
}

export class ProjectXmlParserService {

  private requiredFielNames: string[];

  constructor(
    private schema: ProjectXmlParserService.Schema
  ) {
    this.requiredFielNames = Reflect.ownKeys(this.schema)
        .filter((fieldName: string) => 
            this.schema[fieldName].required) as string[];
  }

  public parse(xmlStr: string): ProjectDocument {
    const parser = new DOMParser();
    const doc: XMLDocument = parser.parseFromString(xmlStr, 'application/xml');

    const nodeCollection = doc.documentElement.children;
    const data = [];

    for (let i = 0; i < nodeCollection.length; i++) {
      const child = nodeCollection[i];
      data.push(this.parseNode(child));
    }

    return this.aggregate(data);
  }

  /**
   * Build a ProjectDocument from provided xml nodes.
   */
  private aggregate(data: ProjectXmlParserService.XMLNodeRaw[]): ProjectDocument {
    const acc = data.reduce((acc, raw) => {
      if (!acc[raw.contentId]) {
        acc[raw.contentId] = {
          contentId: raw.contentId,
          createdBy: raw.createdBy && { name: raw.createdBy },
          verifiedBy: raw.verifiedBy && { name: raw.verifiedBy },
          critical: raw.critical,
          xPath: raw.humanID,
          name: raw.name,
          fullName: raw.parameterFullName,
          recommendation: raw.recommendation,
          summary: raw.summary,
          project: {
            projectId: raw.projectId,
            projectName: raw.projectName,
            clientName: raw.clientName,
            projectScenario: raw.projectScenario,
            projectState: raw.projectState,
            projectVersion: raw.projectVersion
          },
          comment: {
            value: raw.comment,
            context: {
              L: raw.commentL,
              S: raw.commentS,
              M: raw.commentM
            }
          },
          score: {
            value: raw.score,
            context: {
              L: raw.scoreL,
              S: raw.scoreS,
              M: raw.scoreM
            }
          },
          tendency: {
            value: raw.tendency,
            context: {
              L: raw.tendencyL,
              S: raw.tendencyS,
              M: raw.tendencyM
            }
          },
          weight: {
            value: raw.weight,
            context: {
              L: raw.weightL,
              S: raw.weightS,
              M: raw.weightM
            }
          },
          indicators: []
        };
      }

      const nodeFrag: Partial<ProjectDocumentNode> = acc[raw.contentId];

      if (raw.indID) {
        nodeFrag.indicators.push({
          author: raw.indAuthor && { name: raw.indAuthor },
          id: raw.indID,
          name: raw.indName,
          notes: raw.indNotes,
          units: raw.indUnits,
          accessibility: raw.dataAccessibility,
          source: raw.dataSource,
          uap: raw.indUAP,
          values: {
            L: raw.indValueL,
            M: raw.indValueM,
            S: raw.indValueS
          }
        })
      }

      return acc;
    }, {});

    return Reflect.ownKeys(acc).map((key) => acc[key]);
  }

  public parseNode(xml: Element): ProjectDocumentNode  {
    const children = xml.children;
    const nodeData = {};

    for (let i = 0; i < children.length; i++) {
      const element = children[i];
      const propName = element.tagName;
      const schemaType = this.schema[element.tagName];

      if (!schemaType) {
        throw new ErrnoError(
          ERR_UNKNOWN_PROJECT_PROPERTY,
          `Project XML unrecognized property ${propName}`,
          { propName }
        );
      }

      const value = schemaType.parse ? 
          schemaType.parse(element.textContent) : 
          element.textContent;

      switch(schemaType.type) {
        case String:
          nodeData[propName] = 
              this.parseString(propName, value, schemaType);
          break;
        case Number:
            if (value !== '' || schemaType.required) {
              nodeData[propName] = 
                this.parseNumber(propName, value, schemaType);
            }
            break;
        case Boolean:
            nodeData[propName] = this.parseBoolean(propName, value);
            break;
        default:
          throw new ErrnoError(
            ERR_UNKNOWN_PROJECT_PROP_TYPE,
            `Project XML unknown property type: ${schemaType.type}`,
            { type: schemaType.type });
      }
    }

    this.assertRequiredProps(nodeData);

    return nodeData as ProjectDocumentNode;
  }

  private assertRequiredProps(nodeData: any) {
    const missingProps = this.requiredFielNames
        .filter((propName) => !Reflect.has(nodeData, propName));

    if (missingProps.length > 0) {
      throw new ErrnoError(
        ERR_MISSING_REQUIRED_PROP,
        `Props "${missingProps.join(',')}" are required but missing at node "${nodeData.humanID}"`,
        { missingProps, nodeData });
    }
  }

  private parseString(key: string, value: string, schemaType: ProjectXmlParserService.SchemaType) {
    if (!ReflectionHelper.isString(value)) {
      throw new ErrnoError(
        ERR_INVALID_PROJECT_PROP_TYPE,
        `Project XML invalid type. Expected string for "${key}"`,
        { key, value });
    }
    return value;
  }

  private parseNumber(key: string, value: string, schemaType: ProjectXmlParserService.SchemaType) {
    const numVal = Number.parseFloat(value);

    if (isNaN(numVal)) {
      throw new ErrnoError(
        ERR_INVALID_PROJECT_PROP_TYPE,
        `Project XML invalid type. Expected number for "${key}"`,
        { key, value });
    }
    return numVal;
  }

  private parseBoolean(key: string, value: any) {

    if (!ReflectionHelper.isBoolean(value)) {
      throw new ErrnoError(
        ERR_INVALID_PROJECT_PROP_TYPE,
        `Project XML invalid type. Expected boolean for "${key}"`,
        { key, value });
    }
    return value;
  }
}