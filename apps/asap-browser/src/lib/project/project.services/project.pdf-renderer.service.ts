import {
  Project,
  ProjectDocumentCollection,
  ProjectDocumentCollectionRecord,
  ProjectDocumentNode,
  ProjectDocumentNodeHelper,
  ProjectIndicator
  } from '@fct/asap-interfaces';
import { HttpClient } from '@fct/sdk-react';
import {
  PageFormat,
  PageOrientation,
  ParamDetail,
  Pdf,
  PdfUnit,
  renderDefaultSummary,
  renderPageRidge,
  renderPageSidebar,
  renderSectionSummaryPage
  } from '../project.pdf/pdf-components';
import * as bookTheme from '../project.pdf/pdf-components/book.theme';
import { ParamDetailNoChildren } from '../project.pdf/pdf-components/renderers/param-detail-no-children';
import { PageData } from '../project.pdf/pdf-components/types';

export declare namespace ProjectPdfRendererService {
  interface Options {
    separators: boolean;
    parameters: boolean;
  }
}

export class ProjectPdfRendererService {

  constructor(private http: HttpClient) {}

  public render(project: Project, options: ProjectPdfRendererService.Options) {
    const pdfInit: Pdf.Props = {
      orientation: PageOrientation.LANDSCAPE,
      pageFormat: PageFormat.A4,
      unit: PdfUnit.MILIMETERS,
      svgPpu: 24
    };
    const projectCollection = new ProjectDocumentCollection(project);
    const pages = processPageData(project.nodes, project, projectCollection);
    const pageMap = pages.reduce((acc: any, p: any) => {
      acc[p.contentId] = p;
      return acc;
    }, {});

    const pdf = new Pdf(
      pdfInit, 
      bookTheme, 
      (fontUrl) => this.dowlonadFont(fontUrl));

    pdf.registerFont(bookTheme.ASAP_BOOK_FONT_FACES).then(() => {
      pdf.setFont('Nudista', 'light');
      pages.forEach((page: PageData, ix: number) => { 
        const node = projectCollection.getNodeById(page.nodeId);
        if (page.hasChildren) {
          // Add blank page before section separator if it is not an odd one
          
          if (options.parameters) {
            if (pdf.pageNumber % 2 === 0) {
              pdf.addPage();
            }
          }
          
          // No shift, section is always on even odd page
          if (options.separators) {
            renderSectionSummaryPage(pdf, {currentPage: page, pageMap: pageMap, node, project: projectCollection });
            // Add blank page after section separator (always)
            pdf.addPage();
          }

          
          if (options.parameters) {
            // Add page for section summary
            pdf.addPage();
            renderPageRidge(
              pdf, 
              { 
                startY: pdf.pageNumber % 2 === 0 ?
                  pdf.theme.ASAP_BOOK_SORT_MARKER_START_Y :
                  pdf.theme.DOC_HEIGHT - (
                    pdf.theme.ASAP_BOOK_SORT_MARKER_START_Y + 
                    pdf.theme.ASAP_BOOK_SORT_MARKER_RADIUS)
              });
            // No shift for page sidebar
            renderPageSidebar(pdf, page);
            renderDefaultSummary(pdf, { 
              data: page, 
              list: pageMap, 
              project,
              shiftY: !pdf.isOddPage() ? 0 : pdf.theme.DOC_EVEN_SHIFT,
              node
            });
          }
        }

        if (options.parameters) {
          pdf.addPage();
          renderPageRidge(
            pdf, 
            { 
              startY: pdf.pageNumber % 2 === 0 ?
                pdf.theme.ASAP_BOOK_SORT_MARKER_START_Y :
                pdf.theme.DOC_HEIGHT - (
                  pdf.theme.ASAP_BOOK_SORT_MARKER_START_Y + 
                  pdf.theme.ASAP_BOOK_SORT_MARKER_RADIUS)
            });
          renderPageSidebar(pdf, page);

          if (node.hasChildren()) {
            pdf.render(ParamDetail({ 
              project,
              data: page,
              dimensions: {
                start: {
                  x: pdf.theme.ASAP_BOOK_CONTENT_START_X,
                  y: pdf.theme.ASAP_BOOK_CONTENT_START_Y
                }
              },
              shiftY: !pdf.isOddPage() ? 0 : pdf.theme.DOC_EVEN_SHIFT,
              node
            }));
          } else {
            pdf.render(ParamDetailNoChildren({
              project: projectCollection,
              data: node,
              _data: page,
              dimensions: {
                start: {
                  x: pdf.theme.ASAP_BOOK_CONTENT_START_X,
                  y: pdf.theme.ASAP_BOOK_CONTENT_START_Y,
                },
                shiftY: !pdf.isOddPage() ? 0 : pdf.theme.DOC_EVEN_SHIFT
              },
            }));
          }
        }
    });
    pdf.openNewWindow();
    pdf.jsPDF.save('__asap.pdf');
    })
  }

  private dowlonadFont(fontUrl): Promise<string> {
    return this.http
      .get(fontUrl, { 
        // responseType: 'blob' as 'json',
        parseResponse: (response: Response) => response.blob()
      })
      .toPromise()
      .then((blob: Blob) => {
        const reader = new FileReader();
        reader.readAsBinaryString(blob);

        return new Promise<string>((resolve) => {
          reader.onloadend = function() {
            resolve(btoa(reader.result as string));
          }
        });
      });
  }
}

function processPageData(raws: ProjectDocumentNode[], project: Project, projectCollection: ProjectDocumentCollection): PageData[] {
  // Creates empty map
  const idMap = raws.reduce((acc: any, raw: ProjectDocumentNode) => {
    acc[raw.xPath] = {
      data: raw
    };
    return acc;
  }, {});


  raws.forEach((raw) => {
    // Check if it is at least level 2 to have a parent
    if (raw.xPath.length > 1) {
      const level = raw.xPath.split('.').length;
      const parentId = raw.xPath.split('.').slice(0, level -1).join('.');
      
      // Collect all children ids to parent in order to have on each page node
      // a property `childrenIds: string[]` with all children ids.
      if (idMap[parentId]) {
        if (!idMap[parentId].childIds) {
          idMap[parentId].childIds = [];
        }
        idMap[parentId].childIds.push(raw.xPath);
      }

      // Collect parent id in order to have on each page node
      // a property `parentId: string` on each node.
      if (idMap[raw.xPath]) {
        idMap[raw.xPath].parentId = parentId;
      }

      //
    }
  });

  return raws
    .map((raw) => mapToPageData(raw, idMap, raws, project, projectCollection));
}

function getParentName(parentId: string, map: any): string[] {
  const parent = map[parentId];
  if (!parent) { return []; }

  const path = parent.parentId ? 
      getParentName(parent.parentId, map) : [];
  path.push(parent.data['name']);

  return path;
}

function hasContext(node: ProjectDocumentNode, type: string, nodeMap: any) {
  const childIds = nodeMap[node.xPath].childIds || [];
  const children = childIds.map((childId: string) => nodeMap[childId].data);

  return (!!node.comment && !!node.comment.context[type]) || 
      children.some((child: any) => hasContext(child, type, nodeMap));
}

function mapToPageData(node: ProjectDocumentNode, map: any, rawData: any[], project: Project, projectCollection: ProjectDocumentCollection): PageData {
  const contentId = node.xPath;
  const hasChildren = !!(map[contentId].childIds && map[contentId].childIds.length > 0);
  const parentId = map[contentId].parentId;
  const path = getParentName(parentId, map);
  const parentName = map[parentId] ?
    map[parentId].data['name'] : 
    undefined;
  path.push(node['name']);

  const contexts: Array<'S' | 'M' | 'L'> = [];
  const contextData = [];

  const record = projectCollection.getNodeById(node.contentId);


  const [indS, indM, indL] = ['S', 'M', 'L']
      .map((contextType) => record.getIndicators(contextType));

  if (hasContext(node, 'S', map) || (indS && indS.length > 0)) {
    contexts.push('S');
  }

  if ((!!node.comment && node.comment.context.S) || (indS && indS.length > 0)) {
    contextData.push(mapContextData(indS, record, 'S'));
  }
  if (hasContext(node, 'M', map) || (indM && indM.length > 0)) {
    contexts.push('M');
  }
  if ((!!node.comment && node.comment.context.M) || (indM && indM.length > 0)) {
    contextData.push(mapContextData(indM, record, 'M'));
  }
  if (hasContext(node, 'L', map) || (indL && indL.length > 0)) {
    contexts.push('L');
  }
  if ((!!node.comment && node.comment.context.L) || (indL && indL.length > 0)) {
    contextData.push(mapContextData(indM, record, 'L'));
  }

  return {
    // Level can be computed frm id, which is in form 1.1.2.3
    level: node.xPath.split('.').length,
    nodeId: node.contentId,
    contentId,
    hasChildren,
    childIds: map[contentId].childIds || [],
    clientName: project.clientName,
    projectName: project.projectName,
    score: node.score.value,
    tendency: node.tendency.value,
    weight: node.weight.value,
    name: node.name,
    path,
    parameterFullName: node.fullName,
    section: {
      id: Number.parseInt(node.xPath.split('.').shift(), 10),
      name: ProjectDocumentNodeHelper.getNodeLevel(node) + ''
    },
    createdBy: node.createdBy && node.createdBy.name,
    verifiedBy: node.verifiedBy && node.verifiedBy.name,
    parentId: map[contentId].parentId,
    contexts,
    critical: node.critical || false,
    recommendation: node.recommendation || '',
    resume: node.summary,
    parentName,
    contextData,
    //relevancy: true // Debug
    relevancy: node['relevancy'] === 1 || false
  }
}


function mapContextData(
  indicators: ProjectIndicator[],
  record: ProjectDocumentCollectionRecord,
  contextType: string
) {
  return {
    type: contextType,
    comment: record.getCommentForContext(contextType),
    score: record.getScoreForContext(contextType),
    tendency: record.getTendencyForContext(contextType),
    weight: record.getWeightForContext(contextType),
    indicators: (indicators || []).map((ind) => ({
          name: ind.name,
          unit: ind.units,
          source: ind.source,
          availability: ind.accessibility,
          value: ind.values ? 
              Reflect.get(ind.values, contextType) :
              undefined
        }))
  }
}