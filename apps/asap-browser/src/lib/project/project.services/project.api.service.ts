import { ProjectHeaders } from '@fct/asap-interfaces';
import { HttpClient } from '@fct/sdk-react';
import { Observable } from 'rxjs';

export declare namespace ProjectApiService {
  interface CreateProjectPayload {
    projectName: string;
    dataImport: any;
  }
}

export class ProjectApiService {
  constructor(
    private http: HttpClient
  ) {}

  public createProject(
    payload: any
  ): Observable<any> {
    return this.http.post('/v1/api/project', {
      data: payload
    });
  }

  public updateProject(
    payload: any
  ): Observable<any> {
    return this.http.put('/v1/api/project', {
      data: payload
    });
  }

  public getProject(projectId: string) {
    return this.http.get(`/v1/api/project/${projectId}`);
  }

  public getProjectHeaders(): Observable<ProjectHeaders> {
    return this.http.get<ProjectHeaders>(`/v1/api/project/headers`);
  }
}