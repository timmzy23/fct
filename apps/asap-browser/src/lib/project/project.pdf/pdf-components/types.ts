export interface PageSection {
  id: number;
  name: string;
}

export interface PageData {
  nodeId: number;
  section: PageSection;
  level: number;
  contentId: string;
  parameterFullName: string;
  hasChildren: boolean;
  childIds: string[];
  clientName: string;
  projectName: string;
  score: number;
  tendency: number;
  weight: number;
  path: string[];
  parentId: string;
  name: string;
  createdBy?: string;
  verifiedBy?: string;
  contexts: Array<'S' | 'M' | 'L'>;
  critical?: boolean;
  recommendation?: string;
  resume?: string;
  parentName?: string;
  contextData: PageContextData[];
  relevancy?: boolean;
}

export interface PageContextData {
  type: string;
  comment: string;
  score: number;
  tendency: number;
  weight: number;
  indicators?: PageIndicatorData[];
}

export interface PageIndicatorData {
  name: string;
  unit: string;
  value: number;
  source: string;
  availability: string;
}

export interface PdfBlock {
  width: number;
  height: number;
  startX: number;
  startY: number;
}

export interface SortMarkers {
  startY: number;
  radius: number;
  borderSize: number;
  startX: number[];
}

export interface PageSettings {
  leftSidebar: PdfBlock;
  ridge: PdfBlock;
  sorterMarkers: SortMarkers;
}

export interface BookSettings {
  page: PageSettings;
  units: string;
  orientation: 'landscape' | 'portrait',
  format: string,
  resolution: { width: number; height: number },
}