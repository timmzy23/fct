/**
 * All units in his pdf document theme are in milimeters if not set otherwise.
 */
// Pixel per unit
const WIDTH_A4_MM = 297;
const PPI = 12;

const SHRINK_BY_MM = 4;

const unit = (value: number) => value / PPI;

const translate = (ppi: number) => {
  return (value: number) => value / ppi;
};

const scale = (milimeters: number, origPpi: number, sizeMM: number) => {
  const scaledSizeMM = sizeMM - (2 * milimeters); // We are scaling from both sizes
  const sizePx = origPpi * sizeMM;
  const scaledPPI = sizePx / scaledSizeMM;


  return (value: number) => {
    return value / scaledPPI;
  }
};

const transform = (transformation: number) => {
  return (value: number) => {
    return value + transformation;
  }
};

const compose = (...fns: Array<any>) => {
  return (value: number): number => {
    return fns.reduce((res, fn) => {
      return fn(res);
    }, value);
  };
}

const unitDistance = compose(
  translate(PPI), 
  scale(SHRINK_BY_MM, PPI, WIDTH_A4_MM));

const unitPoint = compose(
  translate(PPI), 
  scale(SHRINK_BY_MM, PPI, WIDTH_A4_MM), 
  transform(SHRINK_BY_MM));

// Fonts -----------------------------------------------------------------------
export const ASAP_BOOK_FONT_FACES = [
  { 
    fontName: 'Muller-Regular', 
    fontFamily: 'Muller',
    fontStyle: 'regular',
    url: '/assets/fonts/muller/37C235_2_0.ttf',
  },
  { 
    fontName: 'Muller-Bold', 
    fontFamily: 'Muller',
    fontStyle: 'bold',
    url: '/assets/fonts/muller/37C235_1_0.ttf',
  },
  { 
    fontName: 'Muller-Extra', 
    fontFamily: 'Muller',
    fontStyle: 'extra',
    url: '/assets/fonts/muller/37C235_0_0.ttf',
  },
  { 
    fontName: 'Nudista-Bold', 
    fontFamily: 'Nudista',
    fontStyle: 'bold',
    url: '/assets/fonts/nudista/Nudista-Bold.ttf',
  },
  { 
    fontName: 'Nudista-Bold-Italic', 
    fontFamily: 'Nudista',
    fontStyle: 'bold-italic',
    url: '/assets/fonts/nudista/Nudista-Bold-Italic.ttf',
  },
  { 
    fontName: 'Nudista-Light', 
    fontFamily: 'Nudista',
    fontStyle: 'light',
    url: '/assets/fonts/nudista/Nudista-Light.ttf',
  },
  { 
    fontName: 'Nudista-Light-Italic', 
    fontFamily: 'Nudista',
    fontStyle: 'lightitalic',
    url: '/assets/fonts/nudista/Nudista-Light-Italic.ttf',
  },
  { 
    fontName: 'Nudista-Medium', 
    fontFamily: 'Nudista',
    fontStyle: 'medium',
    url: '/assets/fonts/nudista/Nudista-Medium.ttf',
  },
  { 
    fontName: 'Nudista-Medium-Italic', 
    fontFamily: 'Nudista',
    fontStyle: 'medium-italic',
    url: '/assets/fonts/nudista/Nudista-Medium-Italic.ttf',
  },
  { 
    fontName: 'Nudista-Semibold', 
    fontFamily: 'Nudista',
    fontStyle: 'semibold',
    url: '/assets/fonts/nudista/Nudista-Semibold.ttf',
  },
  { 
    fontName: 'Nudista-Semibold-Italic', 
    fontFamily: 'Nudista',
    fontStyle: 'semibold-italic',
    url: '/assets/fonts/nudista/Nudista-Semibold-Italic.ttf',
  },
  { 
    fontName: 'Nudista-Thin', 
    fontFamily: 'Nudista',
    fontStyle: 'thin',
    url: '/assets/fonts/nudista/Nudista-Thin.ttf',
  },
  { 
    fontName: 'Nudista-Thin-Italic', 
    fontFamily: 'Nudista',
    fontStyle: 'thin-italic',
    url: '/assets/fonts/nudista/Nudista-Thin-Italic.ttf',
  },
];

export const ASAP_BOOK_RESOLUTION_WIDTH = 3504;
export const ASAP_BOOK_PAGE_HEIGHT = 210;

// Commons ---------------------------------------------------------------------
export const DOC_HEIGHT = unit(2520);
export const DOC_EVEN_SHIFT = unit(-130);

export const ASAP_BOOK_TEXT_LINE_HEIGHT = unit(70);
export const ASAP_BOOK_GAP = unit(36);
export const ASAP_TEXT_COLOR = '#58595B';

// Book ridge sort marks -------------------------------------------------------
export const ASAP_BOOK_SORT_MARKER_BORDER_COLOR = '#e4e4e4';
export const ASAP_BOOK_SORT_MARKER_RADIUS = unit(38);
export const ASAP_BOOK_SORT_MARKER_START_Y = unit(96);
export const ASAP_BOOK_SORT_MARKER_START_X = [ 
  unit(306),
  unit(1266.18),
  unit(2225.82),
  unit(3186)
];
export const ASAP_BOOK_SORT_MARKER_BORDER_SIZE = unit(4.8);

// Page sidebar ----------------------------------------------------------------
export const ASAP_BOOK_SIDEBAR_WIDTH = unit(210);
export const ASAP_BOOK_SIDEBAR_HEIGHT = ASAP_BOOK_PAGE_HEIGHT;
export const ASAP_BOOK_SIDEBAR_TEXT_COLOR = '#ffffff';

export const ASAP_BOOK_SIDEBAR_LEVEL_FONT_SIZE = unit(72);
export const ASAP_BOOK_SIDEBAR_LEVEL_FONT_STYLE = 'bold';
export const ASAP_BOOK_SIDEBAR_LEVEL_START_Y = unit(2322);

export const ASAP_BOOK_SIDEBAR_PARAM_FONT_SIZE = unit(72);
export const ASAP_BOOK_SIDEBAR_PARAM_TITLE_X = unit(88);
export const ASAP_BOOK_SIDEBAR_PARAM_SUBTITLE_X = unit(32);
export const ASAP_BOOK_SIDEBAR_PARAM_SUBTITLE_FONT_SIZE = unit(36);

export const ASAP_BOOK_SIDEBAR_PARAM_ID_START_Y = unit(114.63);
export const ASAP_BOOK_SIDEBAR_PARAM_ID_FONT_SIZE = unit(36);
export const ASAP_BOOK_SIDEBAR_PARAM_ID_FONT_STYLE = 'semibold';

/*
 * Logo start X position has to be computed, because of centered on sidebar.
 */
export const ASAP_BOOK_SIDEBAR_LOGO_WIDTH = unit(150.96);
export const ASAP_BOOK_SIDEBAR_LOGO_HEIGHT = unit(167.64);
export const ASAP_BOOK_SIDEBAR_LOGO_START_Y = unit(207.96);

// Colors ----------------------------------------------------------------------
export const ASAP_BOOK_SECTION_CONTEXT_COLOR = '#005CA9';
export const ASAP_BOOK_SECTION_CONTEXT_COLOR_LIGHT = '#CCDEEE';
export const ASAP_BOOK_SECTION_LIMITS_COLOR = '#D51C29'; 
export const ASAP_BOOK_SECTION_LIMITS_COLOR_LIGHT = '#EED0D2'; 
export const ASAP_BOOK_SECTION_INFRASTRUCTURE_COLOR = '#FFCB05';
export const ASAP_BOOK_SECTION_INFRASTRUCTURE_COLOR_LIGHT = '#FFF5CD';
export const ASAP_BOOK_SECTION_DEFAULT_COLOR = '#A7A9AC';
export const ASAP_BOOK_SECTION_DEFAULT_COLOR_LIGHT = '#E6E7E8';
export const ASAP_BOOK_CRITICAL_COLOR = '#FF9C40';

// Footer ----------------------------------------------------------------------
export const ASAP_BOOK_FOOTER_START_X = unit(288);
export const ASAP_BOOK_FOOTER_START_Y = unit(2244);
// Keep column position relatives
export const ASAP_BOOK_FOOTER_COL_GAP = unit(36);
export const ASAP_BOOK_FOOTER_COL_LENGTH_SHORT = unit(180);
export const ASAP_BOOK_FOOTER_COL_LENGTH_LONG = unit(1176);
export const ASAP_BOOK_FOOTER_HEIGHT = unit(180);

// Summary ---------------------------------------------------------------------
export const ASAP_BOOK_SUMMARY_DEFAULT_BACKGROUND = '#E6E7E8';
export const ASAP_BOOK_SUMMARY_DEFAULT_COLOR = '#58595B';

export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_WIDTH = unit(1236);
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_HEIGHT = unit(69.96);
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_X = unit(882);
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y = unit(2244);
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_BACKGROUND = ASAP_BOOK_SUMMARY_DEFAULT_BACKGROUND;
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_COLOR = ASAP_BOOK_SUMMARY_DEFAULT_COLOR;
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_FONT_FAMILY = 'Nudista';
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_FONT_STYLE = 'medium';
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_FONT_SIZE = unit(30);
export const ASAP_BOOK_SUMMARY_PROJECT_HEAD_PADDING_TOP = unit(18.36);
export const ASAP_BOOK_SUPPARY_PROJECT_HEAD_PADDING_LEFT = unit(24);

export const ASAP_BOOK_SUMMARY_PROJECT_PATH_WIDTH = unit(1236);
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_HEIGHT = unit(69.96);
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_START_X = unit(882);
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_START_Y = 
    ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y + unit(36) + unit(72);
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_BACKGROUND = ASAP_BOOK_SUMMARY_DEFAULT_BACKGROUND;
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_COLOR = ASAP_BOOK_SUMMARY_DEFAULT_COLOR;
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_FONT_FAMILY = 'Nudista';
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_FONT_STYLE = 'semibold';
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_FONT_SIZE = unit(30);
export const ASAP_BOOK_SUMMARY_PROJECT_PATH_PADDING_TOP = unit(18.36);
export const ASAP_BOOK_SUPPARY_PROJECT_PATH_PADDING_LEFT = unit(24);

export const ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE = unit(180);
export const ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_TOP = unit(21.96);
export const ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_BOTTOM = unit(27.48);
export const ASAP_BOOK_SUMMARY_SQUARE_INFO_LABEL_FONT_SIZE = unit(28);
export const ASAP_BOOK_SUMMARY_SQUARE_INFO_VALUE_FONT_SIZE = unit(60);
export const ASAP_BOOK_SUMMARY_SQUARE_INFO_FONT_STYLE = 'bold';
export const ASAP_BOOK_SUMMARY_SUQARE_INFO_MARGIN = unit(36);
export const ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_TITLE = unit(19);

export const ASAP_BOOK_SUMARY_INFO_SCORE_X = unit(2148);
export const ASAP_BOOK_SUMARY_INFO_SCORE_Y = ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y;

export const ASAP_BOOK_SUMARY_INFO_TENDENCY_X = 
    ASAP_BOOK_SUMARY_INFO_SCORE_X +
    ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
    ASAP_BOOK_SUMMARY_SUQARE_INFO_MARGIN;
export const ASAP_BOOK_SUMARY_INFO_TENDENCY_Y = ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y;

export const ASAP_BOOK_SUMARY_INFO_WEIGHT_X = 
  ASAP_BOOK_SUMARY_INFO_TENDENCY_X +
  ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
  ASAP_BOOK_SUMMARY_SUQARE_INFO_MARGIN;
export const ASAP_BOOK_SUMARY_INFO_WEIGHT_Y =ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y;

export const ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_1_X = 
  ASAP_BOOK_SUMARY_INFO_WEIGHT_X +
  ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
  ASAP_BOOK_SUMMARY_SUQARE_INFO_MARGIN;
export const ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_1_Y = ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y;

export const ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_2_X = 
  ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_1_X +
  ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
  ASAP_BOOK_SUMMARY_SUQARE_INFO_MARGIN;
export const ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_2_Y = ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y;

export const ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_3_X = 
  ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_2_X +
  ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
  ASAP_BOOK_SUMMARY_SUQARE_INFO_MARGIN;
export const ASAP_BOOK_SUMARY_INFO_PLACEHOLDER_3_Y = ASAP_BOOK_SUMMARY_PROJECT_HEAD_START_Y;

// Section summary =============================================================
export const ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_FONT_SIZE = unit(60);
export const ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_FONT_STYLE = 'medium';
export const ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_START_X = unit(246);
export const ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_START_Y = unit(1585);

export const ASAP_BOOK_SECTION_SUMMARY_LEFT_MARGIN = unit(246);
export const ASAP_BOOK_SECTION_SUMMARY_RIGHT_COLUMN_X = unit(2154);
export const ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_SIZE = unit(36);
export const ASAP_BOOK_SECTION_SUMMARY_LABEL_COLOR = '#ffffff';
export const ASAP_BOOK_SECTION_SUMMARY_TEXT_LINE_BACKGROUND = '#ffffff';
export const ASAP_BOOK_SECTION_SUMMARY_TEXT_LINE_COLOR = '#58595B';

export const ASAP_BOOK_SECTION_SUMMARY_FOOTER_Y = unit(2046);
export const ASAP_BOOK_SECTION_SUMMARY_FOOTER_BOTOM_Y = unit(2152);

export const ASAP_BOOK_SECTION_SUMMARY_TITLE_FONT_SIZE = unit(120);
export const ASAP_BOOK_SECTION_SUMMARY_TITLE_X = ASAP_BOOK_SECTION_SUMMARY_LEFT_MARGIN;
export const ASAP_BOOK_SECTION_SUMMARY_TITLE_Y = unit(1692);
export const ASAP_BOOK_SECTION_SUMMARY_TITLE_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_WIDTH = unit(1872)

export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_COLOR = '#58595B';
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_FONT_SIZE = unit(40);
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_BACKGROUND = '#ffffff';
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_HEIGHT = unit(70);
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_WIDTH = unit(1236);
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_X = ASAP_BOOK_SECTION_SUMMARY_LEFT_MARGIN;
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_Y = ASAP_BOOK_SECTION_SUMMARY_FOOTER_Y;
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_PADDING_LEFT = unit(24);

export const ASAP_BOOK_SECTION_SUMMARY_LOGO_Y = unit(1325);

export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_LABEL_Y = ASAP_BOOK_SECTION_SUMMARY_FOOTER_BOTOM_Y;
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_PROJECT_FONT_SIZE = unit(36);

export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_X = unit(2154);
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_Y = ASAP_BOOK_SECTION_SUMMARY_FOOTER_Y;
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_HEIGHT = unit(70);
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_WIDTH = unit(600);
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_BACKGROUND = '#ffffff';
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_FONT_SIZE = unit(46);
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_PADDING_LEFT = unit(26);
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_COLOR = '#58595B';

export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_X = unit(2154);
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_Y = ASAP_BOOK_SECTION_SUMMARY_FOOTER_BOTOM_Y;
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_FONT_STYLE = ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_STYLE;
export const ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_FONT_SIZE = ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_SIZE;

export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_X = unit(2790);
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_Y = ASAP_BOOK_SECTION_SUMMARY_FOOTER_Y;
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_HEIGHT = unit(70);
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_WIDTH = unit(600);
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_BACKGROUND = '#ffffff';
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_FONT_STYLE = ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_STYLE;
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_FONT_SIZE = unit(46);
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_PADDING_LEFT = unit(26);
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_COLOR = '#58595B';

export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_X = unit(2790);
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_Y = ASAP_BOOK_SECTION_SUMMARY_FOOTER_BOTOM_Y;
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_FONT_STYLE = ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_STYLE;
export const ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_FONT_SIZE = ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_SIZE;

export const ASAP_BOOK_SECTION_SUMMARY_CHILD_GAP = unit(36);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LABEL_GAP = unit(39);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_BOTTOM_Y = unit(1798 + 70);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_NAME_WIDTH = unit(812);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_FONT_SIZE = unit(40);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_PADDING_LEFT = unit(26)
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_S_X = unit(3107.99);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_M_X = unit(3213.98);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_L_X = unit(3319.98);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_BACKGROUND = '#E6E7E8';
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_ACTIVE = '#58595B';
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_INACTIVE = '#ffffff';
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_SIZE = unit(70);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_SIZE = unit(47);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_STYLE = 'bold';
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_CHECK_PADDING = unit(13);
export const ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_CHECK_X = unit(3001.99);

export const ASAP_BOOK_SECTION_SUMMARY_SQUARE_COL_X = unit(3002);

export const ASAP_BOOK_SUMMARY_PADDING_VERTICAL = unit(18);

export const ASAP_BOOK_PAGE_TITLE_FONT_SIZE = unit(80);
export const ASAP_BOOK_PAGE_TITLE_FONT_STYLE = 'bold';
export const ASAP_BOOK_PAGE_TITLE_COLOR = '#58595B';
export const ASAP_BOOK_PAGE_TITLE_X = unit(288);
export const ASAP_BOOK_PAGE_TITLE_Y = unit(244);


// Recommendation --------------------------------------------------------------
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_Y = unit(1236);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_X = unit(2148);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_BACKGROUND = '#E6E7E8';
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_HEIGHT = unit(972);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_WIDTH = unit(1260);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_Y = 
  ASAP_BOOK_SUMMARY_RECOMMENDATION_Y + unit(22);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_X = 
  ASAP_BOOK_SUMMARY_RECOMMENDATION_X + 
  ASAP_BOOK_SUMMARY_PADDING_VERTICAL;
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_COLOR = '#005CA9';
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_FONT_SIZE = unit(48);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_FONT_STYLE = 'bold';
export const ASAP_BOOK_SUMMARY_RECOMENDATION_CONTENT_COLOR = '#58595B';
export const ASAP_BOOK_SUMMARY_RECOMENDATION_CONTENT_FONT_SIZE = unit(30);
export const ASAP_BOOK_SUMMARY_RECOMENDATION_CONTENT_FONT_STYLE = 'medium';
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_X = 
  ASAP_BOOK_SUMMARY_RECOMMENDATION_X + 
  ASAP_BOOK_SUMMARY_PADDING_VERTICAL;
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_Y = 
  ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_Y + 
  ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_FONT_SIZE +
  unit(36);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_WIDTH =
  ASAP_BOOK_SUMMARY_RECOMMENDATION_WIDTH - 
  (2 * ASAP_BOOK_SUMMARY_PADDING_VERTICAL);
export const ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_HEIGHT = unit(831);

// Resume ----------------------------------------------------------------------
export const ASAP_BOOK_SUMMARY_RESUME_X = unit(2172);

export const ASAP_BOOK_SUMMARY_RESUME_TITLE_COLOR = '#58595B';
export const ASAP_BOOK_SUMMARY_RESUME_TITLE_FONT_SIZE = unit(48);
export const ASAP_BOOK_SUMMARY_RESUME_TITLE_FONT_STYLE = 'bold';
export const ASAP_BOOK_SUMMARY_RESUME_TITLE_X = ASAP_BOOK_SUMMARY_RESUME_X;
export const ASAP_BOOK_SUMMARY_RESUME_TITLE_Y = unit(362);

export const ASAP_BOOK_SUMMARY_RESUME_TEXT_COLOR = '#58595B';
export const ASAP_BOOK_SUMMARY_RESUME_TEXT_FONT_SIZE = unit(30);
export const ASAP_BOOK_SUMMARY_RESUME_TEXT_FONT_STYLE = 'medium';
export const ASAP_BOOK_SUMMARY_RESUME_TEXT_X = ASAP_BOOK_SUMMARY_RESUME_X;
export const ASAP_BOOK_SUMMARY_RESUME_TEXT_Y =
  ASAP_BOOK_SUMMARY_RESUME_TITLE_Y +
  ASAP_BOOK_SUMMARY_RESUME_TITLE_FONT_SIZE +
  unit(36);
export const ASAP_BOOK_SUMMARY_RESUME_TEXT_WIDTH = unit(1200);

// Summary Path ----------------------------------------------------------------
export const ASAP_BOOK_SUMMARY_PATH_X = unit(258);
export const ASAP_BOOK_SUMMARY_PATH_Y = unit(2459);
export const ASAP_BOOK_SUMMARY_PATH_COLOR = '#58595B';
export const ASAP_BOOK_SUMMARY_PATH_FONT_STYLE = 'light';
export const ASAP_BOOK_SUMMARY_PATH_FONT_SIZE = unit(24);

// Summary chart ---------------------------------------------------------------
export const ASAP_BOOK_SUMMARY_CHART_AXE_COLOR = '#CCCCCC';
export const ASAP_BOOK_SUMMARY_CHART_AXE_WIDTH = unit(1.33);
export const ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_WIDTH = unit(1.33);
export const ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_COUNT = 5;
export const ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_X = unit(289.78 + 216);
export const ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_Y =  unit(545.09);
export const ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_RADIUS = unit(1221.57) / 2;
export const ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_GAP = unit(55.48);
export const ASAP_BOOK_SUMMARY_CHART_WEIGHT_COLOR = '#58595B';

export const ASAP_BOOK_SUMMARY_CHART_LABEL_FONT_SIZE = unit(30);
export const ASAP_BOOK_SUMMARY_CHART_LABEL_FONT_STYLE = 'bold';
export const ASAP_BOOK_SUMMARY_CHART_LABEL_COLOR = '#4D4D4D';
export const ASAP_BOOK_SUMMARY_CHART_LABEL_WIDTH = unit(270);
export const ASAP_BOOK_SUMMARY_CHART_LABEL_HEIGHT = unit(60);

export const ASAP_BOOK_SUMMARY_CHART_LEGEND_FONT_SIZE = unit(20);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_FONT_STYLE = 'medium';
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_COLOR = '#58595B';
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_FONT_SIZE = unit(24);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_HEIGHT = unit(29);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_FONT_STYLE = 'semibold';
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_COLOR = '#58595B';
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_X = unit(100);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_WIDTH = unit(270);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_X = unit(22 + 29);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_ROW_X = unit(288);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_ROW_Y = unit(1960);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_GAP = unit(40);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_LENGTH = unit(90);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE = unit(10);
export const ASAP_BOOK_SUMMARY_CHART_LEGEND_ARROWS_GAP = unit(10);

// Small triangle icon ---------------------------------------------------------
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE = unit(24);
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_HEIGHT = unit(12);
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_MEDIUM_HEIGHT = unit(36);

export const ASAP_BOOK_ICON_SMALL_TRIANGLE_SMALL_BASE = unit(28.13);
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_SMALL_HEIGHT = unit(24.17);
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_LARGE_BASE = unit(40);
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_LAGRE_HEIGHT = unit(47.81);
export const ASAP_BOOK_ICON_SMALL_TRIANGLE_COLOR_DEFAULT = '#4D4D4D';


// Page content starts
export const ASAP_BOOK_CONTENT_START_X = unit(288);
export const ASAP_BOOK_CONTENT_START_Y = unit(244);


// Contexts --------------------------------------------------------------------
export const ASAP_BOOK_CONTEXT_START_Y = unit(340);
export const ASAP_BOOK_CONTEXT_WIDTH = unit(3120);
export const ASAP_BOOK_CONTEXT_HEIGHT = unit(600);
export const ASAP_BOOK_CONTEXT_SEP_COLOR = '#707070';
export const ASAP_BOOK_CONTEXT_SEP_SIZE = unit(2);
export const ASAP_BOOK_CONTEXT_INDICATOR_LENGTH = unit(1560);
export const ASAP_BOOK_CONTEXT_BIG_GAP = unit(84);
export const ASAP_BOOK_CONTEXT_COMMENT_LENGTH = unit(1260);
export const ASAP_BOOK_CONTEXT_COMMENT_HEIGHT = unit(210);
export const ASAP_BOOK_CONTEXT_COMMENT_FONT_STYLE = 'medium';
export const ASAP_BOOK_CONTEXT_COMMENT_FONT_SIZE = unit(24);
export const ASAP_BOOK_CONTEXT_TITLE_FONT_SIZE = unit(48);
export const ASAP_BOOK_CONTEXT_TITLE_FONT_STYLE = 'bold';
export const ASAP_BOOK_CONTEXT_TITLE_COLOR = '#58595B';
export const ASAP_BOOK_CONTEXT_INDICATOR_UNIT_CENTER_X = unit(501);
export const ASAP_BOOK_CONTEXT_INDICATOR_VALUE_RIGHT_X = unit(851);
export const ASAP_BOOK_CONTEXT_INDICATOR_SOURCE_LEFT_X = unit(1024);
export const ASAP_BOOK_CONTEXT_INDICATOR_LABEL_FONT_SIZE = unit(24);
export const ASAP_BOOK_CONTEXT_INDICATOR_LABEL_FONT_STYLE = 'bold';
export const ASAP_BOOK_CONTEXT_INDICATOR_LINE_HEIGHT = unit(72);
export const ASAP_BOOK_CONTEXT_INDICATOR_VALUE_FONT_SIZE = 
    ASAP_BOOK_CONTEXT_COMMENT_FONT_SIZE;
export const ASAP_BOOK_CONTEXT_INDICATOR_VALUE_FONT_STYLE = 'medium';
export const ASAP_BOOK_CONTEXT_INDICATOR_SEC_VALUE_FONT_SIZE = unit(18);
export const ASAP_BOOK_CONTEXT_INDICATOR_SEC_VALUE_FONT_STYLE = 'medium';
export const ASAP_BOOK_CONTEXT_INDICATOR_LINE_SIZE = unit(1);
export const ASAP_BOOK_CONTEXT_INDICATOR_LINE_COLOR = '#707070';

// Detail no children ----------------------------------------------------------
export const DOC_DNOCH_CONTEXT_START_Y = unit(367);
export const DOC_DNOCH_CONTEXT_GAP_Y = unit(36);
export const DOC_DNOCH_CONTEXT_HEIGHT = unit(573);
export const DOC_DNOCH_CONTEXT_WIDTH = unit(1776);
export const DOC_DNOCH_CONTEXT_LINE_HEIHGT = unit(2);
export const DOC_DNOCH_CONTEXT_SIDEBAR_CENTER_REL_X = unit(90);
// Put on mmiddle baseline!!!
export const DOC_DNOCH_CONTEXT_TENDECY_REL_START_Y = unit(180 + 36);
export const DOC_DNOCH_CONTEXT_SCORE_REL_START_Y = 
    DOC_DNOCH_CONTEXT_TENDECY_REL_START_Y +
    unit(18 + 95 + 18);
export const DOC_DNOCH_CONTEXT_SCORE_VAL_REL_START_Y = 
    DOC_DNOCH_CONTEXT_SCORE_REL_START_Y +
    unit(18 + 9 + 21);
export const DOC_DNOCH_CONTEXT_WEIGHT_REL_START_Y = 
    DOC_DNOCH_CONTEXT_SCORE_REL_START_Y +
    unit(18 + 87 + 18);
export const DOC_DNOCH_CONTEXT_WEIGHT_VAL_REL_START_Y = 
    DOC_DNOCH_CONTEXT_WEIGHT_REL_START_Y +
    unit(18 + 8 + 21);
export const DOC_DNOCH_CONTEXT_TRIANGE_REL_CENTER_Y = 
    DOC_DNOCH_CONTEXT_TENDECY_REL_START_Y +
    unit(18 + 16 + 20);
export const DOC_DNOCH_CONTEXT_INFO_VAL_FONT_SIZE = unit(36);
export const DOC_DNOCH_CONTEXT_COMMNT_REL_START_X = unit(180 + 36);
export const DOC_DNOCH_CONTEXT_COMMENT_FONT_STYLE = 'medium';
export const DOC_DNOCH_CONTEXT_COMMENT_FONT_SIZE = unit(24);
export const DOC_DNOCH_CONTEXT_COMMENT_COLOR = '#58595B';
export const DOC_DNOCH_CONTEXT_COMMENT_LINE_HEIGHT = unit(29);

export const DOC_DNOCH_CONTEXT_IND_PADDING = unit(9);
export const DOC_DNOCH_CONTEXT_IND_HEIGHT = unit(18 + 36);
export const DOC_DNOCH_CONTEXT_IND_REL_BASELINE = DOC_DNOCH_CONTEXT_IND_HEIGHT / 2;
export const DOC_DNOCH_CONTEXT_IND_FONT_SIZE = unit(30);
export const DOC_DNOCH_CONTEXT_IND_FONT_STYLE = 'medium';
export const DOC_DNOCH_CONTEXT_IND_FONT_COLOR = '#58595B';
export const DOC_DNOCH_CONTEXT_IND_UNIT_REL_START_X = unit(591);
export const DOC_DNOCH_CONTEXT_IND_SEP_COLOR = '#707070';
export const DOC_DNOCH_CONTEXT_IND_SEP_HEIGHT = unit(2);
export const DOC_DNOCH_CONTEXT_IND_VAL_REL_START_X = unit(732);

export const DOC_DNOCH_CONTEXT_IND_INFO_REL_START_X = unit(869);
export const DOC_DNOCH_CONTEXT_IND_INFO_FONT_SIZE = unit(22);
export const DOC_DNOCH_CONTEXT_IND_INFO_FONT_STYLE = 'medium';
export const DOC_DNOCH_CONTEXT_IND_INFO_FONT_COLOR = '#58595B';
export const DOC_DNOCH_CONTEXT_IND_INFO_TOP_REL_BASELINE = DOC_DNOCH_CONTEXT_IND_HEIGHT * 0.25;
export const DOC_DNOCH_CONTEXT_IND_INFO_BOTTOM_REL_BASELINE = DOC_DNOCH_CONTEXT_IND_HEIGHT * 0.75;
export const DOC_DNOCH_CONTEXT_IND_HEADER_FONT_SIZE = unit(24);
export const DOC_DNOCH_CONTEXT_IND_HEADER_TITLE_FONT_SIZE = unit(48);
export const DOC_DNOCH_CONTEXT_IND_HEADER_FONT_STYLE = 'bold';
export const DOC_DNOCH_CONTEXT_IND_HEADER_HEIGHT = unit(57);
export const DOC_DNOCH_CONTEXT_IND_HEADER_BOTTOM_MARGIN = unit(37);
export const DOC_DNOCH_CONTEXT_PARAM_TABLE_REL_START_X = unit(1860);

export const DOC_IND_TABLE_COL_NAME_X = unit(0);
export const DOC_IND_TABLE_COL_UNITS_X = 
    DOC_IND_TABLE_COL_NAME_X + 
    unit(585);
export const DOC_IND_TABLE_COL_VALUE_X =
    DOC_IND_TABLE_COL_UNITS_X +
    unit(130);
export const DOC_IND_TABLE_COL_SOURCES_X =
    DOC_IND_TABLE_COL_VALUE_X +
    unit(130);

export const DOC_PARAM_TABLE_TEXT_COLOR = '#58595B';
export const DOC_PARAM_TABLE_TEXT_FONT_SIZE = unit(24);
export const DOC_PARAM_TABLE_TEXT_FONT_STYLE = 'light';
export const DOC_PARAM_TABLE_HEADER_FONT_STYLE = 'bold';
export const DOC_PARAM_TABLE_HEADER_FONT_SIZE = unit(24);
export const DOC_PARAM_TABLE_TITLE_FONT_STYLE = unit(48);
export const DOC_PARAM_TABLE_ROW_HEIGHT = unit(36);
export const DOC_PARAM_TABLE_WIDTH = unit(1260);
export const DOC_PARAM_COL_SCORE_X = unit(550);
export const DOC_PARAM_COL_TENDENCY_X = 
    DOC_PARAM_COL_SCORE_X +
    unit(230);
export const DOC_PARAM_COL_WEIGHT_X = 
    DOC_PARAM_COL_TENDENCY_X + 
    unit(230);

export const DOC_CHART_TENDENCY_LINE_HEIGHT = unit(12);


