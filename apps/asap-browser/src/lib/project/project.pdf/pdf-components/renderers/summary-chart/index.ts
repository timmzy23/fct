import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';
import { PageData } from '../../types';
import { TriangleIcon } from '../icons';
import {
  getSectionColor,
  getSectionLightColor
  } from '../utils';
import { ChartLegendRow } from './chart-legend-row';
import { ScoreLabel } from './score-label';

export declare namespace SummaryChart {
  interface Props {
    pageData: PageData;
    childParams: PageData[];
  }
}

export function renderSummaryChart(pdf: Pdf, props: SummaryChart.Props) {
  const circleGap = pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_GAP;
  const circleRadius = pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_RADIUS;
  const getValueRadius = getValueRadiusGetter(circleGap, circleRadius);
  const smallCircleRadius = circleGap * 2.5;
  // Circle center:
  const circleCenter = {
    x: 
      pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_X + 
      pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_RADIUS,
    y:
      pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_Y +
      pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_RADIUS
  };

  // Param scores background - positionned bellow all axes to reach
  // transparent effect.
  const filledPolygonPath = props.childParams.map((child, ix) => 
  Geometry2D.circle.getPosition(
    getValueRadius(child.score),
    ((2 * Math.PI) / props.childParams.length * ix),
    circleCenter));

  pdf.polygon({
    dimensions: {
      absolutePath: filledPolygonPath
    },
    styles: {
      background: { 
        color: getSectionLightColor(props.pageData.section.id, pdf.theme) 
      }
    }
  });
  if (filledPolygonPath.length === 2) {
    const svgHeight = circleGap;
    const svgWidth = Geometry2D.line.getLength({
      start: filledPolygonPath[0],
      end: filledPolygonPath[1]
    });

    /*
    pdf.svg({
      dimensions: {
        height: svgHeight,
        width: svgWidth,
        x: Math.min(filledPolygonPath[0].x, filledPolygonPath[1].x),
        y: Math.min(filledPolygonPath[0].y, filledPolygonPath[1].y)
      },
      svg: ``
    })
    */

  }

  // Circles
  for (let i = 0; i < pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_COUNT; i++) {
    pdf.circle({
      borderColor: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_COLOR,
      radius: circleRadius - (circleGap * i),
      start: {
        x: pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_X + (i * circleGap),
        y: pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_Y + (i * circleGap)
      },
      borderSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_WIDTH
    });
  }
  // Small circles
  for (let i = 0; i < pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_COUNT; i++) {
    const frag = smallCircleRadius / pdf.theme.ASAP_BOOK_SUMMARY_CHART_SCALE_CIRCLE_COUNT;
    const radius = smallCircleRadius - (frag * i);
    
    pdf.circle({
      borderColor: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_COLOR,
      radius: radius,
      center: circleCenter,
      borderSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_WIDTH
    });
  }

  // Axes
  for (let i = 0; i < props.childParams.length; i++) {
    pdf.line({
      dimensions: { 
        end: Geometry2D.circle.getPosition(
          circleRadius,
          ((2 * Math.PI) / props.childParams.length * i),
          circleCenter),
        start: circleCenter,
      },
      styles: {
        color: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_COLOR,
        size: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_WIDTH
      }
    });
  }

  // Constant circle to mark middle value axe.
  pdf.circle({
    borderColor: pdf.theme.ASAP_BOOK_SUMMARY_CHART_WEIGHT_COLOR,
    radius: getValueRadius(3),
    center: circleCenter,
    borderSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_AXE_WIDTH
  });

  // Param scores background - positionned bellow all axes to reach
  // transparent effect.
  pdf.polygon({
    dimensions: {
      absolutePath: props.childParams.map((child, ix) => Geometry2D.circle.getPosition(
        getValueRadius(child.score),
        ((2 * Math.PI) / props.childParams.length * ix),
        circleCenter))
    },
    styles: {
      border: {
        borderColor: getSectionColor(props.pageData.section.id, pdf.theme),
        borderSize: 6.66 / 12,
      }
    }
  });

  pdf.polygon({
    styles: {
      border: {
        borderColor: getSectionColor(props.pageData.section.id, pdf.theme),
        borderSize: pdf.theme.DOC_CHART_TENDENCY_LINE_HEIGHT,
        borderDashSize: pdf.theme.DOC_CHART_TENDENCY_LINE_HEIGHT,
        borderDashGapSize: pdf.theme.DOC_CHART_TENDENCY_LINE_HEIGHT      
      },
    },
    dimensions: {
      absolutePath: props.childParams.map((child, ix) => Geometry2D.circle.getPosition(
        getValueRadius(child.tendency),
        ((2 * Math.PI) / props.childParams.length * ix),
        circleCenter))
    }
  });

  // Param weight
  for (let i = 0; i < props.childParams.length; i++) {
    const child = props.childParams[i];
    if (child.weight === 0) {
      console.warn(`Document ${child.contentId} is missing weight!!!`);
      continue;
    }
    const radiusLength = child.weight * (smallCircleRadius / 5);
    const size = 7.89 / 12;
    const lineEnd = Geometry2D.circle.getPosition(
      radiusLength,
      ((2 * Math.PI) / props.childParams.length * i),
      circleCenter);
    const lineStyle = {
      color: '#58595B',
      size: size,
    };
    const headLineLength = size * 4;
    const baseLine = {
      end: lineEnd,
      start: circleCenter,
    };
    const baseAngle = Geometry2D.line.getAngle(baseLine);
    const headLine = {
      start: Geometry2D.circle.getPosition(
        headLineLength / 2,
        circleCenter.y < lineEnd.y ? 
            Geometry2D.angeToRad(-90) - baseAngle :
            Geometry2D.angeToRad(-90) + baseAngle, // 90 degrees
        lineEnd
      ),
      end: Geometry2D.circle.getPosition(
        headLineLength / 2,
        circleCenter.y < lineEnd.y ? 
            Geometry2D.angeToRad(90) - baseAngle :
            Geometry2D.angeToRad(90) + baseAngle,
        lineEnd
      )
    }

    pdf.line({
      dimensions: baseLine,
      styles: lineStyle
    });

    pdf.line({
      dimensions: headLine,
      styles: lineStyle
    });
  }
  // Child score numbers
  for (let i = 0; i < props.childParams.length; i++) {
    const child = props.childParams[i];
    pdf.text({
      color: '#4D4D4D',
      fontStyle: 'bold',
      fontSize: 29 / 12,
      text: (child.score || 0).toFixed(1),
      start: Geometry2D.circle.getPosition(
          getValueRadius(Math.floor(child.score) + 1.5),
          ((2 * Math.PI) / props.childParams.length * (i - 0.045)),
          circleCenter),
      jsPdfOptions: {
        align: PdfTextAlign.CENTER,
        baseline: PdfTextBaseline.MIDDLE
      }
    });
  }
  // Paint child score indicators
  for (let i = 0; i < props.childParams.length; i++) {
    const child = props.childParams[i];

    if (child.score === 3) {
      const indicatorCenter = Geometry2D.circle.getPosition(
        getValueRadius(6),
        ((2 * Math.PI) / props.childParams.length * i),
        circleCenter);
      const baseLineStyle = {
        color: '#4D4D4D',
        size: 6.65 / 12
      };
      const baseLine = { start: circleCenter, end: indicatorCenter };
      const baseAngle = Geometry2D.line.getAngle(baseLine);
      const indicatorLineLength = baseLineStyle.size * 4;
      const indicatorLine = {
        start: Geometry2D.circle.getPosition(
          indicatorLineLength / 2,
          circleCenter.y < indicatorCenter.y ? 
              Geometry2D.angeToRad(-90) - baseAngle :
              Geometry2D.angeToRad(-90) + baseAngle, // 90 degrees
          indicatorCenter
        ),
        end: Geometry2D.circle.getPosition(
          indicatorLineLength / 2,
          circleCenter.y < indicatorCenter.y ? 
              Geometry2D.angeToRad(90) - baseAngle :
              Geometry2D.angeToRad(90) + baseAngle,
          indicatorCenter
        )
      }
     
      pdf.line({
        dimensions: indicatorLine,
        styles: baseLineStyle
      });
    } else {
      const triangleValue = child.tendency - child.score;
      const triangleHeight = triangleValue < -1 || triangleValue > 1 ?
        pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LAGRE_HEIGHT :
        pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_SMALL_HEIGHT;
      const triangleCenter = Geometry2D.circle.getPosition(
          getValueRadius(6),
          ((2 * Math.PI) / props.childParams.length * i),
          circleCenter);
      const modifier = triangleValue < 0 ? 
          (triangleHeight / 2) * -1 : // Point to chart center
          triangleHeight / 2; // Point out of chart center
      // Get point which rotate triangle to. (To or out of chart center)
      const axeIntersection = Geometry2D.circle.getPosition(
        getValueRadius(6) + modifier,
        ((2 * Math.PI) / props.childParams.length * i),
        circleCenter);
      const rotationRad = Geometry2D.triangle.getIsoscelesAngle({
        a: axeIntersection,
        b: triangleCenter,
        c: { x: triangleCenter.x, y: triangleCenter.y - (triangleHeight / 2)}
      });

      pdf.render(TriangleIcon({
        dimensions: { 
          center: triangleCenter,
          height: triangleHeight,
          base: child.score < 2 || child.score > 4 ?
            pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LAGRE_BASE :
            pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_SMALL_BASE,
        },
        styles: {
          rotation: axeIntersection.x < triangleCenter.x ? 
              360 - Geometry2D.radToAngle(rotationRad) :
              Geometry2D.radToAngle(rotationRad),
          critical: child.critical
        }
      }));
    }
  }

  // Score label
  const radiusA = getValueRadius(8);
  const radiusB = getValueRadius(7);
  for (let i = 0; i < props.childParams.length; i++) {
    const child = props.childParams[i];
    const center = Geometry2D.ellipse.getPosition(
      radiusA,
      radiusB,
      ((2 * Math.PI) / props.childParams.length * i) + 0.15,
      circleCenter
    );
    pdf.render(ScoreLabel({
      dimensions: { center },
      text: child.name,
    }));
  }

  pdf.render(ChartLegendRow({ sectionId: props.pageData.section.id }));
}

function getValueRadiusGetter(circleGap: number, circleRadius: number) {
  return (value: number) => 
    (circleRadius - (5 * circleGap)) + (circleGap * value);
}
