import { ProjectDocumentCollectionRecord } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfLine,
  PdfTextAlign
  } from '../../lib';
import { ParamTableText } from './param-table-text';

export declare namespace ParamTableRow {
  interface Props {
    param: ProjectDocumentCollectionRecord;
    contextType: string;
    dimensions: {
      start: Geometry2D.Point.Dimensions;
      width: number;
    }
    columns: number[];
  }
}

export function ParamTableRow(props: ParamTableRow.Props) {
  return (pdf: Pdf) => {
    const param = props.param;
    const endX = props.dimensions.start.x + props.dimensions.width;

    pdf.render(PdfLine({
      dimensions: {
        start: props.dimensions.start,
        end: {
          x: endX,
          y: props.dimensions.start.y
        }
      },
      styles: {
        color: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_COLOR,
        size: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_HEIGHT
      }
    }));

    // Param NAME
    pdf.render(ParamTableText({
      text: param.node.name || '/',
      startX: props.dimensions.start.x,
      startY: props.dimensions.start.y,
    }));

    // Param SCORE
    const score = param.getScoreForContext(props.contextType);
    pdf.render(ParamTableText({
      text: score ? (score).toFixed(1) || '/' : '/',
      startX: props.dimensions.start.x +  props.columns[1] + ((props.columns[2] - props.columns[1]) / 2), // align to center
      startY: props.dimensions.start.y,
      textAlign: PdfTextAlign.CENTER
    }));

    // Param TENDENCY
    const tendency = param.getTendencyForContext(props.contextType);
    pdf.render(ParamTableText({
      text: tendency ? (tendency).toFixed(1) || '/' : '/',
      startX: props.dimensions.start.x +  props.columns[2] + ((props.columns[3] - props.columns[2]) / 2), // align to center
      startY: props.dimensions.start.y,
      textAlign: PdfTextAlign.CENTER
    }));

    // Param WEIGHT
    const weight = param.getWeightForContext(props.contextType);
    pdf.render(ParamTableText({
      text: weight ? (weight).toFixed(1) || '/' : '/',
      startX: props.dimensions.start.x +  props.columns[3] + ((props.dimensions.width - props.columns[3]) / 2), // align to center
      startY: props.dimensions.start.y,
      textAlign: PdfTextAlign.CENTER
    }));
  }
}
