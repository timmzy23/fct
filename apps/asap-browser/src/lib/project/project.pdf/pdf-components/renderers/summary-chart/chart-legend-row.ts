import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../../lib';
import { TriangleIcon } from '../icons';
import { getSectionColor } from '../utils';
import { ChartLegend } from './chart-legend';

function BlueLineLegendIcon(props: ChartLegend.LegendIconProps & { sectionId: number }) {

  return (pdf: Pdf) => {
    const lineY = props.dimensions.start.y + 
        (props.dimensions.height / 2) -
        (pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE /2);

    pdf.line({
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: lineY
        },
        end: {
          x: props.dimensions.start.x + pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_LENGTH,
          y: lineY
        }
      },
      styles: {
        color: getSectionColor(props.sectionId, pdf.theme),
        size: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE
      }
    });
  }
}

function BlueDottedLineLegendIcon(props: ChartLegend.LegendIconProps & { sectionId: number }) {
  return (pdf: Pdf) => {
    const lineY = props.dimensions.start.y + 
      (props.dimensions.height / 2) -
      (pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE /2);
    pdf.line({
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: lineY
        },
        end: {
          x: props.dimensions.start.x + pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_LENGTH,
          y: lineY
        }
      },
      styles: {
        color: getSectionColor(props.sectionId, pdf.theme),
        size: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE,
        dashGapSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE,
        dashSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE
      }
    });
  }
}

function GreyLineLegendIcon(props: ChartLegend.LegendIconProps & { sectionId: number }) {
  return (pdf: Pdf) => {
    const lineY = props.dimensions.start.y + 
      (props.dimensions.height / 2) -
      (pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE /2);
    const headLineLength = pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE * 4;
    const baseLine: Geometry2D.Line.Dimensions = {
      start: {
        x: props.dimensions.start.x,
        y: lineY
      },
      end: {
        x: props.dimensions.start.x + pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_LENGTH,
        y: lineY
      }
    };
    const headLine: Geometry2D.Line.Dimensions = {
      start: {
        x: baseLine.end.x,
        y: baseLine.end.y - (headLineLength / 2)
      },
      end: {
        x: baseLine.end.x,
        y: baseLine.end.y + (headLineLength / 2)
      }
    };
    const lineStyle = {
      color: '#58595B',
      size: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_SIZE,
    };

    pdf.line({ 
      dimensions: baseLine,
      styles: lineStyle
    });
    pdf.line({ 
      dimensions: headLine,
      styles: lineStyle
    });
  }
}

function ArrowLegendIcon(props: ChartLegend.LegendIconProps & { sectionId: number }) {
  return (pdf: Pdf) => {
    const lineY = props.dimensions.start.y + (props.dimensions.height / 2);
    const centerX = props.dimensions.start.x + (props.dimensions.height / 2);

    pdf.render(TriangleIcon({
      dimensions: {
        center: {
          y: lineY,
          x: centerX - ((
            pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ARROWS_GAP +
            pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE) / 2)
        },
        base: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE,
        height: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_HEIGHT
      },
      styles: {
        color: '#58595B'
      }
    }));
    pdf.render(TriangleIcon({
      dimensions: {
        center: {
          y: lineY,
          x: centerX + ((
            pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ARROWS_GAP +
            pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE) / 2)
        },
        base: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE,
        height: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_MEDIUM_HEIGHT
      },
      styles: {
        color: '#58595B'
      }
    }));
  }
}

function ImportantArrowLegendIcon(props: ChartLegend.LegendIconProps & { sectionId: number }) {
  return (pdf: Pdf) => {
    const lineY = props.dimensions.start.y + (props.dimensions.height / 2);
    const centerX = props.dimensions.start.x + 
        pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ICON_LINE_LENGTH -
        pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE

    pdf.render(TriangleIcon({
      dimensions: {
        center: {
          y: lineY,
          x: centerX
        },
        base: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_BASE,
        height: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_TINY_HEIGHT
      },
      styles: {
        color: pdf.theme.ASAP_BOOK_CRITICAL_COLOR
      }
    }));
  }
}

const legeds = [
  {
    label: 'hodnocení',
    legend: 
      'Hodnocení je číselné vyjádření deviace parametru oproti vyváženému ' +
      'stavu (hodnota 3) svého okolí či referenční lokality (S,M,L).',
    renderLegendIcon: BlueLineLegendIcon
  },
  {
    label: 'tendence',
    legend: 
      'Tendence je předpokládané hodnocení v čase, predikované na základě ' +
      'současné situace. Je stanovováno zpravidla ' +
      've střednědobém horizontu ve vztahu ke konkrétnímu parametru ' +
      '(není-li v komentáři uvedeno jinak).'
    ,
    renderLegendIcon: BlueDottedLineLegendIcon
  },
  {
    label: 'váha',
    legend: 
      'Váha paramteru udává relativní důležitost parametru ve vztahu' +
      ' konkrétního zadání ASaPu vůči ostatním parametrům stejné úrovně.',
    renderLegendIcon: GreyLineLegendIcon
  },
  {
    label: 'směr tendence',
    legend: 
      'Směr tendence je grafickým vyjádřením rozdílu hodnocení a tendence, '+
      'jedná se tedy o předpokládaný vývoj hodnoty v čase',
    renderLegendIcon: ArrowLegendIcon
  },
  /*{
    label: 'Významný parametr',
    legend: 
      'Označení významného parametru slouží k upozornění na důležitý bod' +
      ' analýzy, jež je zpravidla podrobněji rozveden v textu. Nemá vazbu na ' +
      'váhu, významný může být i proto že má váhu disproporčně nízkou.',
    renderLegendIcon: ImportantArrowLegendIcon
  },*/
];

export declare namespace ChartLegendRow {
  interface Props {
    sectionId: number;
  }
}

export function ChartLegendRow(props: ChartLegendRow.Props) {
  return (pdf: Pdf) => {
    legeds.forEach((legend, ix) => {
      const gaps = ix > 0 ? ix * pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_GAP : 0;

      pdf.render(ChartLegend({
        legend: legend.legend,
        label: legend.label,
        dimensions: {
          start: {
            y: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ROW_Y,
            x: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_ROW_X +
              (ix * pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_WIDTH) +
              gaps,
          },
        },
        renderLegendIcon: (childProps) => 
            legend.renderLegendIcon({ 
              ...childProps, sectionId: props.sectionId
            })
      }));
    });
  }
}