import {
  Pdf,
  PdfImageCompression,
  PdfTextAlign,
  PdfTextBaseline
  } from '../lib';
import { PageData } from '../types';
import { renderLogo } from './render-logo-svg';
import {
  getSectionColor,
  pageLevelToString
  } from './utils';

export function renderPageSidebar(pdf: Pdf, page: PageData) {
  pdf.fillRect({
    background: getSectionColor(page.section.id, pdf.theme),
    dimensions: {
      x: 0,
      y: 0,
      height: pdf.theme.ASAP_BOOK_SIDEBAR_HEIGHT,
      width: pdf.theme.ASAP_BOOK_SIDEBAR_WIDTH
    }
  });

  pdf.text({
    text: page.name,
    color: pdf.theme.ASAP_BOOK_SIDEBAR_TEXT_COLOR,
    computeStart: ({ length }) => ({
      x: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_TITLE_X + 
        pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_FONT_SIZE,
      y: (pdf.theme.ASAP_BOOK_SIDEBAR_HEIGHT / 2) + (length / 2)
    }),
    fontStyle: 'bold',
    fontSize: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_FONT_SIZE,
    jsPdfOptions: {
      angle: 90,
    }
  });

  if (page.parentName) {
    pdf.text({
      text: page.parentName,
      color: pdf.theme.ASAP_BOOK_SIDEBAR_TEXT_COLOR,
      computeStart: ({ length }) => ({
        x: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_SUBTITLE_X + 
          pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_SUBTITLE_FONT_SIZE,
        y: (pdf.theme.ASAP_BOOK_SIDEBAR_HEIGHT / 2) + (length / 2)
      }),
      fontStyle: 'medium',
      fontSize: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_SUBTITLE_FONT_SIZE,
      jsPdfOptions: {
        angle: 90,
      }
    });
  }

  pdf.text({
    text: pageLevelToString(page.level),
    color: pdf.theme.ASAP_BOOK_SIDEBAR_TEXT_COLOR,
    start: {
      x: (pdf.theme.ASAP_BOOK_SIDEBAR_WIDTH / 2),
      y: pdf.theme.ASAP_BOOK_SIDEBAR_LEVEL_START_Y
    },
    fontStyle: pdf.theme.ASAP_BOOK_SIDEBAR_LEVEL_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SIDEBAR_LEVEL_FONT_SIZE,
    jsPdfOptions: {
      align: PdfTextAlign.CENTER,
      baseline: PdfTextBaseline.TOP
    }
  });

  pdf.text({
    text: page.contentId,
    start: {
      x: (pdf.theme.ASAP_BOOK_SIDEBAR_WIDTH / 2),
      y: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_ID_START_Y
    },
    color: pdf.theme.ASAP_BOOK_SIDEBAR_TEXT_COLOR,
    fontStyle: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_ID_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SIDEBAR_PARAM_ID_FONT_SIZE,
    jsPdfOptions: {
      align: PdfTextAlign.CENTER,
      baseline: PdfTextBaseline.TOP
    }
  });

  pdf.svg({
    renderSvg: ({ canvasWidth, canvasHeigh }) => renderLogo({
      width: canvasWidth,
      height: canvasHeigh,
      color: getSectionColor(page.section.id, pdf.theme)
    }),
    dimensions: {
      height: pdf.theme.ASAP_BOOK_SIDEBAR_LOGO_HEIGHT,
      width: pdf.theme.ASAP_BOOK_SIDEBAR_LOGO_WIDTH,
      x: (pdf.theme.ASAP_BOOK_SIDEBAR_WIDTH - pdf.theme.ASAP_BOOK_SIDEBAR_LOGO_WIDTH) / 2,
      y: pdf.theme.ASAP_BOOK_SIDEBAR_LOGO_START_Y
    },
    alias: 'book-logo',
    compression: PdfImageCompression.NONE,
    backgroundColor: getSectionColor(page.section.id, pdf.theme)
  });
}
