import {
  Pdf,
  PdfRectShapeProps
  } from '../lib';
import { PageData } from '../types';

export interface ParamChartProps {
  /**
   * Resolution: pixels per unit
   */
  ppu: number;

  dimensions: PdfRectShapeProps;

  node: PageData;

  nodeList: PageData[];
}

export function renderParamChart(pdf: Pdf, props: ParamChartProps) {
  const canvas = document.createElement('canvas');
    canvas.width = pdf.computeWidth(props.dimensions.width) * props.ppu;
    canvas.height = pdf.computeHeight(props.dimensions.height) * props.ppu;
  const ctx = canvas.getContext('2d');

  pdf.canvas({
    canvas,
    dimensions: props.dimensions
  });
}
