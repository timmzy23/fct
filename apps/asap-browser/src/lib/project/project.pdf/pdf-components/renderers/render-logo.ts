import {
  Pdf,
  PdfImageCompression,
  PdfRectShapeProps
  } from '../lib';
import { PageData } from '../types';
import { renderLogo } from './render-logo-svg';
import { getSectionColor } from './utils';

export interface AsapLogoProps {
  currentPage: PageData;
  dimensions?: Partial<PdfRectShapeProps>;
}

export function renderAsapLogo(pdf: Pdf, props: AsapLogoProps) {
  pdf.svg({
    renderSvg: ({ canvasWidth, canvasHeigh }) => renderLogo({
      width: canvasWidth,
      height: canvasHeigh,
      color: getSectionColor(props.currentPage.section.id, pdf.theme)
    }),
    dimensions: {
      height: pdf.theme.ASAP_BOOK_SIDEBAR_LOGO_HEIGHT,
      width: pdf.theme.ASAP_BOOK_SIDEBAR_LOGO_WIDTH,
      x: 0, y: 0,
      ...(props.dimensions || {})
    },
    compression: PdfImageCompression.NONE,
    backgroundColor: getSectionColor(props.currentPage.section.id, pdf.theme)
  });
}