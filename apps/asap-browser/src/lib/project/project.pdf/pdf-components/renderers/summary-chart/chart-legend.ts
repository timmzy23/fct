import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfTextBaseline
  } from '../../lib';

export declare namespace ChartLegend {

  interface LegendIconProps {
    dimensions: {
      start: Geometry2D.Point.Dimensions,
      width: number;
      height: number;
    }
  }

  interface Props {
    label: string;
    legend?: string;
    dimensions: {
      start: Geometry2D.Point.Dimensions
    },
    renderLegendIcon: (props: LegendIconProps) => (pdf: Pdf) => void;
  }
}

export function ChartLegend(props: ChartLegend.Props) {
  return (pdf: Pdf) => {
    pdf.render(props.renderLegendIcon({
      dimensions: {
        start: props.dimensions.start,
        width: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_WIDTH,
        height: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_HEIGHT
      }
    }))

    pdf.text({
      color: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_COLOR,
      text: props.label,
      fontSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_FONT_STYLE,
      jsPdfOptions: {
        maxWidth: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_WIDTH,
        baseline: PdfTextBaseline.TOP
      },
      start: {
        x: props.dimensions.start.x + pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_LABEL_X,
        y: props.dimensions.start.y
      }
    });

    // Legend
    if (props.legend) {
      pdf.text({
        color: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_COLOR,
        text: props.legend,
        fontSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_FONT_SIZE,
        fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_FONT_STYLE,
        jsPdfOptions: {
          maxWidth: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_WIDTH,
          baseline: PdfTextBaseline.TOP
        },
        start: {
          x: props.dimensions.start.x,
          y: props.dimensions.start.y + pdf.theme.ASAP_BOOK_SUMMARY_CHART_LEGEND_X
        }
      });
    }
  }
}
