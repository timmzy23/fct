import {
  Pdf,
  PdfTextAlign,
  PdfTextBaseline
  } from '../lib';

export declare namespace SummaryResume {
  interface Props {
    text?: string;
    shiftY?: number;
  }
}

export function renderSummaryResume(pdf: Pdf, props: SummaryResume.Props) {
  pdf.text({
    text: 'Shrnutí'.toUpperCase(),
    color: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TITLE_COLOR,
    start: {
      x: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TITLE_X,
      y: props.shiftY ?
        pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TITLE_Y + props.shiftY :
        pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TITLE_Y
    },
    fontSize: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TITLE_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TITLE_FONT_STYLE,
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP
    }
  });

  if (props.text) {
    pdf.text({
      text: props.text,
      color: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_COLOR,
      start: {
        x: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_X,
        y: props.shiftY ? 
          pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_Y + props.shiftY :
          pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_Y
      },
      fontSize: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_FONT_STYLE,
      jsPdfOptions: {
        baseline: PdfTextBaseline.TOP,
        maxWidth: pdf.theme.ASAP_BOOK_SUMMARY_RESUME_TEXT_WIDTH,
        align: PdfTextAlign.JUSTIFY
      }
    });
  }
}