import {
  Pdf,
  PdfTextAlign,
  PdfTextBaseline
  } from '../lib';
import { getSectionColor } from './utils';

export declare namespace SummaryRecommendation {
  interface Props {
    text?: string;
    sectionId: number;
    shiftY?: number;
  }
}

export function renderSummaryRecommendation(pdf: Pdf, props: SummaryRecommendation.Props) {
  pdf.fillRect({
    dimensions: {
      width: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_WIDTH,
      height: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_HEIGHT,
      x: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_X,
      y: props.shiftY ? 
        pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_Y + props.shiftY :
        pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_Y
    },
    background: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_BACKGROUND
  });

  pdf.text({
    text: 'Doporučení'.toUpperCase(),
    color: getSectionColor(props.sectionId, pdf.theme),
    start: {
      x: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_X,
      y: props.shiftY ? 
        pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_Y + props.shiftY :
        pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_Y
    },
    fontSize: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_TITLE_FONT_STYLE,
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP
    }
  });

  if (props.text) {
    pdf.text({
      text: props.text,
      color: pdf.theme.ASAP_BOOK_SUMMARY_RECOMENDATION_CONTENT_COLOR,
      start: {
        x: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_X,
        y: props.shiftY ? 
          pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_Y + props.shiftY :
          pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_Y
      },
      fontSize: pdf.theme.ASAP_BOOK_SUMMARY_RECOMENDATION_CONTENT_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_RECOMENDATION_CONTENT_FONT_STYLE,
      jsPdfOptions: {
        baseline: PdfTextBaseline.TOP,
        maxWidth: pdf.theme.ASAP_BOOK_SUMMARY_RECOMMENDATION_CONTENT_WIDTH,
        align: PdfTextAlign.JUSTIFY
      }
    });
  }
}