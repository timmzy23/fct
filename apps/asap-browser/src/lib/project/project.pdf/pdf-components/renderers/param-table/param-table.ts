import { ProjectDocumentCollectionRecord } from '@fct/asap-interfaces';
import { Pdf } from '../../lib';
import { ParamTableHeader } from './param-table-header';
import { ParamTableRow } from './param-table-row';

export declare namespace ParamTable {
  interface Props {
    params: ProjectDocumentCollectionRecord[];
    startX: number;
    startY: number;
    width: number;
    contextType: string;
    headless?: boolean;
    maxCount?: number;
  }
}

export function ParamTable(props: ParamTable.Props) {

  return (pdf: Pdf) => {
    const columsStartX = [
      props.startX,
      pdf.theme.DOC_PARAM_COL_SCORE_X,
      pdf.theme.DOC_PARAM_COL_TENDENCY_X,
      pdf.theme.DOC_PARAM_COL_WEIGHT_X,
    ];

    if (!props.headless) {
      pdf.render(ParamTableHeader({
        baseline: pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_HEIGHT / 2,
        startX: props.startX,
        startY: props.startY,
        columns: columsStartX,
        width: props.width
      }));
    }

    const tableBodyStartY = !props.headless ?
        props.startY +
        pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_HEIGHT + 
        pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_BOTTOM_MARGIN :
        props.startY;

    props.params.forEach((param, ix) => {
      pdf.render(ParamTableRow({
        columns: columsStartX,
        contextType: props.contextType,
        dimensions: {
          width: props.width,
          start: {
            x: props.startX,
            y: tableBodyStartY +
              (pdf.theme.DOC_PARAM_TABLE_ROW_HEIGHT * ix)
          }
        },
        param
      }));
    });
  }
}
