import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfText,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';

export declare namespace PagePath {
  interface Props {
    path: string[],
    dimensions: {
      start: Geometry2D.Point.Dimensions;
    },
    shiftY: number;
  }
}

export function PagePath(props: PagePath.Props) {
  return (pdf: Pdf) => {
    pdf.render(PdfText({
      text: `ASaP - ${props.path.join(' - ')}`,
      dimensions: {
        start: props.dimensions.start,
        shiftY: props.shiftY
      },
      styles: {
        text: {
          color: pdf.theme.ASAP_BOOK_SUMMARY_PATH_COLOR,
          fontSize: pdf.theme.ASAP_BOOK_SUMMARY_PATH_FONT_SIZE,
          fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_PATH_FONT_STYLE,
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.TOP,
        align: PdfTextAlign.JUSTIFY
      },
    }))
  }
}