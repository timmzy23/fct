import {
  Pdf,
  PdfTextAlign
  } from '../../lib';
import {
  IndicatorTableText,
  IndicatorTableTextHeader,
  IndicatorTableTextTitle
  } from './indicator-table-text';

export declare namespace IndicatorTableHeader {
  interface Props {
    startX: number;
    startY: number;
    width: number;
    baseline: number;
    columns: number[];
  }
}

export function IndicatorTableHeader(props: IndicatorTableHeader.Props) {
  return (pdf: Pdf) => {
    const endX = props.startX + props.width;

    pdf.render(IndicatorTableTextTitle({
      text: 'INDIKÁTOR',
      startX: props.startX,
      startY: props.startY,
      baseline: props.baseline
    }));

    pdf.render(IndicatorTableTextHeader({
      text: 'jednotka',
      startX: props.startX + props.columns[2], // align to the end
      startY: props.startY,
      baseline: props.baseline,
      textAlign: PdfTextAlign.RIGHT
    }));

    pdf.render(IndicatorTableTextHeader({
      text: 'hodnota',
      startX: props.startX + props.columns[3], // align to the end
      startY: props.startY,
      baseline: props.baseline,
      textAlign: PdfTextAlign.RIGHT
    }));

    pdf.render(IndicatorTableTextHeader({
      text: 'zdroj a dostupnost',
      startX: endX,
      startY: props.startY,
      baseline: props.baseline,
      textAlign: PdfTextAlign.RIGHT
    }));
  }
}
