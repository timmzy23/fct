import { Pdf } from '../lib';

export interface PageRidgeProps {
  /**
   * Circle colors
   */
  color?: string;

  startY: number;
}
/**
 * Render paper ridge with sorter markers
 */
export function renderPageRidge(pdf: Pdf, props: PageRidgeProps) {
  pdf.theme.ASAP_BOOK_SORT_MARKER_START_X.forEach((coordX: number) => {
    pdf.circle({
      start: {
        x: coordX,
        y: props.startY,
      },
      borderColor: props.color || pdf.theme.ASAP_BOOK_SORT_MARKER_BORDER_COLOR,
      borderSize: pdf.theme.ASAP_BOOK_SORT_MARKER_BORDER_SIZE,
      radius: pdf.theme.ASAP_BOOK_SORT_MARKER_RADIUS,
    });
  });
}