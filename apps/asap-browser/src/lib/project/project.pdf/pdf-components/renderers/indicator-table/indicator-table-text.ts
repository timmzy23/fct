import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfText,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';

export declare namespace IndicatorTableText {
  interface Props {
    text?: string;
    startX: number;
    startY: number;
    fontSize?: number;
    fontStyle?: number;
    baseline?: number;
    textAlign?: PdfTextAlign;
  }
}

export function IndicatorTableText(props: IndicatorTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(PdfText({
      text: props.text || '',
      dimensions: {
        start: {
          x: props.startX,
          y: props.startY + (props.baseline || pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE)
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_COLOR,
          fontStyle: props.fontStyle || pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_STYLE,
          fontSize: props.fontSize || pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE,
        align: props.textAlign || PdfTextAlign.LEFT
      }
    }));
  }
}

export function IndicatorTableTextSmall(props: IndicatorTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(IndicatorTableText({
      fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_SIZE,
      ...props
    }));
  }
}

export function IndicatorTableTextHeader(props: IndicatorTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(IndicatorTableText({
      fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_FONT_SIZE,
      fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_FONT_STYLE,
      ...props
    }));
  }
}

export function IndicatorTableTextTitle(props: IndicatorTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(IndicatorTableTextHeader({
      fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_TITLE_FONT_SIZE,
      ...props
    }));
  }
}