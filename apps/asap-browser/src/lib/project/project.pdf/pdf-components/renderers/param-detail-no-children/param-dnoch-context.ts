import { ProjectDocumentCollectionRecord } from '@fct/asap-interfaces';
import {
  Geometry2D,
  ReflectionHelper
  } from '@fct/sdk';
import {
  Pdf,
  PdfLine,
  PdfText,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';
import { InfoSquare } from '../customs';
import { TriangleIcon } from '../icons';
import { IndicatorTable } from '../indicator-table';

interface ValueLabelProps {
  centerX: number;
  start: Geometry2D.Point.Dimensions;
  text: string;
  fontSize?: number;
}

function InfoText(props: ValueLabelProps) {
  return (pdf: Pdf) => {
    pdf.render(PdfText({
      text: props.text,
      dimensions: {
        start: {
          x: props.centerX,
          y: props.start.y
        }
      },
      jsPdfOptions: {
        align: PdfTextAlign.CENTER,
        baseline: PdfTextBaseline.MIDDLE
      },
      styles: {
        text: {
          color: pdf.theme.ASAP_BOOK_SUMMARY_DEFAULT_COLOR,
          fontSize: props.fontSize || pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_LABEL_FONT_SIZE,
          fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_FONT_STYLE,
        }
      }
    }));
  }
}

export declare namespace ParamDnochContext {
  interface Props {
    dimensions: {
      start: Geometry2D.Point.Dimensions,
    },
    contextType: string,
    data: ProjectDocumentCollectionRecord
  }
}

export function ParamDnochContext(props: ParamDnochContext.Props) {
  return (pdf: Pdf) => {

    const bottomY =  props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_HEIGHT;
    // Info square
    pdf.render(InfoSquare({
      label: 'měřítko',
      value: props.contextType,
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: props.dimensions.start.y
        }
      }
    }));

    const centerX = props.dimensions.start.x +
      pdf.theme.DOC_DNOCH_CONTEXT_SIDEBAR_CENTER_REL_X

      
    pdf.render(InfoText({
      centerX,
      start: {
        x: props.dimensions.start.x,
        y: props.dimensions.start.y + 
            pdf.theme.DOC_DNOCH_CONTEXT_TENDECY_REL_START_Y
      },
      text: 'tendence'
    }));

    const tendency = props.data.getTendencyForContext(props.contextType);
    if (tendency !== undefined) {
      pdf.render(TriangleIcon({
        dimensions: {
          base: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LARGE_BASE,
          height: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LAGRE_HEIGHT,
          center: {
            x: centerX,
            y: props.dimensions.start.y +
                pdf.theme.DOC_DNOCH_CONTEXT_TRIANGE_REL_CENTER_Y
          }
        },
        styles: {
          color: pdf.theme.ASAP_BOOK_SUMMARY_DEFAULT_COLOR,
          // Never reach value of 180 or 0, minimum has to be 10 and max 170
          // So we are in range of 160 angles between 10 and 170.
          rotation: 180 - (10 + ((160 / 5) * 
              (props.data.getTendencyForContext(props.contextType) || 0)))
        }
      }));
    }

    pdf.render(InfoText({
      centerX,
      start: {
        x: props.dimensions.start.x,
        y: props.dimensions.start.y + 
            pdf.theme.DOC_DNOCH_CONTEXT_SCORE_REL_START_Y
      },
      text: 'hodnocení'
    }));

    const score = props.data.getScoreForContext(props.contextType);
    pdf.render(InfoText({
      centerX,
      start: {
        x: props.dimensions.start.x,
        y: props.dimensions.start.y + 
            pdf.theme.DOC_DNOCH_CONTEXT_SCORE_VAL_REL_START_Y
      },
      text: ReflectionHelper.isNumber(score) ?
          (score as number).toFixed(1) : '-',
      fontSize: pdf.theme.DOC_DNOCH_CONTEXT_INFO_VAL_FONT_SIZE
    }));

    pdf.render(InfoText({
      centerX,
      start: {
        x: props.dimensions.start.x,
        y: props.dimensions.start.y + 
            pdf.theme.DOC_DNOCH_CONTEXT_WEIGHT_REL_START_Y
      },
      text: 'váha měřítka'
    }));

    const weight = props.data.getWeightForContext(props.contextType);
    pdf.render(InfoText({
      centerX,
      start: {
        x: props.dimensions.start.x,
        y: props.dimensions.start.y + 
            pdf.theme.DOC_DNOCH_CONTEXT_WEIGHT_VAL_REL_START_Y
      },
      text: ReflectionHelper.isNumber(weight) ?
          (weight as number).toFixed(1) : '-',
      fontSize: pdf.theme.DOC_DNOCH_CONTEXT_INFO_VAL_FONT_SIZE
    }));

    // Bottom line
    pdf.render(PdfLine({
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: bottomY
        },
        end: {
          x: props.dimensions.start.x + pdf.theme.DOC_DNOCH_CONTEXT_WIDTH,
          y: bottomY
        }
      },
      styles: {
        color: '#707070',
        size: pdf.theme.DOC_DNOCH_CONTEXT_LINE_HEIHGT
      }
    }));

    if (props.data.getCommentForContext(props.contextType)) {
      pdf.render(PdfText({
        dimensions: {
          start: {
            x: props.dimensions.start.x + pdf.theme.DOC_DNOCH_CONTEXT_COMMNT_REL_START_X,
            y: props.dimensions.start.y
          },
        },
        text: props.data.getCommentForContext(props.contextType),
        styles: {
          text: {
            color: pdf.theme.DOC_DNOCH_CONTEXT_COMMENT_COLOR,
            fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_COMMENT_FONT_STYLE,
            fontSize: pdf.theme.DOC_DNOCH_CONTEXT_COMMENT_FONT_SIZE,
          }
        },
        jsPdfOptions: {
          align: PdfTextAlign.JUSTIFY,
          baseline: PdfTextBaseline.TOP,
          maxWidth: pdf.theme.DOC_DNOCH_CONTEXT_WIDTH - 
            pdf.theme.DOC_DNOCH_CONTEXT_COMMNT_REL_START_X,
        }
      }));
    };

    const indicators = props.data.getIndicators(props.contextType, false);
    const totalHeight = indicators.length * pdf.theme.DOC_DNOCH_CONTEXT_IND_HEIGHT;
    const absStartY = bottomY - (totalHeight);

    pdf.render(IndicatorTable({
      headless: true,
      contextType: props.contextType,
      startX: 
        props.dimensions.start.x + 
        pdf.theme.DOC_DNOCH_CONTEXT_COMMNT_REL_START_X,
      startY: absStartY,
      width: pdf.theme.DOC_DNOCH_CONTEXT_WIDTH - 
        pdf.theme.DOC_DNOCH_CONTEXT_COMMNT_REL_START_X,
      indicators,
      node: props.data
    }));
  }
}