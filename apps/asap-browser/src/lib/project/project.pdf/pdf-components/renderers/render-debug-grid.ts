import { Pdf } from '../lib';

// Render grid for each inch or cm
export function renderDebugGrid(pdf: Pdf, scale: number = 100) {

  // horizontal line
  for (let i = 100; i < pdf.theme.ASAP_BOOK_RESOLUTION_WIDTH; i =i+100) {
    pdf.line({
      dimensions: {
        start: { x: i, y: 0 },
        end: { x: i, y: pdf.theme.ASAP_BOOK_RESOLUTION_HEIGHT},
      },
      styles: {
        color: '#A7A9AC',
        size: 5,
      }
    });
  }

  for (let i = 100; i < pdf.theme.ASAP_BOOK_RESOLUTION_HEIGHT; i =i+100) {
    pdf.line({
      dimensions: {
        start: { x: 0, y: i },
        end: { x: pdf.theme.ASAP_BOOK_RESOLUTION_WIDTH, y: i },
      },
      styles: {
        color: '#A7A9AC',
        size: 5,
      }
    });
  } 
}