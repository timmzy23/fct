import {
  Project,
  ProjectDocumentCollectionRecord
  } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../../lib';
import { PageData } from '../../types';
import { PagePath } from '../customs';
import { PageFooter } from '../page-footer';
import { PageTitle } from '../page-title';
import { ParamDetailContext } from './param-detail-context';

export declare namespace ParamDetail {
  interface Props {
    data: PageData;
    project: Project;
    dimensions: {
      start: Geometry2D.Point.Dimensions
    }
    shiftY: number,
    node: ProjectDocumentCollectionRecord
  }
}

export function ParamDetail(props: ParamDetail.Props) {
  return (pdf: Pdf) => {
    pdf.render(PageTitle({ 
      title: 'SUMÁRNÍ VÝSLEDEK - DETAIL',
      shiftY: props.shiftY
    }));


    props.node.getAvailableContexts().forEach((contextType, ix) => {
      pdf.render(ParamDetailContext({
        required: props.data.relevancy,
        dimensions: { 
          start: {
            x: props.dimensions.start.x,
            y: pdf.theme.ASAP_BOOK_CONTEXT_START_Y + 
              (pdf.theme.ASAP_BOOK_CONTEXT_HEIGHT * ix)
          }
        },
        shiftY: props.shiftY,
        node: props.node,
        contextType: contextType
      }));
    });

    pdf.render(PageFooter({
      data: props.data,
      project: props.project,
      dimensions: {
        start: {
          x: pdf.theme.ASAP_BOOK_FOOTER_START_X,
          y: pdf.theme.ASAP_BOOK_FOOTER_START_Y
        }
      },
      shiftY: props.shiftY,
      node: props.node
    }));

    pdf.render(PagePath({
      path: props.data.path,
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: pdf.theme.ASAP_BOOK_SUMMARY_PATH_Y
        }
      },
      shiftY: props.shiftY
    }));
  }
}