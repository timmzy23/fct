import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';

export declare namespace ScoreLabel {
  interface Props {
    dimensions: {
      center: Geometry2D.Point.Dimensions;
    };
    text: string;
  }
}

export function ScoreLabel(props: ScoreLabel.Props) {
  return (pdf: Pdf) => {
    pdf.text({
      color: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LABEL_COLOR,
      text: props.text,
      jsPdfOptions: {
        align: PdfTextAlign.CENTER,
        baseline: PdfTextBaseline.MIDDLE,
        maxWidth: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LABEL_WIDTH
      },
      start: {
        x: props.dimensions.center.x,
        y: props.dimensions.center.y
      },
      fontSize: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LABEL_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_CHART_LABEL_FONT_STYLE
    });
  }
}