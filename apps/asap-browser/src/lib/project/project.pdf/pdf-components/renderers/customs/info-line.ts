import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfRectangle,
  PdfText,
  PdfTextBaseline
  } from '../../lib';

export declare namespace InfoLine {
  interface Props {
    dimensions: Geometry2D.Rect.Dimensions,
    styles?: {
      backgroundColor?: string;
      textColor?: string;
      fontSize?: string;
      fontStyle?: string;
      paddingLeft?: number;
    },
    label: string;
  }
}

export function InfoLine(props: InfoLine.Props) {
  return (pdf: Pdf) => {
    const backgroundColor = props.styles && props.styles.backgroundColor ?
        props.styles.backgroundColor :
        pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_BACKGROUND;
    const textColor = props.styles && props.styles.textColor ?
        props.styles.textColor :
        pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_COLOR;
    const fontSize = props.styles && props.styles.fontSize ?
        props.styles.fontSize :
        pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_FONT_SIZE;
    const fontStyle = props.styles && props.styles.fontStyle ?
        props.styles.fontStyle :
        pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_FONT_STYLE;
    const paddingLeft = props.styles && props.styles.paddingLeft ?
        props.styles.paddingLeft :
        pdf.theme.ASAP_BOOK_SUPPARY_PROJECT_HEAD_PADDING_LEFT; 

    pdf.render(PdfRectangle({
      dimensions: props.dimensions,
      styles: {
        background: { color: backgroundColor }
      }
    }));

    pdf.render(PdfText({
      text: props.label,
      dimensions: {
        start: {
          x: props.dimensions.start.x + paddingLeft,
          y: props.dimensions.start.y + (props.dimensions.height / 2),
        },
      },
      styles: {
        text: {
          fontSize, 
          fontStyle,
          color: textColor,
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE
      }
    }));
  }
}