import {
  Pdf,
  PdfTextAlign,
  PdfTextBaseline
  } from '../lib';

export declare namespace SummaryPath {
  interface Props {
    path: string[];
  }
}

export function renderSummaryPath(pdf: Pdf, props: SummaryPath.Props) {
  pdf.text({
    color: pdf.theme.ASAP_BOOK_SUMMARY_PATH_COLOR,
    fontSize: pdf.theme.ASAP_BOOK_SUMMARY_PATH_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_PATH_FONT_STYLE,
    start: {
      x: pdf.theme.ASAP_BOOK_SUMMARY_PATH_X,
      y: pdf.theme.ASAP_BOOK_SUMMARY_PATH_Y
    },
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP,
      align: PdfTextAlign.JUSTIFY
    },
    text: `ASaP - ${props.path.join(' - ')}`,
  });
}