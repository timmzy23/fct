import { ProjectIndicator } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfLine,
  PdfTextAlign
  } from '../../lib';
import {
  IndicatorTableText,
  IndicatorTableTextSmall
  } from './indicator-table-text';

export declare namespace IndicatorTableRow {
  interface Props {
    indicator: ProjectIndicator;
    contextType: string;
    dimensions: {
      start: Geometry2D.Point.Dimensions;
      width: number;
    }
    columns: number[];
  }
}

export function IndicatorTableRow(props: IndicatorTableRow.Props) {
  return (pdf: Pdf) => {
    const indicator = props.indicator;
    const endX = props.dimensions.start.x + props.dimensions.width;

    pdf.render(PdfLine({
      dimensions: {
        start: props.dimensions.start,
        end: {
          x: endX,
          y: props.dimensions.start.y
        }
      },
      styles: {
        color: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_COLOR,
        size: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_HEIGHT
      }
    }));

    // INDICATOR NAME
    pdf.render(IndicatorTableText({
      text: indicator.name || '',
      startX: props.dimensions.start.x,
      startY: props.dimensions.start.y
    }));

    // INDICATOR UNITS
    pdf.render(IndicatorTableText({
      text: indicator.units || '',
      startX: props.dimensions.start.x + props.columns[2], // align to the end
      startY: props.dimensions.start.y,
      textAlign: PdfTextAlign.RIGHT
    }));

    // INDICATOR VALUE
    pdf.render(IndicatorTableText({
      text: indicator.values && indicator.values[props.contextType] !== undefined ?
          indicator.values[props.contextType] : 
          '/',
      startX: props.dimensions.start.x + props.columns[3], // align to the end
      startY: props.dimensions.start.y,
      textAlign: PdfTextAlign.RIGHT
    }));
   
    pdf.render(PdfLine({
      dimensions: {
        start: {
          x: props.dimensions.start.x + pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_REL_START_X,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        },
        end: {
          x: endX,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        }
      },
      styles: {
        color: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_COLOR,
        size: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_HEIGHT
      }
    }));

    // INDICATOR SOURCE
    pdf.render(IndicatorTableTextSmall({
      text: indicator.source || 'Vlastní šetření',
      startX: endX,
      startY: props.dimensions.start.y,
      baseline: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_TOP_REL_BASELINE,
      textAlign: PdfTextAlign.RIGHT
    }));
    // INDICATOR ACCESS
    pdf.render(IndicatorTableTextSmall({
      text: indicator.accessibility || '',
      startX: endX,
      startY: props.dimensions.start.y,
      baseline: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_BOTTOM_REL_BASELINE,
      textAlign: PdfTextAlign.RIGHT
    }));
  }
}
