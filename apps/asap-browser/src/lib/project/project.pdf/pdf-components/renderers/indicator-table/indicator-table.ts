import {
  ProjectDocumentCollectionRecord,
  ProjectIndicator
  } from '@fct/asap-interfaces';
import { Pdf } from '../../lib';
import { IndicatorTableHeader } from './indicator-table-header';
import { IndicatorTableRow } from './indicator-table-row';

export declare namespace IndicatorTable {
  interface Props {
    indicators: ProjectIndicator[];
    startX: number;
    startY: number;
    width: number;
    node: ProjectDocumentCollectionRecord;
    contextType: string;
    headless?: boolean;
    maxCount?: number;
  }
}

const MAX_TABLE_COUNT = 7;

export function IndicatorTable(props: IndicatorTable.Props) {

  return (pdf: Pdf) => {
    const columsStartX = [
      pdf.theme.DOC_IND_TABLE_COL_NAME_X,
      pdf.theme.DOC_IND_TABLE_COL_UNITS_X,
      pdf.theme.DOC_IND_TABLE_COL_VALUE_X,
      pdf.theme.DOC_IND_TABLE_COL_SOURCES_X,
    ];

    if (!props.headless) {
      pdf.render(IndicatorTableHeader({
        baseline: pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_HEIGHT / 2,
        startX: props.startX,
        startY: props.startY,
        columns: columsStartX,
        width: props.width
      }));
    }

    const tableBodyStartY = !props.headless ?
        props.startY +
        pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_HEIGHT + 
        pdf.theme.DOC_DNOCH_CONTEXT_IND_HEADER_BOTTOM_MARGIN :
        props.startY;

    props.indicators
      .filter((indicator, ix) => ix < MAX_TABLE_COUNT + 1)
      .map((indicator, ix) => ix < MAX_TABLE_COUNT ?
          indicator : 
          { 
            name: 'Další indikátory viz podřazené parametry',
            units: '/',
          } as ProjectIndicator
      )
      .forEach((indicator, ix) => {
      pdf.render(IndicatorTableRow({
        columns: columsStartX,
        contextType: props.contextType,
        dimensions: {
          width: props.width,
          start: {
            x: props.startX,
            y: tableBodyStartY +
              (pdf.theme.DOC_DNOCH_CONTEXT_IND_HEIGHT * ix)
          }
        },
        indicator
      }));
    });
  }
}
