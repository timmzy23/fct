import {
  ProjectDocumentCollection,
  ProjectDocumentCollectionRecord
  } from '@fct/asap-interfaces';
import {
  Pdf,
  PdfTextBaseline
  } from '../lib';
import { PageData } from '../types';
import { renderCharSquare } from './render-char-square';
import { renderCheckBoxSquare } from './render-checkbox-square';
import { renderAsapLogo } from './render-logo';
import { renderPageRidge } from './render-page-ridge';
import { renderTextLine } from './render-text-line';
import { getSectionColor } from './utils';

export interface SectionSummaryPageProps {
  currentPage: PageData;
  node: ProjectDocumentCollectionRecord;
  pageMap: { [id: string]: PageData };
  project: ProjectDocumentCollection
}

export function renderSectionSummaryPage(pdf: Pdf, props: SectionSummaryPageProps) {
  const sectionColor = getSectionColor(props.currentPage.section.id, pdf.theme);

  pdf.addPage();

  pdf.fillRect({
    background: sectionColor,
    dimensions: {
      x: 0, y: 0,
      width: '100%', height: '100%'
    }
  });

    // PARAM PARENT TITLE
    const parent = props.node.getParent();
    pdf.text({
      color: '#ffffff',
      text: (parent && parent.node.name ? parent.node.name : 'ASaP').toUpperCase(),
      fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_FONT_STYLE,
      start: {
        x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_START_X,
        y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_SUBTITLE_START_Y
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.TOP,
        maxWidth: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_WIDTH,
      }
    });

  // PARAM TITLE
  pdf.text({
    color: '#ffffff',
    text: (props.node.node.name || '').toUpperCase(),
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_TITLE_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_TITLE_FONT_STYLE,
    start: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_TITLE_X,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_TITLE_Y
    },
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP,
      maxWidth: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_WIDTH,
    }
  });

  // PROJECT NAME
  renderTextLine(pdf, {
    color: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_COLOR,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_FONT_STYLE,
    backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_BACKGROUND,
    paddingLeft: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_PADDING_LEFT,
    dimensions: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_X,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_Y,
      width: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_WIDTH,
      height: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_INFO_HEIGHT
    },
    text: `${props.currentPage.projectName}`
  });

  renderAsapLogo(pdf, {
    currentPage: props.currentPage,
    dimensions: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LEFT_MARGIN,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LOGO_Y
    }
  });

  pdf.text({
    text: 'projekt',
    start: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LEFT_MARGIN,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_LABEL_Y
    },
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_PROJECT_FONT_SIZE,
    color: '#ffffff',
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP
    }
  });

  const author = props.node.getAuthor();
  renderTextLine(pdf, {
    color: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_COLOR,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_FONT_STYLE,
    backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_BACKGROUND,
    paddingLeft: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_PADDING_LEFT,
    dimensions: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_X,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_Y,
      width: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_WIDTH,
      height: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_HEIGHT
    },
    text: `${author ? author.name || '' : ''}`
  });

  const verificator = props.node.getVerificator();
  renderTextLine(pdf, {
    color: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_COLOR,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_FONT_SIZE,
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_FONT_STYLE,
    backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_BACKGROUND,
    paddingLeft: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_PADDING_LEFT,
    dimensions: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_X,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_Y,
      width: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_WIDTH,
      height: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_HEIGHT
    },
    text: `${verificator ? verificator.name || '' : ''}`
  });

  pdf.text({
    text: 'zpracoval',
    start: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_X,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_Y
    },
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CREATED_BY_LABEL_FONT_SIZE,
    color: '#ffffff',
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP
    }
  });

  pdf.text({
    text: 'ověřil',
    start: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_X,
      y: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_Y
    },
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_VERIFIED_BY_LABEL_FONT_SIZE,
    color: '#ffffff',
    jsPdfOptions: {
      baseline: PdfTextBaseline.TOP
    }
  });

  const childListLabeY = pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_BOTTOM_Y - (
    // Children count * text line height
    props.currentPage.childIds.length * pdf.theme.ASAP_BOOK_TEXT_LINE_HEIGHT +
    // With margin between 
    (props.currentPage.childIds.length - 1) * pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_GAP +
    // Plus margin bellow the label
    pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LABEL_GAP);

  props.currentPage.childIds.forEach((childPageId: string, ix: number, childIds: string[]) => {
    const childPage = props.pageMap[childPageId]
    const childName = childPage.name;
    const height = 
        (pdf.theme.ASAP_BOOK_TEXT_LINE_HEIGHT * childIds.length) + 
        (pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_GAP * (childIds.length -1));
    const absStartY = pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_BOTTOM_Y -
        height;
    const startY = absStartY + (ix  * (
        pdf.theme.ASAP_BOOK_TEXT_LINE_HEIGHT + 
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_GAP));

    const param = props.project.getNodeByXPath(childPageId);

    const childY = startY;

    renderTextLine(pdf, {
      backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_TEXT_LINE_BACKGROUND,
      color: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_TEXT_LINE_COLOR,
      dimensions: {
        x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_RIGHT_COLUMN_X,
        y: childY,
        width: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_NAME_WIDTH,
        height: pdf.theme.ASAP_BOOK_TEXT_LINE_HEIGHT,
      },
      fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_FONT_STYLE,
      paddingLeft: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_PADDING_LEFT,
      text: childName
    });

    const checkboxColor = 
        childPage.critical ?
            pdf.theme.ASAP_BOOK_CRITICAL_COLOR :
        childPage.contexts.length > 0 ?
            pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_ACTIVE:
            pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_INACTIVE;

    renderCheckBoxSquare(pdf, {
      color: checkboxColor,
      backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_BACKGROUND,
      dimension: {
        size: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_SIZE,
        x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_CHECK_X,
        y: childY,
      },
      padding: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_CHECK_PADDING
    });

    renderCharSquare(pdf, {
      backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_BACKGROUND,
      color: param.hasContextValue('score', 'S', true) ?
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_ACTIVE :
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_INACTIVE,
      char: 'S',
      dimension: {
        size: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_SIZE,
        x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_S_X,
        y: childY
      },
      fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_STYLE
    });

    renderCharSquare(pdf, {
      backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_BACKGROUND,
      color: param.hasContextValue('score', 'M', true) ?
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_ACTIVE :
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_INACTIVE,
      char: 'M',
      dimension: {
        size: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_SIZE,
        x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_M_X,
        y: childY
      },
      fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_STYLE
    });

    renderCharSquare(pdf, {
      backgroundColor: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_BACKGROUND,
      color: param.hasContextValue('score', 'L', true) ?
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_ACTIVE :
        pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_COLOR_INACTIVE,
      char: 'L',
      dimension: {
        size: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_SIZE,
        x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_L_X,
        y: childY
      },
      fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_SIZE,
      fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_CHILD_LIST_SQUARE_FONT_STYLE
    });
  });

  pdf.text({
    text: 'parametry',
    start: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_RIGHT_COLUMN_X,
      // Compute position from the bottom of the children list, because it 
      // dependes on how many direct children section has.
      y: childListLabeY 
    },
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_SIZE,
    color: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LABEL_COLOR,
    jsPdfOptions: {
      baseline: PdfTextBaseline.BOTTOM
    }
  });

  pdf.text({
    text: 'relevance',
    start: {
      x: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_SQUARE_COL_X,
      // Compute position from the bottom of the children list, because it 
      // dependes on how many direct children section has.
      y: childListLabeY 
    },
    fontStyle: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_STYLE,
    fontSize: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LABEL_FONT_SIZE,
    color: pdf.theme.ASAP_BOOK_SECTION_SUMMARY_LABEL_COLOR,
    jsPdfOptions: {
      baseline: PdfTextBaseline.BOTTOM
    }
  });
}
