import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfText,
  PdfTextBaseline
  } from '../lib';

export declare namespace PageTitle {
  interface Props {
    title: string;
    dimensions?: { start: Geometry2D.Point.Dimensions; };
    shiftY?: number;
  }
}

export function PageTitle(props: PageTitle.Props) {
  return (pdf: Pdf) => {
    const start = {
      x: props.dimensions ? props.dimensions.start.x :
          pdf.theme.ASAP_BOOK_PAGE_TITLE_X,
      y: props.dimensions ? props.dimensions.start.y :
          pdf.theme.ASAP_BOOK_PAGE_TITLE_Y
    };

    const startY = props.shiftY ?
        start.y + props.shiftY : start.y;

    pdf.render(PdfText({
      text: props.title,
      dimensions: { start: {
        ...start,
        y: startY
      } },
      styles: {
        text: {
          color: pdf.theme.ASAP_BOOK_PAGE_TITLE_COLOR,
          fontSize: pdf.theme.ASAP_BOOK_PAGE_TITLE_FONT_SIZE,
          fontStyle: pdf.theme.ASAP_BOOK_PAGE_TITLE_FONT_STYLE,
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.TOP
      }
    }));
  };
}