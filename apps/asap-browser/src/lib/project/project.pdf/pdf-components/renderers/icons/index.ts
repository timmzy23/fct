import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../../lib';

export declare namespace TriangleIcon {
  interface Props {
    dimensions: {
      center: Geometry2D.Point.Dimensions;
      height?: number;
      base?: number;
    },
    styles?: {
      color?: string;
      rotation?: number;
      critical?: boolean;
    },
  }
}

export function TriangleIcon(props: TriangleIcon.Props) {
  if (!props.dimensions.center) {
    throw new Error('SmallTriangle Icons needs dimensions: center');
  }

  return (pdf: Pdf) => {
    const triangleHeight = props.dimensions.height || pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_SMALL_HEIGHT;
    let triangle = Geometry2D.triangle.getIsoscelesTriangle({
      base:  props.dimensions.base || pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LARGE_BASE,
      height: triangleHeight,
      center: props.dimensions.center
    });
    let polygon: Geometry2D.Polygon.Dimensions = {
      absolutePath: [
        triangle.a, triangle.b, triangle.c,
        { x: triangle.b.x, y: triangle.a.y - (triangleHeight * 0.25)}
      ]
    }

    
    if (props.styles && props.styles.rotation) {
      polygon = {
        ...polygon,
        absolutePath: Geometry2D.polygon.rotate(
          polygon.absolutePath!,
          props.dimensions.center,
          Geometry2D.angeToRad(props.styles.rotation))
      }
    }

    pdf.polygon({
      dimensions: polygon,
      styles: {
        background: {
          color:
            props.styles && props.styles.critical ? 
              pdf.theme.ASAP_BOOK_CRITICAL_COLOR :
            props.styles && props.styles.color ? 
              props.styles.color :
              pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_COLOR_DEFAULT
        }
      }
    });
  }
}
