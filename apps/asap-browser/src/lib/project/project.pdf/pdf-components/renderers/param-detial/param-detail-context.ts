import { ProjectDocumentCollectionRecord } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfLine
  } from '../../lib';
import { InfoSquare } from '../customs';
import { IndicatorTable } from '../indicator-table';
import { ParamTable } from '../param-table';

export declare namespace ParamDetailContext {
  interface Props {
    dimensions: {
      start: Geometry2D.Point.Dimensions,
    },
    required?: boolean;
    shiftY?: number;
    node?: ProjectDocumentCollectionRecord;
    contextType: string;
  }
}

export function ParamDetailContext(props: ParamDetailContext.Props) {
  return (pdf: Pdf) => {
    const columns = [
      props.dimensions.start.x,
      props.dimensions.start.x + 
          pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
          pdf.theme.ASAP_BOOK_GAP,
      props.dimensions.start.x +
          pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE +
          pdf.theme.ASAP_BOOK_GAP +
          pdf.theme.ASAP_BOOK_CONTEXT_INDICATOR_LENGTH +
          pdf.theme.ASAP_BOOK_CONTEXT_BIG_GAP
    ];

    pdf.render(PdfLine({
      dimensions: {
        start: props.dimensions.start,
        end: { 
          x: props.dimensions.start.x + pdf.theme.ASAP_BOOK_CONTEXT_WIDTH,
          y: props.dimensions.start.y
        },
        shiftY: props.shiftY
      },
      styles: {
        color: pdf.theme.ASAP_BOOK_CONTEXT_SEP_COLOR,
        size: pdf.theme.ASAP_BOOK_CONTEXT_SEP_SIZE
      }
    }));

    pdf.render(InfoSquare({
      label: 'měřítko',
      value: props.contextType,
      dimensions: {
        start: {
          x: columns[0],
          y: props.shiftY ?
            props.dimensions.start.y + pdf.theme.ASAP_BOOK_GAP + props.shiftY: 
            props.dimensions.start.y + pdf.theme.ASAP_BOOK_GAP
        }
      }
    }));

    const tablesY = props.shiftY ?
        props.dimensions.start.y + pdf.theme.ASAP_BOOK_GAP + props.shiftY :
        props.dimensions.start.y + pdf.theme.ASAP_BOOK_GAP;
    pdf.render(IndicatorTable({
      indicators: props.node.getIndicators(),
      startX: columns[1],
      startY: tablesY,
      width: pdf.theme.ASAP_BOOK_CONTEXT_INDICATOR_LENGTH,
      contextType: props.contextType,
      node: props.node
    }));


    pdf.render(ParamTable({
      contextType: props.contextType,
      startX: 
        props.dimensions.start.x +
        pdf.theme.DOC_DNOCH_CONTEXT_PARAM_TABLE_REL_START_X,
      startY: tablesY,
      params: props.node.getChildren(),
      width: pdf.theme.DOC_PARAM_TABLE_WIDTH
    }));
  }
}
