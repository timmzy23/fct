import {
  ProjectDocumentCollection,
  ProjectDocumentCollectionRecord
  } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../../lib';
import { PageData } from '../../types';
import { PagePath } from '../customs';
import { PageFooter } from '../page-footer';
import { PageTitle } from '../page-title';
import { renderSummaryRecommendation } from '../render-summary-recommendation';
import { renderSummaryResume } from '../render-summary-resume';
import { ParamDnochContext } from './param-dnoch-context';

export declare namespace ParamDetailNoChildren {
  interface Props {
    data: ProjectDocumentCollectionRecord;
    _data: PageData,
    project: ProjectDocumentCollection;
    dimensions: {
      start: Geometry2D.Point.Dimensions;
      shiftY: number;
    }
  }
}

export function ParamDetailNoChildren(props: ParamDetailNoChildren.Props) {
  return (pdf: Pdf) => {
    // Page Title
    pdf.render(PageTitle({ 
      title: 'DETAIL PARAMETRU',
      shiftY: props.dimensions.shiftY
    }));

    const contextStartY = props.dimensions.shiftY ?
      pdf.theme.DOC_DNOCH_CONTEXT_START_Y + props.dimensions.shiftY :
      pdf.theme.DOC_DNOCH_CONTEXT_START_Y;

    props.data.getAvailableContexts()
        .forEach((contextType, ix) => pdf.render(ParamDnochContext({
          contextType,
          data: props.data,
          dimensions: {
            start: {
              x: props.dimensions.start.x,
              y: contextStartY + 
                ((pdf.theme.DOC_DNOCH_CONTEXT_HEIGHT + 
                  pdf.theme.DOC_DNOCH_CONTEXT_GAP_Y) * ix)
            }
          }
        })))

    // Page Footer
    pdf.render(PageFooter({
      data: props._data,
      project: props.project.project,
      dimensions: {
        start: {
          x: pdf.theme.ASAP_BOOK_FOOTER_START_X,
          y: pdf.theme.ASAP_BOOK_FOOTER_START_Y
        }
      },
      shiftY: props.dimensions.shiftY,
      node: props.data
    }));

    // Asap name path
    pdf.render(PagePath({
      path: props._data.path,
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: pdf.theme.ASAP_BOOK_SUMMARY_PATH_Y
        }
      },
      shiftY: props.dimensions.shiftY
    }));

    // Sumary
    renderSummaryResume(
      pdf, 
      { 
        text: props.data.node.summary,
        shiftY: props.dimensions.shiftY
      });

    // Recomendation
    renderSummaryRecommendation(
      pdf, 
      { 
        text: props.data.node.recommendation, 
        sectionId: props.data.getSectionId(),
        shiftY: props.dimensions.shiftY
      });
  }
}