import {
  Pdf,
  PdfSuqareDimension
  } from '../lib';

export interface CheckBoxSquareProps {
  dimension: PdfSuqareDimension;
  padding: number;
  backgroundColor?: string;
  color?: string;
}

export function renderCheckBoxSquare(pdf: Pdf, props: CheckBoxSquareProps) {
  pdf.fillRect({
    dimensions: {
      x: props.dimension.x,
      y: props.dimension.y,
      height: props.dimension.size,
      width: props.dimension.size,
    },
    background: props.backgroundColor || '#ffffff',
  });

  pdf.fillRect({
    dimensions: {
      x: props.dimension.x + props.padding,
      y: props.dimension.y + props.padding,
      height: pdf.computeHeight(props.dimension.size) - (2 * props.padding),
      width: pdf.computeWidth(props.dimension.size) - (2 * props.padding),
    },
    background: props.color,
  });
}