
export function getSectionColor(sectionId: number, theme: any) {
  switch (sectionId) {
    case 3: return theme.ASAP_BOOK_SECTION_CONTEXT_COLOR;
    case 1: return theme.ASAP_BOOK_SECTION_LIMITS_COLOR;
    case 2: return theme.ASAP_BOOK_SECTION_INFRASTRUCTURE_COLOR;
    default: return theme.ASAP_BOOK_SECTION_DEFAULT_COLOR;
  }
}

export function getSectionLightColor(sectionId: number, theme: any) {
  switch (sectionId) {
    case 3: return theme.ASAP_BOOK_SECTION_CONTEXT_COLOR_LIGHT;
    case 1: return theme.ASAP_BOOK_SECTION_LIMITS_COLOR_LIGHT;
    case 2: return theme.ASAP_BOOK_SECTION_INFRASTRUCTURE_COLOR_LIGHT;
    default: return theme.ASAP_BOOK_SECTION_DEFAULT_COLOR_LIGHT;
  }
}

export function pageLevelToString(level: number) {
  switch (level) {
    case 1: return 'I';
    case 2: return 'II';
    case 3: return 'III';
    case 4: return 'IV';
    case 5: return 'V';
  }
}
