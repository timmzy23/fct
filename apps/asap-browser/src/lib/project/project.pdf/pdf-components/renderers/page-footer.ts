import {
  Project,
  ProjectDocumentCollectionRecord
  } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../lib';
import { PageData } from '../types';
import {
  InfoLine,
  InfoSquare
  } from './customs';
import { TriangleIcon } from './icons';
import { getSectionColor } from './utils';

export declare namespace PageFooter {
  interface Props {
    data: PageData;
    node: ProjectDocumentCollectionRecord,
    project: Project;
    shiftY?: number;
    dimensions: {
      start?: Geometry2D.Point.Dimensions
    }
  }
}

export function PageFooter(props: PageFooter.Props) {

  return (pdf: Pdf) => {
    const start = props.dimensions.start || { 
      x: pdf.theme.ASAP_BOOK_FOOTER_START_X,
      y: pdf.theme.ASAP_BOOK_FOOTER_START_Y
    };
    const smallColumnSize = 
        pdf.theme.ASAP_BOOK_FOOTER_COL_LENGTH_SHORT + 
        pdf.theme.ASAP_BOOK_FOOTER_COL_GAP;
    const longColumnSize = 
        pdf.theme.ASAP_BOOK_FOOTER_COL_LENGTH_LONG + 
        pdf.theme.ASAP_BOOK_FOOTER_COL_GAP;

    const columnStarts = [
      start.x,
      start.x + (smallColumnSize * 1),
      start.x + (smallColumnSize * 2),
      start.x + (smallColumnSize * 3),
      start.x + longColumnSize + (smallColumnSize * 3),
      start.x + longColumnSize + (smallColumnSize * 4),
      start.x + longColumnSize + (smallColumnSize * 5),
      start.x + longColumnSize + (smallColumnSize * 6),
      start.x + longColumnSize + (smallColumnSize * 7),
      start.x + longColumnSize + (smallColumnSize * 8),
    ];
    const startY = start.y + props.shiftY;

    pdf.render(InfoSquare({
      label: 'scénář',
      dimensions: { start: {  x: columnStarts[0], y: startY } },
      value: props.project.projectScenario
    }));
    pdf.render(InfoSquare({
      label: 'verze',
      dimensions: { start: {  x: columnStarts[1], y: startY } },
      value: props.project.projectVersion
    }));
    pdf.render(InfoSquare({
      label: 'stav',
      dimensions: { start: {  x: columnStarts[2], y: startY } },
      value: props.project.projectState
    }));

    pdf.render(InfoLine({
      label: props.data.projectName,
      dimensions: { 
        start: { x: columnStarts[3], y: startY},
        width: pdf.theme.ASAP_BOOK_FOOTER_COL_LENGTH_LONG,
        height: pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_HEIGHT
      }
    }));
    pdf.render(InfoLine({
      label: props.data.clientName,
      dimensions: { 
        start: { 
          x: columnStarts[3], 
          y: startY +
            pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_HEIGHT +
            pdf.theme.ASAP_BOOK_FOOTER_COL_GAP
        },
        width: pdf.theme.ASAP_BOOK_FOOTER_COL_LENGTH_LONG,
        height: pdf.theme.ASAP_BOOK_SUMMARY_PROJECT_HEAD_HEIGHT
      }
    }));

    pdf.render(InfoSquare({
      label: 'hodnocení',
      value: (props.data.score || 0).toFixed(1),
      dimensions: { start: {  x: columnStarts[4], y: startY } },
      styles: {
        background: { color: getSectionColor(props.data.section.id, pdf.theme) },
        textColor: '#ffffff'
      }
    }));
    pdf.render(InfoSquare({
      label: 'tendence',
      value: (valueProps) => (pdf: Pdf) => {
        pdf.render(TriangleIcon({
          dimensions: {
            base: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LARGE_BASE,
            height: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LAGRE_HEIGHT,
            center: valueProps.dimensions.center
          },
          styles: {
            color: '#ffffff',
            rotation: 180 - ((180 / 5) * (props.data.tendency || 0))
          }
        }));
      },
      dimensions: { start: {  x: columnStarts[5], y: startY } },
      styles: {
        background: { color: getSectionColor(props.data.section.id, pdf.theme) },
        textColor: '#ffffff'
      }
    }));
    pdf.render(InfoSquare({
      label: 'váha',
      value: (props.data.weight || 0).toFixed(1),
      dimensions: { start: {  x: columnStarts[6], y: startY } },
      styles: {
        background: { color: getSectionColor(props.data.section.id, pdf.theme) },
        textColor: '#ffffff'
      }
    }));
    pdf.render(InfoSquare({
      label: 'podřazené parametry',
      value: props.node.getChildrenCount(),
      dimensions: { start: {  x: columnStarts[7], y: startY } }
    }));
    pdf.render(InfoSquare({
      label: 'podřazené indikátory',
      value: props.node.getIndicatorsCount(),
      dimensions: { start: {  x: columnStarts[8], y: startY } }
    }));
    pdf.render(InfoSquare({
      label: 'indikátory ÚAP',
      value: props.node.getUAPIndicatorsCount(),
      dimensions: { start: {  x: columnStarts[9], y: startY } }
    }));
  }
}
