import {
  Geometry2D,
  ReflectionHelper
  } from '@fct/sdk';
import {
  Pdf,
  PdfRectangle,
  PdfStyles,
  PdfText,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';
import { TriangleIcon } from '../icons';

export declare namespace InfoSquare {
  interface ValueRenderProps {
    dimensions: {
      center: Geometry2D.Point.Dimensions;
    }
  }

  interface Props {
    dimensions: {
      start: Geometry2D.Point.Dimensions,
    },
    styles?: {
      background?: PdfStyles.PdfBackgroundStyle;
      textColor?: string;
    },
    label?: string;
    value?: ((props: ValueRenderProps) =>  (pdf: Pdf) => void) | number | string | null;
  }

  interface TendencyProps {
    dimensions: {
      start: Geometry2D.Point.Dimensions
    },
    styles?: {
      background?: PdfStyles.PdfBackgroundStyle;
      textColor?: string;
      iconColor?: string;
    },
    value: number;
    required?: boolean;
  }

  interface NumberProps {
    dimensions: {
      start: Geometry2D.Point.Dimensions
    },
    styles?: {
      background?: PdfStyles.PdfBackgroundStyle;
      textColor?: string;
      fixed?: number;
    },
    label?: string;
    value?: number;
    required?: boolean;
  }
}

export function InfoSquare(props: InfoSquare.Props) {

  return (pdf: Pdf) => {
    const backgroundColor = props.styles && props.styles.background ?
        props.styles.background.color :
        pdf.theme.ASAP_BOOK_SUMMARY_DEFAULT_BACKGROUND;
    const textColor = props.styles && props.styles.textColor ?
        props.styles.textColor :
        pdf.theme.ASAP_BOOK_SUMMARY_DEFAULT_COLOR

    pdf.render(PdfRectangle({
      dimensions: {
        width: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE,
        height: pdf.theme.ASAP_BOOK_FOOTER_HEIGHT,
        ...props.dimensions,

      },
      styles: { background: { color: backgroundColor }}
    }));

    if (props.label) {
      pdf.render(PdfText({
        text: props.label,
        dimensions: {
          start: {
            x: props.dimensions.start.x + (pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE / 2),
            y: props.dimensions.start.y + pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_TOP
          },
        },
        styles: {
          text: {
            color: textColor,
            fontSize: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_LABEL_FONT_SIZE,
            fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_FONT_STYLE,
          }
        },
        jsPdfOptions: {
          align: PdfTextAlign.CENTER,
          baseline: PdfTextBaseline.TOP,
          maxWidth: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE - 
              (2 * pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_TITLE)
        }
      }));
    }

    if (ReflectionHelper.isFunction(props.value)) {
      pdf.render((props.value as any)({
        dimensions: {
          center: {
            x: props.dimensions.start.x + (pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE / 2),
            y: props.dimensions.start.y + (
              pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE - 
              pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_BOTTOM -
              (pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_VALUE_FONT_SIZE * 0.75))
          },
        }
      }));
    } else if (props.value !== '' && props.value !== undefined) {
      pdf.render(PdfText({
        text: props.value === null ? 
            '0' :
            ReflectionHelper.isNumber(props.value) ? 
                `${props.value}` : 
                props.value as string,
        dimensions: {
          start: {
            x: props.dimensions.start.x + (
                pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE / 2),
            y: props.dimensions.start.y + (
                pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_SIZE - 
                pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_PADDING_BOTTOM)
          },
        },
        styles: {
          text: {
            color: props.value !== null ? textColor : pdf.theme.ASAP_BOOK_CRITICAL_COLOR,
            fontSize: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_VALUE_FONT_SIZE,
            fontStyle: pdf.theme.ASAP_BOOK_SUMMARY_SQUARE_INFO_FONT_STYLE,
          }
        },
        jsPdfOptions: {
          align: PdfTextAlign.CENTER,
          baseline: PdfTextBaseline.BOTTOM
        }
      }));
    }
  }
}

InfoSquare.Tendency = (props: InfoSquare.TendencyProps) => {
  const iconColor = props.styles && props.styles.iconColor ?
      props.styles.iconColor : '#ffffff';
  const renderValue = (valueProps: any) => (pdf: Pdf) => {
    pdf.render(TriangleIcon({
      dimensions: {
        base: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LARGE_BASE,
        height: pdf.theme.ASAP_BOOK_ICON_SMALL_TRIANGLE_LAGRE_HEIGHT,
        center: valueProps.dimensions.center
      },
      styles: {
        color: iconColor,
        // Never reach value of 180 or 0, minimum has to be 10 and max 170
        // So we are in range of 160 angles between 10 and 170.
        rotation: 180 - (10 + ((160 / 5) * (props.value || 0)))
      }
    }));
  };

  return InfoSquare({
    dimensions: props.dimensions,
    styles: props.styles,
    label: 'tendence',
    value: props.value !== undefined ?  
        renderValue : 
        props.required ? null : ''
      
  });
}

InfoSquare.Number = (props: InfoSquare.NumberProps) => {
  const fixed = props.styles && props.styles.fixed ? props.styles.fixed : 1;
  const value = props.value !== undefined ? 
      (props.value).toFixed(fixed) :
      props.required ? null : ''

  return InfoSquare({ ...props, value });
}

// undefined & required
// undefined not:required
// not:undefined 