import { Pdf } from '../lib';

/**
 * Render degbug grid of milimiters over page
 */
export function renderPageMmGrid(pdf: Pdf) {
  // Suppose document in mm units to go simple

  const pageSize = pdf.jsPDF.internal.pageSize;

  for (let i = 1; i < pageSize.height; i = i + 1) {
    pdf.line({
      dimensions: {
        start: { x: 0, y: i},
        end: { x: pageSize.width, y: i},
      },
      styles: {
        color:  i % 10 === 0 ? '#e988f9' : '#E6E7E8',
        size: 0.3
      }
    });
  }

  for (let i = 1; i < pageSize.width; i = i + 1) {
    pdf.line({
      dimensions: {
        start: { x: i, y: 0 },
        end: { x: i, y: pageSize.height },
      },
      styles: {
        color: i % 10 === 0 ? '#e988f9' : '#E6E7E8',
        size: 0.3
      }
    });
  }
}
