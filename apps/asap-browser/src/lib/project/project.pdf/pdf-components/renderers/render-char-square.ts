import {
  Pdf,
  PdfSuqareDimension,
  PdfTextAlign,
  PdfTextBaseline
  } from '../lib';

interface CharSquareProps {
  dimension: PdfSuqareDimension;
  backgroundColor?: string;
  char: string;
  color?: string;
  fontSize: number;
  fontStyle?: string;
}

export function renderCharSquare(pdf: Pdf, props: CharSquareProps) {
  pdf.fillRect({
    dimensions: {
      x: props.dimension.x,
      y: props.dimension.y,
      height: props.dimension.size,
      width: props.dimension.size,
    },
    background: props.backgroundColor || '#ffffff',
  });

  pdf.text({
    text: props.char,
    start: {
      x: props.dimension.x + (pdf.computeWidth(props.dimension.size) / 2),
      y: props.dimension.y + (pdf.computeHeight(props.dimension.size) / 2),
    },
    color: props.color || '#ffffff',
    fontSize: props.fontSize,
    fontStyle: props.fontStyle,
    jsPdfOptions: {
      align: PdfTextAlign.CENTER,
      baseline: PdfTextBaseline.MIDDLE,
    }
  });
}