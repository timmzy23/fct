import {
  Pdf,
  PdfText,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';

export declare namespace ParamTableText {
  interface Props {
    text?: string;
    startX: number;
    startY: number;
    fontSize?: number;
    fontStyle?: number;
    baseline?: number;
    textAlign?: PdfTextAlign;
  }
}

export function ParamTableText(props: ParamTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(PdfText({
      text: props.text || '',
      dimensions: {
        start: {
          x: props.startX,
          y: props.startY + (props.baseline || (pdf.theme.DOC_PARAM_TABLE_ROW_HEIGHT / 2))
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_PARAM_TABLE_TEXT_COLOR,
          fontStyle: props.fontStyle || pdf.theme.DOC_PARAM_TABLE_TEXT_FONT_STYLE,
          fontSize: props.fontSize || pdf.theme.DOC_PARAM_TABLE_TEXT_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE,
        align: props.textAlign || PdfTextAlign.LEFT
      }
    }));
  }
}

export function ParamTableTextHeader(props: ParamTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(ParamTableText({
      fontSize: pdf.theme.DOC_PARAM_TABLE_HEADER_FONT_SIZE,
      fontStyle: pdf.theme.DOC_PARAM_TABLE_HEADER_FONT_STYLE,
      ...props
    }));
  }
}

export function ParamTableTextTitle(props: ParamTableText.Props) {
  return (pdf: Pdf) => {
    pdf.render(ParamTableTextHeader({
      fontSize: pdf.theme.DOC_PARAM_TABLE_TITLE_FONT_STYLE,
      ...props
    }));
  }
}