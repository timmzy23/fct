import {
  Pdf,
  PdfRectShapeProps,
  PdfTextBaseline
  } from '../lib';

export interface LineTextProps {
  dimensions: PdfRectShapeProps;
  backgroundColor?: string;  
  paddingLeft?: number;
  text: string;
  fontSize?: number;
  fontStyle?: string;
  color?: string
}

export function renderTextLine(pdf: Pdf, props: LineTextProps) {
  pdf.fillRect({
    dimensions: props.dimensions,
    background: props.backgroundColor
  });
  const paddingLeft = props.paddingLeft || 0;

  pdf.text({
    text: props.text,
    fontSize: props.fontSize,
    fontStyle: props.fontStyle,
    color: props.color,
    start: {
      x: props.dimensions.x + paddingLeft,
      y: props.dimensions.y + (pdf.computeHeight(props.dimensions.height) / 2)
    },
    jsPdfOptions: {
      baseline: PdfTextBaseline.MIDDLE
    }
  });
}
