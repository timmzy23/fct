import {
  Project,
  ProjectDocumentCollectionRecord
  } from '@fct/asap-interfaces';
import {
  Pdf,
  PdfTextBaseline
  } from '../lib';
import { PageData } from '../types';
import { PageFooter } from './page-footer';
import { PageTitle } from './page-title';
import { renderSummaryPath } from './render-summary-path';
import { renderSummaryRecommendation } from './render-summary-recommendation';
import { renderSummaryResume } from './render-summary-resume';
import { renderSummaryChart } from './summary-chart';

export declare namespace DefaultSummary {
  interface Props {
    data: PageData;
    list: { [id: string]: PageData },
    project: Project;
    shiftY: number,
    node: ProjectDocumentCollectionRecord,
  }
}

export function renderDefaultSummary(pdf: Pdf, { data, list, project, shiftY, node }: DefaultSummary.Props) {
  pdf.render(PageFooter({ 
    dimensions: {
      start: {
        x: pdf.theme.ASAP_BOOK_FOOTER_START_X,
        y: pdf.theme.ASAP_BOOK_FOOTER_START_Y
      }
    },
    data,
    project,
    shiftY,
    node
  }));

  renderSummaryRecommendation(
    pdf, 
    { text: data.recommendation, sectionId: data.section.id });
  renderSummaryResume(pdf, { text: data.resume });
  renderSummaryPath(pdf, { path: data.path });

  renderSummaryChart(pdf, {
    childParams: data.childIds.map((childId) => list[childId]),
    pageData: data
  });

  pdf.render(PageTitle({ title: 'SUMÁRNÍ VÝSLEDEK' }));
}