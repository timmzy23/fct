import {
  Pdf,
  PdfTextAlign
  } from '../../lib';
import {
  ParamTableText,
  ParamTableTextHeader,
  ParamTableTextTitle
  } from './param-table-text';

export declare namespace ParamTableHeader {
  interface Props {
    startX: number;
    startY: number;
    width: number;
    baseline: number;
    columns: number[];
  }
}

export function ParamTableHeader(props: ParamTableHeader.Props) {
  return (pdf: Pdf) => {
    pdf.render(ParamTableTextTitle({
      text: 'PARAMETRY',
      startX: props.startX,
      startY: props.startY,
      baseline: props.baseline
    }));

    pdf.render(ParamTableTextHeader({
      text: 'hodnocení',
      startX: props.startX + props.columns[1] + ((props.columns[2] - props.columns[1]) / 2), // align to center
      startY: props.startY,
      baseline: props.baseline,
      textAlign: PdfTextAlign.CENTER
    }));

    pdf.render(ParamTableTextHeader({
      text: 'tendence',
      startX: props.startX + props.columns[2] + ((props.columns[3] - props.columns[2]) / 2), // align to center
      startY: props.startY,
      baseline: props.baseline,
      textAlign: PdfTextAlign.CENTER
    }));

    pdf.render(ParamTableTextHeader({
      text: 'váha',
      startX: props.startX + props.columns[3] + ((props.width - props.columns[3]) / 2), // align to center,
      startY: props.startY,
      baseline: props.baseline,
      textAlign: PdfTextAlign.CENTER
    }));
  }
}
