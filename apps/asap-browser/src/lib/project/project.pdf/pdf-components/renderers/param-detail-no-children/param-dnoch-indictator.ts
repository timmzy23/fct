import { ProjectIndicator } from '@fct/asap-interfaces';
import { Geometry2D } from '@fct/sdk';
import {
  Pdf,
  PdfLine,
  PdfText,
  PdfTextAlign,
  PdfTextBaseline
  } from '../../lib';

export interface ParamDNochIndicatorProps {
  indicator: ProjectIndicator;
  contextType: string;
  dimensions: {
    start: Geometry2D.Point.Dimensions;
    width: number;
  }
}

export function ParamDNochIndicator(props: ParamDNochIndicatorProps) {
  return (pdf: Pdf) => {
    const indicator = props.indicator;

    pdf.render(PdfLine({
      dimensions: {
        start: props.dimensions.start,
        end: {
          x: props.dimensions.start.x + props.dimensions.width,
          y: props.dimensions.start.y
        }
      },
      styles: {
        color: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_COLOR,
        size: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_HEIGHT
      }
    }));

    // INDICATOR NAME
    pdf.render(PdfText({
      text: indicator.name || '',
      dimensions: {
        start: {
          x: props.dimensions.start.x,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_COLOR,
          fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_STYLE,
          fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE
      }
    }));

    // INDICATOR UNIT
    pdf.render(PdfText({
      text: indicator.units || '',
      dimensions: {
        start: {
          x: props.dimensions.start.x + pdf.theme.DOC_DNOCH_CONTEXT_IND_UNIT_REL_START_X,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_COLOR,
          fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_STYLE,
          fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE
      }
    }));

    // INDICATOR VALUE
    pdf.render(PdfText({
      text: indicator.values && indicator.values[props.contextType] !== undefined ?
          '' + indicator.values[props.contextType] : '',
      dimensions: {
        start: {
          x: props.dimensions.start.x + pdf.theme.DOC_DNOCH_CONTEXT_IND_VAL_REL_START_X,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_COLOR,
          fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_STYLE,
          fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE
      }
    }));

    pdf.render(PdfLine({
      dimensions: {
        start: {
          x: props.dimensions.start.x + pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_REL_START_X,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        },
        end: {
          x: props.dimensions.start.x + props.dimensions.width,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_REL_BASELINE
        }
      },
      styles: {
        color: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_COLOR,
        size: pdf.theme.DOC_DNOCH_CONTEXT_IND_SEP_HEIGHT
      }
    }));

    // INDICATOR SOURCE
    pdf.render(PdfText({
      text: indicator.source || '',
      dimensions: {
        start: {
          x: props.dimensions.start.x + props.dimensions.width,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_TOP_REL_BASELINE
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_COLOR,
          fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_STYLE,
          fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE,
        align: PdfTextAlign.RIGHT
      }
    }));

    // INDICATOR ACCESS
    pdf.render(PdfText({
      text: indicator.accessibility || '',
      dimensions: {
        start: {
          x: props.dimensions.start.x + props.dimensions.width,
          y: props.dimensions.start.y + pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_BOTTOM_REL_BASELINE
        }
      },
      styles: {
        text: {
          color: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_COLOR,
          fontStyle: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_STYLE,
          fontSize: pdf.theme.DOC_DNOCH_CONTEXT_IND_INFO_FONT_SIZE
        }
      },
      jsPdfOptions: {
        baseline: PdfTextBaseline.MIDDLE,
        align: PdfTextAlign.RIGHT
      }
    }));
  }
}