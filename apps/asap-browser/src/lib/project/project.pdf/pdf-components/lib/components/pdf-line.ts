import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../pdf';

export declare namespace PdfLine {
  interface Props {
    dimensions: Geometry2D.Line.Dimensions & { shiftY?: number };
    styles?: {
      dashSize?: number;
      dashGapSize?: number;
      color?: string;
      size?: number;
    }
  }
}

export function PdfLine(props: PdfLine.Props) {
  return (pdf: Pdf) => {
    const styles = props.styles || {};

    if (styles.color) {
      pdf.jsPDF.setDrawColor(styles.color);
    }
    if (styles.size) {
      pdf.jsPDF.setLineWidth(styles.size);
    }
    if (styles.dashSize) {
      const dashArgArr = [styles.dashSize];
      if (styles.dashGapSize) {
        dashArgArr.push(styles.dashGapSize);
      }
      pdf.jsPDF['setLineDash'](dashArgArr);
    }
    const startY = props.dimensions.shiftY ?
        props.dimensions.shiftY + props.dimensions.start.y :
        props.dimensions.start.y;
    const endY = props.dimensions.shiftY ?
        props.dimensions.shiftY + props.dimensions.end.y :
        props.dimensions.end.y;

    pdf.jsPDF.line(
      props.dimensions.start.x,
      startY,
      props.dimensions.end.x,
      endY);
    // Go back to black color.
    pdf.jsPDF.setDrawColor('#000000');
    pdf.jsPDF['setLineDash']([]);
  }
}