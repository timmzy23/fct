import { Geometry2D } from '@fct/sdk';
import { Pdf } from './pdf';

export enum PageFormat {
  A0 = 'a0',
  A1 = 'a1',
  A2 = 'a2',
  A3 = 'a3',
  A4 = 'a4',
  A5 = 'a5',
  A6 = 'a6',
  A7 = 'a7',
  A8 = 'a8',
  A9 = 'a9',
  A10 = 'a10',
  B0 = 'b0',
  B1 = 'b1',
  B2 = 'b2',
  B3 = 'b3',
  B4 = 'b4',
  B5 = 'b5',
  B6 = 'b6',
  B7 = 'b7',
  B8 = 'b8',
  B9 = 'b9',
  B10 = 'b10',
  C0 = 'c0',
  C1 = 'c1',
  C2 = 'c2',
  C3 = 'c3',
  C4 = 'c4',
  C5 = 'c5',
  C6 = 'c6',
  C7 = 'c7',
  C8 = 'c8',
  C9 = 'c9',
  C10 = 'c10'
}

export enum PageOrientation {
  LANDSCAPE = 'landscape',
  PORTRAIT = 'portrait'
}

export enum PdfUnit {
  PT = 'pt',
  MILIMETERS = 'mm',
  CM = 'cm',
  IN = 'in',
  PX = 'px',
  PC = 'pc',
  EM = 'em',
  EX = 'ex'
}

export interface PdfCoord {
  x: number;
  y: number;
}

export interface PdfFontFace {
  fontName: string,
  fontFamily: string,
  url: string,
  fontStyle: string
}

export interface PdfShape {
  startX: number;
  startY: number;
}

export interface PdfCircleShape {
  start?: PdfCoord;
  center?: PdfCoord;
  radius: number;
  borderColor?: string;
  borderSize?: number;
  backgroundColor?: string;
}

export enum PdfTextAlign {
  LEFT = 'left',
  CENTER = 'center',
  RIGHT = 'right',
  JUSTIFY = 'justify'
}

export enum PdfTextBaseline {
  ALPHABETIC  = 'alphabetic', 
  IDEOGRAPHIC = 'ideographic', 
  BOTTOM = 'bottom', 
  TOP = 'top', 
  MIDDLE = 'middle', 
  HANGING = 'hanging'
}

export enum PdfTextRotationDirection {
  CLOCKWISE = 0,
  CONTERCLOCKWISE = 1
}

export interface PdfTextStartProps {
  /**
   * String length in resolution points
   */
  length: number;
}

export interface PdfTextProps {
  text: string;
  start?: PdfCoord;
  color?: string;
  fontStyle?: string;
  fontFamily?: string;
  fontSize?: number;
  lineWidth?: number;
  jsPdfOptions?: {
    angle?: number;
    align?: PdfTextAlign;
    baseline?: PdfTextBaseline;
    rotationDirection?: PdfTextRotationDirection;
    charSpace?: number;
    lineHeightFactor?: number;
    maxWidth?: number;
  },
  computeStart?: (props: PdfTextStartProps) => PdfCoord;
}

export interface PdfShapeBorderProps {
  borderColor?: string;
  borderSize?: number;
  borderDashSize?: number;
  borderDashGapSize?: number;
}

export interface PdfRectShapeProps extends PdfCoord {
  /** Width in number or percents */
  width: number | string;
  /** Height in number or percents */
  height: number | string; 
}

export interface PdfSuqareDimension extends PdfCoord {
  /** Square size in number or percents */
  size: number | string;
}

export enum PdfImageCompression {
  NONE = 'NONE',
  FAST = 'FAST',
  MEDIUM = 'MEDIUM',
  SLOW = 'SLOW'
}

export interface PdfResolution {
  with: number;
  height: number;
}

export interface PdfSvgRenderProps {
  /** Canvas width in points per unit */
  canvasWidth: number;
  /** Canvas height in points per unit */
  canvasHeigh: number;
  /** Points per unit */
  ppu: number;
}

export interface PdfSvgProps {
  svg?: string;
  dimensions: PdfRectShapeProps;
  alias?: string;
  compression?: PdfImageCompression
  rotation?: number;
  /**
   * svg points per pdf document unit
   */
  ppu?: number;
  backgroundColor?: string;
  renderSvg?: (props: PdfSvgRenderProps) => string;
}

export interface PdfRectProps {
  dimensions: PdfRectShapeProps;
  background?: string;
}

export interface PdfCanvasProps {
  canvas: HTMLCanvasElement,
  dimensions: PdfRectShapeProps,
  compression?: PdfImageCompression,
  rotation?: number;
}

export interface PdfTriangleProps {
  dimensions: Geometry2D.Triangle.Dimensions;
  isAbsolute?: boolean;
  styles: {
    border?: PdfStyles.PdfBorderStyle;
    background?: PdfStyles.PdfBackgroundStyle;
  }
}

export interface PdfPolygonProps {
  dimensions: Geometry2D.Polygon.Dimensions;
  styles: {
    border?: PdfStyles.PdfBorderStyle;
    background?: PdfStyles.PdfBackgroundStyle;
  },
  jsPdfOptions?: {
    scale?: number[];
  }
}

export interface PdfLineProps {
  dimensions: Geometry2D.Line.Dimensions;
  styles: {
    color?: string;
    size?: number;
    dashSize?: number;
    dashGapSize?: number;
  }
}

export declare namespace PdfStyles {  
  interface PdfBorderStyle {
    borderColor?: string;
    borderSize?: number;
    borderDashSize?: number;
    borderDashGapSize?: number;
  }

  interface PdfBackgroundStyle {
    color?: string;
  }

  interface PdfTextStyles {
    fontStyle?: string;
    fontSize?: number;
    fontFamily?: string;
    color: string;
  }
}