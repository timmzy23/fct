import {
  Geometry2D,
  ReflectionHelper
  } from '@fct/sdk';
import { Pdf } from '../pdf';
import {
  PdfStyles,
  PdfTextAlign,
  PdfTextBaseline,
  PdfTextRotationDirection
  } from '../types';

export declare namespace PdfText {
  interface StartProps {
    length: number;
  }

  interface Props {
    dimensions: {
      start: ((props: StartProps) => 
        Geometry2D.Point.Dimensions) | 
        Geometry2D.Point.Dimensions;
      shiftY?: number;
    };
    styles?: {
      text?: PdfStyles.PdfTextStyles;
    };
    text: string,
    jsPdfOptions?: {
      angle?: number;
      align?: PdfTextAlign;
      baseline?: PdfTextBaseline;
      rotationDirection?: PdfTextRotationDirection;
      charSpace?: number;
      lineHeightFactor?: number;
      maxWidth?: number;
    }
  }
}

export function PdfText(props: PdfText.Props) {
  return (pdf: Pdf) => {
    const textStyles: PdfStyles.PdfTextStyles = props.styles && props.styles.text ? 
      props.styles.text : 
      {} as PdfStyles.PdfTextStyles;
    const text = props.text;
    const start = ReflectionHelper.isFunction(props.dimensions.start) ?
      (props.dimensions.start as any)({ length: pdf.jsPDF.getTextWidth(text) }) :
      props.dimensions.start;

    if (textStyles.color) {
      pdf.setTextColor(textStyles.color);
    }

    if (!textStyles.fontFamily && textStyles.fontStyle) {
      pdf.jsPDF.setFontStyle(textStyles.fontStyle);
    } else if (textStyles.fontFamily && textStyles.fontStyle) {
      pdf.jsPDF.setFont(textStyles.fontFamily, textStyles.fontStyle);
    }

    if (textStyles.fontSize) {
      pdf.jsPDF.setFontSize(pdf.unitToPt(textStyles.fontSize));
    }

    const startY = props.dimensions.shiftY ? 
        start.y + props.dimensions.shiftY : start.y;

    try {
      pdf.jsPDF.text(
        text,
        start.x,
        startY,
        props.jsPdfOptions);
    } catch (err) {
      console.error(err);
    }
  }
}
