import {
  Geometry2D,
  NumberHelper,
  ReflectionHelper
  } from '@fct/sdk';
import canvg from 'canvg';
import JsPDF from 'jspdf';
import { Color } from './color';
import {
  PageFormat,
  PageOrientation,
  PdfCanvasProps,
  PdfCircleShape,
  PdfCoord,
  PdfFontFace,
  PdfLineProps,
  PdfPolygonProps,
  PdfRectProps,
  PdfStyles,
  PdfSvgProps,
  PdfTextProps,
  PdfTriangleProps,
  PdfUnit
  } from './types';

export declare namespace Pdf {
  interface Props {
    unit: PdfUnit;
    pageFormat: PageFormat;
    orientation: PageOrientation;
    /**
     * Svg graphics points per unit
     */
    svgPpu: number;
  }
}

// Expose to global namespace because jsPdf is dump about that: :(
(window as any).canvg = canvg;

export class Pdf {

  /** Milimeters per inch */
  public static MM_PER_IN = 25.4;

  /** Points per inch */
  public static PT_PER_IN = 72;

  /** Point per milimeter */
  public static PT_PER_MM = Pdf.PT_PER_IN / Pdf.MM_PER_IN;

  public jsPDF: JsPDF;
  
  public pageNumber = 0;

  public static mmToPt(mm: number): number {
    return Pdf.PT_PER_MM * mm;
  }

  public static ptToMm(pt: number): number {
    return pt / Pdf.PT_PER_MM;
  }

  constructor(
    private props: Pdf.Props, 
    public theme: any,
    private downloadRawFont: (url: string) => Promise<any>
  ) {
    this.jsPDF = new JsPDF(
      props.orientation,
      props.unit,
      props.pageFormat,
      1.0
    );
  }


  public isOddPage() {
    return this.pageNumber % 2 === 1;
  }

  public addPage(count = 1) {
    for (let i = 0; i < count; i++) {
      this.pageNumber = this.pageNumber + 1;
      this.jsPDF.addPage();
    }
  }

  public registerFont(fontFace: PdfFontFace | PdfFontFace[]) {
    const fontFaces = Array.isArray(fontFace) ? fontFace : [fontFace];

    return Promise.all(fontFaces.map(({ url, fontName, fontFamily, fontStyle}) => 
        this.downloadFont(url)
            .then((fontEncodedString: string) => 
                this.jsPDF.addFileToVFS(fontName, fontEncodedString))
            .then(() => this.jsPDF.addFont(fontName, fontFamily, fontStyle))))
            .then(() => fontFace);
  }

  public setFont(fontFamily: string, fontStyle: string = 'normal') {
    this.jsPDF.setFont(fontFamily, fontStyle);
  }

  public addFont(postScriptName: string, fontName: string, fontStyle: string): this {
    this.jsPDF.addFont(postScriptName, fontName, fontStyle);

    return this;
  }

  public text(props: PdfTextProps): this {
    if (props.color) {
      this.setTextColor(props.color);
    }

    if (props.fontSize) {
      this.jsPDF.setFontSize(this.unitToPt(props.fontSize));
    }

    if (!props.fontFamily && props.fontStyle) {
      this.jsPDF.setFontStyle(props.fontStyle);
    } else if (props.fontFamily && props.fontStyle) {
      this.jsPDF.setFont(props.fontFamily, props.fontStyle);
    }

    const start = props.computeStart ?
        props.computeStart({
          length: this.jsPDF.getTextWidth(props.text),
        }) :
        props.start ||
        { x: 0, y: 0};

    this.jsPDF.text(
      props.text, 
      start.x, 
      start.y,
      props.jsPdfOptions);

    return this;
  }

  public setTextColor(color: string): this {
    const { red, green, blue } = Color.fromString(color);
    this.jsPDF.setTextColor(red, green, blue);

    return this;
  }

  public unitToPt(number: number): number {
    switch (this.props.unit) {
      case PdfUnit.MILIMETERS:
        return Pdf.mmToPt(number);
      default: 
        throw new Error(
          `Specific "unitToPt" method for "${this.props.unit}" not implemented!`);
    }
  }

  public ptToUnit(number: number): number {
    switch (this.props.unit) {
      case PdfUnit.MILIMETERS:
        return Pdf.ptToMm(number);
      default: 
        throw new Error(
          `"ptToUnit" method for "${this.props.unit}" not implemented!`);
    }
  }

  public svg(props: PdfSvgProps) {
    const ppu = props.ppu || this.props.svgPpu;
    const canvas = document.createElement('canvas');
    canvas.width = this.computeWidth(props.dimensions.width) * ppu;
    canvas.height = this.computeHeight(props.dimensions.height) * ppu;
    // Make it transparent
    const ctx = canvas.getContext('2d');
    if (props.backgroundColor) {
      ctx.fillStyle = props.backgroundColor;
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
    const svgStr = props.svg || props.renderSvg({
      canvasHeigh: canvas.height,
      canvasWidth: canvas.width,
      ppu
    });

    canvg(canvas, svgStr, {
      ignoreMouse: true,
      ignoreAnimation: true,
      ignoreDimensions: true,
      ignoreClear: true
    });

    this.canvas({
      canvas,
      dimensions: props.dimensions,
      compression: props.compression,
      rotation: props.rotation
    });
  }

  public canvas(props: PdfCanvasProps) {
    (this.jsPDF as any).addImage(
      props.canvas.toDataURL('image/jpeg', 1.0),
      props.dimensions.x,
      props.dimensions.y,
      this.computeWidth(props.dimensions.width),
      this.computeHeight(props.dimensions.height),
      props.compression,
      props.rotation);
  }

  public line(props: PdfLineProps) {
    if (props.styles) {
      if (props.styles.color) {
        this.jsPDF.setDrawColor(props.styles.color);
      }
      if (props.styles.size) {
        this.jsPDF.setLineWidth(props.styles.size);
      }
      if (props.styles.dashSize) {
        const dashArray = [props.styles.dashSize];
        if (props.styles.dashGapSize) {
          dashArray.push(props.styles.dashGapSize);
        }
        this.jsPDF['setLineDash'](dashArray);
      }
    }
    try {
      this.jsPDF.line(
        props.dimensions.start.x, 
        props.dimensions.start.y, 
        props.dimensions.end.x, 
        props.dimensions.end.y);
    } catch (err) {
      console.error(err);
    }
    
    this.jsPDF.setDrawColor('#000000');
    this.jsPDF['setLineDash']([]);
  }


  public triangle(props: PdfTriangleProps) {
    const path = [props.dimensions.a, props.dimensions.b, props.dimensions.c];
    const polygonDimensions = props.isAbsolute ? 
      { absolutePath: path } :  { path }
    this.polygon({
      ...props,
      dimensions: polygonDimensions
    });
  }

  public polygon(props: PdfPolygonProps) {
    const path = props.dimensions.path || props.dimensions.absolutePath
    if (!path) {
      throw new Error('Polygon requires "path" or "absolutePath"');
    }
    const start = Geometry2D.polygon.getStartPoint(path);
    const style = this.getFillStyle(props.styles.border, props.styles.background);
    const lines = props.dimensions.absolutePath ?
       Geometry2D.polygon
        .convertToRelatives(path, Geometry2D.polygon.EXCLUDE_START)
        .map((point) => [point.x, point.y]) :
      path
        .filter((point, ix) => ix > 0) // Skip first point
        .map((point) => [point.x, point.y]);
    const scale = props.jsPdfOptions && props.jsPdfOptions.scale ?
        props.jsPdfOptions.scale : undefined;

    this.setupBorderStyles(props.styles.border);
    this.setupBackgroundStyles(props.styles.background);

    this.jsPDF.lines(lines, start.x, start.y, scale, style, true);

    this.resetBorderStyles();
  }

  public setupBorderStyles(border?: PdfStyles.PdfBorderStyle) {
    if (!border) { return; }
    
    if (border.borderSize) {
      this.jsPDF.setLineWidth(border.borderSize);
    }

    if (border.borderDashSize) {
      const dashArray = [border.borderDashSize];
      if (border.borderDashGapSize) {
        dashArray.push(border.borderDashGapSize);
      }
      this.jsPDF['setLineDash'](dashArray);
    }

    if (border.borderColor) {
      this.jsPDF.setDrawColor(border.borderColor);
    }
  }
  
  public setupBackgroundStyles(background?: PdfStyles.PdfBackgroundStyle) {
    const backgroundColor = background && background.color ? background.color : undefined;

    if (backgroundColor) {
      this.jsPDF.setFillColor(backgroundColor);
    }
  }

  public getFillStyle(border?: PdfStyles.PdfBorderStyle, background?: PdfStyles.PdfBackgroundStyle): string {
    const backgroundColor = background && background.color ? background.color : undefined;
    const borderColor = border && border.borderColor ? border.borderColor : undefined;

    let style = '';

    if (backgroundColor && !borderColor) {
      style = 'F';
    } else if (!backgroundColor && borderColor) {
      style = 'S';
    } else if (backgroundColor && borderColor) {
      style = 'FD';
    } else {
      style = 'S';
    }

    return style;
  }

  public resetBorderStyles() {
    this.jsPDF['setLineDash']();
    this.jsPDF.setDrawColor('#000000');
  }

  public computeSize(width: number | string, sizeOf: number): number {
    if (ReflectionHelper.isNumber(width)) {
      return width as number;
    } else {
      const percent = NumberHelper.fromPercent(width as string);

      if (Number.isNaN(percent)) { 
        throw new Error(`Invalid width format: "${width}"`);
      }
      return (sizeOf / 100) * percent;
    }
  }

  public computeWidth(width: number | string): number {
    return this.computeSize(width, this.jsPDF.internal.pageSize.width);
  }

  public computeHeight(height: number | string): number {
    return this.computeSize(height, this.jsPDF.internal.pageSize.height);
  }


  public fillRect(props: PdfRectProps) {
    if (props.background) {
      this.jsPDF.setFillColor(props.background);
    }
    this.jsPDF.rect(
      props.dimensions.x,
      props.dimensions.y,
      this.computeWidth(props.dimensions.width),
      this.computeHeight(props.dimensions.height),
      'F');
  }

  public circle(props: PdfCircleShape) {
    if (props.backgroundColor) {
      this.jsPDF.setFillColor(props.backgroundColor);
    }
    if (props.borderColor) {
      this.jsPDF.setDrawColor(props.borderColor);
    }
    let style: string;

    if (props.backgroundColor && !props.borderColor) {
      style = 'F';
    } else if (!props.backgroundColor && props.borderColor) {
      style = 'S';
    } else if (props.backgroundColor && props.borderColor) {
      style = 'FD';
    } else {
      style = 'S';
    }
    if (props.borderSize) {
      this.jsPDF.setLineWidth(props.borderSize);
    }
    if (!props.start && !props.center) {
      throw new TypeError('Circle has to set eather start or center coordinates!');
    }
    const start: PdfCoord = props.center ? 
      props.center : {
        x: props.start!.x + props.radius,
        y: props.start!.y + props.radius
      };

    // jsPdf start x and y means circle center not the right top corner!
    this.jsPDF.circle(
      start.x, 
      start.y,
      props.radius, 
      style);
  }

  public setFillColor(color: string) {
    const { red, green, blue } = Color.fromString(color);

    this.jsPDF.setFillColor(red, green, blue);
  }

  public openNewWindow() {
    this.jsPDF.output('dataurlnewwindow');
  }

  /**
   * Downloads font file and resolve it into base64 encoded string.
   */
  public downloadFont(fontUrl: string): Promise<string> {
    return this.downloadRawFont(fontUrl);
  }

  public render(component: (pdf: Pdf) => void) {
    component(this);
  }
}