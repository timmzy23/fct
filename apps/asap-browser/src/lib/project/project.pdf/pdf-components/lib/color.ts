export declare namespace Color {
  interface Model {
    red: number;
    blue: number;
    green: number;
    alpha: number;
  }
}

export class Color {

  public static HEX_COLOR_LENGTHS = [
    4, // single digit for each color
    7, // 2 digits for each color
    9, // with alpha canal
  ]

  constructor( 
    public red: number,
    public green: number,
    public blue: number,
    public alpha: number = 1
  ) {}

  /**
   * @param color 
   */
  public static fromString(color: string): Color {
    if (color[0] === '#') {
      return Color.fromHexString(color);
    } else if (/^(rgb)/.test(color)) {
      return Color.fromRgbString(color);
    } else if (/^(rgba)/.test(color)) {
      return Color.fromRgbaString(color);
    } else {
      throw new Error(`Unknown color string format: "${color}"`);
    }
  }

  public static fromHexString(color: string): Color {
    if (!Color.HEX_COLOR_LENGTHS.includes(color.length)) {
      throw new Error(
          `Invalid hex color string length "${color.length}". ` +
          `Expected length: ${Color.HEX_COLOR_LENGTHS.join(',')}`);
    } 
    // Color string starts by #!
    const [red, green, blue, alpha] = [
      color.length === 3 ? color[1] : color.slice(1, 3), // red
      color.length === 3 ? color[2] : color.slice(3, 5), // green
      color.length === 3 ? color[3] : color.slice(5, 7), // blue
      color.length === 8 ? color.slice(-2) : 1 // alpha
    ].map(Color.hecToDec);

    return new Color(red, green, blue, alpha);
  }

  public static fromRgbString(color: string): Color {
    if (/^rgb\((\d{1,3}\,?\s?){3}\)$/.test(color)) {
      throw new Error(
        `Invalid rgb color string format: "${color}"! ` +
        'Expected: "rgb(rrr, ggg, bbb)".' 
      );
    }

    const [red, green, blue] = color
        .match(/(\d{1,3})/g)!
        .map((num: string) => Number.parseInt(num, 10));

    return new Color(red, green, blue);
  }

  public static fromRgbaString(color: string): Color {
    if (/^rgba\((\d{1,3},?\s?){3},\s?[1|0](\.\d+)?\)$/.test(color)) {
      throw new Error(
        `Invalid rgb color string format: "${color}"! ` +
        'Expected: "rgb(rrr, ggg, bbb, aaa)".' 
      );
    }

    const [red, green, blue, alpha] = color
        .replace(/^rgba\(((\d{1,3},?\s?){3}),\s?([1|0](\.\d+)?)\)$/g, '$1, $3')!
        .split(',')
        .map((num: string) => Number.parseFloat(num)); // alpha is in range 0-1

    return new Color(red, green, blue, alpha);
  }

  private static hecToDec(hecNum: string): number {
    return Number.parseInt(hecNum, 16);
  }
}
