import { Geometry2D } from '@fct/sdk';
import { Pdf } from '../pdf';
import { PdfStyles } from '../types';

export declare namespace PdfRectangle {
  interface Props {
    dimensions: Geometry2D.Rect.Dimensions;
    styles?: {
      background?: PdfStyles.PdfBackgroundStyle;
      border?: PdfStyles.PdfBorderStyle;
    }
  }
}

export function PdfRectangle(props: PdfRectangle.Props) {
  
  return (pdf: Pdf) => {
    let fillStyle;

    if (props.styles) {
      fillStyle = pdf.getFillStyle(props.styles.border, props.styles.background);
      pdf.setupBackgroundStyles(props.styles.background);
      pdf.setupBorderStyles(props.styles.border);
    }
    const startY = props.dimensions.shiftY ?
        props.dimensions.start.y + props.dimensions.shiftY :
        props.dimensions.start.y;

    try {
      pdf.jsPDF.rect(
        props.dimensions.start.x,
        startY,
        pdf.computeWidth(props.dimensions.width),
        pdf.computeHeight(props.dimensions.height),
        fillStyle
      );
    } catch(err) {
      console.error(err);
    }
    pdf.resetBorderStyles();
  }
}
