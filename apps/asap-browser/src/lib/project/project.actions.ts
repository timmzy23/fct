import { FluxStandardAction } from '@fct/sdk-react';
import { NavigateToCommand } from '../common';

enum PROJECT_ACTION_TYPES {
  OPEN_PROJECT_EXPLORER = 'cmd.projectExplorer.open',
  CLOSE_PROJECT_EXPLORER = 'cmd.projectExplorer.close',
}

export declare namespace ProjectNavigateToCommand {
  type ACTION_TYPE = NavigateToCommand.ACTION_TYPE;
  type Action = NavigateToCommand.Action;
  type Payload = NavigateToCommand.Payload;
}

export class ProjectNavigateToCommand {
  public static ACTION_TYPE: ProjectNavigateToCommand.ACTION_TYPE = 
    NavigateToCommand.ACTION_TYPE;

  public static goToProject(projectId: string, projectName?: string) {
    return NavigateToCommand.create(`/editor/project/${projectId}`, {
      state: { projectName }
    });
  }

  public static goToNewProject() {
    return NavigateToCommand.create('/editor/project/new');
  }

  public static goToProjectRender(projectId: string) {
    return NavigateToCommand.create(`/editor/project/${projectId}/render`);
  }

  public static goToProjectImport(projectId: string) {
    return NavigateToCommand.create(`/editor/project/${projectId}/import`);
  }
}

export declare namespace ProjectExplorerOpenCommand {
  type ACTION_TYPE = PROJECT_ACTION_TYPES.OPEN_PROJECT_EXPLORER;
  type Action = FluxStandardAction.Action<ACTION_TYPE>
}

export class ProjectExplorerOpenCommand {
  public static ACTION_TYPE: ProjectExplorerOpenCommand.ACTION_TYPE = 
    PROJECT_ACTION_TYPES.OPEN_PROJECT_EXPLORER;

  public static create(): ProjectExplorerOpenCommand.Action {
    return FluxStandardAction.create(ProjectExplorerOpenCommand.ACTION_TYPE);
  }
}

export declare namespace ProjectExplorerCloseCommand {
  type ACTION_TYPE = PROJECT_ACTION_TYPES.CLOSE_PROJECT_EXPLORER;
  type Action = FluxStandardAction.Action<ACTION_TYPE>
}

export class ProjectExplorerCloseCommand {
  public static ACTION_TYPE: ProjectExplorerCloseCommand.ACTION_TYPE = 
    PROJECT_ACTION_TYPES.CLOSE_PROJECT_EXPLORER;

  public static create(): ProjectExplorerCloseCommand.Action {
    return FluxStandardAction.create(ProjectExplorerCloseCommand.ACTION_TYPE);
  }
}
