export const PROJECT_MSG_RENDER_BTN = {
  id: 'projects.render.btn.render'
}
export const PROJECT_MSG_RENDER_SEPARATORS_LABEL = {
  id: 'projects.render.label.separators'
}
export const PROJECT_MSG_RENDER_PARAMETERS_LABEL = {
  id: 'projects.render.label.parameters'
}