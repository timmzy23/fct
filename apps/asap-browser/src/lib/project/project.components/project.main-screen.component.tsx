import {
  Heading,
  TextFontSize,
  TextLineHeight
  } from '@fct/ui-react';
import * as React from 'react';
import {
  Route,
  RouteComponentProps,
  Switch,
  useRouteMatch
  } from 'react-router';
import { useProject } from '../project.hooks';
import { ProjectCreateScreen } from './create';
import { ProjectDetailScreen } from './detail';
import { ProjectImportScreen } from './import/project.import-screen.component';
import { ProjectInfoScreen } from './info/project.info-screen';
import { ProjectRenderScreen } from './render/project.render-screen.component';
import { ProjectSidebar } from './sidebar/project.sidebar.component';

const styles = require('./project.main-screen.component.scss');

export declare namespace ProjectMainScreen {
  type Component = React.FunctionComponent;
}

export const ProjectMainScreen: ProjectMainScreen.Component = (props) => {
  const match = useRouteMatch();
  const projectId: string = match.params['projectId'];
  const project = useProject(projectId);

  return (
    <div className={styles.host}>
      <ProjectSidebar 
          projectId={projectId}
          project={project} />
      <div>
        <Heading.H2 
          className={styles.projectHeading}
          lineHeight={TextLineHeight.LARGE}>
          {project && project.projectName}
        </Heading.H2>
        <Switch>
          <Route 
            path={`${match.path}/render`} 
            exact>
            {project && <ProjectRenderScreen 
                project={project}
                projectId={projectId}/>}
          </Route>
          <Route 
            path={`${match.path}/import`} 
            exact>
            {project && <ProjectImportScreen
                project={project}
                projectId={projectId}/>}
          </Route>
          <Route 
            path={`${match.path}/`} 
            exact>
            {project && <ProjectInfoScreen project={project} />}
          </Route>
        </Switch>
      </div>
    </div>
  );
};

ProjectMainScreen.displayName = 'ProjectMainScreen';

ProjectMainScreen.defaultProps = {};
