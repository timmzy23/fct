import {
  Button,
  Heading,
  IntlMessage
  } from '@fct/ui-react';
import * as React from 'react';
import {
  RouteComponentProps,
  useLocation,
  useRouteMatch
  } from 'react-router';
import {
  useProject,
  useProjectPdfRenderer
  } from '../../project.hooks';

// const styles = require('./project.detail-screen.component.scss');

export declare namespace ProjectDetailScreen {
  interface Props {
    title: IntlMessage.Message;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectDetailScreen: ProjectDetailScreen.Component = (props) => {
  return (
    <article>
      <Heading.H1>
        <IntlMessage message={props.title}/>
      </Heading.H1>
      {props.children}
    </article>
  );
};

ProjectDetailScreen.displayName = 'ProjectDetailScreen';

ProjectDetailScreen.defaultProps = {};
