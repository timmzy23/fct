import { Project } from '@fct/asap-interfaces';
import * as React from 'react';
import { useGoToProject } from '../../project.hooks';
import { ProjectRenderForm } from './project.render-form.component';

const styles = require('./project.render-screen.component.scss');

export declare namespace ProjectRenderScreen {
  interface Props {
    project: Project;
    projectId: string;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectRenderScreen: ProjectRenderScreen.Component = (props) => {
  const goToProjectInfo = useGoToProject(props.projectId);

  return (
    <div className={styles.host}>
      <ProjectRenderForm 
        onCancel={goToProjectInfo}
        project={props.project} />
    </div>
  );
};

ProjectRenderScreen.displayName = 'ProjectRenderScreen';

ProjectRenderScreen.defaultProps = {};
