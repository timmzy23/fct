import { Project } from '@fct/asap-interfaces';
import { useRefByCallback } from '@fct/sdk-react';
import {
  CheckboxField,
  FormBasic,
  FormControl,
  FormGroup,
  Heading
  } from '@fct/ui-react';
import * as React from 'react';
import { useProjectPdfRenderer } from '../../project.hooks';
import {
  PROJECT_MSG_RENDER_BTN,
  PROJECT_MSG_RENDER_PARAMETERS_LABEL,
  PROJECT_MSG_RENDER_SEPARATORS_LABEL
  } from '../../project.messages';

// const styles = require('./project.render-form.component.scss');

export declare namespace ProjectRenderForm {
  interface Props {
    project: Project;
    onCancel: () => void;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectRenderForm: ProjectRenderForm.Component = (props) => {
  const form = useRefByCallback(() => new FormGroup('projectRender', {
    'separators': new FormControl('separators', true),
    'parameters': new FormControl('parameters', true),
  })).current;
  const pdfRenderer = useProjectPdfRenderer();

  const handleSubmit = React.useCallback((payload: any) => {
    if (props.project) {
      pdfRenderer.render(props.project, payload);
    }
  }, [props.project]);

  return (
    <FormBasic
      form={form}
      submitBtn={PROJECT_MSG_RENDER_BTN}
      onReset={props.onCancel}
      onSubmit={handleSubmit}
    >
      <Heading.H4>Include:</Heading.H4>

      <CheckboxField 
        control={form.get('separators') as FormControl}
        labelIntl={PROJECT_MSG_RENDER_SEPARATORS_LABEL}
      />
      <CheckboxField 
        control={form.get('parameters') as FormControl}
        labelIntl={PROJECT_MSG_RENDER_PARAMETERS_LABEL}
      />
    </FormBasic>
  );
};

ProjectRenderForm.displayName = 'ProjectRenderForm';

ProjectRenderForm.defaultProps = {};
