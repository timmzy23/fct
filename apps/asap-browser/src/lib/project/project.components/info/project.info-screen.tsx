import { Project } from '@fct/asap-interfaces';
import {
  Text,
  TextFontStyle
  } from '@fct/ui-react';
import * as React from 'react';

export declare namespace ProjectInfoScreen {
  interface Props {
    project: Project;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectInfoScreen: ProjectInfoScreen.Component = (props) => {
  return (
    <div>
      <Text fontStyle={TextFontStyle.LIGHT}>Client:</Text> <Text>{props.project.clientName}</Text>
    </div>
  );
};

ProjectInfoScreen.displayName = 'ProjectInfoScreen';

ProjectInfoScreen.defaultProps = {};
