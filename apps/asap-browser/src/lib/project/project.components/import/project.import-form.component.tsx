import { ProjectDocumentNode } from '@fct/asap-interfaces';
import {
  useRefByCallback,
  useSubscription
  } from '@fct/sdk-react';
import {
  FileField,
  FormBasic,
  FormControl,
  FormGroup,
  InputField,
  Validators
  } from '@fct/ui-react';
import * as React from 'react';
import {
  BehaviorSubject,
  from
  } from 'rxjs';
import {
  filter,
  map,
  switchMap
  } from 'rxjs/operators';
import { useProjectXmlImportParser } from '../../project.hooks';

const styles = require('./project.import-form.component.scss');

export declare namespace ProjectImportForm {
  interface Props {
    onCancel: () => void;
    onSubmit: (payload: SubmitPayload) => void;
  }
  type Component = React.FunctionComponent<Props>;

  interface FormPayload {
    projectImport: FileList,
    projectName: string,
    projectId: string,
    projectVersion: string,
    projectState: string,
    projectScenario: string,
    clientName: string,
  }

  interface SubmitPayload {
    projectInfo: {
      projectName: string,
      projectId: string,
      projectVersion: string,
      projectState: string,
      projectScenario: string,
      clientName: string,
    },
    nodes: ProjectDocumentNode[];
  }
}

function useProjectImportField(
  fieldName: string, 
  document$: BehaviorSubject<ProjectDocumentNode[] | null>
): FormControl {
  const parser = useProjectXmlImportParser();
  const projectImportField = 
      useRefByCallback(() => new FormControl(fieldName, '')).current;

  const parseFile = React.useCallback((file: File) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (() => {
        const textXml = reader.result as string;
        try {
          const document = parser.parse(textXml);

          resolve(document);
        } catch (err) {
          reject(err);
        }
      });
      reader.readAsText(file);
    });
  }, []);

  useSubscription(() => projectImportField.valueChanges
      .pipe(
        map((files: FileList) => files.item(0)),
        filter((file: File | null) => !!file),
        switchMap((file: File) => from(parseFile(file))))
      .subscribe(
        (document: any) => document$.next(document),
        (err: any) => console.error(err)),
    [projectImportField]);

  return projectImportField;
}

function useCreateProjectForm(document$: BehaviorSubject<ProjectDocumentNode[] | null>) {
  const projectImportField = useProjectImportField('projectImport', document$);

  const form = useRefByCallback<FormGroup<ProjectImportForm.FormPayload>>(() => {
    return new FormGroup('projectCreate', {
      projectImport: projectImportField,
      projectName: new FormControl('projectName', '', [Validators.required]),
      projectId: new FormControl('projectId', '', [Validators.required]),
      projectVersion: new FormControl('projectVersion', ''),
      projectState: new FormControl('projectState', ''),
      projectScenario: new FormControl('projectScenario', ''),
      clientName: new FormControl('clientName', ''),
    })
  }).current;

  useSubscription(
    () => document$
      .pipe(
        filter((document) => !!document),
        map((document) => document.find((node) => !!node.project)),
        filter((documentNode) => !!documentNode)
      )
      .subscribe((node: ProjectDocumentNode) => {
        form.get('projectName').setValue(node.project.projectName);
        form.get('clientName').setValue(node.project.clientName);
        form.get('projectId').setValue(node.project.projectId);
        form.get('projectScenario').setValue(node.project.projectScenario);
        form.get('projectState').setValue(node.project.projectState);
        form.get('projectVersion').setValue(node.project.projectVersion);
      }), 
    []);

    return form;
}

export const ProjectImportForm: ProjectImportForm.Component = (props) => {
  const document$ = useRefByCallback<BehaviorSubject<ProjectDocumentNode[] | null>>(
      () => new BehaviorSubject(null)).current;
  const form = useCreateProjectForm(document$);

  const handleSubmit = React.useCallback((payload: ProjectImportForm.FormPayload) => {
    props.onSubmit({
      projectInfo: {
        projectName: payload.projectName,
        clientName: payload.clientName,
        projectId: payload.projectId,
        projectVersion: payload.projectVersion,
        projectState: payload.projectState,
        projectScenario: payload.projectScenario
      },
      nodes: document$.getValue()
    });
  }, [document$, props.onSubmit]);

  return (
    <FormBasic
      form={form}
      onSubmit={handleSubmit}
      onReset={props.onCancel}
      formContentClassName={styles.formContent}
    >
      <FileField
        control={form.get('projectImport') as FormControl}
        labelIntl="Import"
      />
      <InputField 
        control={form.get('projectName') as FormControl}
        labelIntl="Project name"/>
      <InputField 
        control={form.get('projectId') as FormControl}
        labelIntl="Project ID"/>
      <InputField 
        control={form.get('projectVersion') as FormControl}
        labelIntl="Project version"/>
      <InputField 
        control={form.get('projectState') as FormControl}
        labelIntl="Project state"/>
      <InputField 
        control={form.get('projectScenario') as FormControl}
        labelIntl="Project scenario"/>
    </FormBasic>
  );
};

ProjectImportForm.displayName = 'ProjectImportForm';

ProjectImportForm.defaultProps = {};
