import { Project } from '@fct/asap-interfaces';
import * as React from 'react';
import {
  useGoToProject,
  useProjectApi
  } from '../../project.hooks';
import { ProjectImportForm } from './project.import-form.component';

const styles = require('./project.import-screen.component.scss');

export declare namespace ProjectImportScreen {
  interface Props {
    projectId: string;
    project: Project;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectImportScreen: ProjectImportScreen.Component = (props) => {
  const goToProject = useGoToProject(props.projectId);
  const projectApi = useProjectApi();
  const handleSubmit = React.useCallback((payload: ProjectImportForm.SubmitPayload) => {
    projectApi.updateProject({
      ...payload,
      projectId: props.projectId
    }).subscribe({
      next: () => goToProject(),
      error: (err) => console.error(err),
    })
  }, [projectApi, props.projectId]);

  return (
    <div className={styles.host}>
      <ProjectImportForm 
        onCancel={goToProject}
        onSubmit={handleSubmit}
      />
    </div>
  );
};

ProjectImportScreen.displayName = 'ProjectImportScreen';

ProjectImportScreen.defaultProps = {};
