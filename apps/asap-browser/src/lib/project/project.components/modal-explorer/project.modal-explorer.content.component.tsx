import { ProjectHeader } from '@fct/asap-interfaces';
import { useCommands } from '@fct/sdk-react';
import {
  useOnKeyboardEscapeEvent,
  useVerticlaScrollBarClassName
  } from '@fct/ui-react';
import * as React from 'react';
import {
  ProjectExplorerCloseCommand,
  ProjectNavigateToCommand
  } from '../../project.actions';
import { useProjectList } from '../../project.hooks';
import { ProjectModalExplorerSidebar } from './project.modal-explorer.sidebar.component';

const styles = require('./project.modal-explorer.content.component.scss');

export declare namespace ProjectModalExplorerContent {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectModalExplorerContent: ProjectModalExplorerContent.Component = (props) => {
  const projectList = useProjectList();
  const scrollbarClassName = useVerticlaScrollBarClassName();
  const commands$ = useCommands();
  const handleSelect = React.useCallback(
    (project: ProjectHeader) => {
      commands$.emit(ProjectNavigateToCommand.goToProject(
          project._id,
          project.projectName));
      commands$.emit(ProjectExplorerCloseCommand.create());
    }, [commands$]);
  useOnKeyboardEscapeEvent(() => 
      commands$.emit(ProjectExplorerCloseCommand.create()));

  return (
    <article className={`${styles.host} ${scrollbarClassName}`}>
      <ProjectModalExplorerSidebar 
          projectList={projectList}
          onSelect={handleSelect} />
    </article>
  );
};

ProjectModalExplorerContent.displayName = 'ProjectModalExplorerContent';

ProjectModalExplorerContent.defaultProps = {};
