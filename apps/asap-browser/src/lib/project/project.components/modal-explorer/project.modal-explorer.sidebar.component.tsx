import { ProjectHeader } from '@fct/asap-interfaces';
import { CurrentIndexSubject } from '@fct/sdk';
import {
  useRefByCallback,
  useSubscription
  } from '@fct/sdk-react';
import {
  FolderIcon,
  IconColor,
  IconSize,
  Text,
  TextBlock,
  TextFontSize,
  TextFontStyle,
  useOnKeyboardArrowDownEvent,
  useOnKeyboardArrowUpEvent,
  useOnKeyboardEnterEvent
  } from '@fct/ui-react';
import * as React from 'react';

const styles = require('./project.modal-explorer.sidebar.component.scss');

export declare namespace ProjectModalExplorerSidebar {
  interface Props {
    projectList: ProjectHeader[];
    onSelect: (project: ProjectHeader) => void;
  }
  type Component = React.FunctionComponent<Props>;
}


export const ProjectModalExplorerSidebar: ProjectModalExplorerSidebar.Component = (props) => {
  const currentIx$ = useRefByCallback(
    () => new CurrentIndexSubject(props.projectList.length)).current;
  const [currentIx, setCurrentIx] = React.useState(currentIx$.currentIx);

  const handleArrowUp = React.useCallback(
      () => currentIx$.jumpToPrev(),
      [currentIx$]);
  const handleArrowDown = React.useCallback(
    () => currentIx$.jumptToNext(),
    [currentIx$]);
  const handleSelectOnEnter = React.useCallback(
    () => props.onSelect(props.projectList[currentIx$.currentIx]),
    [currentIx$, props.onSelect, props.projectList]
  )

  if (props.projectList.length !== currentIx$.length) {
    currentIx$.length = props.projectList.length;
  }

  const handleClick = React.useCallback((event: React.MouseEvent) => {
    const projectId = (event.currentTarget as HTMLElement).dataset.projectId;

    const project = props.projectList
        .find((currProject) => currProject._id === projectId);

    if (project) {
      props.onSelect(project);
    }
  }, [props.projectList, props.onSelect]);

  useOnKeyboardArrowDownEvent(handleArrowDown);
  useOnKeyboardArrowUpEvent(handleArrowUp);
  useOnKeyboardEnterEvent(handleSelectOnEnter);

  useSubscription(
    () => currentIx$.subscribe((currIx) => setCurrentIx(currIx)),
    [setCurrentIx, currentIx$]);

  return (
    <ul>
      {props.projectList.map((project, ix) => (
        <li 
          key={project._id}
          className={`${styles.listItem} ${ix === currentIx ? styles.selected : ''}`}
          data-project-id={project._id}
          onClick={handleClick}>
          <div className={styles.listIcon}>
            <FolderIcon 
              size={IconSize.SMALL} 
              iconFillColor={IconColor.SECONDARY}/>  
          </div>  
          
          <Text
            fontSize={TextFontSize.MEDIUM}
            fontStyle={TextFontStyle.LIGHT}
          >
            {project.projectName}
          </Text>
        </li>
      ))}
    </ul>);
};

ProjectModalExplorerSidebar.displayName = 'ProjectModalExplorerSidebar';

ProjectModalExplorerSidebar.defaultProps = {};
