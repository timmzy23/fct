import {
  actionOf,
  AnyAction,
  useCommands,
  useSubscription
  } from '@fct/sdk-react';
import { QuickModal } from '@fct/ui-react';
import * as React from 'react';
import { map } from 'rxjs/operators';
import { NavigateToCommand } from '../../../common';
import {
  ProjectExplorerCloseCommand,
  ProjectExplorerOpenCommand
  } from '../../project.actions';
import { ProjectModalExplorerContent } from './project.modal-explorer.content.component';

// const styles = require('./project.modal-explorer.component.scss');

export declare namespace ProjectModalExplorer {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectModalExplorer: ProjectModalExplorer.Component = (props) => {
  const command$ = useCommands();
  const [visible, setVisible] = React.useState(false);

  useSubscription(
    () => command$
      .pipe(
        actionOf(ProjectExplorerOpenCommand, ProjectExplorerCloseCommand),
        map((cmd: AnyAction) => {
          switch (cmd.type) {
            case ProjectExplorerOpenCommand.ACTION_TYPE:
              return true;
            case ProjectExplorerCloseCommand.ACTION_TYPE:
              return false;
          }
        }))
      .subscribe((isVisible) => setVisible(isVisible)),
    []);

  return (
    <QuickModal 
      visible={visible}
      render={() => (
        <ProjectModalExplorerContent />
      )} />
  );
};

ProjectModalExplorer.displayName = 'ProjectModalExplorer';

ProjectModalExplorer.defaultProps = {};
