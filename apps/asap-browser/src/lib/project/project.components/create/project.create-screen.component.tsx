import { useDispatchCommand } from '@fct/sdk-react';
import {
  Heading,
  TextColor,
  TextLineHeight
  } from '@fct/ui-react';
import * as React from 'react';
import { NavigateToCommand } from '../../../common';
import { ProjectNavigateToCommand } from '../../project.actions';
import { ProjectCreateForm } from './project.create-form.component';

const styles = require('./project.create-screen.component.scss');

export declare namespace ProjectCreateScreen {
  interface Props {
    
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectCreateScreen: ProjectCreateScreen.Component = (props) => {
  const dispatchCommand = useDispatchCommand();
  const handleFormCancel = React.useCallback(() => {
    dispatchCommand(NavigateToCommand.goToEditorSplashScreen());
  }, [dispatchCommand]);
  const handleFormSuccess = React.useCallback((payload: ProjectCreateForm.SuccessPayload) => {
    dispatchCommand(ProjectNavigateToCommand.goToProject(
      payload.id,
      payload.projectName
    ));
  }, [dispatchCommand]);


  return (
    <article className={styles.host}>
      <header>
        <Heading.H1 lineHeight={TextLineHeight.EXTRA_LARGE}>
          Create new ASaP
        </Heading.H1>
      </header>
      <section>
        <ProjectCreateForm 
            onCancel={handleFormCancel}
            onSuccess={handleFormSuccess}/>
      </section>
    </article>
  );
};

ProjectCreateScreen.displayName = 'ProjectCreateScreen';

ProjectCreateScreen.defaultProps = {};
