import * as React from 'react';
import { ProjectSidebarHeading } from './project.sidebar-heading.component';

const styles = require('./project.sidebar-link.component.scss');

export declare namespace ProjectSidebarLink {
  interface Props {
    onClick?: () => void;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectSidebarLink: ProjectSidebarLink.Component = (props) => {
  return (
    <li className={styles.host} onClick={props.onClick}>
      <ProjectSidebarHeading>
        {props.children}
      </ProjectSidebarHeading>
    </li>
  );
};

ProjectSidebarLink.displayName = 'ProjectSidebarLink';

ProjectSidebarLink.defaultProps = {};
