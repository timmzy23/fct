import {
  Project,
  ProjectDocumentCollection
  } from '@fct/asap-interfaces';
import { Text } from '@fct/ui-react';
import * as React from 'react';
import { ProjectSidebarHeading } from './project.sidebar-heading.component';

const styles = require('./project.sidebar-param-tree.component.scss');

export declare namespace ProjectSidebarParamTree {
  interface Props {
    project: Project;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectSidebarParamTree: ProjectSidebarParamTree.Component = (props) => {
  const project = React.useMemo(
      () => props.project && new ProjectDocumentCollection(props.project),
      [props.project]);
  return (
    <li>
      <ProjectSidebarHeading>
        PARAMETERS
      </ProjectSidebarHeading>
      <ul className={styles.paramList}>
        {project && project.getFlattenChildren().map((node) => 
          <li><Text>{node.xPath}: {node.name}</Text></li>)}
      </ul>
    </li>
  );
};

ProjectSidebarParamTree.displayName = 'ProjectSidebarParamTree';

ProjectSidebarParamTree.defaultProps = {};
