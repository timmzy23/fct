import {
  Heading,
  TextFontSize
  } from '@fct/ui-react';
import * as React from 'react';

const styles = require('./project.sidebar-heading.component.scss');

export declare namespace ProjectSidebarHeading {
  interface Props {
    className?: string;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectSidebarHeading: ProjectSidebarHeading.Component = (props) => {
  return (
    <Heading.H3 
      fontSize={TextFontSize.SMALL}
      className={`${styles.host} ${props.className}`}>
        {props.children}
    </Heading.H3>
  );
};

ProjectSidebarHeading.displayName = 'ProjectSidebarHeading';

ProjectSidebarHeading.defaultProps = {};
