import { Project } from '@fct/asap-interfaces';
import {
  Heading,
  TextFontSize,
  TextFontStyle,
  TextLineHeight
  } from '@fct/ui-react';
import * as React from 'react';
import {
  useGoToProject,
  useGoToProjectImport,
  useGoToProjectRender
  } from '../../project.hooks';
import { ProjectSidebarLink } from './project.sidebar-link.component';
import { ProjectSidebarParamTree } from './project.sidebar-param-tree.component';

const styles = require('./project.sidebar.component.scss');

export declare namespace ProjectSidebar {
  interface Props {
    projectId: string;
    project: Project;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ProjectSidebar: ProjectSidebar.Component = (props) => {
  const goToRender = useGoToProjectRender(props.projectId);
  const goToProject = useGoToProject(props.projectId);
  const goToProjectImport = useGoToProjectImport(props.projectId);

  return (
    <ul className={styles.host}>
      <Heading.H2 
        fontStyle={TextFontStyle.THIN}
        fontSize={TextFontSize.SMALL}
        lineHeight={TextLineHeight.LARGE}
      >
        Project Explorer
      </Heading.H2>
      <ProjectSidebarLink onClick={goToProject}>
        PROJECT INFO
      </ProjectSidebarLink>
      <ProjectSidebarLink onClick={goToProjectImport}>
        PROJECT IMPORT
      </ProjectSidebarLink>
      <ProjectSidebarLink onClick={goToRender}>
        PROJECT RENDER
      </ProjectSidebarLink>
      {props.project && <ProjectSidebarParamTree project={props.project} />}
    </ul>
  );
};

ProjectSidebar.displayName = 'ProjectSidebar';

ProjectSidebar.defaultProps = {};
