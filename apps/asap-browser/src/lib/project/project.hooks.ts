import {
  Project,
  ProjectHeader
  } from '@fct/asap-interfaces';
import {
  FactoryProvider,
  HttpClient,
  useCommands,
  useDispatchCommand,
  useInjector,
  useSubscription
  } from '@fct/sdk-react';
import { useOnKeyboardOpenEvent } from '@fct/ui-react';
import * as React from 'react';
import {
  ProjectExplorerOpenCommand,
  ProjectNavigateToCommand
  } from './project.actions';
import { PROJECT_XML_SCHEMA } from './project.model';
import {
  ProjectApiService,
  ProjectXmlParserService
  } from './project.services';
import { ProjectPdfRendererService } from './project.services/project.pdf-renderer.service';

export function useProjectXmlImportParser(): ProjectXmlParserService {
  const injector = useInjector();

  if (!injector.hasProvider(ProjectXmlParserService)) {
    injector.use(new FactoryProvider(
      ProjectXmlParserService,
      () => new ProjectXmlParserService(PROJECT_XML_SCHEMA)
    ));
  }

  return injector.inject(ProjectXmlParserService) as ProjectXmlParserService;
}

export function useProjectApi(): ProjectApiService {
  const injector = useInjector();

  if (!injector.hasProvider(ProjectApiService)) {
    injector.use(new FactoryProvider(
      ProjectApiService,
      (http: HttpClient) => new ProjectApiService(http),
      [HttpClient]
    ));
  }

  return injector.inject(ProjectApiService) as ProjectApiService;
}

export function useProjectList(): ProjectHeader[] {
  const projectApi = useProjectApi();
  const [projectList, setProjectList] = React.useState([]);

  useSubscription(
    () => projectApi
      .getProjectHeaders()
      .subscribe(({ items }) => setProjectList(items)),
    [projectApi, setProjectList]);

  return projectList;
}

/**
 * Listen for open keyboard shortcut event and
 * open project explorer modal.
 */
export function useOpenProjectExplorerShortcut() {
  const commands$ = useCommands();

  const handleKeyboardOpenEvt = React.useCallback(
    (evt: KeyboardEvent) => {
      evt.preventDefault();
      commands$.emit(ProjectExplorerOpenCommand.create());
    }, 
    [commands$]);

  useOnKeyboardOpenEvent(handleKeyboardOpenEvt);
}

export function useProjectPdfRenderer() {
  const injector = useInjector();

  if (!injector.hasProvider(ProjectPdfRendererService)) {
    injector.use(new FactoryProvider(
      ProjectPdfRendererService,
      (http: HttpClient) => new ProjectPdfRendererService(http),
      [HttpClient]
    ));
  }

  return injector.inject(ProjectPdfRendererService) as ProjectPdfRendererService;
}

export function useProject(projectId: string): Project | null {
  const projects = useProjectApi();
  const [ project, setProject ] = React.useState(null);

  useSubscription(
    () => projects.getProject(projectId).subscribe({
      next: (currProject) => setProject(currProject),
      error: (err) => console.error(err)
    }),
    [projectId]);

  return project;
}

export function useGoToProject(projectId: string) {
  const dispatch = useDispatchCommand();

  return React.useCallback(() => {
    dispatch(ProjectNavigateToCommand.goToProject(projectId));
  }, [dispatch, projectId]);
}

export function useGoToProjectRender(projectId: string) {
  const dispatch = useDispatchCommand();

  return React.useCallback(() => {
    dispatch(ProjectNavigateToCommand.goToProjectRender(projectId));
  }, [dispatch, projectId]);
}

export function useGoToProjectImport(projectId: string) {
  const dispatch = useDispatchCommand();

  return React.useCallback(() => {
    dispatch(ProjectNavigateToCommand.goToProjectImport(projectId));
  }, [dispatch, projectId]);
}
