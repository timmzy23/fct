import { FluxStandardAction } from '@fct/sdk-react';
import { IntlMessage } from '@fct/ui-react';
export enum COMMON_ACTION_TYPES {
  NAVIGATE_TO_CMD = 'app.navigateTo.cdm'
}


export declare namespace NavigateToCommand {
  type ACTION_TYPE = COMMON_ACTION_TYPES.NAVIGATE_TO_CMD;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    path: string,
    opts?: Opts
  }

  interface Opts  {
    queryParams?: any,
    state?: any
  }

  interface MessageState {
    title?: IntlMessage.Message;
    subtitle?: IntlMessage.Message;
    text?: IntlMessage.Message;
  }
}

export class NavigateToCommand {
  public static ACTION_TYPE: NavigateToCommand.ACTION_TYPE = 
      COMMON_ACTION_TYPES.NAVIGATE_TO_CMD;

  public static create(path: string, opts?: NavigateToCommand.Opts): NavigateToCommand.Action {
    return FluxStandardAction.create(NavigateToCommand.ACTION_TYPE, { path, opts });
  }

  public static goToNewProject() {
    return NavigateToCommand.create('/editor/project/new');
  }

  public static goToEditorSplashScreen() {
    return NavigateToCommand.create('/editor');
  }
}
