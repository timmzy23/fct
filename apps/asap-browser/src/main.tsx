import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { App } from './app';
import { environment } from './environments/environment';

const appEnv = environment.production ? 'prod' : 'dev';

const intlMessages = environment.locals.reduce((acc, locale) => {
  acc[locale] = require(`./local/${locale}.local.yaml`);

  return acc;
}, {});


setTimeout(() => {
  ReactDOM.render(
    <BrowserRouter>
      <App 
        appEnv={appEnv} 
        env={environment}
        intlMessages={intlMessages}
      />
    </BrowserRouter>,
    document.getElementById('root')
  );
}, 60);
