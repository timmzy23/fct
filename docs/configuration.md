# Configuration

## Angular Frontend

### Resources

 - [Compile-time vs. Runtime configuration of your Angular App](https://juristr.com/blog/2018/01/ng-app-runtime-config/?utm_source=devto&utm_medium=crosspost)
 - [Angular 5: External configuration](https://robinraju.dev/developer/2017-11-14-loading-external-config-in-angular/)

### Content

There is two kind of configuration available for angular app:

  - `compiled`
  - `runtime`

#### Compiled configuration

Compiled configuration is usualy handled by `environment.ts` file in apps `environments` folder. This configuration is a part of compiled code and it`s change requires to recompile app again from sources.

If using webpack, then importing `json` or `yaml` files directly from source code would also result into compiled configuration.

This configuration is imported in `main.ts` and used to early config settings
such as `appEnv: prod | dev` or where to load config files.

We can make this configuration accessible in app via dependeny token:
`APP_ENV_CONFIG` and app env settings via `APP_ENV`:

```ts
# main.ts
const EXTRA_PROVIDERS [
  { provide: APP_ENV_CONFIG, useValue: environment },
  { provide: APP_ENV, useValue: environment.production ? AppEnv.PROD :  AppEnv.DEV },
]
```

Use `ConfigService#getEnv` to access the environment values from components.

#### Runtime 

Runtime configuration is loaded by angular app, when started and before 
bootstrap phase is done. Use this configuration to store all values, which
should be possible to change without re-compilation from sources.

We support right now loading from `json` files or asking api endpoint to
get json response as config. (Yaml parse is not included on frontend, because
it would meant to import two or three libraries into our release code 
(and increase package size) for one time job).

Use `ConfigService` to load as many config files or config responses as you need.

Set your config file url paths in `environment.base.ts` under `configUrls`
directive as any array of strings.



