# Lesson learned: NEST

## Get Started

- add backend app support: `npx ng add @nrwl/nest`
- make sure you have version `>=8.4.3` for `@nrwl` libraries
  to avoid some errors durring code generating
- genereate app via angular console:
  `generate -> @nrwl/nest/application` which creates this command:
  `ng generate @nrwl/schematics:application apps/auth-server --no-interactive`
- add support for validations:
  `npm i --save class-validator class-transformer`
- See validators documentation (here)[https://github.com/typestack/class-validator]

