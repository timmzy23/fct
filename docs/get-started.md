# Get Started

## Resources:

- [Javascript/HTML/CSS documentation](https://developer.mozilla.org/en-US/)
- [Monorepo tool management](https://nx.dev/angular/getting-started/what-is-nx)
- [List of javascript features](http://kangax.github.io/compat-table/es6/)
- [Can I use](https://caniuse.com/)

## Fist steps:

### Install prerequisities:
 - install `git`
 - install `nodejs` and `npm` in version `10.16` or above.
 - install `Visual Studio Code` with those plugins:
    - `Angular Console`
    - `Angular Language Service`
    - `Sort Typescript Imports`

### Create monorepo workspace for angular app
 - open terminal and creates a monorepo workspace:
 - choose your workspace namespace. Keep it short
 - run: `npx create-nx-workspace@latest [workspace namespace]` during 
   install choose:
    -  `empty` (if you want to see the minimal setup)
    - `Angluar CLI`
    - `CSS` (support for `less`, `sass` or `stylus` can be added later and 
       managed per app);
    - you should see new directory `[workspace namespace]` created. Go in
      and check empty monorepo structure.

- add capabilities for angular: `npm install --dev @nrwl/angular`

### Generate and run your first app:

#### Generate
 - open `visual studio code`
 - make sure you have installed `angular console`
 - choose `add folder to workspace` and select your `[workspace namespace]`
   directory.
 - select `angular consol` icon on left control sidebar
 - select option `generate` and choose `application` from `@nrwl/angular`
   package. (!not from `@schematics/angular`)
 - put there application name (`auth-browser`)
 - generate.

#### Run
 - choose again `angular console`
 - then `projects`
 - select `[your app]`
 - press `run` and choose `serve`
 - press `run`

 Your first app is now running at http://localhost:4200
