import * as React from 'react';
import { useRefByCallback } from '../../custom-hooks';
import {
  FactoryProvider,
  useInject,
  useProvide
  } from '../../di';
import { HttpClient } from './http-client';

export function useHttpClientInit(init: HttpClient.Init) {
  const providers = useRefByCallback(() => {
    return [
      new FactoryProvider(HttpClient, () => HttpClient.create(init)),
    ];
  });

  useProvide(providers.current);
}

export function useHttp(): HttpClient {
  const { http } = useInject({ http: HttpClient });

  return http as HttpClient;
}


export function useUrlSearchParam(key: string): any {
  const param = React.useMemo(
    () => {
      const search = new URLSearchParams(location.search);
      return search.get(key);
    },
    [location.search]);

  return param;
}

export function useUrlSearchParams(): URLSearchParams {
  const urlSearch = React.useMemo(
    () => new URLSearchParams(location.search),
    [location.search]);

  return urlSearch;
}
