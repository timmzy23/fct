import {
  from,
  Observable,
  of,
  OperatorFunction
  } from 'rxjs';
import {
  reduce,
  switchMap
  } from 'rxjs/operators';
import { HttpClient } from './http-client';
import { HttpRequest } from './http-request';
import { HttpResponse } from './http-response';

export declare namespace HttpInterceptor {
  type Interceptor<T> = (
      http$: Observable<T>, 
      fetchInit: HttpClient.FetchInit) => Observable<T>;

  type RequestInterceptor = Interceptor<HttpRequest>;

  type ResponseInterceptor<D> = Interceptor<HttpResponse<D>>;

  interface RetryResponseOpts {
    retryAttempts?: number;
    retryStrategy?: (errors$: Observable<any>) => Observable<any>;
  }
}

export class HttpInterceptor<T> {
  private interceptors: HttpInterceptor.Interceptor<T>[] = [];

  public add(
    interceptor: 
        HttpInterceptor.Interceptor<T> | 
        Array<HttpInterceptor.Interceptor<T>>
  ): this {
    Array.isArray(interceptor) ? 
        this.interceptors = this.interceptors.concat(interceptor):
        this.interceptors.push(interceptor);

    return this;
  }

  public intercept<D>(fetchInit: HttpClient.FetchInit): OperatorFunction<T, T> {
    return switchMap((http: T) => from(this.interceptors).pipe(
        reduce<HttpInterceptor.Interceptor<T>, Observable<T>>(
            (http$, interceptor) => interceptor(http$, fetchInit),
            of(http)),
        switchMap((http$) => http$)
    ));
  }
}
