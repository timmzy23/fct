
/**
 * HttpParams are not a smart envelop for URLSearchParams. The idea of this 
 * object is to hold all url parameters together. Some of them will become part
 * of search params, but some of them may replace placeholders in url path like:
 * /books/:bookId
 */
export declare namespace HttpParams {
  type ParamType = string | boolean | number;

  type ParamTypes = ParamType | ParamType[];

  interface Init {
    [paramName: string]: ParamTypes;
  }
}

export class HttpParams extends Map<
  string, HttpParams.ParamType | HttpParams.ParamType[]
> {

  constructor(params: HttpParams.Init | HttpParams) {
    // Convert HttpParams instance to params object.
    const paramObj = params instanceof HttpParams ? params.toObject() : params;
    // Converts params object into structure [[a, b], [a, b]] requested by 
    // Map#constructor.
    const entries: Array<[string, HttpParams.ParamTypes]> =
        Reflect.ownKeys(paramObj)
            .map<[string, HttpParams.ParamTypes]>(
                (paramName: string) => [paramName, paramObj[paramName]]);
    // Call Map#constructor
    super(entries);
  }

  /**
   * Appends a specified key/value pair as a new search parameter or add
   * a value to existing one.
   */
  public append(name: string, value: HttpParams.ParamType) {
    if (this.has(name)) {
      const entry = this.get(name)!;
      this.set(name, Array.isArray(entry) ?
          entry.concat(value) : [entry, value]);
    } else {
      this.set(name, value);
    }
  }

  /**
   * Returns the first value associated to the given search parameter.
   */
  public get(name: string): HttpParams.ParamType {
    const entry = super.get(name)!;
    return Array.isArray(entry) ? entry[0] : entry;
  }

  /**
   * Returns all the values association with a given search parameter.
   */
  public getAll(name: string): HttpParams.ParamTypes {
    return this.get(name);
  }

  public toObject(): HttpParams.Init {
    const entries = {};
    this.forEach((param, paramName) => {
      entries[paramName] = param;
    });
    return entries;
  }

  public clone(): HttpParams {
    return new HttpParams(this);
  }
}
