export interface ContentType {
  mediaType: string;
  charset?: string;
  boundary?: string;
}

export class HttpContentType {
  public static parse(contentType: string): ContentType {
    return contentType.split(';').reduce<ContentType>(
      (acc, keyVal: string, ix: number) => {
        if (ix === 0) {
          acc.mediaType = keyVal;
        } else {
          const [key, value] = keyVal.split('=');

          acc[key] = value;
        }
        return acc;
    }, {} as any);
  }
}
