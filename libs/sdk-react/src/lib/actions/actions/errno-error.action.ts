import { ErrnoError } from '@fct/sdk';
import { FluxErrorAction } from './flux-error.action';

export declare namespace ErrnoErrorAction {
  type Action<P extends ErrnoError, M = any> = 
    FluxErrorAction.Action<string, P, M>;
}

export class ErrnoErrorAction {
  public static create<P extends ErrnoError, M = any>(errno: P, meta?: M) {
    return FluxErrorAction.create<string, P, M>(errno.errno, errno, meta);
  }
}
