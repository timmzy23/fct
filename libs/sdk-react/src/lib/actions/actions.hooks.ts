import * as React from 'react';
import { Observable } from 'rxjs';
import {
  FactoryProvider,
  useInject,
  useProvide
  } from '../di';
import {
  CommandActionBus,
  EventActionBus,
  RootActionBus
  } from './action-bus';
import { AnyAction } from './actions/any.action';
import { FluxStandardAction } from './actions/flux-standard.action';

export function useCommands(): CommandActionBus {
  const { commands$ } = useInject({ commands$: CommandActionBus });

  return commands$;
}

export function useEvents(): EventActionBus {
  const { events$ } = useInject({ events$: EventActionBus });

  return events$;
}

export function useActions(): RootActionBus {
  const { actions$ } = useInject({ actions$: RootActionBus });

  return actions$;
}

export function useDispatchCommand() {
  const commands$ = useCommands();

  return React.useCallback((action: FluxStandardAction.Action<any>) => {
    commands$.emit(action);
  }, [commands$]);
}

export function useDispatchEvent() {
  const events$ = useEvents();

  return React.useCallback((action: FluxStandardAction.Action<any>) => {
    events$.emit(action);
  }, [events$]);
}

export function useDispatchAction() {
  const actions$ = useActions();

  return React.useCallback((action: FluxStandardAction.Action<any>) => {
    actions$.emit(action);
  }, [actions$]);
}

export type CommandMiddleware = 
    (commands$: Observable<AnyAction<any>>) => Observable<AnyAction<any>>;

export function useCommandEffect(effect: CommandMiddleware) {
  const commands$ = useCommands();

  React.useEffect(() => {
    const subscription = commands$.addEffectClb(effect);

    return () => subscription.unsubscribe();
  }, [commands$, effect]);
}

const ACTION_PROVIDERS = [
  new FactoryProvider(RootActionBus, () => new RootActionBus()),
  new FactoryProvider(
      CommandActionBus, 
      (root$: RootActionBus) => root$.getOrCreateChild('commands'),
      [RootActionBus]),
  new FactoryProvider(
      EventActionBus, 
      (root$: RootActionBus) => root$.getOrCreateChild('events'),
      [RootActionBus]),
];

export function useActionsFeature() {
  useProvide(ACTION_PROVIDERS);
}
