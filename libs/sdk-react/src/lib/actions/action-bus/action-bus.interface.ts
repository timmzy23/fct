import {
  Observable,
  PartialObserver,
  Subscription
  } from 'rxjs';
import { AnyAction } from '../actions';

export declare namespace ActionBus {
  type Listener<T extends string, A extends AnyAction<T>> = (action: A) => void;

  interface EffectInit<T extends string = any, A extends AnyAction<T> = any> {
    
    handle(actions$: Observable<A>): Observable<AnyAction>;
  }

  interface Effect<T extends string = any, A extends AnyAction<T> = any> 
    extends EffectInit<T, A> {
      topicMap: {[topic: string]: string[]};
      getActionTypesForTopic(topic: string): string[];
      forEachTopic(clb: (topic: string) => void): void;
    }
}

export interface ActionBus {
  addChild(child: string | ActionBus): ActionBus;
  addEffect<T extends string = any, A extends AnyAction<T> = any>(
      effect: ActionBus.Effect<T, A>): Subscription;
  emit<T extends string = any>(action: AnyAction<T>): void;
  getChild(path: string | string[]);
  getOrCreateChild(path: string | string[]): ActionBus;
  subscribe(observer?: PartialObserver<AnyAction>): Subscription;
}