import { ActionBusImpl } from './action-bus.impl';

export class RootActionBus extends ActionBusImpl {
  constructor() { super('root'); }
}

export class CommandActionBus extends ActionBusImpl {
  constructor() {
    super('commands');
  }
}

export class EventActionBus extends ActionBusImpl {
  constructor() {
    super('events');
  }
}
