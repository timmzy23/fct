import {
  ErrnoError,
  ReflectionHelper
  } from '@fct/sdk';
import {
  BehaviorSubject,
  Observable,
  PartialObserver,
  Subject,
  Subscription
  } from 'rxjs';
import {
  filter,
  share
  } from 'rxjs/operators';
import { AnyAction } from '../actions';
import { ERR_ACTION_CHILD_NOT_FOUND } from '../actions.errors';
import { ActionBus } from './action-bus.interface';


export class ActionBusImpl implements ActionBus {

  private actions$ = new Subject<AnyAction<any>>();

  private observer = this.actions$.pipe(share());

  /**
   * Children are streams, which action bus is listening to. Parent
   * always aggregate children`s messages. So listening on child
   * will not intercept other messages.
   */
  private children = new Map<string, ActionBus>();

  constructor(public name: string = 'root') {}

  public getChild(path: string | string[]): ActionBus {
    const fragments = Array.isArray(path) ? path : path.split('.');
    const child = this.children.get(fragments[0]);

    if (!child) {
      throw new ErrnoError(
        ERR_ACTION_CHILD_NOT_FOUND, 
        `Child action stream "${fragments[0]}" for ${this.name} does not exists!`,
        { childName: fragments[0], name: this.name });
    }

    return fragments.length <= 1 ? 
        child : 
        child.getChild(fragments.slice(1));
  }

  public getOrCreateChild(path: string | string[]) {
    const fragments = Array.isArray(path) ? path : path.split('.');
    let child = this.children.get(fragments[0]);

    if (!child) {
      child = this.addChild(fragments[0]);
    }

    return fragments.length <= 1 ? 
        child : 
        child.getOrCreateChild(fragments.slice[1]);
  }

  public addChild(child: string | ActionBus) {
    const childActionBus = ReflectionHelper.isString(child) ?
        new ActionBusImpl(child as string) : child as ActionBus;

    this.children.set(childActionBus.name, childActionBus);

    childActionBus.subscribe({
      next: (action: AnyAction<any>) => this.emit(action),
      // Remove child but still keep running for the others
      complete: () => this.children.delete(childActionBus.name),
      // @TODO add error handler here....
    });

    return childActionBus;
  }

  public addEffectClb(effectClb: Function): Subscription {
    const actionSubject = new Subject<AnyAction>();

    const effect$ = effectClb(actionSubject);

    this.observer.subscribe(actionSubject);

    return effect$.subscribe(this.actions$);
  }

  public addEffect(effect: ActionBus.Effect): Subscription {
    const actionSubject = new Subject<AnyAction>();
    const actionTypes = effect.getActionTypesForTopic(this.name);

    const effect$ = effect.handle(actionTypes.length > 0 ?
        actionSubject.pipe(
            filter((action) => actionTypes.includes(action.type))) :
        actionSubject);

    this.observer
        .subscribe(actionSubject);
    return effect$.subscribe(this.actions$);
  }

  public emit(action: AnyAction<any>) {
    const target = action.meta && action.meta.target ? action.meta.target : '';

    const decoratedAction = {
      ...action,
      meta: {
        ...(action.meta || {}),
        target: target ? `${this.name}.${target}` : this.name,
        currentTarget: this.name
      }
    }

    this.actions$.next(decoratedAction);
  };

  public subscribe(observer?: PartialObserver<any>): Subscription {
    return this.observer.subscribe(observer);
  }

  public pipe(...args: any[]): Observable<any> {
    return Reflect.apply(this.observer.pipe, this.observer, args);
  }
}

export function actionOf(...args: Array<{ACTION_TYPE: string}>) {
  const actionTypes = args.map((a) => a.ACTION_TYPE);

  return (observable: Observable<AnyAction>) => 
      observable.pipe(filter((action) => {
        return actionTypes.includes(action.type);
      }));
}