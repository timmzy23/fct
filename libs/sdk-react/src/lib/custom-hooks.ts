import * as React from 'react';

export function useRefByCallback<T>(
  callback: () => T
): React.MutableRefObject<T | undefined> {
  const ref = React.useRef<T>();

  if (!ref.current) {
    ref.current = callback();
  }
  return ref;
}
