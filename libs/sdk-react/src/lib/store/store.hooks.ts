import * as React from 'react';
import { Subscription } from 'rxjs';
import { useRefByCallback } from '../custom-hooks';
import {
  FactoryProvider,
  useProvide,
  ValueProvider
  } from '../di';
import { ApplicationState } from './state';

export function useStores(stateHandler: ApplicationState) {
  useProvide([
    new ValueProvider(ApplicationState, stateHandler)
  ]);
}

export function useSubscription(
  subscribe: () => Subscription | Subscription[], 
  deps: any[]
) {
  React.useEffect(() => {
    const subscription = subscribe();

    return () => Array.isArray(subscription) ? 
        subscription.forEach((s) => s.unsubscribe()):
        subscription.unsubscribe();
  }, deps);
}

export function useEarlySubscription(
  subscribe: () => Subscription | Subscription[]
) {
  const subscription = useRefByCallback<Subscription | Subscription[]>(subscribe).current;

  React.useEffect(() => {
    return () => Array.isArray(subscription) ? 
        subscription.forEach((s) => s.unsubscribe()):
        subscription.unsubscribe();
  }, []);
}
