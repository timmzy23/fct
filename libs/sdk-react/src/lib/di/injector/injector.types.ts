import { InjectionToken } from './injector.tokens';

export type InjectionType = string | symbol | InjectionToken<any>;
