import * as React from 'react';
import { Provider } from '../providers';
import { Injector } from './injector';
import { InjectorContext } from './injector.context';

export declare namespace UseInjectorHook {
  interface Props {
    tokens: {[depName: string]: any}
  }
}

const EMPTY = Object.freeze({});

export function useInjector(): Injector {
  const injector = React.useContext(InjectorContext);

  if (!injector) {
    throw new Error('No injector found!');
  }

  return injector;
}


export function useInject<D>(deps: {[key: string]: any}): D {
  const injector = React.useContext(InjectorContext);

  if (!injector) {
    throw new Error('No injector found!');
  }

  const resolvedDeps = React.useRef<D | undefined>();

  if (!resolvedDeps.current) {
    resolvedDeps.current = Reflect
      .ownKeys(deps)
      .reduce<D>((acc, depName: string) => {
        acc[depName] = injector.inject(deps[depName]);
        return acc;
      }, ({} as any));
  }
  return resolvedDeps.current as D;
}

export function useProvide(providers: Provider<any>[]) {
  const injector = React.useContext(InjectorContext);

  if (!injector) {
    throw new Error('No injector found!');
  }

  const handled = React.useRef<boolean>(false);

  if (!handled.current) {
    providers.forEach((provider) => injector.use(provider));
    handled.current = true;
  }
}

export function useDependencyInitialize(types: any[]) {
  const injector = React.useContext(InjectorContext);
  
  if (!injector) {
    throw new Error('No injector found!');
  }
  const handled = React.useRef<boolean>(false);
  if (!handled.current) {
    injector.initialize(types);
    handled.current = true;
  }
}