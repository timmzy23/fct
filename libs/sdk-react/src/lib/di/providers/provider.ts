import { Injector } from '../injector';

export declare namespace Provider {
  type Config<T = any> = (value: T, ...args: any[]) => T;
}

export abstract class Provider<T = any> {

  protected configs: Array<Provider.Config<T>>;

  constructor(
    public injectionType: any, 
    public multi?: false
  ) {}

  public abstract provide(injector: Injector): T;

  public configure(configFn: Provider.Config<T>): this {
    if (!this.configs) {
      this.configs = [];
    }
    this.configs.push(configFn);

    return this;
  }
}
