import { Injector } from '../injector';
import { Provider } from './provider';

export class ValueProvider<V> extends Provider<V> {
  private value: V;

  constructor(type: any, value: V) {
    super(type);
    this.value = value;
  }

  public provide(injector: Injector) {
    return this.value;
  }
}
