export class DiNoInjectorInContextError extends TypeError {
  constructor(componentName: string) {
    super(`DI: There is no injector in "${componentName}" context!`);
  }
}
