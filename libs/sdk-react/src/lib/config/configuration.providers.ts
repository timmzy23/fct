import {
  actionOf,
  EventActionBus,
  EventBuss
  } from '../actions';
import {
  APP_ENV_TOKEN,
  FactoryProvider,
  ValueProvider
  } from '../di';
import { HttpClient } from '../http';
import { ApplicationState } from '../store';
import { ConfigurationDataAction } from './configuration.events';
import { ConfigurationService } from './configuration.service';
import { ConfigurationStore } from './configuration.store';

export const APP_CONFIG_DEFAULT = Symbol('app.config.default');

// Store here config compiled settings
export const APP_CONFIG_ENV_VALUES = Symbol('app.config.env.values');

// Feel free to override in any main app providers.
const appDefaultConfig = new ValueProvider(APP_CONFIG_DEFAULT, {});

const configStoreProvider = new FactoryProvider(
  ConfigurationStore,
  (appState: ApplicationState, config: Partial<ConfigurationStore.State>) => 
      new ConfigurationStore(appState, config),
  [ApplicationState, APP_CONFIG_DEFAULT]
);

const configServiceProvider = new FactoryProvider(
  ConfigurationService,
  (http: HttpClient, appEnv: string, events$: EventActionBus, store: ConfigurationStore) => {
    const service = new ConfigurationService(http, appEnv, events$, store);

    events$
        .pipe(actionOf(ConfigurationDataAction))
        .subscribe({ next: service.reduce });

    return service;
  },
  [ HttpClient, APP_ENV_TOKEN, EventActionBus, ConfigurationStore ]
);

export const SDK_CONFIG_PROVIDERS = [
  appDefaultConfig,
  configStoreProvider,
  configServiceProvider
]