import { Configuration } from '@fct/sdk';
import * as React from 'react';
import { filter } from 'rxjs/operators';
import {
  actionOf,
  useEvents
  } from '../actions';
import { useInject } from '../di';
import { ConfigurationDataAction } from './configuration.events';
import { ConfigurationService } from './configuration.service';
import { ConfigurationStore } from './configuration.store';

export function useConfiguration(): ConfigurationService {
  return useInject<ConfigurationService>(ConfigurationService);
}

export function useConfigStore(): ConfigurationStore {
  return useInject<ConfigurationStore>(ConfigurationStore);
}

export function useConfigurationFeature() {
  const configStore = useConfigStore();
  const events$ = useEvents();

  React.useEffect(
    () => {
      const sub = events$
          .pipe(actionOf(ConfigurationDataAction))
          .subscribe((action: ConfigurationDataAction.Action) => 
              configStore.merge(action.payload));

      return () => sub.unsubscribe();
    },
    [configStore, events$]
  );
}
