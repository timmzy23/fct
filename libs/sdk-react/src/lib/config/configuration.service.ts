import { Observable } from 'rxjs';
import {
  actionOf,
  EventActionBus
  } from '../actions';
import { HttpClient } from '../http';
import {
  ConfigurationDataAction,
  ConfigurationLoadeEndAction
  } from './configuration.events';
import { ConfigurationStore } from './configuration.store';

export class ConfigurationService {
  public constructor(
    private http: HttpClient,
    private appEnv: string,
    private events$: EventActionBus,
    private configStore: ConfigurationStore
  ) {}

  public load(configUrl: string | string[]) {
    const configUrls = Array.isArray(configUrl) ? configUrl : [configUrl];
    
    return Promise.all(configUrls
        .map(this.buildUrl)
        .map((url) => this.http.get(url).toPromise()
            .then((config) => 
                this.events$.emit(ConfigurationDataAction.create(config)))))
      .then(() => this.events$.emit(ConfigurationLoadeEndAction.create()));
  }

  private buildUrl = (configUrlTemplate: string) => {
    return configUrlTemplate.replace('{env}', this.appEnv);
  }
}