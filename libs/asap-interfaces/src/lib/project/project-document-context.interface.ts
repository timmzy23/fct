export interface ProjectDocumentContext<T> {
  S: T;
  M: T;
  L: T;
}