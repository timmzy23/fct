export interface ProjectEditor {
  name: string;
  id?: string;
  createdAt?: Date;
}
