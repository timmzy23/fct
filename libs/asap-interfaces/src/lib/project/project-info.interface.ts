export interface ProjectInfo {
  projectName?: string;
  projectId?: string;
  clientName?: string;
  projectScenario?: string;
  projectVersion?: string;
  projectState?: string;
  _id?: string;
}