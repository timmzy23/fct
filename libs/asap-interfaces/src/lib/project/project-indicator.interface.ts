import { ProjectDocumentContext } from './project-document-context.interface';
import { ProjectEditor } from './project-editor.interface';

export interface ProjectIndicator {
  author?: ProjectEditor;
  id?: string;
  name?: string;
  notes?: string;
  units?: string;
  accessibility?: string;
  source?: string;
  uap?: boolean;
  values: ProjectDocumentContext<number>;
}