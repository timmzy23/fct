import { ProjectDocumentNode } from './project-document-node.interface';
import { ProjectDocumentCollectionRecord } from './project-document.collection-record';
import { Project } from './project-document.interface';

export interface ProjectDocumentCollectionInterface {

  getNodeById(nodeId: number): ProjectDocumentCollectionRecord | undefined;

  getNodeByXPath(xPath: string): ProjectDocumentCollectionRecord | undefined;

  getChildren(nodeId: number): ProjectDocumentCollectionRecord[];

  getFlattenChildren(nodeId: number): ProjectDocumentCollectionRecord[];

  hasChildren(nodeId: number): boolean;
}