import { ProjectDocumentNode } from './project-document-node.interface';
import { ProjectIndicator } from './project-indicator.interface';

export type ProjectDocument = ProjectDocumentNode[];

export interface Project {
  projectName: string;
  projectId: string;
  clientName?: string;
  projectScenario?: string;
  projectVersion?: string;
  projectState?: string;
  nodes: ProjectDocumentNode[];
  indicators: ProjectIndicator[];
}
