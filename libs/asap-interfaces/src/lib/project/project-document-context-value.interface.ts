import { ProjectDocumentContext } from './project-document-context.interface';

export interface ProjectDocumentContextValue<T> {
  context: ProjectDocumentContext<T>;
  value: T;
}