import { ProjectDocumentNode } from './project-document-node.interface';
import { Project } from './project-document.interface';
import { ProjectIndicator } from './project-indicator.interface';

export abstract class ProjectDocumentNodeHelper {
  public static X_PATH_SEP = '.';

  public static getParentXpath(node: ProjectDocumentNode): string {
    const sep = ProjectDocumentNodeHelper.X_PATH_SEP;

    return node.xPath.split(sep).slice(0, -1).join(sep);
  }

  public static getNodeLevel(node: ProjectDocumentNode): number {
    const sep = ProjectDocumentNodeHelper.X_PATH_SEP;

    return node.xPath.split(sep).length;
  }

  /**
   * Attach indicators to context if they have contextual value or
   * when no value is provided at all.
   */
  public static filterIndicatorsByContext(
    node: ProjectDocumentNode, 
    contextType: string,
    project?: Project
  ): ProjectIndicator[] {
    const indicators = node.indicators || [];

    return indicators.filter((indicator) => 
        !indicator.values || 
        Reflect.has(indicator.values, contextType));
  }
}
