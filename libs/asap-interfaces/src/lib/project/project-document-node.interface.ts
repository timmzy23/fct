import { ProjectDocumentContextValue } from './project-document-context-value.interface';
import { ProjectEditor } from './project-editor.interface';
import { ProjectIndicator } from './project-indicator.interface';
import { ProjectInfo } from './project-info.interface';

export interface ProjectDocumentNode {
  contentId: number;
  createdBy?: ProjectEditor;
  verifiedBy?: ProjectEditor;
  critical?: boolean;
  xPath: string;
  name: string;
  fullName: string;
  recommendation?: string;
  summary?: string;
  indicators?: ProjectIndicator[];
  project: ProjectInfo;
  comment: ProjectDocumentContextValue<string>;
  score: ProjectDocumentContextValue<number>;
  tendency?: ProjectDocumentContextValue<number>;
  weight?: ProjectDocumentContextValue<number>;
}
