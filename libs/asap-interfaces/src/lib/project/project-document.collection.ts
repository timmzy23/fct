import { ProjectDocumentNode } from './project-document-node.interface';
import { ProjectDocumentCollectionRecord } from './project-document.collection-record';
import { Project } from './project-document.interface';

export class ProjectDocumentCollection {

  private nodeIdIndex: Map<number, ProjectDocumentCollectionRecord> = 
      this.project.nodes.reduce((acc, node) => {
        acc.set(node.contentId, new ProjectDocumentCollectionRecord(node, this));

        return acc;
      }, new Map<number, ProjectDocumentCollectionRecord>());

  private nodeXPathIdIndex: Map<string, number> =
      this.project.nodes.reduce((acc, node) => {
        acc.set(node.xPath, node.contentId);

        return acc;
      }, new Map<string, number>());

  private nodeIdChildrenIdsIndex: Map<number, number[]> =
        this.project.nodes.reduce((acc, node) => {
          const parentXPath = node.xPath.split('.').slice(0, -1).join('.');
          const parent = this.getNodeByXPath(parentXPath);

          if (!parent) { return acc; }

          if (!acc.has(parent.node.contentId)) {
            acc.set(parent.node.contentId, []);
          }
          
          acc.get(parent.node.contentId).push(node.contentId);

          return acc;
        }, new Map<number, number[]>());

  constructor(public readonly project: Project) {}

  public getNodeById(nodeId: number) {
    return this.nodeIdIndex.get(nodeId);
  }

  public getNodeByXPath(xPath: string) {
    const nodeId = this.nodeXPathIdIndex.get(xPath);
    return nodeId && this.getNodeById(nodeId);
  }

  public getChildren(nodeId: number): ProjectDocumentCollectionRecord[] {
    return (this.nodeIdChildrenIdsIndex.get(nodeId) || [])
        .map((childNodeId) => this.getNodeById(childNodeId));
  }

  /**
   * Get all children and theirs childre and...
   */
  public getFlattenChildren(nodeId: number) {
    return this
        .getChildren(nodeId)
        .reduce((acc, record) => {
          acc.push(record);

          const recordChildren = record.getChildren();
          acc = [...acc, recordChildren];

          return acc;
        }, []);
  }

  public hasChildren(nodeId: number) {
    return (this.nodeIdChildrenIdsIndex.get(nodeId) || []).length > 0;
  }
}