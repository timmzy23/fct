import { ProjectDocumentNode } from './project-document-node.interface';
import { ProjectDocumentCollectionInterface } from './project-document.collection.interface';
import { ProjectIndicator } from './project-indicator.interface';

export class ProjectDocumentCollectionRecord {
  constructor(
    public readonly node: ProjectDocumentNode,
    private collection: ProjectDocumentCollectionInterface
  ) {}

  public hasContext(contextType: string) {
    return (      
      this.hasContextValue('tendency', contextType) ||
      this.hasContextValue('score', contextType) || 
      this.hasContextValue('weight', contextType) ||
      this.hasContextValue('comment', contextType) ||
      this.getIndicators(contextType).length > 0
    );
  }

  public getParent(): ProjectDocumentCollectionRecord | undefined {
    const parent = this.collection.getNodeByXPath(this.getParentXPath());

    return parent;
  }

  public hasContextValue(
    propType: string, 
    contextType: string, 
    withChildren: boolean = false
  ) {
    let hasValue = this.node[propType] &&
      this.node[propType].context &&
      this.node[propType].context[contextType];

    if (!hasValue && withChildren && this.hasChildren()) {
      hasValue = this.getChildren().some(
        (child) => child.hasContextValue(propType, contextType, withChildren));
    }

    return !!hasValue;
  }

  public getAvailableContexts() {
    return ['S', 'M', 'L']
        .filter((contextType) => this.hasContext(contextType));
  }

  public getVerificator() {
    return this.node.verifiedBy;
  }

  public getAuthor() {
    return this.node.createdBy;
  }

  public getIndicators(contextType?: string, withChildren = true): ProjectIndicator[] {
    const indicators = (this.node.indicators || [])
        .filter((indicator) => 
          !contextType || !indicator.values || 
          Reflect.has(indicator.values, contextType));

    let flattIndicators = [...indicators];

    if (withChildren) {
      (this.getChildren() || []).forEach((child) => {
        flattIndicators = [
          ...flattIndicators,
          ...child.getIndicators(contextType)
        ];
      });
    }

    const indicatorsMap = flattIndicators.reduce((acc, ind) => {
      acc[ind.id] = ind;
      return acc;
    }, {});

    // Filter out duplicates:
    return Reflect
      .ownKeys(indicatorsMap)
      .map((indId) => indicatorsMap[indId]);
  }

  public getIndicatorsCount(contentType?: string, withChildren = true): number {
    return this.getIndicators(contentType, withChildren).length;
  }

  public getUAPIndicatorsCount(contentType?: string, withChildren = true): number {
    return this.getIndicators(contentType, withChildren)
        .filter((indicator) => indicator.uap)
        .length;
  }

  public getChildrenCount(recursive: boolean = false): number {
    return recursive ? 
      this.getFlattenChidren().length :
      this.getChildren().length;
  }

  public getChildren(nested = false) {
    return this.collection.getChildren(this.node.contentId);
  }

  public hasChildren() {
    return this.collection.hasChildren(this.node.contentId);
  }

  public getFlattenChidren() {
    return this.collection.getFlattenChildren(this.node.contentId);
  }

  public getTendencyForContext(contextType: string) {
    return this.getValueForContext(contextType, 'tendency');
  }

  public getScoreForContext(contextType: string) {
    return this.getValueForContext(contextType, 'score');
  }

  public getWeightForContext(contextType: string) {
    return this.getValueForContext(contextType, 'weight');
  }

  public getCommentForContext(contextType: string) {
    return this.getValueForContext(contextType, 'comment');
  }

  public getValueForContext(contextType: string, propName: string) {
    return this.node[propName] && 
        this.node[propName].context &&
        this.node[propName].context[contextType];
  }

  public getSectionId() {
    return Number.parseInt(this.node.xPath.split('.').shift(), 10);
  }

  public getParentXPath() {
    const currentXPath = this.node.xPath.split('.');
    return currentXPath.slice(0, currentXPath.length - 1).join('.');
  }
}
