
export interface ProjectHeader {
  projectName: string;
  projectId: string;
  clientName?: string;
  createdAt: Date;
  createdBy: { name: string};
  nodesCount: number;
  _id: string;
}

export interface ProjectHeaders {
  items: ProjectHeader[];
}