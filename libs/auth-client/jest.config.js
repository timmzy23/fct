module.exports = {
  name: 'auth-client',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/auth-client',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
