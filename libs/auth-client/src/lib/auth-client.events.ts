import {
  HttpErrorResponse,
  HttpRequest
  } from '@angular/common/http';
import { FluxStandardAction } from '@fct/sdk-ng';

enum AuthClientEventTypes {
  UNAUTHORIZED = 'auth.client.unauthorized'
}


export declare namespace AuthClientUnauthorizedEvent {
  type ACTION_TYPE = AuthClientEventTypes.UNAUTHORIZED;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    request: HttpRequest<any>;
    error: HttpErrorResponse;
  }
}

export class AuthClientUnauthorizedEvent {
  public static ACTION_TYPE: AuthClientUnauthorizedEvent.ACTION_TYPE = 
    AuthClientEventTypes.UNAUTHORIZED;

  public static create(payload: AuthClientUnauthorizedEvent.Payload): AuthClientUnauthorizedEvent.Action {
    return FluxStandardAction.create(AuthClientUnauthorizedEvent.ACTION_TYPE, payload);
  }
}