import { CommonModule } from '@angular/common';
import {
  HTTP_INTERCEPTORS,
  HttpClientModule
  } from '@angular/common/http';
import {
  ModuleWithProviders,
  NgModule
  } from '@angular/core';
import {
  AUTH_CONFIG_DATA,
  AuthConfiguration
  } from './auth-client.config';
import { AuthClientResponseInterceptor } from './auth-client.response.interceptor';

@NgModule({
  imports: [
    CommonModule, 
    HttpClientModule
  ]
})
export class AuthClientModule {
  public static forRoot(config: AuthConfiguration.Config): ModuleWithProviders {
    return {
      ngModule: AuthClientModule,
      providers: [
        { provide: AUTH_CONFIG_DATA, useValue: config },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthClientResponseInterceptor,
          multi: true
        }
      ]
    }
  }
}
