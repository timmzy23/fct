import {
  HttpClient,
  HttpErrorResponse
  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  assert,
  ErrnoError,
  HttpStatusCode,
  PollingObservable
  } from '@fct/sdk';
import {
  Observable,
  Observer,
  of,
  Subscription,
  throwError
  } from 'rxjs';
import {
  catchError,
  ignoreElements,
  map,
  switchMap,
  tap
  } from 'rxjs/operators';
import { AuthConfiguration } from './auth-client.config';
import {
  ERR_AUTH_IDENTIFY_INVALID_EMAIL,
  ERR_AUTH_NOT_AUTHENTICATED,
  ERR_AUTH_SERVER_UNAVAILABLE,
  ERR_AUTH_UNKNOWN,
  ERR_AUTH_UNTRUSTED_REDIRECT_ORIGIN,
  ERR_INVALID_ACCESS_TOKEN
  } from './auth-client.errors';

export declare namespace AuthClientService {
  interface LoginSuccessPayload {
    authorizationCode: string;
    accessToken: string;
  }
}


@Injectable({ providedIn: 'root' })
export class AuthClientService {

  public static VERIFY_SILENT = true;

  private allowedOrigins: string[];

  private refreshPollingSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private config: AuthConfiguration
  )  {}

  public identify(identifier: string) {
    return this.http.post(this.getUrl('pathIdentify'), { identifier })
        .pipe(catchError((err: HttpErrorResponse) => {
          if (err.status >= 500)  {
            return throwError(new ErrnoError(ERR_AUTH_SERVER_UNAVAILABLE));
          } else if (err.status === HttpStatusCode.BAD_REQUEST) {
            return throwError(new ErrnoError(ERR_AUTH_IDENTIFY_INVALID_EMAIL))
          } else {
            return throwError(new ErrnoError(ERR_AUTH_UNKNOWN))
          }
        }));
  }

  public login(identifier: string, password: string): Observable<AuthClientService.LoginSuccessPayload> {
    return this.http.post(this.getUrl('pathLogin'), { identifier, password })
        .pipe(
          map((payload) => ({ 
            ...payload, 
            authorizationCode: btoa('' + Math.random()).slice(0, 12)
          })),
          tap(({ accessToken }: any) => this.setAccessToken(accessToken)),
          tap(({ authorizationCode }: any) => 
            localStorage.setItem('authorizationCode', authorizationCode)),
          catchError((error: HttpErrorResponse) => {
            if (error.status === HttpStatusCode.UNAUTHORIZED) {
              return throwError(new ErrnoError(ERR_AUTH_NOT_AUTHENTICATED));
            } else {
              return throwError(new ErrnoError(ERR_AUTH_UNKNOWN));
            }
        }));
  }

  public getAllowedOrigins(): Observable<string[]> {
    if (this.allowedOrigins) {
      return of(this.allowedOrigins);
    } else {
      return this.http.get(this.getUrl('pathAllowedOrigins')).pipe(
        map(({ origins }: any) => origins),
        tap((origins) => this.allowedOrigins = origins));
    }
  }

  public isOriginAllowed(origin: string) {
    return this.getAllowedOrigins()
        .toPromise()
        .then((origins) => origins.includes(origin))
        .catch((err) => {
          console.error(err);
          return Promise.resolve(false);
        });
  }

  public trustedRedirect(url: URL) {
    const origin = url.origin;

    this.isOriginAllowed(origin).then((isAllowed) => {
      if (isAllowed) {
        location.href = url.toString()
      } else {
        throw new ErrnoError(ERR_AUTH_UNTRUSTED_REDIRECT_ORIGIN);
      }        
    });
  }

  public getAccessToken() {
    return localStorage.getItem('accessToken');
  }

  public setAccessToken(token: string) {
    localStorage.setItem('accessToken', token);
  }

  public getAuthorizationCode() {
    return localStorage.getItem('authorizationCode');
  }

  /**
   * @param silent Silent means: do not trigger an unauthorized event on failure!
   */
  public verify(silent: boolean = false) {
    return of(this.getAccessToken()).pipe(
      assert((accessToken) => !!accessToken, ERR_INVALID_ACCESS_TOKEN),
      switchMap((accessToken) => this.http.options(
          this.getUrl('pathVerify'), {
            headers: {
              'x-auth-silent': silent ? 'Y' : 'N',
              ...this.getAuthRequestHeaders(accessToken),
            }
          })),
      catchError(() => throwError(new ErrnoError(ERR_INVALID_ACCESS_TOKEN)))
    );
  }

  public refreshToken(): Observable<string> {
    return of(this.getAccessToken()).pipe(
      switchMap((accessToken) => this.http.get(
          this.getUrl('pathRefresh'), 
          { headers: this.getAuthRequestHeaders(accessToken)})),
      tap(({ accessToken }: any) => this.setAccessToken(accessToken)),
      catchError(() => throwError(new ErrnoError(ERR_INVALID_ACCESS_TOKEN)))
    );
  }

  public pollToken(pollingObserver: Partial<Observer<any>>) {
    if (this.refreshPollingSubscription) { return; }

    this.refreshPollingSubscription = PollingObservable
        .create(
          this.config.get('refreshIntervalMs'), 
          this.config.get('refreshIntervalMs')
        )
        .subscribe((tick) => {
          this.refreshToken().subscribe({
            next: ({ accessToken }: any) => {
              if (pollingObserver.next) { pollingObserver.next(accessToken) }
              tick.next();
            },
            error: (err) => {
              if (pollingObserver.error) { pollingObserver.error(err); }
              tick.next();
            },
            complete: () => {}
          });
        });
  }

  public stopTokenPolling() {
    if (!this.refreshPollingSubscription) { return; }

    this.refreshPollingSubscription.unsubscribe();

    this.refreshPollingSubscription = null;
  }

  private getUrl(pathname: string) {
    return `${this.config.get('authAuthorityUrl')}${this.config.get(pathname)}`;
  }

  public getAuthRequestHeaders(accessToken: string) {
    return { 'Authorization': `bearer ${accessToken} `}
  }
}
