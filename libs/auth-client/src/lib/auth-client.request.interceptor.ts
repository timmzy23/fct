import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthClientService } from './auth-client.service';

@Injectable({ providedIn: 'root'})
export class AuthClientRequestInterceptor implements HttpInterceptor {

  constructor(private auth: AuthClientService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clone = req.clone({
      setHeaders: this.auth.getAuthRequestHeaders(this.auth.getAccessToken()),
    });

    return next.handle(clone);
  }
}
