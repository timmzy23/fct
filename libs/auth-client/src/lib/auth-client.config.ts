import {
  Inject,
  Injectable,
  InjectionToken
  } from '@angular/core';
import { Configuration } from '@fct/sdk';

export const AUTH_CONFIG_DATA = 
    new InjectionToken<AuthConfiguration.Config>('auth.config.data');

export declare namespace AuthConfiguration {
  interface Config {
    pathIdentify: string;
    pathLogin: string;
    pathVerify: string;
    pathRefresh: string;
    pathAllowedOrigins: string;
    refreshIntervalMs: number;
    authAuthorityUrl: string;
  }
}

@Injectable({ providedIn: 'root' })
export class AuthConfiguration extends Configuration<AuthConfiguration.Config> {
  constructor(
    @Inject(AUTH_CONFIG_DATA)
    config: AuthConfiguration.Config
  ) {
    super(config);
  }
}
