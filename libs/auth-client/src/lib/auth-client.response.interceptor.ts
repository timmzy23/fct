import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpStatusCode } from '@fct/sdk';
import { EventActionBus } from '@fct/sdk-ng';
import {
  Observable,
  throwError
  } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthClientUnauthorizedEvent } from './auth-client.events';

/**
 * Fetch for 401 responses and react according to provided strategy.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthClientResponseInterceptor implements HttpInterceptor {

  constructor(private events$: EventActionBus) {}

  public intercept(
    request: HttpRequest<any>, next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error) => {
        if (
          error instanceof HttpErrorResponse && 
          error.status === HttpStatusCode.UNAUTHORIZED &&
          request.headers.get('x-auth-silent') !== 'Y'
        ) {
          this.events$.emit(AuthClientUnauthorizedEvent.create({ request, error}));
          return throwError(error);
        } else {
          return throwError(error);
        }
      })
    );
  }
}