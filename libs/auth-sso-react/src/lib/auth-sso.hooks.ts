import {
  ApplicationState,
  FactoryProvider,
  HttpHeader,
  HttpRequest,
  useHttp,
  useInject,
  useProvide,
  useSubscription
  } from '@fct/sdk-react';
import * as React from 'react';
import { AuthSsoService } from './auth-sso.service';
import { AuthSsoStore } from './auth-sso.store';

const PROVIDERS = [
  new FactoryProvider(
      AuthSsoStore,
      (appState: ApplicationState) => new AuthSsoStore(appState),
      [ApplicationState]),
  new FactoryProvider(
    AuthSsoService,
    (store: AuthSsoStore) => new AuthSsoService(store),
    [AuthSsoStore])
];


export function useAuthSsoInit() {
  const providers = React.useRef(PROVIDERS);
  useProvide(providers.current);
  
  const firstRun = React.useRef(true);
  const http = useHttp();
  const store = useAuthStore();


  if (firstRun) {
    http.requestInterceptors.add(HttpRequest.getRequestHeaderDecorator(
        HttpHeader.AUTHORIZATION, 
        () => `bearer ${store.getAuthToken()} `));
  }

  firstRun.current = false;
}

export function useAuthSsoService(): AuthSsoService {
  const { service } = useInject({ service: AuthSsoService });

  return service;
}

export function useAuthStore(): AuthSsoStore {
  const { store } = useInject({ store: AuthSsoStore });

  return store;
}

export function useAuthToken(): string {
  const store = useAuthStore();
  const [authToken, setAuthToken] = React.useState(store.getAuthToken());

  useSubscription(
    () => store
      .observeAuthToken()
      .subscribe((token) => setAuthToken(token)),
    [setAuthToken, store])

  return authToken;
}

export function useAuthIdentity(): string {
  const store = useAuthStore();
  const [identity, setIdentity] = React.useState(store.getIdentitiy());

  useSubscription(
    () => store
      .observeIdentity()
      .subscribe((_identity) => setIdentity(_identity)),
    [setIdentity, store])

  return identity;
}

export function useIsAuthenticated(): boolean {
  const store = useAuthStore();
  const [isAuthenticated, setIsAuthenticated] = 
      React.useState(!!store.getAuthToken());

  useSubscription(
    () => store
      .observeAuthToken()
      .subscribe((token) => setIsAuthenticated(!!token)),
    [setIsAuthenticated, isAuthenticated, store])

  return isAuthenticated;
}
