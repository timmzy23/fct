import { AuthSsoStore } from './auth-sso.store';

export class AuthSsoService {

  private authCode: string;

  constructor(private store: AuthSsoStore) {}

  public getAuthCode() {
    const authCodeFromUrl = new URL(location.href).searchParams.get('code');
    const authCodeFromStorage = localStorage.getItem('authCode');

    if (authCodeFromUrl) {
      localStorage.setItem('authCode', authCodeFromUrl);

      this.authCode = localStorage.getItem('authCode');
    } else if (authCodeFromStorage) {
      this.authCode = authCodeFromStorage;
    }

    return this.authCode;
  }

  public observeAuthToken() {
    return this.store.observeAuthToken();
  }

  public observeIdentity() {
    return this.store.observeIdentity();
  }

  public getAuthToken() {
    return this.store.getAuthToken();
  }

  public getIdentity() {
    return this.store.getIdentitiy();
  }
}