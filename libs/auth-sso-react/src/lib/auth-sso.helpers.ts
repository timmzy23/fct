import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

export function messageOf(...messageTypes: Array<string>) {
  return (observable: Observable<MessageEvent>) => 
      observable.pipe(filter((message) => {
        return message.data && messageTypes.includes(message.data.type);
      }));
}
