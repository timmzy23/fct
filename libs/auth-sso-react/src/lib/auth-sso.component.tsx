import {
  useRefByCallback,
  useSubscription
  } from '@fct/sdk-react';
import * as React from 'react';
import {
  fromEvent,
  Observable
  } from 'rxjs';
import {
  filter,
  map,
  share,
  tap
  } from 'rxjs/operators';
import { messageOf } from './auth-sso.helpers';
import {
  useAuthSsoService,
  useAuthStore
  } from './auth-sso.hooks';
import { AuthSsoService } from './auth-sso.service';
import { AuthSsoStore } from './auth-sso.store';

export declare namespace AuthSso {
  interface Props {
    ssoServiceUrl: string;
  }
  type Component = React.FunctionComponent<Props>;
}

function useTrustedIFrameUrl(untrustedUrl: string) {
  const trustedUrlRef = React.useRef('');

  if (!trustedUrlRef.current) {
    const url = new URL(untrustedUrl);

    // Prevent any kind of url protocol like: "javascript:doSomeBadCode"
    if (!['http:', 'https:'].includes(url.protocol)) {
      throw new Error(`Untrusted url: ${url}`);
    }

    trustedUrlRef.current = url.toString();
  }

  return trustedUrlRef.current;
}

function useDispatchMessage(iFrame: React.MutableRefObject<HTMLIFrameElement>, origin: string) {
  return React.useCallback((message: any) => {
    if (!iFrame.current) { return; }
    iFrame.current.contentWindow.postMessage(message, origin);
  }, [iFrame, origin]);
}

function useIFrameMessageBus(origin: string): Observable<MessageEvent> {
  const iFrameMessages$ = React.useRef<Observable<MessageEvent>>(null);

  if (!iFrameMessages$.current) {
    iFrameMessages$.current = fromEvent(window, 'message').pipe(
      filter((message: MessageEvent) => message.origin === origin),
      tap((message) => console.log('PARENT MSG >>>', message)),
      share())
  }

  return iFrameMessages$.current;
}

function useListenUnauthorizedMessage(iFrameMessages$: Observable<MessageEvent>) {
  useSubscription(
    () => iFrameMessages$
      .pipe(
        messageOf('NOT_AUTHENTICATED'),
        map((message) => message.data))
      .subscribe((action: any) => {
        const loginUrl = action.payload.url;
        // Redirect on unauthorized moment
        location.href = loginUrl;
      }), 
    [iFrameMessages$])
}

function useListenSsoReadyMessage(
  iFrameMessages$: Observable<MessageEvent>,
  dispatchMessage: (message: any) => void,
  authSsoService: AuthSsoService
) {
  useSubscription(
    () => iFrameMessages$
      .pipe(messageOf('SSO_READY'))
      .subscribe(() => dispatchMessage({
        type: 'REGISTER',
        payload: { 
          authCode: authSsoService.getAuthCode(),
          redirectPath: '/'
        }
      })),
    [iFrameMessages$, dispatchMessage, authSsoService]
  )
}

function useListentAuthtokenMessage(
  iFrameMessages$: Observable<MessageEvent>,
  dispatchMessage: (message: any) => void,
  store: AuthSsoStore
) {
  useSubscription(
    () => iFrameMessages$
      .pipe(messageOf('AUTH_TOKEN'))
      .subscribe((message: MessageEvent) => {
        const indentity = message.data.payload.identity;
        const accessToken = message.data.payload.authToken;

        store.setIdentity(indentity);
        store.setAuthToken(accessToken);
      }),
    [iFrameMessages$, dispatchMessage, store]);
}

export const AuthSso: AuthSso.Component = (props) => {
  const authSsoService = useAuthSsoService();
  const authSsoStore = useAuthStore();
  const iFrameRef = React.useRef(null);
  const trustedUrl = useTrustedIFrameUrl(props.ssoServiceUrl);
  const origin = useRefByCallback(() => new URL(props.ssoServiceUrl).origin).current;
  const iFrameMessages$ = useIFrameMessageBus(origin);
  const dispatchMessage = useDispatchMessage(iFrameRef, origin);

  useListenUnauthorizedMessage(iFrameMessages$);
  useListenSsoReadyMessage(iFrameMessages$, dispatchMessage, authSsoService);
  useListentAuthtokenMessage(iFrameMessages$, dispatchMessage, authSsoStore);

  return (
    <iframe ref={iFrameRef} src={trustedUrl} width="0" height="0" style={{display: 'none'}}/>
  );
};

AuthSso.displayName = 'AuthSso';

AuthSso.defaultProps = {};
