# auth-sso-react

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test auth-sso-react` to execute the unit tests via [Jest](https://jestjs.io).
