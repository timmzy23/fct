import { BehaviorSubject } from 'rxjs';

export class CurrentIndexSubject extends BehaviorSubject<number> {
  constructor(public length: number, value: number = 0) {
    super(value);
  }

  public jumptToNext(stepBy: number = 1) {
    const newCurrIx = Math.min(this.getValue() + stepBy, this.length - 1);

    this.currentIx = newCurrIx;
  }

  public jumpToPrev(stepBy: number = 1) {
    const newCurrIx = Math.max(this.getValue() - stepBy, 0);

    this.currentIx = newCurrIx;
  }

  public jumpToStart() {
    this.currentIx = 0;
  }

  public jumpToEnd() {
    this.currentIx = this.length - 1;
  }

  public get currentIx(): number {
    return this.getValue();
  }

  public set currentIx(currentIx: number) {
    this.next(currentIx);
  }
}
