import {
  from,
  MonoTypeOperatorFunction,
  Observable,
  of,
  pipe,
  throwError
  } from 'rxjs';
import {
  concatMap,
  filter,
  map,
  switchMap,
  tap
  } from 'rxjs/operators';
import { ErrnoError } from '../errno-error';
import { ReflectionHelper } from '../type-helpers';

function resolveError<V>(
  error: ((value: V) => ErrnoError) | string,
  value: V
): Observable<never> {
  if (ReflectionHelper.isString(error)) {
    const errno = error as string;
    return throwError(new ErrnoError(errno))
  } else {
    const errorFactory = error as (value: V) => ErrnoError;
    return throwError(errorFactory(value));
  }
}

export function assert<V>(
  assertClb: (value: V) => boolean,
  createError: ((value: V) => ErrnoError) | string,
) {
  return switchMap((value: V) => assertClb(value) ? 
      of(value) : 
      resolveError(createError, value));
}

export function assertPromise<V>(
  assertClb: (value: V) => Promise<boolean>,
  createError: ((value: V) => ErrnoError) | string
) {
  return switchMap((value: V) => {
    return from(assertClb(value)).pipe(
        switchMap((isValid) => isValid ? 
          of(value) : 
          resolveError(createError, value)))
  });
}

export function switchPromise<V, R>(promiseClb: (value: V) => Promise<R>) {
  return switchMap((value: V) => from(promiseClb(value)));
}

// Inspired by: https://gist.github.com/bjesuiter/288326f9822e0bc82389976f8da66dd8
export function filterAsync<T>(predicate: (value: T, index: number) => Promise<boolean>): MonoTypeOperatorFunction<T> {
  let count = 0;
  return pipe(
      // Convert the predicate Promise<boolean> to an observable (which resolves the promise,
      // Then combine the boolean result of the promise with the input data to a container object
      concatMap((data: T) => {
          return from(predicate(data, count++))
              .pipe(map((isValid) => ({filterResult: isValid, entry: data})));
      }),
      // Filter the container object synchronously for the value in each data container object
      filter(data => data.filterResult === true),
      // remove the data container object from the observable chain
      map(data => data.entry)
  );
}
