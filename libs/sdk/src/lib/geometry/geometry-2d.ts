export declare namespace Geometry2D {

  namespace Point {
    interface Dimensions {
      x: number;
      y: number;
    }
  }


  namespace Rect {
    interface Dimensions {
      start: Point.Dimensions;
      width: number;
      height: number;
      shiftY?: number;
    }
  }

  namespace Triangle {
    interface Dimensions {
      a: Point.Dimensions;
      b: Point.Dimensions;
      c: Point.Dimensions;
    }

    interface Isosceles {
      height: number;
      base: number;
      center: Point.Dimensions;
    }
  }

  namespace Line {
    interface Dimensions {
      start: Point.Dimensions;
      end: Point.Dimensions;
    }
  }

  namespace Polygon {
    interface Dimensions {
      path?: Path;
      absolutePath?: Path;
    }

    type Path = Point.Dimensions[];
  }
}

class Rect {

  public static getRectCenter(rect: Geometry2D.Rect.Dimensions): Geometry2D.Point.Dimensions {
    return {
      x: rect.start.x + (rect.width / 2), 
      y: rect.start.y + (rect.height / 2)
    };
  }

  public static getRectStart(rect: Geometry2D.Rect.Dimensions): Geometry2D.Point.Dimensions {
    return {
      x: rect.start.x - (rect.width / 2),
      y: rect.start.y - (rect.height / 2)
    }
  }
}

class Circle {
  public static getPosition(
    radius: number,
    rad: number,
    center: Geometry2D.Point.Dimensions
  ): Geometry2D.Point.Dimensions {
    return {
      x: center.x + (Math.cos(rad) * radius),
      y: center.y + (Math.sin(rad) * radius)
    }
  }
}

class Ellipse {
  public static getPosition(
    radiusA: number,
    radiusB: number,
    rad: number,
    center: Geometry2D.Point.Dimensions
  ): Geometry2D.Point.Dimensions {
    return {
      x: center.x + (radiusA * Math.cos(rad)),
      y: center.y + (radiusB * Math.sin(rad))
    }
  }  
}

class Point {
  public static rotate(
    point: Geometry2D.Point.Dimensions,
    center: Geometry2D.Point.Dimensions,
    rad: number
  ): Geometry2D.Point.Dimensions {
    // https://www.mathsisfun.com/algebra/trig-finding-angle-right-triangle.html
    // Compute rotation based on angle computation in triangle from hypotenuse
    // and oposite.
    const hypotenuse = Line.getLength({ start: point, end: center }); // Radius
    const oposite = Line.getLength({
      start: point,
      end: { x: point.x, y: center.y }
    });
    const baseRad = 
      point.x <= center.x && point.y <= center.y ? // I. quadrant
        Math.asin(oposite / hypotenuse) :
      point.x > center.x && point.y <= center.y ? // II. quadrant
        Math.PI - Math.asin(oposite / hypotenuse) :
      point.x > center.x && point.y > center.y ? // III. quadrant
        Math.PI + Math.asin(oposite / hypotenuse) :
      point.x <= center.x && point.y > center.y ? // IV. quadrant
        Math.asin(oposite / hypotenuse) * -1 :
        0;

    const absRotationRad = baseRad + rad;

    return {
      x: center.x - (Math.cos(absRotationRad) * hypotenuse),
      y: center.y - (Math.sin(absRotationRad) * hypotenuse)
    };
  }
}

class Triangle {

  public static getTriangIncenter(triangle: Geometry2D.Triangle.Dimensions): Geometry2D.Point.Dimensions {
    const lengthA = Triangle.getLengthA(triangle);
    const lengthB = Triangle.getLengthB(triangle);
    const lengthC = Triangle.getLengthC(triangle);

    const totalLength = lengthA + lengthB + lengthC;
    const iCenterX = 
      ((lengthA * triangle.a.x) +
      (lengthB * triangle.b.x) +
      (lengthC * triangle.c.x)) /
      totalLength;
    const iCenterY = 
      ((lengthA * triangle.a.y) +
      (lengthB * triangle.b.y) +
      (lengthC * triangle.c.y)) /
      totalLength;

    return { x: iCenterX, y: iCenterY };
  }

  public static getLengthA(triangle: Geometry2D.Triangle.Dimensions): number {
    return Line.getLength({ start: triangle.a, end: triangle.b });
  }

  public static getLengthB(triangle: Geometry2D.Triangle.Dimensions): number {
    return Line.getLength({ start: triangle.b, end: triangle.c });
  }

  public static getLengthC(triangle: Geometry2D.Triangle.Dimensions): number {
    return Line.getLength({ start: triangle.c, end: triangle.a });
  }

  public static getIsoscelesTriangle(
    triangle: Geometry2D.Triangle.Isosceles
  ): Geometry2D.Triangle.Dimensions {
    const a: Geometry2D.Point.Dimensions = {
      x: triangle.center.x - (triangle.base / 2),
      y: triangle.center.y + (triangle.height / 2)
    };
    const b: Geometry2D.Point.Dimensions = {
      x: triangle.center.x,
      y: triangle.center.y - (triangle.height / 2)
    };
    const c: Geometry2D.Point.Dimensions = {
      x: triangle.center.x + (triangle.base / 2),
      y: triangle.center.y + (triangle.height / 2)
    };

    return { a, b, c };
  }

  public static rotateTriangle(
    triangle: Geometry2D.Triangle.Dimensions,
    center: Geometry2D.Point.Dimensions,
    angle: number,
    unit: string = Geometry2D.UNIT_RAD
  ) {
    const rotationRad = unit === Geometry2D.UNIT_RAD ?
        angle : Geometry2D.angeToRad(angle);
    const [rotA, rotB, rotC] = [triangle.a, triangle.b, triangle.c].map(
        (point) => Point.rotate(point, center, rotationRad));

    return { a: rotA, b: rotB, c: rotC };
  }

  /**
   * Compute the angle in isoceles triange between legs in radians.
   * 
   * Legs are lines: AB and CB.
   * Base is line AC
   * 
   * 
   * https://keisan.casio.com/exec/system/1273850202
   */
  public static getIsoscelesAngle(triangle: Geometry2D.Triangle.Dimensions): number {
    // Used formula: angle = atan(2height / 2)
    const base = Line.getLength({ start: triangle.a, end: triangle.c });
    const legA = Line.getLength({ start: triangle.a, end: triangle.b });
    const legB = Line.getLength({ start: triangle.c, end: triangle.b });

    // Round legs length with precision to 6 decimal digits to compareq equality.
    // It has to be rounded with some amount of precision, because
    // the floating results of sin/cos/asin etc operations may difffer
    // in high decimal resolution in javascript.
    const roundLegA = Math.round(legA * 1000000) / 1000000;
    const roundLegB = Math.round(legB * 1000000) / 1000000;

    if (roundLegA !== roundLegB) { 
      throw new TypeError(
        'Given triangle is not the isosceles, legs do not have same length!'
      ); 
    }

    // LegA is acting as base in pythagorien theorem, because the right angle
    // is between height and half of isosceles triangle base.
    const height = Math.sqrt(Math.pow(legA, 2) - Math.pow(base / 2, 2));

    return Math.PI - (2 * Math.atan((2 * height) / base));
  }
}

class Line {
  public static getLength(line: Geometry2D.Line.Dimensions): number {
    const lengthX = Math.abs(line.start.x - line.end.x);
    const lengthY = Math.abs(line.start.y - line.end.y);

    const size = Math.pow(lengthX, 2) + Math.pow(lengthY, 2);

    return size !== 0 ? Math.sqrt(size) : 0;
  }

  /**
   * Computes angle in radians of line in two dimension space related to
   * on of the line points like it would be a center of a circle.
   */
  public static getAngle(line: Geometry2D.Line.Dimensions): number {
    // Use rules of isoscele triangle to get the angle between legs.
    const legLength = Line.getLength(line);
    const legA = line;
    const legB = { 
      start: line.start, 
      end: { 
        x: line.start.x - legLength,
        y: line.start.y
      }
    };
    return Geometry2D.triangle.getIsoscelesAngle({
      a: legA.end,
      b: legA.start,
      c: legB.end
    });
  }
}

class Polygon {

  public static EXCLUDE_START = true;

  /**
   * Keeps first point in absolute coordinates and the rest in relative to
   * previous one.
   */
  public static convertToRelatives(
    path: Geometry2D.Polygon.Path,
    excludeStart: boolean = false
  ): Geometry2D.Polygon.Path {
    const start = Polygon.getStartPoint(path);
    let currX = start.x;
    let currY = start.y;
    const acc: Geometry2D.Polygon.Path = [];

    if (!excludeStart) {
      acc.push(start);
    }

    for (let i = 1; i < path.length; i++) {
      const point = path[i];
      const relX = point.x - currX;
      const relY = point.y - currY;

      acc.push({ x: relX, y: relY });
      currX += relX;
      currY += relY;
    }

    return acc;
  }

  public static getStartPoint(path: Geometry2D.Polygon.Path): Geometry2D.Point.Dimensions {
    return path[0];
  }

  public static rotate(
    absolutePath: Geometry2D.Polygon.Path,
    center: Geometry2D.Point.Dimensions,
    rotationRad: number
  ) {
    return absolutePath.map((point) => Point.rotate(point, center, rotationRad))
  }

  public static rotateAngle(
    absolutePath: Geometry2D.Polygon.Path,
    center: Geometry2D.Point.Dimensions,
    rotationAngle: number
  ) {
    const rotationRad = Geometry2D.angeToRad(rotationAngle);

    return absolutePath.map((point) => Point.rotate(point, center, rotationRad));
  }
}

export class Geometry2D {
  public static UNIT_RAD = 'radians';
  public static UNIT_ANGLE = 'angles';

  public static rect = Rect;

  public static circle = Circle;

  public static triangle = Triangle;

  public static line = Line;

  public static polygon = Polygon;

  public static point = Point;

  public static ellipse = Ellipse;

  public static angeToRad(angle: number) {
    return angle * Math.PI / 180;
  }

  public static radToAngle(rad: number) {
    return rad * 180 / Math.PI;
  }
}
