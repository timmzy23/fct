import {
  Observable,
  Observer
  } from 'rxjs';

/**
 * Runs periodic interval which waits until `next` function
 * is called to start another interval period.
 *
 * To be used when task on tick moment may take a while and we want
 * to prevent unfinished jobs to stuck.
 */
export declare namespace PollingObservable {
  interface Tick {
    next: () => void;
    loopNo: number;
    isFirst: boolean;
  }
}

export class PollingObservable {

  private currentTimeout: number | any;

  private loopNo = 0;


  /**
   * @param dueTimeMs Time before start in milliseconds
   * @param periodMs Polling cycle period in milliseconds
   */
  public static create(
    dueTimeMs: number, periodMs: number
  ): Observable<PollingObservable.Tick> {
    return new Observable((observer: Observer<PollingObservable.Tick>) =>
        new PollingObservable(periodMs, observer).run(dueTimeMs));
  }

  /**
   * @param periodMs
   * @param observer
   */
  private constructor(
    private periodMs: number,
    private observer: Observer<PollingObservable.Tick>
  ) {}

  public dispose = () => {
    if (this.currentTimeout) {
      clearTimeout(this.currentTimeout);
    }
  };

  public next(timeoutMs: number) {
    if (this.observer.closed) { return; }

    if (this.currentTimeout) {
      clearTimeout(this.currentTimeout);
    }

    this.currentTimeout = setTimeout(() => {
      this.observer.next({
        isFirst: this.loopNo === 0,
        loopNo: this.loopNo,
        next: () => this.next(this.periodMs),
      });
      this.loopNo++;
    }, timeoutMs);
  }

  public run(dueTimeMs: number) {
    this.next(dueTimeMs);

    return this.dispose;
  }
}
