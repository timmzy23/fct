import { ErrnoError } from './errno-error';

export const ERR_CONFIG_MISSING_REQURIED_KEY = 'ERR_CONFIG_MISSING_REQURIED_KEY';

/**
 * Basic configuration container, which throws an error when 
 * required config value is missing.
 */
export class Configuration<C = any> {

  private config: C;

  constructor(config: C) {
    this.config = { ...config };
  }

  public get<T = string>(key: string, required: boolean = true): T {
    const value = this.config[key];

    if (value === undefined && required) {
      throw new ErrnoError(
          ERR_CONFIG_MISSING_REQURIED_KEY,
          `Config: Missing required for "${key}"`,
          { key });
    }

    return value;
  }

  public set(key: string, value: any) {
    this.config[key] = value;
  }
}
