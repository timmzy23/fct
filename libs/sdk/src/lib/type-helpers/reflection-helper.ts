export abstract class ReflectionHelper {
  public static isSymbol(value: any): boolean {
    const type = typeof value;
    return type === 'symbol';
  }

  public static isObject(value: any): boolean {
    const type = typeof value;
    return type === 'object' && value != null || type === 'function';
  }

  public static isFunction(value: any): boolean {
    const type = typeof value;
    return type === 'function';
  }

  public static isString(value: any): boolean {
    const type = typeof value;
    return type === 'string';
  }

  public static isEmpty(val: any): boolean {
    return val === undefined || Number.isNaN(val) ||
        val === false || val === '' ||
      (Array.isArray(val) && val.length === 0) ||
      (val instanceof Map && val.size === 0) ||
      (val instanceof Set && val.size === 0) ||
      (ReflectionHelper.isObject(val) && Reflect.ownKeys(val).length === 0);
  }

  public static forEachOwnKey(
    object: {[key: string]: any},
    callback: (key: string, value: any, index: number, object: {[key: string]: any}) => void
  ) {
    Reflect.ownKeys(object)
        .forEach((key: string, index: number) => 
            callback(key, object[key], index, object));
  }

  public static removeUndefinedProps(obj: any) {
    return Reflect.ownKeys(obj)
        .filter((propName) => obj[propName] !== undefined)
        .reduce((acc, propName) => {
          acc[propName] = obj[propName];
          return acc;
        }, {});
  }

  public static isInEnum(enumDef: any, value: any): boolean {
    return Object.values(enumDef).includes(value);
  }

  public static isNumber(value: any): boolean {
    /* tslint:disable triple-equals */
    return typeof value == 'number';
    /* tslint:enable */
  }

  public static isBoolean(value: any): boolean {
    return typeof value === 'boolean';
  }
}
