export class StringHelper {
  public static getLastChar(string: string): string {
    return string[string.length - 1];
  }
}