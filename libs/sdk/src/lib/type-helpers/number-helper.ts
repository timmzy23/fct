import { StringHelper } from './string-helper';

export class NumberHelper {
  public static fromPercent(percents: string): number {
    if (StringHelper.getLastChar(percents) !== '%') {
      throw new TypeError('Invalid format, missing "%" symbol!');
    }
    return Number.parseFloat(percents.slice(0, percents.length - 1));
  }
}