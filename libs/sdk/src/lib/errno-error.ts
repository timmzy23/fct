export const ERR_UNKNOWN = 'ERR_UNKNOWN';

export declare namespace ErrnoError {
  interface Errno {
    errno: string;
  }
}

export class ErrnoError extends Error {
  constructor(
    public readonly errno: string,
    message: string = errno,
    public readonly params?: {[key: string]: any },
    public origError?: Error
  ) {
    super(message);
  }
}
