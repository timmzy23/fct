import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthSsoStore } from './auth-sso.store';

@Injectable()
export class AuthSsoRequestInterceptor implements HttpInterceptor {

  constructor(private authSso: AuthSsoStore) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clone = req.clone({
      setHeaders: {
        'Authorization': `bearer ${this.authSso.getAuthToken()}`
      },
    });

    return next.handle(clone);
  }
}
