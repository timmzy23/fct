import { Injectable } from '@angular/core';
import {
  ApplicationState,
  ObservableStore
  } from '@fct/sdk-ng';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';

export declare namespace AuthSsoStore {
  interface State {
    identity?: any;
    authToken?: string;
  }
}
@Injectable()
export class AuthSsoStore extends ObservableStore<AuthSsoStore.State> {
  constructor(appState: ApplicationState){
    super('auth.sso', appState, {});
  }

  public setIdentity(identity: any) {
    const prevState = this.getState();
    this.setState({
      ...prevState,
      identity
    });
  }

  public setAuthToken(authToken: string) {
    const prevState = this.getState();

    this.setState({
      ...prevState,
      authToken
    });
  }

  public getIdentitiy() {
    return this.getState().identity;
  }

  public getAuthToken() {
    return this.getState().authToken;
  }

  public observeAuthToken(): Observable<any> {
    return this.change.pipe(
      filter((change) => 
          change.prevState.authToken !== change.nextState.authToken),
      map((change) => change.nextState.authToken));
  }

  public observeIdentity(): Observable<any> {
    return this.change.pipe(
      filter((change) => 
          change.prevState.identity !== change.nextState.identity),
      map((change) => change.nextState.identity));
  }
}