import {
  DomSanitizer,
  SafeResourceUrl
  } from '@angular/platform-browser';
import { useEffect } from '@fct/sdk-ng';
import {
  fromEvent,
  Observable
  } from 'rxjs';
import {
  filter,
  map,
  share,
  tap
  } from 'rxjs/operators';
import { messageOf } from './auth-sso.helpers';
import { AuthSsoService } from './auth-sso.service';
import { AuthSsoStore } from './auth-sso.store';
import {
  Component,
  Input,
  OnInit,
  ViewChild,
  ElementRef,
  } from '@angular/core';

@Component({
  selector: 'fct-auth-sso',
  template: `<iframe #ssoWindow [src]="ssoServiceUrlTrusted" width="0" height="0"></iframe>`,
  styles: [`
    :host {
      width: 0;
      height: 0;
      display: block;
    }
  `]
})
export class AuthSsoComponent implements OnInit {
  @Input()
  public ssoServiceUrl: string;

  public ssoServiceUrlTrusted: SafeResourceUrl;

  @ViewChild('ssoWindow', { static: true})
  private ssoWindow: ElementRef<HTMLIFrameElement>;

  private iFrameMessages$: Observable<MessageEvent> = fromEvent(window, 'message').pipe(
    filter((message: MessageEvent) => message.origin === new URL(this.ssoServiceUrl).origin),
    tap((message) => console.log('PARENT MSG >>>', message)),
    share());

  @useEffect([])
  protected listenUnauthorizedMessages() {
    const subscription = this.iFrameMessages$
        .pipe(
          messageOf('NOT_AUTHENTICATED'),
          map((message) => message.data))
        .subscribe((action: any) => {
          const loginUrl = action.payload.url;
          location.href = loginUrl;
        });

    return () => subscription.unsubscribe();
  }

  @useEffect([])
  protected listenSsoReadyMessage() {
    const subscription = this.iFrameMessages$
      .pipe(messageOf('SSO_READY'))
      .subscribe(() => this.dispatchRegisterMessage())
    return () => subscription.unsubscribe();
  }

  @useEffect([])
  protected listenAuthTokenMessage() {
    const subscription = this.iFrameMessages$
        .pipe(messageOf('AUTH_TOKEN'))
        .subscribe((message: MessageEvent) => {
          const indentity = message.data.payload.identity;
          const accessToken = message.data.payload.authToken;

          this.store.setIdentity(indentity);
          this.store.setAuthToken(accessToken);
        });

    return () => subscription.unsubscribe();
  }

  constructor(
    private sanitizer: DomSanitizer,
    private authSsoService: AuthSsoService,
    private store: AuthSsoStore
  ) {}

  public ngOnInit() {
    // Angular sanitizing does nothing for some uknown reason so, lets sanitize
    // the url protocol by simple way:
    const url = new URL(this.ssoServiceUrl);
    if (!['http:', 'https:'].includes(url.protocol)) {
      throw new Error(`Untrusted url: ${url}`);
    }
    this.ssoServiceUrlTrusted = 
        this.sanitizer.bypassSecurityTrustResourceUrl(this.ssoServiceUrl);
  }

  private dispatchRegisterMessage() {
    this.dispatchMessage({
      type: 'REGISTER',
      payload: { 
        authCode: this.authSsoService.getAuthCode(),
        redirectPath: '/'
      }
    })
  }

  private dispatchMessage(message: any) {
    this.ssoWindow.nativeElement.contentWindow.postMessage(
      message, 
      new URL(this.ssoServiceUrl).origin);
  }
}
