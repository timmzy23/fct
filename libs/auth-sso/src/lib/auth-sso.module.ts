import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthSsoRequestInterceptor } from './auht-sso.request.interceptor';
import { AuthSsoComponent } from './auth-sso.component';
import { AuthSsoService } from './auth-sso.service';
import { AuthSsoStore } from './auth-sso.store';

@NgModule({
  declarations: [AuthSsoComponent],
  imports: [CommonModule],
  exports: [AuthSsoComponent],
  providers: [
    AuthSsoService, 
    AuthSsoStore,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthSsoRequestInterceptor,
      multi: true
    }
  ]
})
export class AuthSsoModule {}
