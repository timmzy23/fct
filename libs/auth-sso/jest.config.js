module.exports = {
  name: 'auth-sso',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/auth-sso',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
