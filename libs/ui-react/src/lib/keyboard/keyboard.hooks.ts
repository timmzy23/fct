import {
  actionOf,
  useEvents,
  useSubscription
  } from '@fct/sdk-react';
import { fromEvent } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';
import {
  KeyboardArrowDownEvent,
  KeyboardArrowLeftEvent,
  KeyboardArrowRightEvent,
  KeyboardArrowUpEvent,
  KeyboardEnterEvent,
  KeyboardESCEvent,
  KeyboardOpenEvent
  } from './keyborad.events';

export function useKeyboard() {
  const events$ = useEvents();

  // Listen for "ESC"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
      .pipe(filter((evt) => evt.code === 'Escape'))
      .subscribe((evt: KeyboardEvent) =>
          events$.emit(KeyboardESCEvent.create(evt))), 
    []);

  // Listen for "Open"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
    .pipe(filter((evt) => (evt.metaKey || evt.ctrlKey) && evt.code === 'KeyO'))
    .subscribe((evt: KeyboardEvent) =>
        events$.emit(KeyboardOpenEvent.create(evt))), 
    []);

  // Listen for "ARROW_UP"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowUp'))
    .subscribe((evt: KeyboardEvent) =>
        events$.emit(KeyboardArrowUpEvent.create(evt))), 
    []);

  // Listen for "ARROW_DOWN"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowDown'))
    .subscribe((evt: KeyboardEvent) =>
        events$.emit(KeyboardArrowDownEvent.create(evt))), 
    []);

  // Listen for "ARROW_LEFT"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowLeft'))
    .subscribe((evt: KeyboardEvent) =>
        events$.emit(KeyboardArrowLeftEvent.create(evt))), 
    []);

  // Listen for "ARROW_RIGHT"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowRight'))
    .subscribe((evt: KeyboardEvent) =>
        events$.emit(KeyboardArrowRightEvent.create(evt))), 
    []);

  // Listen for "KEYBOARD_ENTER"
  useSubscription(
    () => fromEvent<KeyboardEvent>(document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'Enter'))
    .subscribe((evt: KeyboardEvent) =>
        events$.emit(KeyboardEnterEvent.create(evt))), 
    []);
}

export function useOnKeyboardEvent(
  eventTypeDef: any,
  eventHandler: (evt: KeyboardEvent) => void
) {
  const events$ = useEvents();

  useSubscription(
    () => events$
      .pipe(
        actionOf(eventTypeDef),
        map((evt: any) => evt.payload))
      .subscribe(eventHandler),
    [events$, eventHandler]);
}

export function useOnKeyboardOpenEvent(
  eventHandler: (evt: KeyboardEvent
) => void) {
  useOnKeyboardEvent(KeyboardOpenEvent, eventHandler);
}

export function useOnKeyboardEscapeEvent(
  eventHandler: (evt: KeyboardEvent) => void
) {
  useOnKeyboardEvent(KeyboardESCEvent, eventHandler);
}

export function useOnKeyboardArrowUpEvent(
  eventHandler: (evt: KeyboardEvent) => void
) {
  useOnKeyboardEvent(KeyboardArrowUpEvent, eventHandler);
}

export function useOnKeyboardArrowDownEvent(
  eventHandler: (evt: KeyboardEvent) => void
) {
  useOnKeyboardEvent(KeyboardArrowDownEvent, eventHandler);
}

export function useOnKeyboardEnterEvent(
  eventHandler: (evt: KeyboardEvent) => void
) {
  useOnKeyboardEvent(KeyboardEnterEvent, eventHandler);
}