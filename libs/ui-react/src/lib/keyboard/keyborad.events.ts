import { FluxStandardAction } from '@fct/sdk-react';

enum KeyboardEventTypes {
  ESC = 'evt.keyboard.esc',
  OPEN = 'evt.keyboard.open',
  ENTER = 'evt.keyboard.enter',
  ARROW_DOWN = 'evt.keyboard.arrowDown',
  ARROW_UP = 'evt.keyboard.arrowUp',
  ARROW_LEFT = 'evt.keyboard.arrowLeft',
  ARROW_RIGHT = 'evt.keyboard.arrowRight'
}

export declare namespace KeyboardESCEvent {
  type ACTION_TYPE = KeyboardEventTypes.ESC;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
}

export class KeyboardESCEvent {
  public static ACTION_TYPE: KeyboardESCEvent.ACTION_TYPE = 
    KeyboardEventTypes.ESC;

  public static create(payload: KeyboardESCEvent.Payload): KeyboardESCEvent.Action {
    return FluxStandardAction.create(KeyboardESCEvent.ACTION_TYPE, payload);
  }
}

export declare namespace KeyboardEnterEvent {
  type ACTION_TYPE = KeyboardEventTypes.ENTER;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
}

export class KeyboardEnterEvent {
  public static ACTION_TYPE: KeyboardEnterEvent.ACTION_TYPE = 
      KeyboardEventTypes.ENTER;

  public static create(payload: KeyboardEnterEvent.Payload): KeyboardEnterEvent.Action {
    return FluxStandardAction.create(KeyboardEnterEvent.ACTION_TYPE, payload);
  }
}

export declare namespace KeyboardOpenEvent {
  type ACTION_TYPE = KeyboardEventTypes.OPEN;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
}

export class KeyboardOpenEvent {
  public static ACTION_TYPE: KeyboardOpenEvent.ACTION_TYPE = 
      KeyboardEventTypes.OPEN;

  public static create(payload: KeyboardOpenEvent.Payload): KeyboardOpenEvent.Action {
    return FluxStandardAction.create(KeyboardOpenEvent.ACTION_TYPE, payload);
  }
}

export declare namespace KeyboardArrowDownEvent {
  type ACTION_TYPE = KeyboardEventTypes.ARROW_DOWN;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
  
}

export class KeyboardArrowDownEvent {
  public static ACTION_TYPE: KeyboardArrowDownEvent.ACTION_TYPE = 
      KeyboardEventTypes.ARROW_DOWN;

  public static create(payload: KeyboardArrowDownEvent.Payload): KeyboardArrowDownEvent.Action {
    return FluxStandardAction.create(KeyboardArrowDownEvent.ACTION_TYPE, payload);
  }
}

export declare namespace KeyboardArrowUpEvent {
  type ACTION_TYPE = KeyboardEventTypes.ARROW_UP;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
  
}

export class KeyboardArrowUpEvent {
  public static ACTION_TYPE: KeyboardArrowUpEvent.ACTION_TYPE = 
      KeyboardEventTypes.ARROW_UP;

  public static create(payload: KeyboardArrowUpEvent.Payload): KeyboardArrowUpEvent.Action {
    return FluxStandardAction.create(KeyboardArrowUpEvent.ACTION_TYPE, payload);
  }
}

export declare namespace KeyboardArrowLeftEvent {
  type ACTION_TYPE = KeyboardEventTypes.ARROW_LEFT;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
  
}

export class KeyboardArrowLeftEvent {
  public static ACTION_TYPE: KeyboardArrowLeftEvent.ACTION_TYPE = 
      KeyboardEventTypes.ARROW_LEFT;

  public static create(payload: KeyboardArrowLeftEvent.Payload): KeyboardArrowLeftEvent.Action {
    return FluxStandardAction.create(KeyboardArrowLeftEvent.ACTION_TYPE, payload);
  }
}

export declare namespace KeyboardArrowRightEvent {
  type ACTION_TYPE = KeyboardEventTypes.ARROW_RIGHT;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  type Payload = KeyboardEvent;
  
}

export class KeyboardArrowRightEvent {
  public static ACTION_TYPE: KeyboardArrowRightEvent.ACTION_TYPE = 
      KeyboardEventTypes.ARROW_RIGHT;

  public static create(payload: KeyboardArrowRightEvent.Payload): KeyboardArrowRightEvent.Action {
    return FluxStandardAction.create(KeyboardArrowRightEvent.ACTION_TYPE, payload);
  }
}