export enum IconSize {
  EXTRA_EXTRA_SMALL = 'xxs',
  EXTRA_SMALL = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  EXTRA_LARGE = 'xl',
  EXTRA_EXTRA_LARGE = 'xxl'
}

export enum IconColor {
  PRIMARY = 'primary',
  INVERTED = 'inverted',
  SECONDARY = 'secondary',
  MARGINAL = 'marginal',
  DISABLED = 'disabled',
  ERROR = 'error',
  SUCCESS = 'success',
  WARNING = 'warning'
}

