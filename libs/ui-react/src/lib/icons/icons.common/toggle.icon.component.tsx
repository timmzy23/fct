import * as React from 'react';
import { IconColor } from '../icons.types';
import { Icon } from './icon.component';

const styles = require('./toggle.icon.component.scss');

export declare namespace ToggleIcon {
  interface Props extends Icon.Props {
    on?: boolean;
    iconColor?: IconColor;
  }
  type Component = React.FunctionComponent<Props>;
}

export const ToggleIcon: ToggleIcon.Component = (props) => {
  const {
    on,
    ...iconProps
  } = props;
  const togglerX = on ? 34 : 16;
  const togglerLayerWidth = on ? 19 : 2;

  return (
  <Icon {...iconProps} iconStrokeColor={props.iconColor}>
    <path
      fill="red"
      strokeWidth="0"
      d="
        M 14 11 
        C 8 11 2 17 2 25 
        C 2 32 8 39 14 39 
        L 14 39
        L 14 11
        Z 
      " />
    <rect 
        className={styles.toggleLayer} 
        x="13" y="11"
        fill="red"
        width={togglerLayerWidth}
        strokeWidth="0"
        height="28" />
    <path
      strokeWidth="3"
      fill="transparent"
      d="
        M 14 11 
        C 8 11 2 17 2 25 
        C 2 32 8 39 14 39 
        L 34 39 
        C 43 39 48 32 48 25 
        C 48 17 43 11 36 11 Z
      "/>
    <circle 
      fill="white" 
      className={styles.toggler}
      r="14" 
      cx={togglerX} 
      cy="25" 
      strokeWidth="3"/>
  </Icon>
  );
};

ToggleIcon.displayName = 'ToggleIcon';

ToggleIcon.defaultProps = {
  on: false,
  iconColor: IconColor.PRIMARY,
};
