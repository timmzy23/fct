import * as React from 'react';
import {
  IconColor,
  IconSize
  } from '../icons.types';

const styles = require('./icon.component.scss');

export declare namespace Icon {
  interface Props {
    size?: IconSize | string;
    className?: string;
    iconFillColor?: IconColor;
    iconStrokeColor?: IconColor;
    onClick?: () => void;
  }
  type Component = React.FunctionComponent<Props>;
}

function useIconClassNames(
  className: string, size: IconSize | string, iconFillColor: IconColor, iconStroleColor: IconColor
) {
  return React.useMemo(() => {
    const classNames = [];

    if (className) {
      classNames.push(className);
    }

    if (size) {
      classNames.push(styles[`icon-size-${size}`]);
    }

    if (iconFillColor) {
      classNames.push(styles[`icon-fill-color-${iconFillColor}`]);
    }

    if (iconStroleColor) {
      classNames.push(styles[`icon-stroke-color-${iconStroleColor}`]);
    }

    return classNames.join(' ');
  }, [className, size, iconFillColor]);
}

export const Icon: Icon.Component = (props) => {
  const className = useIconClassNames(
      props.className, 
      props.size, 
      props.iconFillColor,
      props.iconStrokeColor);

  return (
    <svg xmlns="http://www.w3.org/2000/svg" 
        viewBox="0 0 50 50" 
        className={className}
        onClick={props.onClick} >
      { props.children }
    </svg>
  );
};

Icon.displayName = 'IconComponent';

Icon.defaultProps = {
  size: IconSize.MEDIUM,
};
