const scrollbarStyles = require('./scrollbar.component.scss');

export function useVerticlaScrollBarClassName(): string {
  return scrollbarStyles.host;
}