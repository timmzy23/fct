import * as React from 'react';

const styles = require('./centered-card.component.scss');

export declare namespace CenteredCard {
  interface Props {
    className?: string;
  }

  type Component = React.FunctionComponent<any>;
}

function useClassName(className: string) {
  return React.useMemo(() => {
    const classNames = [styles.host];
    if (classNames) {
      classNames.push(className);
    }
    return classNames.join(' ');
  }, [className]);
}

export const CenteredCard: CenteredCard.Component = (props) => {
  const className = useClassName(props.className);
  return (
    <div className={className}>
      {props.children}
    </div>
  );
};

CenteredCard.displayName = 'CenteredCard';

CenteredCard.defaultProps = {};
