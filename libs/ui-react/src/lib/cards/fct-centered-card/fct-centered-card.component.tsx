import * as React from 'react';
import { IconColor } from '../../icons/icons.types';
import {
  IntlMessage,
  IntlSwitch
  } from '../../intl';
import { FctLogo } from '../../svg';
import { ThemeSwitch } from '../../themes/themes.components/theme.switch.component';
import {
  Heading,
  Subheading,
  TextAlign
  } from '../../typography';
import { CenteredCard } from '../centered-card/centered-card.component';

const styles = require('./fct-centered-card.component.scss');

function useFctCenteredClassName(props: FctCenteredCard.Props) {
  return React.useMemo(() => {
    const classNames = [
      styles.host,
      props.subtitleIntl ? styles.subtitleGrid : styles.defaultGrid
    ];

    if (props.className) {
      classNames.push(styles.className);
    }

    return classNames.join(' ');
  }, [props.className, !!props.subtitleIntl]);
}

/**
 * Centered card with 4ct platform logo
 */
export declare namespace FctCenteredCard {
  interface Props {
    titleIntl: IntlMessage.Message;
    subtitleIntl?: IntlMessage.Message;
    className?: string;
    contentClassName?: string;
  }
  type Component = React.FunctionComponent<Props>;
}

export const FctCenteredCard: FctCenteredCard.Component = (props) => {
  const classNames = useFctCenteredClassName(props);
  return (
    <CenteredCard>
      <article className={classNames}>
        <header grid-area="logo">
          <FctLogo width="100px" height="40px"/>
        </header>
        <section grid-area="title">
          <Heading.H2>
            <IntlMessage message={props.titleIntl}/>
          </Heading.H2>
        </section>
        {props.subtitleIntl && <section grid-area="subtitle">
          <Subheading.H2 textAlign={TextAlign.CENTER}>
            <IntlMessage message={props.subtitleIntl} />
          </Subheading.H2>
        </section>}
        <section 
          grid-area="content"
          className={props.contentClassName}
        >{props.children}</section>
        <footer grid-area="footer-left">
          <ThemeSwitch iconColor={IconColor.SECONDARY}/>
        </footer>
        <footer grid-area="footer-right">
          <IntlSwitch />
        </footer>
      </article>      
    </CenteredCard>
  );
};

FctCenteredCard.displayName = 'FctCenteredCard';

FctCenteredCard.defaultProps = {};
