import {
  useCommands,
  useDispatchCommand,
  useRefByCallback
  } from '@fct/sdk-react';
import * as React from 'react';
import {
  FormControl,
  FormGroup
  } from '../../forms';
import {
  NightIcon,
  SunIcon,
  ToggleIcon
  } from '../../icons';
import {
  IconColor,
  IconSize
  } from '../../icons/icons.types';
import { ThemeChangeCommand } from '../theme.actions';

const styles = require('./theme.switch.component.scss');

export declare namespace ThemeSwitch {
  interface Props {
    iconColor?: IconColor;
    iconSize?: IconSize;
  }
  type Component = React.FunctionComponent<Props>;

  interface FormPayload {
    themeToggle: boolean;
  }
}


export const ThemeSwitch: ThemeSwitch.Component = (props) => {
  const [toggleValue, setToggleValue] = React.useState(false);
  const dispatch = useDispatchCommand();

  const switchToLight = React.useCallback(() => {
    setToggleValue(false);
    dispatch(ThemeChangeCommand.create({ themeName: 'light-theme' }));
  }, [setToggleValue]);

  const switchToDark = React.useCallback(() => {
    setToggleValue(true);
    dispatch(ThemeChangeCommand.create({ themeName: 'dark-theme' }));
  }, [setToggleValue]);

  return (
    <div className={styles.host}>
      <SunIcon 
          iconColor={props.iconColor} 
          size={props.iconSize}
          className={styles.day}
          onClick={switchToLight} />
      <ToggleIcon 
          on={toggleValue} 
          iconColor={props.iconColor} 
          size={props.iconSize}
          className={styles.toggle} />
      <NightIcon 
          iconColor={props.iconColor} 
          size={props.iconSize}
          className={styles.night}
          onClick={switchToDark} />
    </div>
  );
};

ThemeSwitch.displayName = 'ThemeSwitch';

ThemeSwitch.defaultProps = {
  iconColor: IconColor.PRIMARY,
  iconSize: IconSize.MEDIUM
};
