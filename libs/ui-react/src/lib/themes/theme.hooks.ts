import {
  actionOf,
  ApplicationState,
  FactoryProvider,
  useCommands,
  useProvide,
  useSubscription
  } from '@fct/sdk-react';
import { useInject } from '@fct/sdk-react';
import * as React from 'react';
import { map } from 'rxjs/operators';
import { ThemeChangeCommand } from './theme.actions';
import { ThemesConfig } from './theme.interfaces';
import { ThemeStore } from './theme.store';

export function useThemeStore(): ThemeStore {
  const { themeStore } = useInject({ themeStore: ThemeStore });

  return themeStore;
}

export function dispatchChangeTheme() {
  const commands$ = useCommands();

  return React.useCallback((themeName: string) => 
      commands$.emit(ThemeChangeCommand.create({ themeName })), [commands$]);
}

export function useThemes(config: ThemesConfig) {
  useProvide([
    new FactoryProvider(
      ThemeStore, 
      (appState: ApplicationState) => new ThemeStore(appState, { 
        defaultThemeName: config.defaultThemeName,
        themeNames: config.themeNames,
        currentThemeName: config.defaultThemeName
      }),
      [ApplicationState]),
  ]);

  const themeStore = useThemeStore();
  const commands$ = useCommands();

  useSubscription(
    () => commands$
      .pipe(
        actionOf(ThemeChangeCommand),
        map((action: ThemeChangeCommand.Action) => action.payload.themeName))
      .subscribe((themeName: string) => themeStore.setCurrentTheme(themeName)), 
    [themeStore, commands$]);
}

export function useCurrentThemeState() {
  const themeStore = useThemeStore();
  const [theme, setTheme] = React.useState(themeStore.getCurrentThemeName());

  useSubscription(
    () => themeStore.observeCurrentTheme()
      .pipe(map((change) => change.nextTheme))
      .subscribe((nextTheme) => setTheme(nextTheme)),
    [themeStore]);

  return theme;
}
