export interface ThemesConfig {
  defaultThemeName: string;
  themeNames: string[];
}
