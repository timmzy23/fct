import { FluxStandardAction } from '@fct/sdk-react';

export enum THEME_ACTION_TYPES {
  CHANGE = 'ui.themes.change'
}

export declare namespace ThemeChangeCommand {
  type ACTION_TYPE = THEME_ACTION_TYPES.CHANGE;
  interface Payload {
    themeName: string;
  }
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>;
}

export class ThemeChangeCommand {
  public static ACTION_TYPE: THEME_ACTION_TYPES.CHANGE = 
    THEME_ACTION_TYPES.CHANGE;

  public static create(
    payload: ThemeChangeCommand.Payload
  ): ThemeChangeCommand.Action {
    return FluxStandardAction.create(ThemeChangeCommand.ACTION_TYPE, payload);
  }
}