import { ERR_UNKNOWN } from '@fct/sdk';
import * as React from 'react';
import {
  IntlErrnoError,
  IntlMessage,
  useTranslate
  } from '../../../intl';
import {
  TextBlock,
  TextColor,
  TextFontSize,
  TextFontStyle,
  TextLineHeight
  } from '../../../typography';
import {
  ERR_FORM_CONTROL_REQUIRED,
  ErrorMessages
  } from '../../forms.errors';
import { useInputErrors } from '../../forms.hooks';
import { FormControl } from '../../forms.model';
import { Label } from '../label.component/label.component';

const styles = require('./form-field.component.scss');

export enum FormFieldViewStyle {
  CONDENSED = 'CONDENSED',
  EXPANDED = 'EXPANDED'
}

function useFormFieldClassName(props: FormField.Props) {
  return React.useMemo(() => {
    const classNames = [ styles.host ];

    if (props.viewStyle === FormFieldViewStyle.CONDENSED) {
      classNames.push(styles.condensed);
    } else {
      classNames.push(styles.extended);
    }

    return classNames.join(' ');
  }, [props.viewStyle, props.className]);
}

export declare namespace FormField {
  interface Props {
    className?: string;
    viewStyle?: FormFieldViewStyle;
    control: FormControl;
    labelIntl?: IntlMessage.Message;
    errorMessages?: ErrorMessages;
  }
  type Component = React.FunctionComponent<Props>;
}

const DEFAULT_ERROR_MESSAGES = {
  [ERR_FORM_CONTROL_REQUIRED]: { id: 'core.forms.err.fieldRequired' },
  [ERR_UNKNOWN]: { id: 'core.forms.err.unknown' }
}

function useFormFieldErrorMessages(control: FormControl, errors: ErrorMessages): IntlErrnoError[] {
  const controlErrors = useInputErrors(control);

  return React.useMemo(
    () => {
      // Default error me
      const errorMessages = {
        ...DEFAULT_ERROR_MESSAGES,
        ...errors,
      };
      return Reflect.ownKeys(controlErrors || {})
          .filter((errno: string) => Reflect.has(errorMessages, errno))
          .map((errno: string) => controlErrors[errno])
          .reduce((acc, error) => {
            acc.push(IntlErrnoError.from(error, errorMessages[error.errno]));
            return acc;
          }, [])
    }, 
  [control, controlErrors, errors]);
}

export const FormField: FormField.Component = (props) => {
  const className = useFormFieldClassName(props);
  const {
    viewStyle,
    labelIntl,
    ...inputProps
  } = props;
  const translate = useTranslate();
  const errors = useFormFieldErrorMessages(props.control, props.errorMessages);
  const labelTxt = React.useMemo(() => labelIntl && translate(labelIntl), [labelIntl]);
  
  return (
    <div className={className}>
      {viewStyle === FormFieldViewStyle.EXPANDED && 
      <div grid-area="label">
        <Label htmlFor={props.control.name}>{labelTxt}:</Label>
      </div>}
      <div grid-area="input">
        {props.children}
      </div>
      <ul grid-area="messages">
        {props.control.touched && errors.map((error) => (
          <TextBlock 
              as="li" 
              key={error.errno}
              fontSize={TextFontSize.MEDIUM}
              fontStyle={TextFontStyle.LIGHT_ITALIC}
              textColor={TextColor.ERROR}
              lineHeight={TextLineHeight.MEDIUM}>
            {translate(error.intlMessage)}
          </TextBlock>
        ))}
      </ul>
    </div>
  );
};

FormField.displayName = 'FormField';

FormField.defaultProps = {
  viewStyle: FormFieldViewStyle.EXPANDED
};
