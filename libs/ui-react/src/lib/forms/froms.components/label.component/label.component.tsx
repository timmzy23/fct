import * as React from 'react';
import {
  Text,
  TextFontSize
  } from '../../../typography';

export declare namespace Label {
  type Props = Text.Props<React.LabelHTMLAttributes<HTMLLabelElement>>;
  type Component = React.FunctionComponent<Props>;
}

export const Label: Label.Component = (props) => {
  return (<Text {...props} as="label"/>);
};

Label.displayName = 'Label';

Label.defaultProps = {
  fontSize: TextFontSize.MEDIUM,
};
