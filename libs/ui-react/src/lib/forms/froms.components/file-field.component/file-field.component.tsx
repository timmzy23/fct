import * as React from 'react';
import {
  Button,
  ButtonViewStyle
  } from '../../../buttons';
import {
  Text,
  TextFontSize
  } from '../../../typography';
import { FormField } from '../form-field.component/form-field.component';

const styles = require('./file-field.component.scss');

export declare namespace FileField {
  interface Props extends FormField.Props {
    multiple?: boolean;
  }
  type Component = React.FunctionComponent<Props>;
}

export const FileField: FileField.Component = (props) => {
  const inputElmRef = React.useRef<HTMLInputElement>(null);
  const [fileNames, setFileNames] = React.useState<string[]>([]);

  const handleClick = React.useCallback(
    () => {
      if (!inputElmRef.current) { return; }

      if (!props.control.touched) {
        props.control.markAsTouched();
      }

      inputElmRef.current.click();
    },
    [inputElmRef, props.control]);

  const handleFileInput = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const input: HTMLInputElement = event.target as HTMLInputElement;

      if (input.files) {
        const fileNames: string[] = [];
        for (let ix = 0; ix < input.files.length; ix++) {
          fileNames.push(input.files.item(ix).name);
        }
        setFileNames(fileNames);
      }
      
      props.control.setValue(input.files || null);
    },
    []);


  return (
    <FormField
      className={props.className}
      control={props.control}
      errorMessages={props.errorMessages}
      labelIntl={props.labelIntl}
      viewStyle={props.viewStyle}
    >
     <div className={styles.fileInputContainer}>
      <ul>
        {fileNames.map((fileName) => (
          <li key={fileName}>
          <Text fontSize={TextFontSize.SMALL}>{fileName}</Text>
        </li>
        ))}
      </ul>
      <Button 
      viewStyle={ButtonViewStyle.SECONDARY}
      onClick={handleClick}>
      <input 
          ref={inputElmRef}
          type="file" 
          name="upload" 
          id="upload"
          onChange={handleFileInput}
          multiple={props.multiple}
          className={styles.fileInput} />
        Upload
      </Button>
     </div>
    </FormField>
  );
};

FileField.displayName = 'FileField';

FileField.defaultProps = {};
