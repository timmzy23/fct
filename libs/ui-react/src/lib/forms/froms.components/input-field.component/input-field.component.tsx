import * as React from 'react';
import {
  IntlMessage,
  useTranslate
  } from '../../../intl';
import {
  FormField,
  FormFieldViewStyle
  } from '../form-field.component/form-field.component';
import { Input } from '../input.component/input.component';

export declare namespace InputField {
  interface Props extends FormField.Props {
    placeholderIntl?: IntlMessage.Message;
    type?: string;
    autoComplete?: string;
  }
  type Component = React.FunctionComponent<Props>;
}

export const InputField: InputField.Component = (props) => {
  const translate = useTranslate();
  
  return (
    <FormField 
      className={props.className}
      control={props.control}
      errorMessages={props.errorMessages}
      labelIntl={props.labelIntl}
      viewStyle={props.viewStyle}
    >
      <Input 
            control={props.control} 
            type={props.type} 
            autoComplete={props.autoComplete}
            placeholder={ props.viewStyle === FormFieldViewStyle.EXPANDED ?
                props.placeholderIntl ? translate(props.placeholderIntl) : undefined :
                props.labelIntl ? translate(props.labelIntl) : undefined }/>
    </FormField>
  );
};

InputField.displayName = 'InputField';

InputField.defaultProps = {
  type: 'text',
  autoComplete: 'off'
};
