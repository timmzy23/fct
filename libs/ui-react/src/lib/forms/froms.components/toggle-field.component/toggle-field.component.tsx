import * as React from 'react';
import { ToggleIcon } from '../../../icons';
import { Icon } from '../../../icons/icons.common/icon.component';
import {
  IntlMessage,
  useTranslate
  } from '../../../intl';
import { useFormControlValue } from '../../forms.hooks';
import { FormControl } from '../../forms.model';
import { FormFieldViewStyle } from '../form-field.component/form-field.component';
import { Label } from '../label.component/label.component';

const styles = require('./toggle-field.component.scss');

export declare namespace ToggleField {
  interface Props {
    control: FormControl;
    labelIntl?: IntlMessage.Message;
    viewStyle?: FormFieldViewStyle;
    className?: string;
    toggleIcon?: Icon.Props;
  }
  type Component = React.FunctionComponent<Props>;
}

function useClassName(className: string, viewStyle: FormFieldViewStyle) {
  return React.useMemo(() => {
    const classNames = [ styles.host ];

    if (viewStyle === FormFieldViewStyle.EXPANDED) {
      classNames.push(styles.extended);
    }

    return classNames.join(' ');
  }, [viewStyle, className]);
}

export const ToggleField: ToggleField.Component = (props) => {
  const {
    viewStyle,
    labelIntl,
    control,
    toggleIcon,
    ...inputProps
  } = props;
  const translate = useTranslate();
  const labelTxt = React.useMemo(() => labelIntl && translate(labelIntl), [labelIntl]);
  const [toggleValue, setToggleValue] = useFormControlValue(control);
  const setToggleInputValue = React.useCallback(() => {
    setToggleValue(!control.value);
  }, [control]);
  const hostClassName = useClassName(props.className, props.viewStyle);

  return (
    <div className={hostClassName}>
      {viewStyle === FormFieldViewStyle.EXPANDED && <div grid-area="label">
        <Label htmlFor={props.control.name}>{labelTxt}:</Label>
      </div>}
      <div grid-area="input">
        <ToggleIcon
          {...toggleIcon}
          on={!!toggleValue} 
          onClick={setToggleInputValue} />
      </div>
    </div>
  );
};

ToggleField.displayName = 'ToggleField';

ToggleField.defaultProps = {
  toggleIcon: {}
};
