import * as React from 'react';
import {
  Text,
  TextColor,
  TextFontSize,
  TextFontStyle,
  useTextClassName
  } from '../../../typography';
import {
  useFormControlValidity,
  useInputFormValueAccessor,
  usePristine
  } from '../../forms.hooks';
import { FormControl } from '../../forms.model';
const styles = require('./input.component.scss');

function useInputClassName(props: Input.Props, pristine: boolean, valid: boolean) {
  return React.useMemo(() => {
    const classNames = [styles.host];

    if (props.renderBefore && props.renderAfter) {
      classNames.push(styles.preContentPostGrid);
    } else if (props.renderBefore && !props.renderAfter) {
      classNames.push(styles.preContentGrid);
    } else if (!props.renderBefore && props.renderAfter) {
      classNames.push(styles.contentPostGrid);
    }

    if (!pristine) {
      classNames.push(styles.dirty);
    }
    if (!valid) {
      classNames.push(styles.valid);
    }

    return classNames.join(' ');
  }, [props.className, props.renderBefore, props.renderAfter, pristine]);
}

export declare namespace Input {
  type Props =
      React.InputHTMLAttributes<HTMLInputElement> &
      Text.Props & {
        renderBefore?: () => JSX.Element,
        renderAfter?: () => JSX.Element,
        control: FormControl
      };
  
  type Component = React.FunctionComponent<Props>;
}

export const Input: Input.Component = (props) => {
  const pristine = usePristine(props.control);
  const valid = useFormControlValidity(props.control);
  const inputClassName = useInputClassName(props, pristine, valid);
  const textClassName = useTextClassName(props);
  const {
    fontSize, 
    fontStyle, 
    textColor, 
    textTransform, 
    className,
    renderBefore,
    renderAfter,
    inlineStyles, 
    as, 
    control,
    ...elmProps
  } = props;

  const valueAccessor = useInputFormValueAccessor(props.control, props);

  

  return (
    <div className={inputClassName}>
      { renderBefore && <div grid-area="pre">{renderBefore()}</div> }
      <input 
        {...elmProps}
        name={control.name}
        className={textClassName} 
        grid-area="content"
        { ...valueAccessor}
      />
      { renderAfter && <div grid-area="post">{renderAfter()}</div> }
    </div>
    
  );
};

Input.displayName = 'Input';

Input.defaultProps = {
  fontStyle: TextFontStyle.MEDIUM,
  fontSize: TextFontSize.MEDIUM,
  textColor: TextColor.PRIMARY
};
