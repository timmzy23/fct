import * as React from 'react';
import { ERR_FORM_CONTROL_INVALID_EMAIL } from '../../forms.errors';
import { InputField } from '../input-field.component/input-field.component';

const DEFAULT_ERROR_MESSAGES = {
  [ERR_FORM_CONTROL_INVALID_EMAIL]: { id: 'core.forms.err.invalidEmail' },
}

export declare namespace EmailField {
  type Props = InputField.Props;

  type Component = React.FunctionComponent<Props>;
}

export const EmailField: EmailField.Component = (props) => {
  const { 
    errorMessages,
    ...inputFieldProps 
  } = props;

  const errMessages = {
    ...DEFAULT_ERROR_MESSAGES,
    ...errorMessages
  };

  return (<InputField 
      {...inputFieldProps} 
      errorMessages={errMessages}/>);
};

EmailField.displayName = 'EmailField';

EmailField.defaultProps = {};
