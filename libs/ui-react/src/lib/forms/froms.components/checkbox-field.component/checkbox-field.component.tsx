import * as React from 'react';
import { CheckboxIcon } from '../../../icons';
import { Heading } from '../../../typography';
import { useFormControlValue } from '../../forms.hooks';
import { FormField } from '../form-field.component/form-field.component';

// const styles = require('./checkbox-field.component.scss');

export declare namespace CheckboxField {
  interface Props extends FormField.Props {
  }
  type Component = React.FunctionComponent<Props>;
}

export const CheckboxField: CheckboxField.Component = (props) => {
  const [controlValue, setFormControlValue] = useFormControlValue(props.control);
  const handleClick = React.useCallback(() => {
    if (!props.control.touched) {
      props.control.markAsTouched();
    }

    setFormControlValue(!props.control.value);
  }, [props.control]);

  return (
    <FormField
      className={props.className}
      control={props.control}
      errorMessages={props.errorMessages}
      labelIntl={props.labelIntl}
      viewStyle={props.viewStyle}
    >
      <CheckboxIcon 
        checked={controlValue}
        onClick={handleClick}/>
    </FormField>
  );
};

CheckboxField.displayName = 'CheckboxField';

CheckboxField.defaultProps = {};
