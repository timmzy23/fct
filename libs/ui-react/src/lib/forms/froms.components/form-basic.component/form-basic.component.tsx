import * as React from 'react';
import {
  Button,
  ButtonViewStyle
  } from '../../../buttons';
import {
  IntlMessage,
  useTranslate
  } from '../../../intl';
import {
  Text,
  TextColor,
  TextFontSize,
  TextFontStyle
  } from '../../../typography';
import {
  useFormControlValidity,
  useFormEventHandler
  } from '../../forms.hooks';
import { FormGroup } from '../../forms.model';

const styles = require('./form-basic.component.scss');

function useFormBasicClassName(props: FormBasic.Props) {
  return React.useMemo(() => {
    const classNames = [styles.host];

    return classNames.join(' ');
  }, [props.className]);
}

export declare namespace FormBasic {
  type Props<P = any> = {
    form: FormGroup;
    submitBtn?: IntlMessage.Message;
    resetBtn?: IntlMessage.Message;
    onReset?: (payload: P) => void;
    onSubmit?: (payload: P) => void;
    formContentClassName?: string;
  } & Pick<React.FormHTMLAttributes<HTMLFormElement>, 'name' | 'className' | 'id'>;
  type Component = React.FunctionComponent<Props>;
}

export const FormBasic: FormBasic.Component = (props) => {
  const {
    children, 
    submitBtn,
    resetBtn,
    onSubmit,
    onReset,
    form,
    formContentClassName,
    ...elementProps
  } = props;
  const translate = useTranslate();
  const className = useFormBasicClassName(props);

  const handleSubmit = useFormEventHandler(form, onSubmit);
  const handleReset = useFormEventHandler(form, onReset);
  const isValid = useFormControlValidity(form);

  return (
    <form 
      {...elementProps} 
      className={className}
      onReset={handleReset}
      onSubmit={handleSubmit}
    >
      <div grid-area="form-content" className={formContentClassName}>
        {children}
      </div>
      <div grid-area="reset-btn">
        <Button.Reset viewStyle={ButtonViewStyle.SECONDARY}>
          <Text 
            fontStyle={TextFontStyle.SEMIBOLD}
            fontSize={TextFontSize.MEDIUM}
            textColor={TextColor.PRIMARY}>
            {translate(resetBtn)}
          </Text>
        </Button.Reset>
      </div>
      <div grid-area="submit-btn">
        <Button.Submit disabled={!isValid}>
          <Text 
            fontStyle={TextFontStyle.SEMIBOLD}
            fontSize={TextFontSize.MEDIUM}
            textColor={!isValid ? TextColor.DISABLED : TextColor.INVERTED}>
            {translate(submitBtn)}
          </Text>
        </Button.Submit>
      </div>
    </form>
  );
};

FormBasic.displayName = 'FormBasic';

FormBasic.defaultProps = {
  submitBtn: { id: 'core.btn.ok' },
  resetBtn: { id: 'core.btn.cancel' }
};
