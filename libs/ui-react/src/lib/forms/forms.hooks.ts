import {
  useRefByCallback,
  useSubscription
  } from '@fct/sdk-react';
import * as React from 'react';
import {
  filter,
  map
  } from 'rxjs/operators';
import {
  AbstractControl,
  FormControl,
  FormControlStatus,
  FormGroup
  } from './forms.model';
import { FormsValueAccessor } from './forms.model/froms.value-accessor';

export function useInputFormValueAccessor(
  control: FormControl,
  props: React.InputHTMLAttributes<HTMLInputElement>
): Partial<React.InputHTMLAttributes<HTMLInputElement>> {
  const ref = useRefByCallback(() => new FormsValueAccessor(control, props));
  const [value, setValue] = React.useState(control.value);
  const [dirty, setDirty] = React.useState(control.dirty);

  useSubscription(
    () => control.valueChanges
        .subscribe((currVal) => setValue(currVal)),
    [control, setValue]);

  return {
    value,
    onBlur: ref.current.handleBlur,
    onFocus: ref.current.handleFocus,
    onChange: ref.current.handleChange
  };
}

export function useInputErrors(control: AbstractControl) {
  const [errors, setErrors] = React.useState(control.errors);

  useSubscription(
    () => control
      .observeErrors()
      .subscribe((controlErrors) => setErrors(controlErrors)),
    [control, setErrors]);

  return errors;
}

export function useFormControlValidity(control: AbstractControl): boolean {
  const [validity, setValidity] = React.useState(control.valid);

  useSubscription(
    () => control
      .observeValidity()
      .subscribe((isValid) => setValidity(isValid)),
    [control, setValidity]);

  return validity;
}

export function useFormEventHandler<P = any>(
  form: FormGroup,
  handler?: (payload: P) => void, 
): React.FormEventHandler<HTMLFormElement>  {
  return React.useCallback(
    (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
  
      if (handler) {
        handler(form.value);
      }
    }, 
    [handler, form]);
}

export function useFormControlValue(control: FormControl) {
  const [value, setValue] = React.useState(control.value);

  useSubscription(() => control.valueChanges
        .subscribe((controlValue) => setValue(controlValue)),
  [setValue, control]);

  const setFormControlValue = React.useCallback((controlValue: any) => {
    control.setValue(controlValue);
  },
  [setValue, control]);

  return [value, setFormControlValue];
}

export function usePristine(control: FormControl) {
  const [pristine, setPristine] = React.useState(control.pristine);

  useSubscription(
    () => control.observePristine()
        .subscribe((controlPristine) => setPristine(controlPristine)),
    [setPristine, control]);

  return pristine;
}
