import { IntlMessage } from '../intl';

export const ERR_FORM_CONTROL_REQUIRED = 'ERR_FORM_CONTROL_REQUIRED';
export const ERR_FORM_CONTROL_INVALID_EMAIL = 'ERR_FORM_CONTROL_INVALID_EMAIL';

export interface ErrorMessages {
  [errno: string]: IntlMessage.Message;
}