import { ErrnoError } from '@fct/sdk';
import {
  forkJoin,
  from,
  Observable
  } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  ERR_FORM_CONTROL_INVALID_EMAIL,
  ERR_FORM_CONTROL_REQUIRED
  } from '../forms.errors';
import { AbstractControl } from './forms.abstract-control';
import {
  AsyncValidatorFn,
  ValidationErrors,
  ValidatorFn
  } from './forms.types';

const EMAIL_REGEXP =
  /^(?=.{1,254}$)(?=.{1,64}@)[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

export class Validators {

  public static isEmptyInputValue(value: any): boolean {
    return value == null || value.length === 0;
  }

  public static required(control: AbstractControl): ValidationErrors | null  {
    return Validators.isEmptyInputValue(control.value) ? 
      { 
        [ERR_FORM_CONTROL_REQUIRED]: new ErrnoError(
            ERR_FORM_CONTROL_REQUIRED,
            `Form field ${control.name} is required!`,
            { name: control.name }) 
      } : 
      null;
  }

  public static requiredTrue(control: AbstractControl): ValidationErrors | null  {
    return control.value !== true ? 
      { [ERR_FORM_CONTROL_REQUIRED]: new ErrnoError(ERR_FORM_CONTROL_REQUIRED) } : 
      null;
  }

  public static email(control: AbstractControl): ValidationErrors | null {
    return !EMAIL_REGEXP.test(control.value) ?
      { [ERR_FORM_CONTROL_INVALID_EMAIL]: new ErrnoError(ERR_FORM_CONTROL_INVALID_EMAIL) } :
      null;
  }

  public static nullValidator(control: AbstractControl): ValidationErrors|null { return null; }

  public static compose(validators: null): null
  public static compose(validators: (ValidatorFn|null|undefined)[]): ValidatorFn | null
  public static compose(validators: (ValidatorFn|null|undefined)[]|null): ValidatorFn | null {
    if (!validators) { return null; }

    const presentValidators: ValidatorFn[] = 
        validators.filter(Validators.isPresent);

    if (presentValidators.length === 0) { return null; }

    return function(control: AbstractControl) {
      return Validators.mergeErrors(
          Validators.executeValidators(control, presentValidators));
    }
  }

  public static composeAsync(validators: (AsyncValidatorFn|null)[]): AsyncValidatorFn | null {
    if (!validators) { return null; }

    const presentValidators: AsyncValidatorFn[] = 
        validators.filter(Validators.isPresent);

    if (presentValidators.length === 0) { return null; }    

    return function(control: AbstractControl) {
      const obserbables = Validators
          .executeAsyncValidators(control, presentValidators)
          .map(Validators.toObservable);
      
      return forkJoin(obserbables).pipe(map(Validators.mergeErrors));
    }
  }

  public static toObservable(r: any): Observable<any> {
    const obs = (r instanceof Promise) ? from(r) : r;

    return obs;
  }

  private static isPresent(o: any): boolean {
    return o !== null;
  }

  private static mergeErrors(arrayOfErrors: ValidationErrors[]): ValidationErrors | null {
    const res: {[errno: string]: ErrnoError } = arrayOfErrors.reduce(
      (acc: ValidationErrors | null, errors: ValidationErrors | null) => {
        return errors !== null ?  {...acc, ...errors } : acc;
      },
      {},
    );
    return Reflect.ownKeys(res).length === 0 ? null : res;
  }

  private static executeValidators(control: AbstractControl, validators: ValidatorFn[]): any[] {
    return validators.map((validator) => validator(control));
  }

  private static executeAsyncValidators(
    control: AbstractControl, 
    validators: AsyncValidatorFn[]
  ): any[] {
    return validators.map((validator) => validator(control));
  }
}