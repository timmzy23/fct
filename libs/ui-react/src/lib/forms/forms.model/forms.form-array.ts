import { AbstractControl } from './forms.abstract-control';
import { FormControl } from './forms.form-control';
import {
  AsyncValidatorOpts,
  FormMarkEmitOpts,
  ValidatorOrOpts
  } from './forms.types';

export class FormArray<V = any> extends AbstractControl<V[]> {
  constructor(
    name: string,
    public controls: AbstractControl[],
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidatorOpts
  ) {
    super(
        name,
        AbstractControl.coerceToValidator(validatorOrOpts),
        AbstractControl.coerceToAsyncValidator(asyncValidator));

    this.setUpdateStrategy(validatorOrOpts);
    this.setUpControls();
    this.updateValueAndValidity({ noParent: true, emitEvent: false });
  }

  public at(index: number): AbstractControl {
    return this.controls[index];
  }

  public push(control: AbstractControl): void {
    this.controls.push(control);

    this.registerControl(control);
    this.updateValueAndValidity();
    this.onCollectionChange();
  }

  public insert(index: number, control: AbstractControl): void {
    this.controls.splice(index, 0, control);

    this.registerControl(control);
    this.updateValueAndValidity();
  }

  public removeAt(index: number): void {
    if (this.controls[index]) {
      this.controls[index].registerOnCollectionChange(() => {});
    }
    this.controls.splice(index, 1);
    this.updateValueAndValidity();
  }

  public setControl(index: number, control: AbstractControl): void {
    if (this.controls[index]) {
      this.controls[index].registerOnCollectionChange(() => {});
    }
    if (control) {
      this.controls.splice(index, 0, control);
      this.registerControl(control);
    }
    this.updateValueAndValidity();
    this.onCollectionChange();
  }

  public get length(): number { return this.controls.length; }

  public setValue(value: any[], opts: FormMarkEmitOpts = {}): void {
    this.checkAllValuesPresent(value);
    value.forEach((newValue: any, index: number) => {
      this.assertControlExists(index);
      this.at(index).setValue(
          newValue, 
          { noParent: true, emitEvent: opts.emitEvent });
    });
    this.updateValueAndValidity(opts);
  }

  public patchValue(value: any[], opts: FormMarkEmitOpts = {}) {
    value.forEach((newValue: any, index: number) => {
      if (this.at(index)) {
        this.at(index).patchValue(
          newValue, 
          { noParent: true, emitEvent: opts.emitEvent });
      }
    });
    this.updateValueAndValidity(opts);
  }

  public reset(value: any = [], opts: FormMarkEmitOpts = {}) {
    this.forEachChild((control: AbstractControl, index: number) => {
      control.reset(
          value[index], 
          { noParent: true, emitEvent: opts.emitEvent });
    });
    this.updatePristine(opts);
    this.updateTouched(opts);
    this.updateValueAndValidity(opts);
  }

  public getRawValue(): any[] {
    return this.controls.map((control: AbstractControl) => {
      return control instanceof FormControl ? 
          control.value :
          (control as any).getRawValue();
    });
  }

  public clear(): void {
    if (this.controls.length < 1) { return; }

    this.forEachChild((control: AbstractControl) => 
        control.registerOnCollectionChange(() => {}));
    this.controls.splice(0);
    this.updateValueAndValidity();
  }

  public forEachChild(clb: Function): void {
    return this.controls.forEach((control: AbstractControl, index: number) => {
      clb(control, index);
    });
  }

  public syncPendingControls(): boolean {
    const subtreeUpdated = this.controls.reduce<boolean>(
      (updated: boolean, child: AbstractControl) => {
        return child.syncPendingControls() ? true : updated;
      }, 
      false);

    if (subtreeUpdated) {
      this.updateValueAndValidity({ noParent: true });
    }
    return subtreeUpdated;
  }

  public getChild(name: string | number) {
    return this.at(name as number);
  }

  protected updateValue() {
    this.value = this.controls
        .filter((control) => control.enabled || this.disabled)
        .map((control) => control.value);
  }

  protected anyControls(condition: Function): boolean {
    return this.controls.some(
      (control: AbstractControl) => control.enabled && condition(control));
  }

  protected allControlsDisabled(): boolean {
    for (const control of this.controls) {
      if (control.enabled) {
        return false;
      }
    }
    return this.controls.length > 0 || this.disabled;
  }

  private assertControlExists(index: number): void {
    if (!this.controls.length) {
      throw new Error(`There are no form controls registered with this group yet.`);
    }
    if (!this.at(index)) {
      throw new Error(`Cannot find form control at index ${index}`);
    }
  }

  private setUpControls() {
    this.forEachChild((control: AbstractControl) => 
        this.registerControl(control));
  }

  private checkAllValuesPresent(value: any): void {
    this.forEachChild((control: AbstractControl, index: number) => {
      if (value[index] === undefined) {
        throw new Error(`Must supply a value for form control at index: ${index}.`);
      }
    });
  }

  private registerControl(control: AbstractControl) {
    control.setParent(this);
    control.registerOnCollectionChange(this.onCollectionChange);
  }
}