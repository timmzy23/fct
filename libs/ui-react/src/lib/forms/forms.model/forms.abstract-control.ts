import { ErrnoError } from '@fct/sdk';
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription
  } from 'rxjs';
import {
  filter,
  map,
  share
  } from 'rxjs/operators';
import { FormArray } from './forms.form-array';
import { FormGroup } from './forms.form-group';
import {
  AbstractControlOptions,
  AsyncValidatorFn,
  FormControlStatus,
  FormHooks,
  FormMarkEmitOpts,
  FormMarkOpts,
  FormMarkPristinOpts,
  ValidationErrors,
  ValidatorFn,
  ValidatorOrOpts
  } from './forms.types';
import { Validators } from './forms.validators';

export abstract class AbstractControl<V = any> {

  private _updateOn: FormHooks;

  private _parent: FormGroup | FormArray;

  private _statusChanges = new Subject<FormControlStatus>();

  private _valueChanges = new Subject<V>();

  private _asyncValidationSubscription: Subscription;

  private _pristine = new BehaviorSubject(true);

  protected _onDisabledChange: Function[] = [];

  public value: V;

  public pendingDirty: boolean;

  public get parent(): FormGroup | FormArray {
    return this._parent;
  }

  public status: FormControlStatus;

  public get valid(): boolean {
    return this.status === FormControlStatus.VALID;
  }

  public get invalid(): boolean {
    return this.status === FormControlStatus.INVALID;
  }

  public get pending(): boolean {
    return this.status === FormControlStatus.PENDING;
  }

  public get disabled(): boolean {
    return this.status === FormControlStatus.DISABLED;
  }

  public get enabled(): boolean {
    return this.status !== FormControlStatus.DISABLED;
  }

  public errors: ValidationErrors | null;

  public get pristine(): boolean {
    return this._pristine.getValue();
  }

  public get dirty(): boolean {
    return !this._pristine.getValue();
  }

  public touched: boolean

  public get untouched(): boolean {
    return !this.touched;
  }

  public valueChanges: Observable<any> = this._valueChanges.pipe(share());

  public statusChanges: Observable<any> = this._statusChanges.pipe(share());

  public get updateOn(): FormHooks {
    return this._updateOn ? 
        this._updateOn : 
        (this.parent ? this.parent.updateOn : FormHooks.CHANGE);
  }

  /**
   * Gets top level ancestor
   */
  public get root(): AbstractControl {
    let control: AbstractControl = this;

    while (control.parent) {
      control = control.parent;
    }

    return control;
  }

  private static isOptionsObj(validatorOrOpts?: ValidatorOrOpts): boolean {
    return validatorOrOpts != null && !Array.isArray(validatorOrOpts) &&
        typeof validatorOrOpts === 'object';
  }

  public static coerceToValidator(validatorOrOpts?: ValidatorOrOpts): ValidatorFn | null {
    const validator = (AbstractControl.isOptionsObj(validatorOrOpts) ? 
        (validatorOrOpts as AbstractControlOptions).validators :
        validatorOrOpts) as ValidatorFn | ValidatorFn[] | null;

    return Array.isArray(validator) ? 
        AbstractControl.composeValidators(validator) : 
        validator || null;
  }

  public static coerceToAsyncValidator(
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null,
    validatorOrOpts?: ValidatorOrOpts
  ): AsyncValidatorFn | null {
    const origAsyncValidator = (AbstractControl.isOptionsObj(validatorOrOpts) ?
        (validatorOrOpts as AbstractControlOptions).asyncValidators :
        asyncValidator) as AsyncValidatorFn | AsyncValidatorFn | null;

    return Array.isArray(origAsyncValidator) ? 
        AbstractControl.composeAsyncValidators(origAsyncValidator) : 
        origAsyncValidator || null;
  }

  public static composeValidators(
    validators: Array<ValidatorFn>
  ): ValidatorFn|null {
    return validators != null ? Validators.compose(validators) : null;
  }

  public static composeAsyncValidators(
    validators: Array<AsyncValidatorFn>
  ): AsyncValidatorFn|null {
    return validators != null ? Validators.composeAsync(validators) : null;
  }

  constructor(
    public name: string,
    public validator: ValidatorFn | null,
    public asyncValidator: AsyncValidatorFn | null
  ) {}

  public onCollectionChange = () => {};

  public abstract forEachChild(clb: Function): void;

  public setValidators(newValidator: ValidatorFn | ValidatorFn[]): void {
    const validators = Array.isArray(newValidator) ? newValidator : [newValidator];

    this.validator = AbstractControl.coerceToValidator(validators);
  }
  
  public setAsyncValidators(newValidator: AsyncValidatorFn | AsyncValidatorFn[]): void {
    const validators = Array.isArray(newValidator) ? newValidator : [newValidator];

    this.asyncValidator = AbstractControl.coerceToAsyncValidator(validators);
  }

  public clearValidators(): void {
    this.validator = null;
  }

  public clearAsyncValidators(): void {
    this.asyncValidator = null;
  }
  
  public markAsTouched(opts: FormMarkOpts = { noChildren: true }): void {
    this.touched = true;

    if (!opts.noChildren) {
      this.forEachChild((control: AbstractControl) => 
          control.markAsTouched({ noParent: true }));
    }

    if (this.parent && !opts.noParent) {
      this.parent.markAsTouched({ noChildren: true });
    }
  }

  public markAllAsTouched(): void {
    this.markAsTouched({ noParent: true, noChildren: false });
  }

  public markAsUntouched(opts: FormMarkOpts = {}): void {
    this.touched = false;

    if (!opts.noChildren) {
      this.forEachChild((control: AbstractControl) => 
          control.markAsUntouched({ noParent: true }));
    }

    if (this.parent && !opts.noParent) {
      this.parent.markAsUntouched({ noChildren: true });
    }
  }

  public markAsDirty(opts: FormMarkOpts = { noChildren: true }): void {
    this._pristine.next(false);

    if (!opts.noChildren) {
      this.forEachChild((control: AbstractControl) => 
          control.markAsDirty({ noParent: true }));
    }

    if (this.parent && !opts.noParent) {
      this.parent.markAsDirty({ noChildren: true });
    }
  }

  public markAsPristine(opts: FormMarkOpts = {}): void {
    this._pristine.next(true);
    this.pendingDirty = false;

    if (!opts.noChildren) {
      this.forEachChild((control: AbstractControl) => 
        control.markAsPristine({ noParent: true }));
    }

    if (this.parent && !opts.noParent) {
      this.parent.markAsPristine({ noChildren: true });
    }
  }
  
  public markAsPending(opts: FormMarkEmitOpts = {}): void {
    this.status = FormControlStatus.PENDING;

    if (opts.emitEvent !== false) {
      this._statusChanges.next(this.status);
    }

    if (!opts.noChildren) {
      this.forEachChild((child: AbstractControl) => 
          child.markAsPending({ noParent: true, emitEvent: opts.emitEvent }));
    }

    if (this._parent && !opts.noParent) {
      this._parent.markAsPending({ noChildren: true, emitEvent: opts.emitEvent });
    }
  }

  public disable(opts: FormMarkEmitOpts = {}): void {
    // If parent has been marked artificially dirty we don't want to re-calculate the
    // parent's dirtiness based on the children.
    const skipPristineCheck = this._parentMarkedDirty(opts.noParent);

    this.status = FormControlStatus.DISABLED;
    this.errors = null;

    if (!opts.noChildren) {
      this.forEachChild((child: AbstractControl) => 
          child.disable({ ...opts, noParent: true}));
    }

    if (opts.emitEvent !== false) {
      this._valueChanges.next(this.value);
      this._statusChanges.next(this.status);
    }

    this._updateAncestors({...opts, skipPristineCheck });
    this._onDisabledChange.forEach((changeFn) => changeFn(true));
  }

  public enable(opts: FormMarkEmitOpts = {}): void {
    // If parent has been marked artificially dirty we don't want to re-calculate the
    // parent's dirtiness based on the children.
    const skipPristineCheck = this._parentMarkedDirty(opts.noParent);

    this.status = FormControlStatus.VALID;

    if (!opts.noChildren) {
      this.forEachChild((child: AbstractControl) =>
          child.enable({ ...opts, noParent: true }));
    }

    this.updateValueAndValidity({ noParent: true, emitEvent: opts.emitEvent});

    this._updateAncestors({ ...opts, skipPristineCheck });
    this._onDisabledChange.forEach((changeFn) => changeFn(false));
  }

  public setParent(parent: FormGroup | FormArray): void {
    this._parent = parent;
  }

  abstract setValue(value: any, options?: Object): void;

  abstract patchValue(value: any, options?: Object): void;

  abstract reset(value?: any, options?: Object): void;

  public updateValueAndValidity(opts: FormMarkEmitOpts = {}): void {
    this._setInitialStatus();
    this.updateValue();

    if (this.enabled) {
      this._cancelExistingSubscription();
      this.errors = this._runValidator();
      this.status = this._calculateStatus();

      if (this.status === FormControlStatus.VALID || this.status === FormControlStatus.PENDING) {
        this._runAsyncValidator(opts.emitEvent);
      }

      if (opts.emitEvent !== false) {
        this._valueChanges.next(this.value);
        this._statusChanges.next(this.status);
      }

      if (this._parent && !opts.noParent) {
        this._parent.updateValueAndValidity(opts);
      }
    }
  }

  public setErrors(
    errors: ValidationErrors, 
    opts: { emitEvent?: boolean; } = {}
  ): void {
    this.errors = errors;
    this._updateControlsErrors(opts.emitEvent !== false);
  }

  public get(path: string | (string | number)[]): AbstractControl | null {
    const delimiter = '.';
    if (path == null) { 
      return null; 
    } else if (Array.isArray(path) && path.length === 0) {
      return null;
    } else if (!Array.isArray(path)) {
      path = (path as string).split(delimiter);
    }

    return (path as Array<string|number>).reduce(
      (child: AbstractControl | null, name: string | number) => {
        return child.getChild(name);
      },
      this);
  }

  public getError(
    errno: string, 
    path?: string | (string | number)[]
  ): ErrnoError | null {
    const control = path ? this.get(path) : this;

    return control && control.errors ? control.errors[errno] : null;
  }

  public hasError(
    errno: string, 
    path?: string | (string | number)[]
  ): boolean {
    return !!this.getError(errno, path);
  };

  public registerOnCollectionChange(fn: () => void): void {
    this.onCollectionChange = fn;
  }

  public observeErrors() {
    return this.statusChanges.pipe(
      filter((status) => 
          status === FormControlStatus.INVALID ||
          status === FormControlStatus.VALID),
      map(() => this.errors)) ;
  }

  public observePristine() {
    return this._pristine.pipe(share());
  }

  public observeValidity(): Observable<boolean> {
    return this.statusChanges.pipe(
      filter((status) => 
          status === FormControlStatus.INVALID ||
          status === FormControlStatus.VALID),
      map((status) => status === FormControlStatus.VALID)) ;
  }

  protected abstract getChild(path: string | number): AbstractControl | null;

  protected abstract updateValue(): void;

  protected abstract anyControls(condition: Function): boolean;

  protected abstract allControlsDisabled(): boolean;

  public abstract syncPendingControls(): boolean;

  protected _anyControlsHaveStatus(status: string): boolean {
    return this.anyControls((control: AbstractControl) => control.status === status);
  }

    /** @internal */
  protected setUpdateStrategy(opts?: ValidatorOrOpts): void {
    if (
      AbstractControl.isOptionsObj(opts) && 
      (opts as AbstractControlOptions).updateOn != null
    ) {
      this._updateOn = (opts as AbstractControlOptions).updateOn;
    }
  }

  /** @internal */
  public _anyControlsDirty(): boolean {
    return this.anyControls((control: AbstractControl) => control.dirty);
  }

  /** @internal */
  public updatePristine(opts: FormMarkOpts = {}): void {
    this._pristine.next(!this._anyControlsDirty());

    if (this._parent && !opts.noParent) {
      this._parent.updatePristine(opts);
    }
  }

  /** @internal */
  public _anyControlsTouched(): boolean {
    return this.anyControls((control: AbstractControl) => control.touched);
  }

  /** @internal */
  public updateTouched(opts: FormMarkOpts = {}): void {
    this.touched = this._anyControlsTouched();

    if (this._parent && !opts.noParent) {
      this._parent.updateTouched(opts);
    }
  }

  /** @interal */
  public _updateControlsErrors(emitEvent: boolean) {
    this.status = this._calculateStatus();

    if (emitEvent) {
      this._statusChanges.next(this.status);
    }

    if (this._parent) {
      this._parent._updateControlsErrors(emitEvent);
    }
  }

  /**
   * Check to see if parent has been marked artificially dirty.
   */
  private _parentMarkedDirty(onlySelf?: boolean): boolean {
    const parentDirty = this._parent && this._parent.dirty;
    return !onlySelf && parentDirty && !this._parent._anyControlsDirty();
  }

  private _updateAncestors(opts: FormMarkPristinOpts = {}) {
    if (this._parent && !opts.noParent) {
      this._parent.updateValueAndValidity(opts);
      if (!opts.skipPristineCheck) {
        this._parent.updatePristine();
      }
      this._parent.updateTouched();
    }
  }

  private _setInitialStatus() {
    this.status = this.allControlsDisabled() ? 
        FormControlStatus.DISABLED : 
        FormControlStatus.VALID;
  }

  private _cancelExistingSubscription(): void {
    if (this._asyncValidationSubscription) {
      this._asyncValidationSubscription.unsubscribe();
    }
  }

  private _runValidator(): ValidationErrors | null {
    return this.validator ? this.validator(this) : null;
  }

  private _runAsyncValidator(emitEvent?: boolean): void {
    if (this.asyncValidator) {
      this.status = FormControlStatus.PENDING;

      const obs = Validators.toObservable(this.asyncValidator(this));
      this._asyncValidationSubscription = obs.subscribe(
        (errors: ValidationErrors | null) => 
            this.setErrors(errors, { emitEvent }));
    }
  }

  private _calculateStatus(): FormControlStatus {
    if (this.allControlsDisabled()) { 
      return FormControlStatus.DISABLED; 
    }
    if (this.errors) { 
      return FormControlStatus.INVALID; 
    }
    if (this._anyControlsHaveStatus(FormControlStatus.PENDING)) {
      return FormControlStatus.PENDING;
    }
    if (this._anyControlsHaveStatus(FormControlStatus.INVALID)) {
      return FormControlStatus.INVALID;
    }
    return FormControlStatus.VALID;
  }
}
