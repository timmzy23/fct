import { ErrnoError } from '@fct/sdk';
import { Observable } from 'rxjs';
import { AbstractControl } from './forms.abstract-control';

export interface ValidationErrors {
  [errno: string]: ErrnoError;
};

export type ValidatorFn = 
  (control: AbstractControl) => ValidationErrors | null;

export type AsyncValidatorFn = 
  (control: AbstractControl) => 
      Promise<ValidationErrors | null> | 
      Observable<ValidationErrors | null>;

export enum FormHooks {
  CHANGE = 'change',
  BLUR = 'blur',
  SUBMIT = 'submit'
}

export type AsyncValidatorOpts = AsyncValidatorFn | AsyncValidatorFn[] | null;

/**
 * Interface for options provided to an `AbstractControl`.
 *
 * @publicApi
 */
export interface AbstractControlOptions {
  /**
   * @description
   * The list of validators applied to a control.
   */
  validators?: ValidatorFn|ValidatorFn[]|null;
  /**
   * @description
   * The list of async validators applied to control.
   */
  asyncValidators?: AsyncValidatorOpts;
  /**
   * @description
   * The event name for control to update upon.
   */
  updateOn?: FormHooks;
}

export enum FormControlStatus {
  VALID = 'VALID',
  INVALID = 'INVALID',
  PENDING = 'PENDING',
  DISABLED = 'DISABLED'
}

export interface FormMarkOpts {
  noParent?: boolean;
  noChildren?: boolean;
}

export interface FormMarkEmitOpts extends FormMarkOpts {
  emitEvent?: boolean;
}

export interface FormMarkPristinOpts extends FormMarkEmitOpts {
  skipPristineCheck?: boolean;
}

export type ValidatorOrOpts = 
    ValidatorFn | 
    ValidatorFn[] | 
    AbstractControlOptions | 
    null;

export interface FormControlSetValueOpts {
  noParent?: boolean;
  emitEvent?: boolean;
  emitModelToViewChange?: boolean;
  emitViewToModelChange?: boolean;
}

