import * as React from 'react';
import { FormControl } from './forms.form-control';

/**
 * Adapter for communication between Form control and
 * input component.
 */
export class FormsValueAccessor {
  constructor(
    private control: FormControl,
    private props: React.InputHTMLAttributes<HTMLInputElement>
  ) {}

  public handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!this.control.dirty) { this.control.markAsDirty(); }

    this.control.setValue(
        event.currentTarget.value, 
        { emitModelToViewChange: false });
    this.control.pendingChange = false;

    if (this.props.onChange) {
      this.props.onChange(event);
    }
  }

  public handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    if (this.control.untouched) {
      this.control.markAsTouched();
    }

    if (this.props.onFocus) {
      this.props.onFocus(event);
    }
  }

  public handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    if (this.props.onBlur) {
      this.props.onBlur(event);
    }
    this.control.updateValueAndValidity();
  }
}
