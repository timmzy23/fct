import { string } from 'prop-types';
import { AbstractControl } from './forms.abstract-control';
import { FormControl } from './forms.form-control';
import {
  AsyncValidatorFn,
  AsyncValidatorOpts,
  FormMarkEmitOpts,
  ValidatorOrOpts
  } from './forms.types';

export class FormGroup<V = any> extends AbstractControl<V> {


  constructor(
    name: string,
    public controls: { [name: string]: AbstractControl | null },
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidatorOpts
  ) {
    super(
        name,
        AbstractControl.coerceToValidator(validatorOrOpts),
        AbstractControl.coerceToAsyncValidator(asyncValidator));

    this.setUpdateStrategy(validatorOrOpts);
    this.setUpControls();
    this.updateValueAndValidity({ noParent: true, emitEvent: false });
  }

  public registerControl(name: string, control: AbstractControl): AbstractControl {
    if (this.controls[name]) { return this.controls[name]; }

    this.controls[name] = control;

    control.setParent(this);
    control.registerOnCollectionChange(this.onCollectionChange);

    return control;
  }

  public addControl(name: string, control: AbstractControl): void {
    this.registerControl(name, control);
    this.updateValueAndValidity();
    this.onCollectionChange();
  }

  public removeControl(name: string): void {
    if (this.controls[name]) {
      this.controls[name].registerOnCollectionChange(() => {});
    }
    Reflect.deleteProperty(this.controls, name);
    this.updateValueAndValidity();
    this.onCollectionChange();
  }

  public setControl(name: string, control: AbstractControl): void {
    if (this.controls[name]) {
      this.controls[name].registerOnCollectionChange(() => {});
    }
    Reflect.deleteProperty(this.controls, name);
    if (control) {
      this.registerControl(name, control);
    }
    this.updateValueAndValidity();
    this.onCollectionChange();
  }

  public contains(controlName: string): boolean {
    return Reflect.has(this.controls, controlName) &&
        Reflect.get(this.controls, controlName).enabled;
  }

  public setValue(
    value: {[controlName: string]: any},
    opts: FormMarkEmitOpts
  ): void {
    this.checkAllValuesPresent(value);

    Reflect.ownKeys(value).forEach((name: string) => {
      this.assertControlExists(name);
      this.controls[name].setValue(
        value[name], 
        { noParent: true, emitEvent: opts.emitEvent });
    });

    this.updateValueAndValidity(opts);
  }

  public patchValue(
    value: {[controlName: string]: any},
    opts: FormMarkEmitOpts = {}
  ) {
    Reflect.ownKeys(value).forEach((name: string) => {
      if (Reflect.has(this.controls, name)) {
        this.controls[name].patchValue(
          value[name], 
          { noParent: true, emitEvent: opts.emitEvent});
      }
    });
    this.updateValueAndValidity();
  }

  public reset(
    value: {[controlName: string]: any},
    opts: FormMarkEmitOpts = {}
  ) {
    this.forEachChild((control: AbstractControl, name: string) => {
      control.reset(
        value[name], 
        { noParent: true, emitEvent: opts.emitEvent });
    });

    this.updatePristine(opts);
    this.updateTouched(opts);
    this.updateValueAndValidity(opts);
  }

  public getRawValue(): any {
    return this.reduceChildren(
      {},
      (acc: {[name: string]: AbstractControl}, control: AbstractControl, name: string) => {
        acc[name] = control instanceof FormControl ? 
            control.value : 
            (control as any).getRawValue();
        return acc;
      });
  }

  public syncPendingControls(): boolean {
    const subtreeUpdated = this.reduceChildren(
      false, 
      (updated: boolean, child: AbstractControl) => {
        return child.syncPendingControls() ? true : updated;
      });

    if (subtreeUpdated) {
      this.updateValueAndValidity({ noParent: true });
    }
    return subtreeUpdated;
  }

  public forEachChild(clb: (value: any, name: string) => void): void {
    Reflect
        .ownKeys(this.controls)
        .forEach((name: string) => clb(this.controls[name], name));
  }

  protected getChild(path: string | number): AbstractControl | null {
    return Reflect.has(this.controls, path) ? this.controls[path] : null;
  }

  protected updateValue() {
    this.value = this.reduceValue();
  }

  protected anyControls(condition: Function): boolean {
    let res = false;

    this.forEachChild((control: AbstractControl, name: string) => {
      res = res || (this.contains(name) && condition(control));
    });

    return res;
  }

  protected allControlsDisabled(): boolean {
    for (const controlName of Reflect.ownKeys(this.controls)) {
      if (this.controls[controlName as string].enabled) {
        return false;
      }
    }
    return Reflect.ownKeys(this.controls).length > 0 || this.disabled;
  }

  private setUpControls(): void {
    this.forEachChild((control: AbstractControl) => {
      control.setParent(this);
      control.registerOnCollectionChange(this.onCollectionChange);
    });
  }

  private checkAllValuesPresent(value: any): void {
    this.forEachChild((control: AbstractControl, name: string) => {
      if (value[name] === undefined) {
        throw new Error(
          `Must supply a value for form control with name: '${name}'.`
        );
      }
    })
  }

  private assertControlExists(name: string): void {
    if (!Reflect.ownKeys(this.controls).length) {
      throw new Error(`There are no form controls registered with this group yet.`);
    }
    if (!Reflect.has(this.controls, name)) {
      throw new Error(`Cannot find form control with name: ${name}.`);
    }
  }

  private reduceChildren(initValue: any, reduce: Function) {
    let acc = initValue;

    this.forEachChild((control: AbstractControl, name: string) => {
      acc = reduce(acc, control, name);
    });

    return acc;
  }

  private reduceValue() {
    return this.reduceChildren(
      {},
      (acc: {[name: string]: AbstractControl}, control: AbstractControl, name: string) => {
        if (control.enabled || this.disabled) {
          acc[name] = control.value;
        }
        return acc;
      });
  }
}
