import { ReflectionHelper } from '@fct/sdk';
import { AbstractControl } from './forms.abstract-control';
import {
  AsyncValidatorFn,
  AsyncValidatorOpts,
  FormControlSetValueOpts,
  FormHooks,
  FormMarkEmitOpts,
  ValidatorOrOpts
  } from './forms.types';

export class FormControl extends AbstractControl {

  private _onChange: Function[] = [];

  private _pendingValue: any;

  public pendingChange: boolean;

  constructor(
    name: string,
    formState: any,
    validatorOrOpts?: ValidatorOrOpts,
    asyncValidator?: AsyncValidatorOpts
  ) {
    super(
        name,
        AbstractControl.coerceToValidator(validatorOrOpts),
        AbstractControl.coerceToAsyncValidator(asyncValidator, validatorOrOpts));

    this.applyFormState(formState);
    this.setUpdateStrategy(validatorOrOpts);
    this.updateValueAndValidity({ noParent: true, emitEvent: false });
  }

  public setValue(value: any, opts: FormControlSetValueOpts = {}): void {
    this.value = this._pendingValue = value;

    if (this._onChange.length && opts.emitModelToViewChange !== false) {
      this._onChange.forEach((changeFn) => 
          changeFn(this.value, opts.emitViewToModelChange !== false));
    }

    this.updateValueAndValidity(opts);
  }

  public patchValue(value: any, opts: FormControlSetValueOpts = {}): void {
    this.setValue(value, opts);
  }

  public reset(formState: any, opts: FormMarkEmitOpts = {}): void {
    this.applyFormState(formState);
    this.markAsPristine(opts);
    this.markAsUntouched(opts);
    this.pendingChange = false;
  }

  public registerOnChange(fn: Function): void {
    this._onChange.push(fn);
  }

  public registerOnDisabledChange(fn: (isDisabled: boolean) => void): void {
    this._onDisabledChange.push(fn);
  }

  public forEachChild(clb: Function): void {
    /* no child here */
  }

  public syncPendingControls(): boolean {
    if (this.updateOn === FormHooks.SUBMIT) {
      if (this.pendingDirty) { this.markAsDirty(); }
      if (this.pendingChange) {
        this.setValue(
            this._pendingValue,
            { noParent: true, emitModelToViewChange: false });
        return false;
      }
    }
    return false;
  }

  protected getChild(path: string | number) {
    return null;
  }

  protected updateValue(): void {}

  protected anyControls(condition: Function): boolean {
    return false;
  }

  protected allControlsDisabled(): boolean {
    return this.disabled;
  }

  private applyFormState(formState: any) {
    if (this.isBoxedValue(formState)) {
      this.value = this._pendingValue = formState.value;
    } else {
      this.value = this._pendingValue = formState;
    }
  }

  private isBoxedValue(formState: any): boolean {
    return ReflectionHelper.isObject(formState) &&
      Reflect.ownKeys(formState).length === 2 && 
      'value' in formState &&
      'disabled' in formState;
  }
}