import * as React from 'react';

const styles = require('./quick-modal.component.scss');

function useClassName(props): string {
  return React.useMemo(() => {
    const classNames = [styles.host];

    if (props.className) {
      classNames.push(props.className);
    }

    if (props.visible) {
      classNames.push(styles.visible);
    }

    return classNames.join(' ');
  }, [props.className, props.visible]);
}

export declare namespace QuickModal {
  interface Props {
    visible: boolean;
    render: () => JSX.Element
  }
  type Component = React.FunctionComponent<Props>;
}

export const QuickModal: QuickModal.Component = (props) => {
  const className = useClassName(props);
  return (
    <div className={className}>
      {props.visible && (
        <article className={styles.quickModal}>
          {props.render()}
        </article>
      )}
    </div>
  );
};

QuickModal.displayName = 'QuickModal';

QuickModal.defaultProps = {};
