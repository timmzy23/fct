import { IntlStore } from './intl.store';

export interface IntlConfig {
  messages: { [locale: string]: IntlStore.Messages };
  defaultLocal: string;
  enabledLocals: string[]
}