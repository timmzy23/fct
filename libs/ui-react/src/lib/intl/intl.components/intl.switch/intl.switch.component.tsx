import { useDispatchCommand } from '@fct/sdk-react';
import * as React from 'react';
import {
  Text,
  TextColor,
  TextFontSize
  } from '../../../typography';
import { IntlMessage } from '../../intl-message';
import { IntlSwitchCommand } from '../../intl.actions';
import { useAvailableLocals } from '../../intl.hook';

const styles = require('./intl.switch.component.scss');

export declare namespace IntlSwitch {
  type Props = Text.Props;
  type Component = React.FunctionComponent<Props>;
}

export const IntlSwitch: IntlSwitch.Component = (props) => {
  const {
    locals,
    labelIntl,
    ...textProps
  } = props;
  const dispatch = useDispatchCommand();
  const availableLocals = useAvailableLocals();

  const handleSwitch = React.useCallback((event: React.MouseEvent<HTMLElement>) => {
    const local = event.currentTarget.dataset.local;

    dispatch(IntlSwitchCommand.create({ local }));
  },[dispatch]);


  return (
    <div>
      {availableLocals.map((local, ix) => (
        <>
          <Text
            {...textProps} 
            className={styles.switchItem}
            onClick={handleSwitch}
            data-local={local}
            key={local}>
            <IntlMessage message={{ id: `core.btn.${local}` }}/>
          </Text>
          {ix < (availableLocals.length - 1) &&  
          <Text {...textProps} key={`${local}-sep`}> / </Text>}
        </>
      ))}
    </div>
  );
};

IntlSwitch.displayName = 'IntlSwitch';

IntlSwitch.defaultProps = {
  fontSize: TextFontSize.SMALL,
  textColor: TextColor.SECONDARY
};
