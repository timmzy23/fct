import * as React from 'react';
import {
  injectIntl,
  IntlProvider as ReactIntlProvider,
  IntlShape,

  } from 'react-intl';

export const IntlContext = React.createContext<IntlShape | null>(null);

export const IntlContextProvider = injectIntl(({ children, intl }) => {
  return (
    <IntlContext.Provider value={intl}>
      {children}
    </IntlContext.Provider>
  )
});

IntlContextProvider.displayName = 'IntlContextProvider';

export declare namespace IntlProvider {
  type Props = any;

  type Component = React.FunctionComponent<Props>;
}

export const IntlProvider: IntlProvider.Component = ({ children, ...props}: any) => {
  return (
    <ReactIntlProvider {...props}>
      <IntlContextProvider>
        {children}
      </IntlContextProvider>
    </ReactIntlProvider>
  )
};

IntlProvider.displayName = 'IntlProvider';

export const I18N_MESSAGES_TOKEN = Symbol('ui.i18nMessages.token');
