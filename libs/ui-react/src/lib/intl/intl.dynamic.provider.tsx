import { useSubscription } from '@fct/sdk-react';
import * as React from 'react';
import { IntlProvider } from './intl.context';
import { useIntlStore } from './intl.hook';

const IntlContext = React.createContext<
  IntlDynamicProvider.IntlController | null
>(null);

export declare namespace IntlDynamicProvider {
  interface IntlController {
    switchLanguage: (languageCode: string) => void;
  }

  type Component = React.FunctionComponent;
}

export const DynamicIntlConsumer = IntlContext.Consumer;

export const IntlDynamicProvider: IntlDynamicProvider.Component = (props) => {
  const intlStore = useIntlStore();

  const [locale, setLocale] = React.useState<string>(intlStore.getCurrentLocale());

  const switchLanguage = React.useCallback((_locale: string) => {
    intlStore.setLocal(_locale);
  }, [intlStore]);

  useSubscription(() => intlStore
      .observeLocale()
      .subscribe((_locale) => setLocale(_locale)),
  [intlStore, setLocale]);

  return (
    <IntlContext.Provider value={{ switchLanguage }}>
      <IntlProvider
          defaultLocale={intlStore.getDefaultLocale()}
          locale={locale}
          key={locale}
          messages={intlStore.getCurrentMessages()}>
        {props.children}
      </IntlProvider>
    </IntlContext.Provider>
  );
}

IntlDynamicProvider.displayName = 'IntlDynamicProvider';
