import { ReflectionHelper } from '@fct/sdk';
import {
  actionOf,
  ApplicationState,
  FactoryProvider,
  useCommands,
  useInject,
  useInjector,
  useSubscription
  } from '@fct/sdk-react';
import * as React from 'react';
import { IntlShape } from 'react-intl';
import { map } from 'rxjs/operators';
import { IntlMessage } from './intl-message';
import { IntlSwitchCommand } from './intl.actions';
import {
  I18N_MESSAGES_TOKEN,
  IntlContext
  } from './intl.context';
import { IntlStore } from './intl.store';
import { IntlConfig } from './intl.types';

export function useIntl(): IntlShape | null {
  return React.useContext(IntlContext);
}

export function useTranslate(): (message: IntlMessage.Message) => string {
  const intl = useIntl();

  return React.useCallback((message: IntlMessage.Message) => {
    return ReflectionHelper.isString(message) ?
        message as string :
        intl.formatMessage(
            message as any, 
            (message as IntlMessage.FormattedMessageProps).values)
  }, [intl]);
}

export function useI18NMessages() {
  const { messages } = useInject({ messages: I18N_MESSAGES_TOKEN });

  return messages;
}

export function useIntlInit(config: IntlConfig) {
  const injector = useInjector();

  if (!injector.hasProvider(IntlStore)) {
    injector.use(new FactoryProvider(
      IntlStore,
      (appState: ApplicationState) => new IntlStore(appState, {
        currentLocale: config.defaultLocal,
        defaultLocale: config.defaultLocal,
        enabledLocales: config.enabledLocals,
        messages: config.messages
      }),
      [ApplicationState]
    ));
  }
  const intlStore = injector.inject<IntlStore>(IntlStore) as IntlStore;
  const commands$ = useCommands();

  // Subscribe intl store to local change.
  useSubscription(
    () => commands$
      .pipe(
        actionOf(IntlSwitchCommand),
        map((action: IntlSwitchCommand.Action) => action.payload.local))
      .subscribe((local) => intlStore.setLocal(local)),
    [commands$, intlStore]);
}

export const INTL_LANGUAGES_TOKEN = Symbol('app.intl.languages');

export function useIntlStore(): IntlStore {
  const { store } = useInject({ store: IntlStore });

  return store;
}

export function useAvailableLocals(): string[] {
  const store = useIntlStore();
 
  return store.getEnabledLocals();
}