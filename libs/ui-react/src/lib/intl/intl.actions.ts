import { FluxStandardAction } from '@fct/sdk-react';

export enum INTL_EVENT_TYPES {
  SWITCH = 'ui.intl.switch'
}

/**
 * Ask console output feature to print a message on screen.
 */
export declare namespace IntlSwitchCommand {
  type ACTION_TYPE = INTL_EVENT_TYPES.SWITCH;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>;
  interface Payload {
    local: string;
  }
}

export class IntlSwitchCommand {
  public static ACTION_TYPE: INTL_EVENT_TYPES.SWITCH = 
    INTL_EVENT_TYPES.SWITCH;

  public static create(
    payload: IntlSwitchCommand.Payload
  ): IntlSwitchCommand.Action {
    return FluxStandardAction.create(IntlSwitchCommand.ACTION_TYPE, payload);
  }
}