import { ReflectionHelper } from '@fct/sdk';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

export declare namespace IntlMessage {
  interface FormattedMessageProps {
    id: string;
    values?: any;
  }

  type Message = string | FormattedMessageProps;

  interface Props {
    message: Message;
  }

  type Component = React.FunctionComponent<Props>;
}

export const IntlMessage: IntlMessage.Component = (props) => {
  if (ReflectionHelper.isString(props.message)) {
    return (<span>{props.message}</span>);
  } else {
    return (<FormattedMessage {...props.message as any} />);
  }
};

IntlMessage.displayName = 'IntlMessage';
