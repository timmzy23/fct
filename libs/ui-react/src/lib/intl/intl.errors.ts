import {
  ErrnoError,
  ReflectionHelper
  } from '@fct/sdk';
import { IntlMessage } from './intl-message';

export class IntlErrnoError extends ErrnoError {

  private _intlMessage: IntlMessage.Message;

  constructor(
    errno: string,
    message: IntlMessage.Message,
    params?: {[key: string]: any},
    origError?: Error
  ) {
    super(
      errno, 
      ReflectionHelper.isObject(message) ?
          (message as IntlMessage.FormattedMessageProps).id :
          message as string, 
      params, 
      origError);

    if (ReflectionHelper.isObject(message)) {
      this._intlMessage = {
        ...(message as IntlMessage.FormattedMessageProps),
        values: {
          ...(message as IntlMessage.FormattedMessageProps).values,
          ...this.params
        },
      }
    } else {
      this._intlMessage = message;
    }
  }

  public get intlMessage(): IntlMessage.Message {
    return this._intlMessage;
  }

  public static from(error: ErrnoError, message: IntlMessage.Message) {
    return new IntlErrnoError(
      error.errno,
      message,
      error.params,
      error);
  }
}
