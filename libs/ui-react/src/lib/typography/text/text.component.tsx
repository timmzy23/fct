import { ReflectionHelper } from '@fct/sdk';
import * as React from 'react';
import {
  TextColor,
  TextFontSize,
  TextFontStyle,
  TextTransform
  } from '../typography.types';

const styles = require('./text.component.scss');

export declare namespace Text {
  type Props<P = any> = {
    fontStyle?: TextFontStyle;
    fontSize?: TextFontSize | string;
    textColor?: TextColor | string;
    textTransform?: TextTransform;
    className?: string;
    inlineStyles?: any;
    as?: string;
  } & P;

  type Component = React.FunctionComponent<Props>;
}

function useInlineStyles(props: Text.Props) {
  return React.useMemo(
    () => {
      let compStyle = props.inlineStyles;

      if (!ReflectionHelper.isInEnum(TextFontSize, props.fontSize)) {
        compStyle = compStyle || {};
        compStyle.fontSize = props.fontSize;
      }

      if (!ReflectionHelper.isInEnum(TextColor, props.textColor)) {
        compStyle = compStyle || {};
        compStyle.color = props.textColor;
      }

      return compStyle;
    },
    [props.fontSize, props.textColor, props.inlineStyles]
  );
}

export function useTextClassName(props: Text.Props): string {
  return React.useMemo(
    () => {
      const classNames = [styles.host];

      if (props.className) {
        classNames.push(props.className);
      }

      if (props.fontSize && ReflectionHelper.isInEnum(TextFontSize, props.fontSize)) {
        classNames.push(styles[`fontSize_${props.fontSize}`]);
      }

      if (props.fontStyle && ReflectionHelper.isInEnum(TextFontStyle, props.fontStyle)) {
        classNames.push(styles[`fontStyle_${props.fontStyle}`]);
      }

      if (props.textColor && ReflectionHelper.isInEnum(TextColor, props.textColor)) {
        classNames.push(styles[`textColor_${props.textColor}`]);
      }

      if (props.textTransform && ReflectionHelper.isInEnum(TextTransform, props.textTransform)) {
        classNames.push(styles[`textTransform_${props.textTransform}`]);
      }

      return classNames.join(' ');
    },
    [
      props.className, 
      props.fontStyle, 
      props.fontSize, 
      props.textColor, 
      props.textTransform
    ]
  )
}

export const Text: Text.Component = (props) => {
  const elementInlineStyles = useInlineStyles(props);
  const TagName: any = props.as;
  const elementClassName = useTextClassName(props);
  const {
    fontStyle,
    fontSize,
    textColor,
    textTransform,
    className,
    inlineStyles,
    as,
    children,
    ...elementProps
  } = props;

  return (
    <TagName
      className={elementClassName}
      style={elementInlineStyles}
      {...elementProps}>
      {children}
    </TagName>
  );
}

Text.displayName = 'Text';

Text.defaultProps = {
  as: 'span',
  fontStyle: TextFontStyle.LIGHT,
  textColor: TextColor.PRIMARY,
};
