import * as React from 'react';
import { Heading } from '../heading/heading.component';
import {
  HeadingLevel,
  TextColor,
  TextFontSize,
  TextFontStyle
  } from '../typography.types';

export declare namespace Subheading {
  interface Props extends Heading.Props {
    level?: HeadingLevel
  }
  type Component = React.FunctionComponent<Props> & {
    H1: React.FunctionComponent<Props>;
    H2: React.FunctionComponent<Props>;
    H3: React.FunctionComponent<Props>;
    H4: React.FunctionComponent<Props>;
    H5: React.FunctionComponent<Props>;
    H6: React.FunctionComponent<Props>;
  };
}

export const Subheading: Subheading.Component = 
    ((props) => (<Heading {...props}/>)) as Subheading.Component;

Subheading.displayName = 'Subheading';

Subheading.defaultProps = {
  fontStyle: TextFontStyle.LIGHT_ITALIC,
  textColor: TextColor.SECONDARY
};

Subheading.H2 = (props) => (<Subheading level={HeadingLevel.H2} {...props}/>);
Subheading.H2.displayName = 'Subheading.H2';
Subheading.H2.defaultProps = {
  fontSize: TextFontSize.MEDIUM,
}
