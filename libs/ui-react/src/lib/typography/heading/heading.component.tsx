import { ReflectionHelper } from '@fct/sdk';
import * as React from 'react';
import { TextBlock } from '../text-block/text-block.component';
import {
  HeadingLevel,
  TextFontSize,
  TextFontStyle,
  TextLineHeight
  } from '../typography.types';

const styles = require('./heading.component.scss');

function useClassNames(props: Heading.Props): string {
  return React.useMemo(() => {
    const classNames = [];

    if (props.fontSize && ReflectionHelper.isInEnum(TextFontSize, props.fontSize)) {
      classNames.push(styles[`heading-size-${props.fontSize}`]);
    }

    if (props.lineHeight && ReflectionHelper.isInEnum(TextLineHeight, props.lineHeight)) {
      classNames.push(styles[`heading-line-height-${props.lineHeight}`]);
    }

    if (props.className) {
      classNames.push(props.className);
    }

    return classNames.join(' ');
  }, 
  [props.className, props.fontSize]);
}

export declare namespace Heading {
  interface Props extends TextBlock.Props {
    level?: HeadingLevel;
  }

  type Component = React.FunctionComponent<Props> & {
    H1: React.FunctionComponent<Props>;
    H2: React.FunctionComponent<Props>;
    H3: React.FunctionComponent<Props>;
    H4: React.FunctionComponent<Props>;
    H5: React.FunctionComponent<Props>;
    H6: React.FunctionComponent<Props>;
  };
}

export const Heading: Heading.Component = ((props) => {
  const {level, fontSize, lineHeight, ...textBlockProps} = props;
  const className = useClassNames(props);

  return (<TextBlock {...textBlockProps} className={className}/>);
}) as Heading.Component;

Heading.displayName = 'Heading';

Heading.defaultProps = {
  as: HeadingLevel.H1,
  fontSize: TextFontSize.MEDIUM,
  fontStyle: TextFontStyle.SEMIBOLD,
  lineHeight: TextLineHeight.MEDIUM
};

Heading.H1 = (props) => <Heading {...props} as={HeadingLevel.H1} />;
Heading.H1.displayName = 'Heading.H1';
Heading.H1.defaultProps = {
  fontSize: TextFontSize.EXTRA_LARGE,
};

Heading.H2 = (props) => <Heading {...props} as={HeadingLevel.H2} />;
Heading.H2.displayName = 'Heading.H2';
Heading.H2.defaultProps = {
  fontSize: TextFontSize.LARGE,
};

Heading.H3 = (props) => <Heading {...props} as={HeadingLevel.H3} />;
Heading.H3.displayName = 'Heading.H3';

Heading.H4 = (props) => <Heading {...props} as={HeadingLevel.H4} />;
Heading.H4.displayName = 'Heading.H4';

Heading.H5 = (props) => <Heading {...props} as={HeadingLevel.H5} />;
Heading.H5.displayName = 'Heading.H5';

Heading.H6 = (props) => <Heading {...props} as={HeadingLevel.H6} />;
Heading.H6.displayName = 'Heading.H6';