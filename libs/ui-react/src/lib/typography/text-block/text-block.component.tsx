import { ReflectionHelper } from '@fct/sdk';
import * as React from 'react';
import { Text } from '../text/text.component';
import {
  TextAlign,
  TextLineHeight
  } from '../typography.types';

const styles = require('./text-block.component.scss');

export declare namespace TextBlock {
  interface Props extends Text.Props {
    textAlign?: TextAlign;
    lineHeight?: TextLineHeight | string;
  }
  type Component = React.FunctionComponent<Props>;
}

function useInlineStyles(props: TextBlock.Props): string | undefined {
  return React.useMemo(
    () => {
      let compStyle = props.inlineStyles;

      if (!ReflectionHelper.isInEnum(TextLineHeight, props.lineHeight)) {
        compStyle = compStyle || {};
        compStyle.lineHeight = props.lineHeight;
      }

      return compStyle;
    },
    [props.inlineStyles, props.lineHeight]
  );
}


export function useTextBlockClassNames(props: TextBlock.Props): string {
  return React.useMemo(
    () => {
      const classNames = [styles.host];
      if (props.className) {
        classNames.push(props.className);
      }
      if (props.textAlign) {
        classNames.push(styles[`textAlign_${props.textAlign}`]);
      }
      if (props.lineHeight) {
        classNames.push(styles[`textLineHeight_${props.lineHeight}`])
      }
      return classNames.join(' ');
    }, 
    [props.className, props.textAlign, props.lineHeight])
}

export const TextBlock: TextBlock.Component = (props) => {
  const { className, textAlign, lineHeight, ...textProps} = props;
  const textBlockClassName = useTextBlockClassNames(props);
  const inlineStyles = useInlineStyles(props);

  return (
    <Text {...textProps} 
      className={textBlockClassName} 
      inlineStyles={inlineStyles}
    />
  );
};

TextBlock.displayName = 'TextBlock';

TextBlock.defaultProps = {
  as: 'p',
  textAlign: TextAlign.LEFT,
};
