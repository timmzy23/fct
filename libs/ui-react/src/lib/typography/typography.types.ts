export enum TextTransform {
  UPPERCASE = 'uppercase',
  LOWERCASE = 'lowercase',
  CAPITALIZE = 'capitalize',
  NONE = 'none'
}

export enum TextColor {
  PRIMARY = 'primary',
  INVERTED = 'inverted',
  SECONDARY = 'secondary',
  MARGINAL = 'marginal',
  DISABLED = 'disabled',
  ERROR = 'error',
  SUCCESS = 'success',
  WARNING = 'warning'
}

export enum TextFontSize {
  EXTRA_EXTRA_SMALL = 'xxs',
  EXTRA_SMALL = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  EXTRA_LARGE = 'xl',
  EXTRA_EXTRA_LARGE = 'xxl'
}

export enum TextFontStyle {
  BOLD = 'bold',
  ITALIC = 'italic',
  LIGHT = 'light',
  LIGHT_ITALIC = 'light-italic',
  MEDIUM = 'medium',
  MEDIUM_ITALIC = 'medium-italic',
  SEMIBOLD = 'semibold',
  SEMIBOLD_ITALIC = 'semibold-italic',
  THIN = 'thin',
  THIN_ITALIC = 'thin-italic',
}

export enum TextAlign {
  CENTER = 'center',
  RIGHT = 'right',
  JUSTIFY = 'justify',
  LEFT = 'left'
}

export enum TextLineHeight {
  EXTRA_SMALL = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  EXTRA_LARGE = 'xl',
}

export enum  HeadingLevel {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  H6 = 'h6'
}