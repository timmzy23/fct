import * as React from 'react';

const styles = require('./button.component.scss');

function useButtonClassName(props: Button.Props) {
  return React.useMemo(
    () => {
      const buttonClassName = [
        styles.button
      ];

      if (props.className) {
        buttonClassName.push(props.className);
      }

      if (props.fullWidth) {
        buttonClassName.push(styles.fullWidth);
      }

      if (props.viewStyle === ButtonViewStyle.PRIMARY) {
        buttonClassName.push(styles.primary);
      } else if (props.viewStyle === ButtonViewStyle.SECONDARY) {
        buttonClassName.push(styles.secondary);
      }

      return buttonClassName.join(' ');
    },
    [props.className, props.fullWidth, props.viewStyle]
  );
}

export enum ButtonViewStyle {
  PRIMARY = 'primary',
  SECONDARY  ='secondary',
}

export declare namespace Button {
  interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    fullWidth?: boolean;  
    viewStyle?: ButtonViewStyle;
  }

  type Component = React.FunctionComponent<Props> & {
    Submit: React.FunctionComponent<Props>;
    Reset: React.FunctionComponent<Props>;
  };
}

export const Button: Button.Component = (props) => {
  const className = useButtonClassName(props);
  const { viewStyle: buttonStyle, fullWidth, ...htmlProps } = props;

  return (
    <button {...htmlProps} className={className}>
      {props.children}
    </button>
  );
};

Button.displayName = 'Button';

Button.defaultProps = {
  viewStyle: ButtonViewStyle.PRIMARY,
  type: 'button'
};

Button.Submit = (props) => (<Button {...props} type="submit"/>);
Button.Submit.displayName = 'Button.Submit';

Button.Reset = (props) => (<Button {...props} type="reset"/>);
Button.Reset.displayName = 'Button.Reset';

