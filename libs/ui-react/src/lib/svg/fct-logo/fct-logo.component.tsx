import * as React from 'react';

const styles = require('./fct-logo.component.scss');

export declare namespace FctLogo {
  interface Props {
    width: string;
    height: string;
  }
  type Component = React.FunctionComponent<Props>;
}

export const FctLogo: FctLogo.Component = (props) => {
  return (
    <svg 
      viewBox="0 0 324.76 144.47"
      width={props.width}
      height={props.height}>
      <title>4ct LOGO</title>
      <g id="Layer_2" data-name="Layer 2">
        <g id="Layer_1-2" data-name="Layer 1">
          <path className={styles.cls1} d="M70.68,0h37.41V87.3H126.8v31.18H108.09v22.87H71.72V118.48H0V87.3Zm1,87.3V45.94L38.25,87.3Z"/>
          <path className={styles.cls2} d="M157.56,52.17c11.22-9.35,24.73-13.92,40.53-13.92a70,70,0,0,1,30.76,6.86V75.25a50,50,0,0,0-25.35-6.86c-15.6,0-26.61,7.27-26.61,23.07,0,15,11.22,22.86,26,22.86,10.18,0,19.34-2.28,27-6.64v30.14a89.32,89.32,0,0,1-34.29,6.65c-15.18,0-28.28-4.37-38.87-13.09-10.82-8.53-16.22-21.21-16.22-37.84C140.51,75.25,146.13,61.53,157.56,52.17Z"/>
          <path className={styles.cls3} d="M260.32,14.36,295.66,6V45.11h27V74.25h-27V103.1c0,8.11,4.37,12.27,13.1,12.27a48.56,48.56,0,0,0,16-2.5v27.44a84.79,84.79,0,0,1-27,4.16c-21.83,0-37.42-10-37.42-34.3V74.25H244.53V45.11h15.79Z"/>
        </g>
      </g>
    </svg>
  );
};

FctLogo.displayName = 'FctLogo';

FctLogo.defaultProps = {};
