import * as React from 'react';
import { IntlMessage } from '../../intl';
import {
  Text,
  TextAlign,
  TextBlock,
  TextColor,
  TextFontSize,
  TextFontStyle
  } from '../../typography';

const styles = require('./asap-spinner.component.scss');

export declare namespace AsapSpinner {
  interface Props extends React.SVGAttributes<SVGElement> {  
    label?: IntlMessage.Message;
    labelSize?: TextFontSize;
    labelStyle?: TextFontStyle;
    labelColor?: TextColor;
  }
  type Component = React.FunctionComponent<Props>;
}

export const AsapSpinner: AsapSpinner.Component = (props) => {
  const {
    label,
    labelSize,
    labelStyle,
    labelColor,
    ...svgProps
  } = props;
  return (
    <div className={`${styles.host}`}>
      <svg 
        xmlns="http://www.w3.org/2000/svg" 
        viewBox="0 0 117.276 130.295"
        {...svgProps}>
        <g transform="translate(0.687 0.58)">
          <path 
            className={`${styles.frag} ${styles.frag2}`} 
            d="M777.725,466.173a34.579,34.579,0,0,1,3.9,9.959,35.207,35.207,0,0,1,.835,7.586v.005c0,.973-.047,1.937-.127,2.894a34.528,34.528,0,0,1-3.256,12.117l10.5,5.085,9.912,4.8a57.829,57.829,0,0,0,3.947-10.778,58.511,58.511,0,0,0,1.585-9.363c.133-1.567.211-3.156.217-4.759,0-.1.006-.19.006-.287a58.916,58.916,0,0,0-1.275-12.1l-10.951,2.355a45.088,45.088,0,0,0-1.264-4.606l10.685-3.485a57.612,57.612,0,0,0-4.825-10.9Z" transform="translate(-689.147 -416.567)"/>
          <path 
            className={`${styles.frag} ${styles.frag1}`}
            d="M750.167,415.922a58.077,58.077,0,0,0-6.125-8.72l7.817-6.666a68.574,68.574,0,0,0-9.894-9.431l-6.3,8.022a58.21,58.21,0,0,0-11.91-7.241l-4.728,10.391a46.758,46.758,0,0,0-19.111-4.1v11.842c.055,0,.112-.005.169-.005a34.8,34.8,0,0,1,21.319,7.272,34.978,34.978,0,0,1,8.877,10.111Z" transform="translate(-641.707 -377.795)"/>
          <path 
            className={`${styles.frag} ${styles.frag6}`}
            d="M579.256,406.072a34.942,34.942,0,0,1,4.012-5.383c.692-.761,1.422-1.491,2.177-2.189.592-.55,1.208-1.074,1.837-1.583a34.8,34.8,0,0,1,8.831-5.175,34.738,34.738,0,0,1,6.8-1.953,35.088,35.088,0,0,1,6.057-.561V377.387h-.014a46.821,46.821,0,0,0-8.117.715l-2-11.255L597.1,357a67.633,67.633,0,0,0-13.312,3.813l3.694,9.266,4.227,10.6a45.824,45.824,0,0,0-11.861,6.881c-.885.7-1.744,1.433-2.574,2.2l-7.755-8.284L562.7,374.2q-2.018,1.873-3.879,3.906a68.587,68.587,0,0,0-8.063,10.757l8.555,5.185Z" transform="translate(-550.761 -357)"/>
          <path 
            className={`${styles.frag} ${styles.frag5}`}
            d="M559.774,451.918a57.5,57.5,0,0,0-6.569,15.881,58.134,58.134,0,0,0-1.526,9.5c-.107,1.465-.167,2.94-.167,4.437,0,.1.005.19.005.287a58.456,58.456,0,0,0,.385,6.433q.324,2.8.913,5.521l10.826-2.334a45.973,45.973,0,0,0,1.248,4.709,46.613,46.613,0,0,0,2.488,6.183l10.753-5.33a35.5,35.5,0,0,1-1.816-4.574,34.069,34.069,0,0,1-.92-3.511c-.22-1.072-.39-2.163-.511-3.262a35.843,35.843,0,0,1-.212-3.829v-.005c0-.95.048-1.892.122-2.822a34.679,34.679,0,0,1,2.05-9.318,34.4,34.4,0,0,1,2.841-5.89l.03-.049Z" transform="translate(-551.219 -414.874)"/>
          <path 
            className={`${styles.frag} ${styles.frag4}`}
            d="M631.2,587.707a35.147,35.147,0,0,1-7.8-.921,34.675,34.675,0,0,1-13.846-6.731,35.151,35.151,0,0,1-9.657-12.094l-10.753,5.331a46.766,46.766,0,0,0,6.4,9.758l-8.331,7.1a58.3,58.3,0,0,0,8.223,7.884l-6.437,8.2a68.271,68.271,0,0,0,13.683,8.444l4.353-9.554a57.3,57.3,0,0,0,11.324,3.833,58.152,58.152,0,0,0,12.821,1.43h.022v-22.68Z" transform="translate(-572.983 -485.629)"/>
          <path 
            className={`${styles.frag} ${styles.frag3}`}
            d="M699.921,599.292a46.929,46.929,0,0,0,8.209-.742,45.929,45.929,0,0,0,9.159-2.615,47.011,47.011,0,0,0,7.247-3.663A47.669,47.669,0,0,0,729,589.12l6.794,8.536a57.468,57.468,0,0,0,6.335-5.823,58.12,58.12,0,0,0,9.916-14.41l-9.913-4.8-10.5-5.084a35.245,35.245,0,0,1-9.927,12.413,36.2,36.2,0,0,1-3.323,2.328,35.214,35.214,0,0,1-18.293,5.175c-.057,0-.113-.005-.168-.005Z" transform="translate(-641.708 -485.372)"/>
        </g>
      </svg>
      {label && 
        <TextBlock
            textAlign={TextAlign.CENTER} 
            fontStyle={labelStyle}
            fontSize={labelSize}
            textColor={labelColor}
            className={styles.label}>
        <IntlMessage message={label}/>
      </TextBlock>}
    </div>
  );
};

AsapSpinner.displayName = 'AsapSpinner';

AsapSpinner.defaultProps = {
  width: '84px',
  height: '84px',
  labelStyle: TextFontStyle.MEDIUM_ITALIC,
  labelColor: TextColor.SECONDARY
};
