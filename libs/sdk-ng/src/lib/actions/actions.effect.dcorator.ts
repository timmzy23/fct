import { ActionBus } from './action-bus';

/**
 * Action type can be provided in form of [action-type] or
 * with some subtopic namespace like: [command:navigate-to]
 */
export function Effect(actionType: string | string[]) {
  return (EffectInit: {new(...args: any[]): ActionBus.EffectInit}) => {
    const actionTypes = Array.isArray(actionType) ? actionType : [actionType];

    const topicMap = actionTypes.reduce((acc, type) => {
      const fragments = type.split(':');
      const topic = fragments.length === 2 ? fragments[0] : 'root';
      if (!Reflect.has(acc, topic)) {
        acc[topic] = [];
      }
      acc[topic].push(fragments.length === 2 ? fragments[1] : fragments[0]);

      return acc;
    }, {});

    const EffectClass = class extends EffectInit implements ActionBus.Effect {
      public topicMap = topicMap;

      constructor(...args: any[]) {
        super(...args);
      }

      public getActionTypesForTopic(topic: string) {
        return this.topicMap[topic] ? this.topicMap[topic] : [];
      }

      public forEachTopic(clb: (topic: string) => void): void {
        Reflect.ownKeys(this.topicMap).forEach(clb);
      } 
    } as any;

    return Reflect.ownKeys(EffectInit)
        .filter((prop: string) => !['length', 'prototype', 'name'].includes(prop))
        // We have to transfer static properties from parent to child in order 
        // to be reachable by Reflect.ownKeys for Angular framework. Without
        // this a constructor depenencies information would be lost
        .reduce((acc, propKey) => {
          const value = EffectInit[propKey];
          // Delete prop from parent in case it would be read-only
          // to prevent the error on copy. (If parent has read-only prop)
          // then there is no way to override it in child.
          delete EffectInit[propKey];
          acc[propKey] = value;
          return acc;
        }, EffectClass);
  }
}