export interface AnyAction<T extends string = any> {
  type: T;
  [key: string]: any;
}
