import { FluxStandardAction } from './flux-standard.action';

export declare namespace FluxErrorAction {
  interface Action<T extends string, P = any, M = any> 
    extends FluxStandardAction.Action<T, P, M> {
      error: true
    }
}

export abstract class FluxErrorAction {
  public static create<T extends string, P = any, M = any>(
    type: T, payload?: P, meta?: M
  ): FluxErrorAction.Action<T, P ,M> {
    const errorAction = FluxStandardAction
        .create<T, P, M>(type, payload, meta) as FluxErrorAction.Action<T, P, M>;

    errorAction.error = true;

    return errorAction
  }
}
