import { AnyAction } from './any.action';

export declare namespace FluxStandardAction {
  interface Action<T extends string, P = any, M = any> extends AnyAction<T> {
    payload: P;
    meta: M;
  }
}

export abstract class FluxStandardAction {
  public static create<T extends string, P = any, M = any>(
    type: T, payload?: P, meta?: M
  ): FluxStandardAction.Action<T, P, M> {
    return { type, payload, meta }
  }
}