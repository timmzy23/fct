import {
  OnDestroy,
  OnInit
  } from '@angular/core';
import { Subscription } from 'rxjs';

export class SubscriptionHandler {
  private subscriptions: Subscription[] = [];

  public add(s: Subscription) {
    this.subscriptions.push(s);

    return this;
  }

  public unsubscribe() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}