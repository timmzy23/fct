import { Injectable } from '@angular/core';
import { ActionBusImpl } from './action-bus.impl';
import { ActionBus } from './action-bus.interface';

@Injectable({
  providedIn: 'root',
})
export class RootActionBus extends ActionBusImpl {
  constructor() { super('root'); }
}

@Injectable({
  providedIn: 'root',
  useFactory: (rootActionBus: ActionBus) => {
    return rootActionBus.getOrCreateChild('commands');
  },
  deps: [ RootActionBus ]
})
export class CommandActionBus extends ActionBusImpl {
  constructor() {
    super('commands');
  }
}

@Injectable({
  providedIn: 'root',
  useFactory: (rootActionBus: ActionBus) => {
    return rootActionBus.getOrCreateChild('events');
  },
  deps: [ RootActionBus ]
})
export class EventActionBus extends ActionBusImpl {
  constructor() {
    super('events');
  }
}
