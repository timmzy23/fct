import {
  Inject,
  InjectionToken,
  NgModule,
  Optional,
  Self
  } from '@angular/core';
import {
  ActionBus,
  RootActionBus
  } from './action-bus';

export const ACTION_EFFECT = new InjectionToken<ActionBus.Effect[]>('action.effect');

@NgModule({})
export class ActionsModule {
  constructor(
    @Optional()
    @Self()
    @Inject(ACTION_EFFECT) effects: ActionBus.Effect[],
    rootActionBus: RootActionBus
  ) {
    if (effects) {
      effects.forEach((effect: ActionBus.Effect) => {
        effect.forEachTopic((topic) => {
          if (topic === 'root') {
            rootActionBus.addEffect(effect);
          } else {
            rootActionBus.getOrCreateChild(topic).addEffect(effect);
          }
        });
      });
    }
  }
}