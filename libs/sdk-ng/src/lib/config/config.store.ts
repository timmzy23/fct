import { Injectable } from '@angular/core';
import { ErrnoError } from '@fct/sdk';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';
import {
  ApplicationState,
  ObservableStore
  } from '../store';
import { ERR_CONFIG_KEY_MISSING } from './config.errors';

export class ConfigStore extends ObservableStore<any> {
  constructor(appState: ApplicationState, init: any = {}) {
    super('app.config', appState, init);
  }

  public setConfig(config: any) {
    this.setState(config);
  }

  public mergeConfig(config: any) {
    const prevState = this.getState();

    this.setState({
      ...prevState,
      ...config,
    });
  }

  public getConfig(): any {
    return this.getState();
  }

  public get<T>(key: string, isRequired = true): T | undefined {
    const config = this.getState();

    if (!Reflect.has(config, key) && isRequired) {
      throw new ErrnoError(
        ERR_CONFIG_KEY_MISSING,
        `Runtime configuration value "${key}" is required but missing! `+
        `Check your configuration files or default settings`,
        { key });
    }

    return Reflect.get(config, key) as T;
  }

  public observeKey<K>(key: string): Observable<K> {
    return this.observe.pipe(
      filter((change) => change.prevState[key] !== change.nextState[key]),
      map((change) => change.nextState.key));
  }
}