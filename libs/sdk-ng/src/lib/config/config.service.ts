import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AppEnv,
  ErrnoError
  } from '@fct/sdk';
import { Subject } from 'rxjs';
import {
  first,
  share
  } from 'rxjs/operators';
import {
  ERR_CONFIG_KEY_MISSING,
  ERR_CONFIG_NOT_LOADED
  } from './config.errors';
import { ConfigStore } from './config.store';

/**
 * Service to store and share application compiled and runtime configuration
 * values.
 * 
 * The compiled and runtime configs are not merged and cannot override each other.
 */
export class ConfigService {

  public isLoaded = this.store.observe.pipe(first()).toPromise();

  constructor(
    private httpClient: HttpClient,
    private store: ConfigStore,
    private environment: any,
    private appEnv: AppEnv
  ) {}

  /**
   * Load one or more configuration files in json format. 
   * 
   * ## Config url format
   * 
   * Config url can hold a placeholder `{env}`, which is going to
   * be replaced by `dev` or `prod` based on your `environment` configuration.
   * 
   * ## Other config formats
   * If you would need to support other config formats such as `yaml`, 
   * use a backend endpoint to convert your `yaml` file into json response
   * instead of implementing a parser into frontend app. It is because
   * it would be one time job which is not worth of loading bunch of libraries
   * such as js-yaml for it.
   */
  public loadConfigs(configUrl: string | string[]): Promise<this> {
    const urls = Array.isArray(configUrl) ? configUrl : [configUrl];

    return Promise
      .all(urls
          .map(this.resolveConfigUrl)
          .map((url) => 
          this.httpClient.get(url)
            .toPromise()
            .then(this.merge)
            .catch((error) => Promise.reject(new ErrnoError(
                ERR_CONFIG_NOT_LOADED,
                `Configuration file "${url}" cannot be loaded!`,
                { url },
                error)))))
      .then(() => this);
  }

  /**
   * Gets a configuration value. If value is missing but required an error is 
   * triggerd.
   * 
   * @param key 
   * @param isRequired 
   */
  public get<T>(key: string, isRequired = true): T | undefined {
    return this.store.get<T>(key, isRequired);
  }

  public getEnv<T>(key: string, isRequired = true): T | undefined {
    if (!Reflect.has(this.environment, key) && isRequired) {
      throw new ErrnoError(
        ERR_CONFIG_KEY_MISSING,
        `Env configuration value "${key}" is required but missing! `+
        `Check your environment files!`,
        { key });
    }

    return Reflect.get(this.environment, key) as T;
  }

  public observeKey<V>(key: string) {
    return this.store.observeKey<V>(key);
  }

  private merge = (config: any) => {
    this.store.mergeConfig(config);
  }

  private resolveConfigUrl = (configUrl: string) => {
    return configUrl.replace('{env}', this.appEnv);
  }
}
