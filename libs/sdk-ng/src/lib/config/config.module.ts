import { CommonModule } from '@angular/common';
import {
  HttpClient,
  HttpClientModule
  } from '@angular/common/http';
import {
  APP_INITIALIZER,
  InjectionToken,
  ModuleWithProviders,
  NgModule
  } from '@angular/core';
import { AppEnv } from '@fct/sdk';
import {
  ApplicationState,
  StoreModule
  } from '../store';
import { ConfigService } from './config.service';
import { ConfigStore } from './config.store';

export declare namespace ConfigModule {
  interface Init {
    appEnv: AppEnv;
    defaultConfig?: any;
    environment: any;
    configUrls: string[];
  }
}

export const CONFIG_INIT_TOKEN = new InjectionToken<any>('config.init');

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule
  ]
})
export class ConfigModule {
  private static createConfigStore(appState: ApplicationState, init: any) {
    return new ConfigStore(appState, init);
  }

  private static createConfigService(
    http: HttpClient, 
    store: ConfigStore,
    init: ConfigModule.Init
  ) {
    console.log('>>>>> CONFIG CREATE STORE', init);
    return new ConfigService(
    http,
    store,
    init.environment,
    init.appEnv);
  }

  private static initConfig(config: ConfigService, init: ConfigModule.Init) {
    return () => config.loadConfigs(init.configUrls);
  }

  public static forRoot(init: ConfigModule.Init): ModuleWithProviders {
    return {
      ngModule: ConfigModule,
      providers: [
        { provide: CONFIG_INIT_TOKEN, useValue: init },
        {
          provide: ConfigStore,
          useFactory: ConfigModule.createConfigStore,
          deps: [ApplicationState, CONFIG_INIT_TOKEN]
        },
        { 
          provide: ConfigService, 
          useFactory: ConfigModule.createConfigService, 
          deps: [HttpClient, ConfigStore, CONFIG_INIT_TOKEN]
        },
        { 
          provide: APP_INITIALIZER,
          useFactory: ConfigModule.initConfig,
          deps: [ConfigService, CONFIG_INIT_TOKEN],
          multi: true,
        }
      ]
    }
  }
}
