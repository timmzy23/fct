import { InjectionToken } from '@angular/core';
import { AppEnv } from '@fct/sdk';

/**
 * DI token to store and inject AppEnv (dev/prod)
 */
export const APP_ENV = new InjectionToken<AppEnv>('app.env');

export const ANGULAR_VERSION = new InjectionToken<string>('angular.version');
