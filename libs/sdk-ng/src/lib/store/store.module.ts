import {
  ModuleWithProviders,
  NgModule
  } from '@angular/core';
import {
  ApplicationState,
  InMemoryState
  } from './state';

export declare namespace StoreModule {
  interface Init {
    AppStateImpl?: any
  }
}

@NgModule({
  providers: []
})
export class StoreModule {
  public static forRoot(init: StoreModule.Init = {}): ModuleWithProviders {
    return {
      ngModule: StoreModule,
      providers: [
        { provide: ApplicationState, useClass: init.AppStateImpl || InMemoryState }
      ]
    }
  }
}
