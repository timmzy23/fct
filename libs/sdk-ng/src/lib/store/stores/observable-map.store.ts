import { ApplicationState } from '../state';
import { ObservableStore } from './observable-store';

export declare namespace ObservableMapStore {
  type State<R> = Map<string, R>;

  interface ChangeEvent<R> extends ObservableStore.ChangeEvent<State<R>> {
    prevRecord?: R;
    nextRecord?: R;
    key?: string;
  }
}

export class ObservableMapStore<R> extends ObservableStore<ObservableMapStore.State<R>> {

  constructor(
    namespace: string,
    appState: ApplicationState,
    initState: ObservableMapStore.State<R> = new Map()
  ) {
    super(namespace, appState, initState);
  }

  public set(key: string, nextRecord: R | undefined) {
    if (nextRecord === undefined) {
      return this.delete(key);
    } 
    
    const state = this.getState();
    const prevRecord = state.get(key);
    state.set(key, nextRecord);

    this.setState(state, (changeEvent) => ({
      ...changeEvent,
      prevRecord,
      nextRecord,
      key
    }));
  }

  public get(key: string): R | undefined {
    return this.getState().get(key);
  }

  public has(key: string): boolean {
    return this.getState().has(key);
  }

  public delete(key: string): boolean {
    const record = this.get(key);

    if (!record) {
      return false;
    } else {
      const state = this.getState();
      const prevRecord = state.get(key);
      state.delete(key);

      this.setState(state, (changeEvent) => ({
        ...changeEvent,
        prevRecord,
        nextRecord: undefined,
        key
      }));

      return true;
    }
  }
}