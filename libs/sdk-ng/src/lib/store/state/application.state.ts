export declare namespace ApplicationState {
  interface State {
    [namespace: string]: any;
  }
}

export abstract class ApplicationState {

  public get<V>(namespace: string): V | undefined {
    const state = this.getState();

    return state[namespace];
  }

  public has(namespace: string): boolean {
    const state = this.getState();

    return state[namespace];
  }

  public set<V>(namespace: string, value: V): void {
    const state = this.getState();

    const newState = {
      ...state,
      [namespace]: value,
    };
    this.setState(newState);
  }

  public delete(namespace: string): void {
    const state = this.getState();

    if (!state[namespace]) { return; }

    const newState = Reflect.ownKeys(state)
        .filter((key) => key !== namespace)
        .reduce((acc, key: string) => {
          acc[key] = state[key];
          return acc;
        }, {});
    this.setState(newState);
  }

  public clear(): void {
    this.setState({});
  }

  public abstract getState(): ApplicationState.State;

  public abstract setState(state: ApplicationState.State): void;
}
