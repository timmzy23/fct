import {
  SimpleChange,
  SimpleChanges
  } from '@angular/core';

export function useEffect(memoKeys?: string[]) {
  return (target, key) => {
    const disposeClbs = new WeakMap<any, any>();
    const origOnInit = target.ngOnInit;
    const origOnDestroy = target.ngOnDestroy;
    const origOnChanges = target.ngOnChanges;
    
    target.ngOnInit = function() {
      const dispose = Reflect.apply(target[key], this, []);
      if (dispose) {
        disposeClbs.set(this, dispose);
      }
      if (origOnInit) {
        Reflect.apply(origOnInit, this, []);
      }
    }
    
    target.ngOnChanges = function(changes: SimpleChanges) {
      const isChange = !memoKeys || memoKeys
          .map((memoKey) => changes[memoKey])
          .filter((change: SimpleChange) => !!change)
          .some((change) => change.previousValue !== change.currentValue);
      
      if (isChange) {
        const prevDispose = disposeClbs.get(this);
        if (prevDispose) {
          Reflect.apply(prevDispose, this, []);
        }
        const dispose = Reflect.apply(target[key], this, [changes]);
        if (dispose) {
          disposeClbs.set(this, dispose);
        }
      }
      
      if (origOnChanges) {
        Reflect.apply(origOnChanges, this, [changes]);
      }
    }
    
    target.ngOnDestroy = function() {
      const disposeClb = disposeClbs.get(this);
      
      if (disposeClb) {
        Reflect.apply(disposeClb, this, []);
      }
      
      if (origOnDestroy) {
        Reflect.apply(origOnDestroy, this, []);
      }
    }
  }
}