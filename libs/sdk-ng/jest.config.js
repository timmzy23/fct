module.exports = {
  name: 'sdk-ng',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/sdk-ng',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
