export interface UserCreateSuccessResponsePayload {
  identifier: string;
  id: string;
}
