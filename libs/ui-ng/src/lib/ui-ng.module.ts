import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomModule } from './custom/custom.module';
import { ThemesModule } from './themes/themes.module';
import { TypographyModule } from './typography/typography.module';
import { FormsModule } from './forms/forms.module';
import { IconsModule } from './icons/icons.module';
import { ButtonsModule } from './buttons/buttons.module';

@NgModule({
  imports: [CommonModule, ThemesModule, TypographyModule, CustomModule, FormsModule, IconsModule, ButtonsModule],
  exports: [ ThemesModule, TypographyModule, CustomModule ]
})
export class UiNgModule {}
