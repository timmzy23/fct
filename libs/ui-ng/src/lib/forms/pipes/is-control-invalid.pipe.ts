import {
  Pipe,
  PipeTransform
  } from '@angular/core';
import { FormControl } from '@angular/forms';

@Pipe({
  name: 'isControlInvalid',
  pure: false
})
export class IsControlInvalidPipe implements PipeTransform {
  public transform(control: FormControl): boolean {
    return !!(
      control.invalid && 
      control.dirty &&
      control.errors);
  }
}
