import {
  AbstractControl,
  ValidatorFn
  } from '@angular/forms';


export class CustomValidators {

  /**
   * Put types in form of something/something like 'text/xml'
   */
  public static requiredFileType(...types: string[]): ValidatorFn {

    return (control: AbstractControl) => {
      const file = control.value as File;
      if (file && !types.includes(file.type)) {
        return {
          requiredFileType: {
            expected: types,
            asserted: file.type
          }
        };
      }
      return null;
    }
  }
}