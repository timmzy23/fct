import {
  Component,
  Input
  } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import {
  ParagraphLineHeight,
  TextAlign,
  TextColor,
  TextSize,
  TextStyle,
  TextTransform
  } from '../../../typography';

  /*
@Component({
  selector: 'fct-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss']
})
*/
export class TextFieldComponent {
  @Input()
  public parentControl: AbstractControl;

  @Input()
  public name: string;

  @Input()
  public inputType = 'text';

  @Input()
  public inputTextColor = TextColor.PRIMARY;

  @Input()
  public inputFontStyle = TextStyle.LIGHT_ITALIC;

  @Input()
  public labelTextAlign: TextAlign = TextAlign.RIGHT;

  @Input()
  public labelFontSize: TextSize = TextSize.MEDIUM;

  @Input()
  public labelFontStyle: TextStyle = TextStyle.SEMIBOLD;

  @Input()
  public labelTextColor: TextColor = TextColor.PRIMARY;

  @Input()
  public labelTextTransform: TextTransform;

  @Input()
  public lineHeight: ParagraphLineHeight = ParagraphLineHeight.MEDIUM;

  @Input()
  public required = false;

  @Input()
  public autocomplete = 'off';
}
