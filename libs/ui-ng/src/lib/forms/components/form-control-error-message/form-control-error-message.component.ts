import {
  Component,
  Input
  } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'fct-form-control-error-message',
  templateUrl: './form-control-error-message.component.html',
  // styleUrls: ['./form-control-error-message.component.scss']
})
export class FormControlErrorMessageComponent {
  @Input()
  public control: FormControl;

  @Input()
  public errorType: string;

  public showError() {
    return this.control.invalid && 
        (this.control.dirty || this.control.touched) && 
        this.control.errors && 
        this.control.errors[this.errorType]
  }
}
