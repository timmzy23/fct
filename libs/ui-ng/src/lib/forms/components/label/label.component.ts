import {
  Component,
  Input,
  OnInit
  } from '@angular/core';
import { ParagraphComponent } from '../../../typography';

@Component({
  selector: 'fct-label',
  templateUrl: './label.component.html',
  styleUrls: [
    '../../../typography/components/text/font-size.scss',
    '../../../typography/components/text/font-style.scss',
    '../../../typography/components/text/text-color.scss',
    '../../../typography/components/text/text-transform.scss',
    '../../../typography/components/paragraph/text-align.scss',
    '../../../typography/components/paragraph/paragraph-line-height.scss',
    './label.component.scss',
  ]
})
export class LabelComponent extends ParagraphComponent implements OnInit {
  @Input()
  public for: string;

  @Input()
  public required: boolean;

  public ngOnInit() {
    console.log(this.textStyle, this.textSize);
  }
}
