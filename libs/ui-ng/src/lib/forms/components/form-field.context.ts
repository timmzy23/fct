import { InjectionToken } from '@angular/core';
import { FormControl } from '@angular/forms';

export const FORM_FIELD_CONTEXT = new InjectionToken('FORM_FIELD_CONTEXT');

export interface FormFieldContext {
  controlName: string;
  control: FormControl;
}
