import {
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
  } from '@angular/core';
import {
  AbstractControl,
  FormGroup
  } from '@angular/forms';
import { ErrnoError } from '@fct/sdk';

export abstract class AbstractFormComponent<P>  {
  
  public abstract form: FormGroup;

  @Output()
  public resetForm = new EventEmitter<Partial<P>>();

  @Output()
  public submitForm = new EventEmitter<P>();

  public handleReset(event: Event) {
    if (!event) { return; }
    event.stopPropagation();

    this.resetForm.emit(this.form.value);
  }

  public handleSubmit(event: Event) {
    if (!event) { return; }
    event.stopPropagation();
    
    this.submitForm.emit(this.form.value);
  }

  public getFormControl(name: string): AbstractControl {
    const formControl = this.form.get(name);

    if (!formControl) {
      throw new TypeError(`Form control "${name}" does not exist!`);
    }

    return formControl;
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.serverError && changes.serverError.currentValue) {
      this.handleServerError(changes.serverError.currentValue);
    }
  }

  protected abstract handleServerError(serverError: ErrnoError): void;
}