import {
  Component,
  Input
  } from '@angular/core';
import { FormControlName } from '@angular/forms';

@Component({
  selector: 'fct-form-error-message',
  templateUrl: './form-error-message.component.html'
})
export class FormErrorMessageComponent {
  @Input()
  public errorType: string;

  constructor(
    private controlName: FormControlName,
  ) {}

  public isVisible() {
    const control = this.controlName.control;

    return control && 
      control.invalid && 
      control.touched && 
      control.errors && 
      control.errors[this.errorType];
  }
}