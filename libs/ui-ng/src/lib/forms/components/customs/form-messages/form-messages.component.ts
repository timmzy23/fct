import {
  Component,
  Input
  } from '@angular/core';

export enum FormMessageType {
  ERROR = 'error',
  SUCCESS = 'success',
  WARNING = 'warning',
  INFO = 'info'
}

@Component({
  selector: 'fct-form-message',
  templateUrl: './form-message.component.html'
})
export class FormMessageComponent {
  @Input()
  public messageType: FormMessageType = FormMessageType.ERROR;
}
