import {
  Component,
  Input,
  OnInit
  } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ParagraphComponent } from '../../../typography';

@Component({
  selector: 'fct-input',
  templateUrl: './input.component.html',
  styleUrls: [
    './input.component.scss',
    '../../../typography/components/text/font-style.scss',
    '../../../typography/components/text/font-size.scss',
    '../../../typography/components/text/text-color.scss',
    '../../../typography/components/paragraph/text-align.scss',
    '../../../typography/components/paragraph/paragraph-line-height.scss'
  ]
})
export class InputComponent extends ParagraphComponent {
  @Input()
  public name: string;

  @Input()
  public type = 'text';

  @Input()
  public parentControl: AbstractControl;

  @Input()
  public autocomplete: string;
}