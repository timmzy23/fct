import {
  Component,
  Input,
  OnInit,
  ViewChild
  } from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
  } from '@angular/forms';
import { TextFieldComponent } from '../text-field/text-field.component';

@Component({
  selector: 'fct-email-field',
  templateUrl: './email-field.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: EmailFieldComponent,
      multi: true
    }
  ]
})
export class EmailFieldComponent 
  extends TextFieldComponent
  implements ControlValueAccessor, OnInit
{

  @Input()
  public autocomplete = 'email';

  @ViewChild('textField', { static: true })
  public textFieldRef: TextFieldComponent;

  public writeValue(obj: any): void {
    this.textFieldRef.writeValue(obj);
  }

  public registerOnChange(fn: any): void {
    this.textFieldRef.registerOnChange(fn);
  }

  public registerOnTouched(fn: any): void {
    this.textFieldRef.registerOnTouched(fn);
  }

  public setDisabledState?(isDisabled: boolean): void {
    this.textFieldRef.setDisabledState(isDisabled);
  }
}
