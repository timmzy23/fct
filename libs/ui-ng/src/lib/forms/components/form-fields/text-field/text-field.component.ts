import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild
  } from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR
  } from '@angular/forms';
import {
  TextComponent,
  TextStyle
  } from '../../../../typography';

@Component({
  selector: 'fct-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: [
    './text-field.component.scss',
    '../../../../typography/components/text/font-size.scss',
    '../../../../typography/components/text/font-style.scss',
    '../../../../typography/components/text/text-color.scss',
    '../../../../typography/components/text/text-transform.scss',
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TextFieldComponent,
      multi: true
    }
  ]
})
export class TextFieldComponent 
  extends TextComponent
  implements OnInit, ControlValueAccessor
{
  @Input()
  public name: string;

  @Input()
  public textStyle: TextStyle = TextStyle.SEMIBOLD;

  @Input()
  public autocomplete = 'off';

  @Input()
  public gridType: 'simple' | 'expanded' = 'simple';

  @Input()
  public inputType = 'text';

  @Input()
  public label: string;

  @Input()
  public placeholder = '';

  @ViewChild('inputElement', { static: true })
  private inputElm: ElementRef<HTMLInputElement>;

  public control: FormControl;

  private onChange: Function;

  private onTouche: Function;

  constructor(
    private renderer: Renderer2,
    private controlContrainer: ControlContainer
  ) {
    super();
  }

  public ngOnInit() {
    this.control = this.controlContrainer.control.get(this.name) as FormControl;
  }

  public handleChange(event: Event) {
    const input: HTMLInputElement = event.target as HTMLInputElement;

    this.onChange(input.value);
  }

  public handleFocus() {
    if (this.control && !this.control.touched) {
      this.onTouche();
    }
  }

  public writeValue(obj: any): void {
    this.renderer.setAttribute(this.inputElm.nativeElement, 'value', obj);
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouche = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    throw new Error("Method not implemented.");
  }
}
