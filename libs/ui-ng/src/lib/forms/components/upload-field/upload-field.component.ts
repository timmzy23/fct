import {
  Component,
  ElementRef,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Renderer2,
  SkipSelf,
  ViewChild
  } from '@angular/core';
import {
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR
  } from '@angular/forms';
import { first } from 'rxjs/operators';
import { TextFieldComponent } from '../text-field/text-field.component';

// https://netbasal.com/angular-custom-form-controls-made-easy-4f963341c8e2
// https://netbasal.com/how-to-implement-file-uploading-in-angular-reactive-forms-89a3fffa1a03
@Component({
  selector: 'fct-upload-field',
  templateUrl: './upload-field.component.html',
  styleUrls: ['./upload-field.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: UploadFieldComponent,
    multi: true
  }]
})
export class UploadFieldComponent 
  extends TextFieldComponent 
  implements ControlValueAccessor, OnInit, OnDestroy
{
  @Input()
  public fileType: string;

  private onChange: Function;

  private onTouche: Function;

  public control: FormControl;

  public file: File | null = null;

  @ViewChild('fileInput', { static: true })
  private inputElm: ElementRef<HTMLInputElement>;

  public isTouched: boolean;

  private uploadButtonClick$ = new EventEmitter();

  @Output()
  public fileSelect = new EventEmitter<File>();

  constructor(
    private host: ElementRef<HTMLInputElement>,
    private renderer: Renderer2,
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer
  ) {
    super();
  }

  public handleUploadButtonClick() {
    this.uploadButtonClick$.next();
  }

  public handleFileInput(event: Event) {
    const input: HTMLInputElement = event.target as HTMLInputElement;
    const file = input.files ? input.files.item(0) : null;

    this.fileSelect.emit(file);
  }

  public ngOnInit() {
    this.control = this.fetchFormControl(this.name);

    this.uploadButtonClick$.subscribe(() => {
      this.inputElm.nativeElement.click();
    });

    this.uploadButtonClick$.pipe(first()).subscribe(() => {
      this.isTouched = true;
      this.onTouche();
    });

    this.fileSelect.subscribe((file) => {
      this.file = file;
      this.onChange(file);
    });
  }

  public ngOnDestroy() {
    this.uploadButtonClick$.complete();
  }

  public writeValue(): void {
    this.renderer.setValue(this.inputElm, '');
    this.file = null;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouche = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.renderer.addClass(this.host.nativeElement, 'disabled');
    } else {
      this.renderer.removeClass(this.host.nativeElement, 'disabled');
    }
  }

  private fetchFormControl(controlName: string) {
    if (!this.controlContainer) {
      throw new TypeError('Component is not a part of a form group!');
    }
    const formControl = this.controlContainer.control.get(controlName);

    if (!formControl) {
      throw new TypeError(`Cannot find form control of "${controlName}"`);
    }

    return formControl as FormControl;
  }
}
