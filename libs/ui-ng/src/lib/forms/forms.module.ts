import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from '../buttons';
import { TypographyModule } from '../typography';
import {
  EmailFieldComponent,
  FormControlErrorMessageComponent,
  FormErrorMessageComponent,
  FormMessageComponent,
  InputComponent,
  LabelComponent,
  TextFieldComponent,
  UploadFieldComponent
  } from './components';
import { IsControlInvalidPipe } from './pipes';

const COMPONENTS = [
  TextFieldComponent,
  LabelComponent,
  InputComponent,
  UploadFieldComponent,
  FormControlErrorMessageComponent,
  FormMessageComponent,
  FormErrorMessageComponent,
  EmailFieldComponent,
]

@NgModule({
  declarations: [
    ...COMPONENTS,
    IsControlInvalidPipe
  ],
  imports: [
    CommonModule,
    ButtonsModule,
    ReactiveFormsModule,
    TypographyModule
  ],
  exports: [
    ...COMPONENTS,
    IsControlInvalidPipe
  ]
})
export class FormsModule { }
