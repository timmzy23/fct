import {
  Component,
  Input
  } from '@angular/core';
import { BackgroundColor } from '../../directives';
import {
  ParagraphLineHeight,
  TextAlign
  } from '../../typograpy.types';
import { TextComponent } from '../text/text.component';

@Component({
  selector: 'fct-text-block',
  templateUrl: './text-block.component.html',
  styleUrls: [
    '../text/font-size.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../../directives/background-color/background-color.directive.scss',
    './text-align.scss'
  ],
})
export class TextBlockComponent extends TextComponent {
  @Input()
  public textAlign: TextAlign = TextAlign.LEFT;

  @Input()
  public lineHeight: ParagraphLineHeight = ParagraphLineHeight.MEDIUM;

  @Input()
  public backgroundColor: BackgroundColor | string;
}