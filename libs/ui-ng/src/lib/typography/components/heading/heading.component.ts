import {
  ChangeDetectionStrategy,
  Component,
  Input
  } from '@angular/core';
import {
  HeadingType,
  TextSize
  } from '../../typograpy.types';
import { ParagraphComponent } from '../paragraph/paragraph.component';

@Component({
  selector: 'fct-heading',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
})
export class HeadingComponent extends ParagraphComponent {

  public HEADING_TYPE = HeadingType;

  @Input() 
  public level: HeadingType = HeadingType.H1;
}

@Component({
  selector: 'fct-h1',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Heading1Component extends HeadingComponent {
  public level = HeadingType.H1;

  @Input()
  public textSize = TextSize.DOUBLE_EXTRA_LARGE;
}

@Component({
  selector: 'fct-h2',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
})
export class Heading2Component extends HeadingComponent {
  public level = HeadingType.H2;

  @Input()
  public textSize = TextSize.EXTRA_LARGE;
}

@Component({
  selector: 'fct-h3',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
})
export class Heading3Component extends HeadingComponent {
  public level = HeadingType.H3;

  @Input()
  public textSize = TextSize.LARGE;
}

@Component({
  selector: 'fct-h4',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
})
export class Heading4Component extends HeadingComponent {
  public level = HeadingType.H4;

  @Input()
  public textSize = TextSize.MEDIUM;
}

@Component({
  selector: 'fct-h5',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
})
export class Heading5Component extends HeadingComponent {
  public level = HeadingType.H5;

  @Input()
  public textSize = TextSize.SMALL;
}

@Component({
  selector: 'fct-h6',
  templateUrl: './heading.component.html',
  styleUrls: [
    './heading.component.scss',
    '../text/font-style.scss',
    '../text/text-color.scss',
    '../text/text-transform.scss',
    '../paragraph/text-align.scss'
  ],
})
export class Heading6Component extends HeadingComponent {
  public level = HeadingType.H6;

  @Input()
  public textSize = TextSize.EXTRA_SMALL;
}