import {
  ChangeDetectionStrategy,
  Component,
  Input
  } from '@angular/core';
import { TextColor } from '../../directives';
import {
  TextSize,
  TextStyle,
  TextTransform
  } from '../../typograpy.types';

@Component({
  selector: 'fct-text',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextComponent {

  @Input() 
  public textStyle: TextStyle = TextStyle.MEDIUM;

  @Input() 
  public textSize: TextSize = TextSize.MEDIUM;

  @Input()
  public textColor: TextColor | string = TextColor.PRIMARY;

  @Input() 
  public textTransform?: TextTransform;
}

@Component({
  selector: 'fct-text-bold',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextBoldComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.BOLD;
}

@Component({
  selector: 'fct-text-bold-italic',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextBoldIalicComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.BOLD_ITALIC;
}

@Component({
  selector: 'fct-text-italic',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextItalicComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.ITALIC;
}

@Component({
  selector: 'fct-text-light',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextLightComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.LIGHT;
}

@Component({
  selector: 'fct-text-light-italic',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextLightItalicComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.LIGHT_ITALIC;
}

@Component({
  selector: 'fct-text-medium',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextMediumComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.MEDIUM;
}

@Component({
  selector: 'fct-text-medium-italic',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextMediumItalicComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.MEDIUM_ITALIC;
}

@Component({
  selector: 'fct-text-semibold',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextSemiboldComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.SEMIBOLD;
}

@Component({
  selector: 'fct-text-semibold-italic',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextSemiboldItalicComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.SEMIBOLD_ITALIC;
}

@Component({
  selector: 'fct-text-thin',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextThinComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.THIN;
}

@Component({
  selector: 'fct-text-thin-italic',
  templateUrl: './text.component.html',
  styleUrls: [
    './font-size.scss',
    './font-style.scss',
    './text-color.scss',
    './text-transform.scss'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextThinItalicComponent extends TextComponent {
  public textStyle: TextStyle = TextStyle.THIN_ITALIC;
}

