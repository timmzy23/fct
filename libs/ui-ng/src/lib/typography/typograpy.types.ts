export enum TextTransform {
  UPPERCASE = 'uppercase',
  LOWERCASE = 'lowercase',
  CAPITALIZE = 'capitalize',
  NONE = 'none'
}

export enum TextPriority {
  PRIMARY = 'primary',
  PRIMARY_INVERT = 'primary-invert',
  SECONDARY = 'secondary',
  MARGNAL = 'marginal',
  DISABLED = 'disabled',
  DISABLED_INVERT = 'disabled-invert',
  ERROR = 'error'
}

export enum TextStyle {
  BOLD = 'bold',
  BOLD_ITALIC = 'bold-italic',
  ITALIC = 'italic',
  LIGHT = 'light',
  LIGHT_ITALIC = 'light-italic',
  MEDIUM = 'medium',
  MEDIUM_ITALIC = 'medium-italic',
  SEMIBOLD = 'semibold',
  SEMIBOLD_ITALIC = 'semibold-italic',
  THIN = 'thin',
  THIN_ITALIC = 'thin-italic'
}

export enum TextSize {
  DOUBLE_EXTRA_SMALL = 'xxs',
  EXTRA_SMALL = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  EXTRA_LARGE = 'xl',
  DOUBLE_EXTRA_LARGE = 'xxl',
}

export enum  HeadingType {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  H4 = 'h4',
  H5 = 'h5',
  H6 = 'h6'
}

export enum TextAlign {
  CENTER = 'center',
  RIGHT = 'right',
  JUSTIFY = 'justify',
  LEFT = 'left'
}

export enum ParagraphLineHeight {
  EXTRA_SMALL = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  EXTRA_LARGE = 'xl',
  EXTRA_EXTRA_LARGE = 'xxl'
}