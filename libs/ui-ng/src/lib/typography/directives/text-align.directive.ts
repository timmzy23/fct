import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { TextAlign } from '../typograpy.types';

@Directive({
  selector: '[fctTextAlign]'
})
export class TextAlignDirective implements OnChanges {

  @Input('fctTextAlign')
  public textAlign: TextAlign;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const textAlign = changes.textAlign;

    if (!textAlign) { return; } 

    if (textAlign.currentValue !== textAlign.previousValue) {
      this.renderer.setAttribute(
          this.elm.nativeElement,
          'text-align',
          textAlign.currentValue);
    }
  }
}
