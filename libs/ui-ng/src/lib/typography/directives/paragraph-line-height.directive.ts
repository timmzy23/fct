import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ParagraphLineHeight } from '../typograpy.types';

@Directive({
  selector: '[fctLineHeight]'
})
export class ParagraphLineHeightDirective implements OnChanges {

  @Input('fctLineHeight')
  public lineHeight: ParagraphLineHeight | string;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const lineHeight = changes.lineHeight;

    if (!lineHeight) { return; } 

    if (lineHeight.currentValue !== lineHeight.previousValue) {
      const isInEnum = ReflectionHelper.isInEnum(ParagraphLineHeight, lineHeight.currentValue);
      this.renderer.setStyle(
            this.elm.nativeElement,
            'line-height',
            isInEnum ? null : lineHeight.currentValue);
      this.renderer.setAttribute(
          this.elm.nativeElement,
          'line-height',
          isInEnum ? lineHeight.currentValue : null);
    }
  }
}
