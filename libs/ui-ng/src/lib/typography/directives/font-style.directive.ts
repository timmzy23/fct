import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { TextStyle } from '../typograpy.types';

@Directive({
  selector: '[fctFontStyle]'
})
export class FontStyleDirective implements OnChanges {

  @Input('fctFontStyle')
  public fontStyle: TextStyle | string;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const fontStyle = changes.fontStyle;

    if (!fontStyle) { return; } 

    if (fontStyle.currentValue !== fontStyle.previousValue) {
      this.renderer.setAttribute(
          this.elm.nativeElement,
          'font-style',
          fontStyle.currentValue)
    }
  }
}
