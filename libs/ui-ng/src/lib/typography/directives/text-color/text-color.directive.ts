import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { TextColor } from './text-color.enum';

@Directive({
  selector: '[fctTextColor]'
})
export class TextColorDirective implements OnChanges {

  @Input('fctTextColor')
  public textColor: TextColor | string;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const textColor = changes.textColor;

    if (!textColor || textColor.currentValue === textColor.previousValue) { 
      return; 
    } 
    const isInEnum = 
        ReflectionHelper.isInEnum(TextColor, textColor.currentValue);

    this.renderer.setAttribute(
        this.elm.nativeElement,
        'text-color',
        isInEnum ? textColor.currentValue : null);

    this.renderer.setStyle(
        this.elm.nativeElement,
        'color',
        isInEnum ? null : textColor.currentValue);
  }
}
