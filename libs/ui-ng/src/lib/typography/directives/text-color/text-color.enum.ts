
export enum TextColor {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  MARGINAL = 'marginal',
  LIGHT = 'light',
  ULTRA_LIGHT = 'ultra-light',
  ERORR = 'error',
  DISABLED = 'disabled'
}