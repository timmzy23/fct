import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { TextSize } from '../typograpy.types';

@Directive({
  selector: '[fctFontSize]'
})
export class FontSizeDirective implements OnChanges {

  @Input('fctFontSize')
  public fontSize: TextSize | string;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const fontSize = changes.fontSize;

    if (!fontSize) { return; } 

    if (fontSize.currentValue !== fontSize.previousValue) {
      if (ReflectionHelper.isInEnum(TextSize, fontSize.currentValue)) {
        this.setAttribute(fontSize.currentValue);
        this.setStyle(null);
      } else {
        this.setAttribute(null);
        this.setStyle(fontSize.currentValue);
      }
    }
  }

  private setStyle(fontSize: string) {
    this.renderer.setStyle(
        this.elm.nativeElement,
        'font-size',
        fontSize);
  }

  private setAttribute(fontSize: string) {
    this.renderer.setAttribute(
        this.elm.nativeElement,
        'font-size',
        fontSize);
  }
}
