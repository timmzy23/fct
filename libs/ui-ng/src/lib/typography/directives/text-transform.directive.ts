import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { TextTransform } from '../typograpy.types';

@Directive({
  selector: '[fctTextTransform]'
})
export class TextTransformDirective implements OnChanges {

  @Input('fctTextTransform')
  public textAlign: TextTransform;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const textTransform = changes.textTransform;

    if (!textTransform) { return; } 

    if (textTransform.currentValue !== textTransform.previousValue) {
      this.renderer.setAttribute(
          this.elm.nativeElement,
          'text-transform',
          textTransform.currentValue);
    }
  }
}
