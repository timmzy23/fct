export enum BackgroundColor {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  MARGINAL = 'marginal',
  ULTRA_DARK = 'ultra-dark',
  DARK = 'dark',
  LIGHT = 'light',
  ULTRA_LIGHT = 'ultra-light',
  DISABLED = 'disabled',
}