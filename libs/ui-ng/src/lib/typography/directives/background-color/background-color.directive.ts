import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { BackgroundColor } from './background-color.enum';

@Directive({
  selector: '[fctBackgroundColor]'
})
export class BackgroundColorDirective implements OnChanges {

  @Input('fctBackgroundColor')
  public backgroundColor: BackgroundColor | string;

  constructor(private elm: ElementRef, private renderer: Renderer2) { }

  public ngOnChanges(changes: SimpleChanges) {
    const backgroundColor = changes.backgroundColor;

    if (!backgroundColor) { return; } 

    if (backgroundColor.currentValue !== backgroundColor.previousValue) {
      const isInEnum = ReflectionHelper.isInEnum(
          BackgroundColor, backgroundColor.currentValue);

      this.renderer.setStyle(
          this.elm.nativeElement,
          'background-color',
          isInEnum ? null : backgroundColor.currentValue);
      
      this.renderer.setAttribute(
          this.elm.nativeElement,
          'background-color',
          isInEnum ? backgroundColor.currentValue : null);
    }
  }
}
