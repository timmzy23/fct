import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChange,
  SimpleChanges
  } from '@angular/core';
import { FontStyle } from './font-style.enum';

@Directive({
  selector: '[fctFontStyle]'
})
export class FontStyleDirective implements OnChanges {

  @Input('fctFontStyle')
  public fontStyle: FontStyle = FontStyle.MEDIUM;

  constructor(
    private elmRef: ElementRef,
    private renderer: Renderer2
  ) {}

  public ngOnChanges(changes: SimpleChanges): void {
    const fontStyle: SimpleChange = changes.fontStyle;

    if (!fontStyle || fontStyle.currentValue === fontStyle.previousValue) { 
      return; 
    }

    this.renderer.setAttribute(
      this.elmRef.nativeElement,
      'font-style',
      fontStyle.currentValue);
  }
}

