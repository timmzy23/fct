export enum FontStyle {
  BOLD = 'bold',
  BOLD_ITALIC = 'bold-italic',
  ITALIC = 'italic',
  LIGHT = 'light',
  LIGHT_ITALIC = 'light-italic',
  MEDIUM = 'medium',
  MEDIUM_ITALIC = 'medium-italic',
  SEMIBOLD = 'semibold',
  SEMIBOLD_ITALIC = 'semibold-italic',
  THIN = 'thin',
  THIN_ITALIC = 'thin-italic'
}