import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  Heading1Component,
  Heading2Component,
  Heading3Component,
  Heading4Component,
  Heading5Component,
  Heading6Component,
  HeadingComponent,
  ParagraphComponent,
  TextBlockComponent,
  TextBoldComponent,
  TextBoldIalicComponent,
  TextComponent,
  TextItalicComponent,
  TextLightComponent,
  TextLightItalicComponent,
  TextMediumComponent,
  TextMediumItalicComponent,
  TextSemiboldComponent,
  TextSemiboldItalicComponent,
  TextThinComponent,
  TextThinItalicComponent
  } from './components';
import {
  BackgroundColorDirective,
  FontSizeDirective,
  FontStyleDirective,
  ParagraphLineHeightDirective,
  TextAlignDirective,
  TextColorDirective,
  TextTransformDirective
  } from './directives';
import { ArrayJoinPipe } from './pipes';

const COMPONENTS = [
  TextComponent, 
  TextBoldComponent,
  TextBoldIalicComponent,
  TextItalicComponent,
  TextLightComponent,
  TextLightItalicComponent,
  TextMediumComponent,
  TextMediumItalicComponent,
  TextSemiboldComponent,
  TextSemiboldItalicComponent,
  TextThinComponent,
  TextThinItalicComponent,
  HeadingComponent, 
  Heading1Component,
  Heading2Component,
  Heading3Component,
  Heading4Component, 
  Heading5Component,
  Heading6Component,
  ParagraphComponent,
];


@NgModule({
  declarations: [
    ...COMPONENTS,
    FontSizeDirective,
    TextColorDirective,
    TextAlignDirective,
    FontStyleDirective,
    TextTransformDirective,
    ParagraphLineHeightDirective,
    BackgroundColorDirective,
    ArrayJoinPipe,
    TextBlockComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [ 
    ...COMPONENTS,
    FontSizeDirective,
    TextColorDirective,
    TextAlignDirective,
    FontStyleDirective,
    TextTransformDirective,
    ParagraphLineHeightDirective,
    BackgroundColorDirective,
    ArrayJoinPipe,
    TextBlockComponent
  ]
})
export class TypographyModule { }
