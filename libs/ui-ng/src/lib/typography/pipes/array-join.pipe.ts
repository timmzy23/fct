import {
  Pipe,
  PipeTransform
  } from '@angular/core';

/**
 * Print joined array of string into template.
 * 
 * Handle observed array as immutable in order to propagate any change
 * to template!
 */
@Pipe({
  name: 'arrayJoin',
  pure: true
})
export class ArrayJoinPipe implements PipeTransform {
  public transform(arr: string[], joinBy: string = ', ') {
    return arr.join(joinBy);
  }
}
