import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2
  } from '@angular/core';
import { ToggleClassFromPropDirective } from './toggle-class-from-prop.directive';


@Directive({
  selector: '[fctGridArea]'
})
export class GridAreaDirective 
  extends ToggleClassFromPropDirective 
  implements OnChanges 
{
  @Input('fctGridArea')
  public gridArea: string;

  constructor(elm: ElementRef, renderer: Renderer2) {
    super(elm, renderer, 'gridArea');
  }

  protected buildClassName(gridArea: string): string {
    return `fct-grid-area-${gridArea}`;
  }
}