import {
  OnChanges,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ToggleClassDirective } from './toggle-class.directive';

export declare namespace ToggleClassOrStyleDirective {
  interface StylesDefs {
    [styleName: string]: any;
  }
}

export abstract class ToggleClassOrStyleDirective
  extends ToggleClassDirective<any>
  implements OnChanges {

  public abstract value: string;

  protected abstract styleName: string;

  public ngOnChanges(changes: SimpleChanges) {
    if (!this.hasChange(changes)) { return; }
    const nextValue = changes.value.currentValue;
    const prevValue = changes.value.previousValue;

    if (nextValue === prevValue) { return; }

    if (this.isClassDef(nextValue)) {
      this.handleClassNames(prevValue, nextValue);
    } else {
      ReflectionHelper.forEachOwnKey(
          this.getNextStyles(changes),
          (styleName: string, styleValue: any) => 
            this.renderer.setStyle(
                this.elm.nativeElement, styleName, styleValue));
    }
  }

  protected abstract isClassDef(nextValue: any): boolean;

  protected getNextValue(changes: SimpleChanges) {
    return changes.value.currentValue;
  }

  protected getPrevValue(changes: SimpleChanges) {
    return changes.value.previousValue;
  }

  protected hasChange(changes: SimpleChanges) {
    return !!changes.value;
  }

  protected getNextStyles(
    changes: SimpleChanges
  ): ToggleClassOrStyleDirective.StylesDefs {
    return {
      [this.styleName]: changes.value.currentValue
    }
  }
}
