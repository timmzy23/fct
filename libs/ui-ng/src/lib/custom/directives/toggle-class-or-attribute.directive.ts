import {
  OnChanges,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ToggleClassDirective } from './toggle-class.directive';

export declare namespace ToggleClassOrAttributeDirective {
  interface AttributeDefs {
    [attributeDef: string]: any;
  }
}

export abstract class ToggleClassOrAttributeDirective
  extends ToggleClassDirective<any>
  implements OnChanges {

  public abstract value: any;

  protected abstract attrName: string;

  public ngOnChanges(changes: SimpleChanges) {
    if (!this.hasChange(changes)) { return; }
    const nextValue = this.getNextValue(changes)
    const prevValue = this.getPrevValue(changes);

    if (nextValue === prevValue) { return; }

    if (this.isClassDef(nextValue)) {
      this.handleClassNames(prevValue, nextValue);
    } else {
      ReflectionHelper.forEachOwnKey(
          this.getNextAttributes(changes),
          (attrName: string, attrValue: any) => 
            this.renderer.setAttribute(
                this.elm.nativeElement, attrName, attrValue));
    }
  }

  protected abstract isClassDef(nextValue: any): boolean;

  protected getNextValue(changes: SimpleChanges) {
    return changes.value.currentValue;
  }

  protected getPrevValue(changes: SimpleChanges) {
    return changes.value.previousValue;
  }

  protected hasChange(changes: SimpleChanges) {
    return !!changes.value;
  }

  protected getNextAttributes(
    changes: SimpleChanges
  ): ToggleClassOrAttributeDirective.AttributeDefs {
    return {
      [this.attrName]: changes.value.currentValue
    }
  }
}
