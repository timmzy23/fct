import {
  ElementRef,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';

export abstract class ToggleClassDirective<V> implements OnChanges {

  constructor(
    protected elm: ElementRef,
    protected renderer: Renderer2
  ) { }

  public ngOnChanges(changes: SimpleChanges) {
    if (!this.hasChange(changes)) { return; }
    const nextValue = this.getNextValue(changes);
    const prevValue = this.getPrevValue(changes);

    if (nextValue === prevValue) { return; }

    if (!!prevValue) {
      const prevClassName = this.buildClassName(prevValue);
      this.renderer.removeClass(this.elm.nativeElement, prevClassName);
    }
    if (!!nextValue) {
      const nextClassName = this.buildClassName(nextValue);
      this.renderer.addClass(this.elm.nativeElement, nextClassName);
    }
  }

  protected handleClassNames(prevValue: V, nextValue: V) {
    if (!!prevValue) {
      const prevClassName = this.buildClassName(prevValue);
      this.renderer.removeClass(this.elm.nativeElement, prevClassName);
    }
    if (!!nextValue) {
      const nextClassName = this.buildClassName(nextValue);
      this.renderer.addClass(this.elm.nativeElement, nextClassName);
    }
  }

  protected abstract getNextValue(changes: SimpleChanges): V;

  protected abstract getPrevValue(changes: SimpleChanges): V;

  protected abstract hasChange(changes: SimpleChanges): boolean;

  protected abstract buildClassName(value: V): string;
}
