import {
  ElementRef,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { ToggleClassDirective } from './toggle-class.directive';

export abstract class ToggleClassFromPropDirective 
  extends ToggleClassDirective<string>
  implements OnChanges 
{
  constructor(
    elm: ElementRef,
    renderer: Renderer2,
    protected propName: string,
  ) { 
    super(elm, renderer);
  }

  protected getNextValue(changes: SimpleChanges) {
    return changes[this.propName].currentValue;
  }

  protected getPrevValue(changes: SimpleChanges) {
    return changes[this.propName].previousValue;
  }

  protected hasChange(changes: SimpleChanges) {
    return !!changes[this.propName];
  }

  protected abstract buildClassName(value: string): string;
}
