import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GridAreaDirective } from './directives';
import { FctLogoComponent } from './fct-logo/fct-logo.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FctLogoComponent, 
    GridAreaDirective, 
  ],
  exports: [
    FctLogoComponent, 
    GridAreaDirective
  ]
})
export class CustomModule { }
