import {
  Component,
  Input
  } from '@angular/core';

@Component({
  selector: 'fct-logo',
  templateUrl: './fct-logo.component.svg',
  styleUrls: ['./fct-logo.component.scss']
})
export class FctLogoComponent {
  @Input()
  public width: string;

  @Input()
  public height: string;
}
