import { NgModule } from '@angular/core';
import { InOutTransitionStateDirective } from './in-out-transition/in-out-transition.directive';

const DIRECTIVES = [
  InOutTransitionStateDirective
]

@NgModule({
  declarations: [
    ...DIRECTIVES
  ],
  exports: [
    ...DIRECTIVES
  ]
})
export class AnimationModule {}