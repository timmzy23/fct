import {
  Directive,
  ElementRef,
  Renderer2
  } from '@angular/core';
import { useEffect } from '@fct/sdk-ng';
import { InOutTransitionState } from './in-out-transition.state';

@Directive({
  selector: '[fctTransitionInOutClassName]'
})
export class InOutTransitionStateDirective {
  public classNames = {
    IN: 'in',
    OUT: 'out',
    ENTER: 'enter',
    LEAVE: 'leave'
  }

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private inOutState: InOutTransitionState
  ) {}

  @useEffect([])
  private handleEnterStart() {
    const subscription = this.inOutState.onEnterStart(
        () => this.resolveClassName(this.classNames.ENTER));
    return () => subscription.unsubscribe();
  }

  @useEffect([])
  private handleEnter() {
    const subscription = this.inOutState.onEnterStart(
        () => this.resolveClassName(this.classNames.IN));
    return () => subscription.unsubscribe();
  }

  @useEffect([])
  private handleLeaveStart() {
    const subscription = this.inOutState.onEnterStart(
        () => this.resolveClassName(this.classNames.LEAVE));
    return () => subscription.unsubscribe();
  }

  @useEffect([])
  private handleLeave() {
    const subscription = this.inOutState.onEnterStart(
        () => this.resolveClassName(this.classNames.OUT));
    return () => subscription.unsubscribe();
  }

  private resolveClassName(className: string) {
    Reflect.ownKeys(this.classNames)
        .map((key) => this.classNames[key])
        .forEach((currClassName) =>  currClassName === className ?
            this.renderer.addClass(this.elementRef.nativeElement, currClassName) :
            this.renderer.removeClass(this.elementRef.nativeElement, currClassName));
  }
}