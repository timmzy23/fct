import {
  Inject,
  Injectable,
  InjectionToken,
  OnDestroy
  } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  Subscription,
  timer
  } from 'rxjs';
import {
  filter,
  share
  } from 'rxjs/operators';
import { InOutTransitionStateType } from './in-out-transition.state-type';

export const IN_OUT_TRANSITION_DURATION_IN_MS = 
    new InjectionToken('inOutTransitionDurationInMs');
export const IN_OUT_TRANSITION_DURATION_OUT_MS = 
    new InjectionToken('inOutTransitionDurationOutMs');

@Injectable()
export class InOutTransitionState implements OnDestroy {

  private state$: BehaviorSubject<InOutTransitionStateType>;

  private stateObs: Observable<InOutTransitionStateType>;

  private pending: Subscription;

  constructor(
    @Inject(IN_OUT_TRANSITION_DURATION_IN_MS)
    private enteringDurationMs: number,
    @Inject(IN_OUT_TRANSITION_DURATION_OUT_MS)
    private leavingDurationMs: number = enteringDurationMs,
  ) {
    this.state$ = new BehaviorSubject(InOutTransitionStateType.OUT);
    this.stateObs = this.state$.pipe(share());
  }

  public subscribe(clb: (state: InOutTransitionStateType) => void) {
    return this.stateObs.subscribe(clb);
  }

  public onEnter(clb: () => void) {
    return this.stateObs
        .pipe(filter((state) => state === InOutTransitionStateType.IN))
        .subscribe(clb);
  }

  public onEnterStart(clb: () => void) {
    return this.stateObs
        .pipe(filter((state) => state === InOutTransitionStateType.ENTERING))
        .subscribe(clb);
  }

  public onLeave(clb: () => void) {
    return this.stateObs
        .pipe(filter((state) => state === InOutTransitionStateType.OUT))
        .subscribe(clb);
  }

  public onLeaveStart(clb: () => void) {
    return this.stateObs
        .pipe(filter((state) => state === InOutTransitionStateType.LEAVING))
        .subscribe(clb);
  }

  public observe() {
    return this.stateObs;
  }

  public getState(): InOutTransitionStateType {
    return this.state$.getValue();
  }

  public enter() {
    this.changeState(
      InOutTransitionStateType.ENTERING,
      InOutTransitionStateType.IN,
      this.enteringDurationMs);
  }

  public leave() {
    this.changeState(
      InOutTransitionStateType.LEAVING,
      InOutTransitionStateType.OUT,
      this.leavingDurationMs);
  }

  public ngOnDestroy() {
    this.state$.complete();
  }

  private changeState(
    startState: InOutTransitionStateType,
    endState: InOutTransitionStateType,
    durationMs: number
  ) {
    if (this.pending) {
      this.pending.unsubscribe();
    }

    this.state$.next(startState);

    this.pending = timer(durationMs)
        .subscribe(() => {
          this.state$.next(endState);
          this.pending.unsubscribe();
        });
  }
}