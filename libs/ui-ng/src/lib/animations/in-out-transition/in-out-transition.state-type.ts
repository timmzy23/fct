
export enum InOutTransitionStateType {
  IN = 'in',
  OUT = 'out',
  ENTERING = 'entering',
  LEAVING = 'leaving'
}
