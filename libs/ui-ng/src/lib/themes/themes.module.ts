import { CommonModule } from '@angular/common';
import {
  APP_INITIALIZER,
  ModuleWithProviders,
  NgModule
  } from '@angular/core';
import { SetThemeDirective } from './set-theme.directive';
import {
  ThemesConfig,
  UI_THEMES_CONFIG
  } from './themes.config';
import { ThemesService } from './themes.service';

export declare namespace ThemesModule {
  type Init = ThemesConfig;
}

export function initTheme(themeService: ThemesService, config: ThemesModule.Init) {
  return () => themeService.setTheme(config.defaultThemeName);
}

@NgModule({
  declarations: [
    SetThemeDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [SetThemeDirective]
})
export class ThemesModule { 
  public static forRoot(init: ThemesModule.Init): ModuleWithProviders {
    return {
      ngModule: ThemesModule,
      providers: [
        { provide: UI_THEMES_CONFIG, useValue: init },
        {
          provide: APP_INITIALIZER,
          useFactory: initTheme,
          deps: [ThemesService, UI_THEMES_CONFIG],
          multi: true
        }
      ] 
    }
  }
}
