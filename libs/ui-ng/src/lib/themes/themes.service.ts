import {
  Inject,
  Injectable
  } from '@angular/core';
import {
  BehaviorSubject,
  Subject
  } from 'rxjs';
import { share } from 'rxjs/operators';
import {
  ThemesConfig,
  UI_THEMES_CONFIG
  } from './themes.config';

@Injectable({
  providedIn: 'root'
})
export class ThemesService {
  private theme$ = new BehaviorSubject<string>(this.config.defaultThemeName);

  public change = this.theme$.pipe(share());

  constructor(
    @Inject(UI_THEMES_CONFIG) private config: ThemesConfig
  ) {}

  public setTheme(newThemeName: string) {
    const prevThemeName = this.theme$.getValue();

    if (newThemeName === prevThemeName) { return; }

    document.body.classList.remove(prevThemeName);
    document.body.classList.add(newThemeName);
    
    this.theme$.next(newThemeName);
  }

  public getThemeName(): string {
    return this.theme$.getValue();
  }

  public observeThemeChange() {
    return this.change;
  }
}
