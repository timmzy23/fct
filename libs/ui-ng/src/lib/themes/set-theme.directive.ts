import {
  Directive,
  HostListener,
  Input
  } from '@angular/core';
import { ThemesService } from './themes.service';

/**
 * Allows to change theme on click on given host element. 
 * (Button, icon, text, whatever).
 */
@Directive({
  selector: '[fctSetTheme]'
})
export class SetThemeDirective {

  @Input('fctSetTheme') 
  public themeName: string;

  constructor(private themeService: ThemesService) {}

  @HostListener('click') 
  public handleMouseClick() {
    this.themeService.setTheme(this.themeName);
  }
}
