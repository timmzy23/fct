import { InjectionToken } from '@angular/core';

export interface ThemesConfig {
  defaultThemeName: string;
}

export const UI_THEMES_CONFIG = 
    new InjectionToken<ThemesConfig>('ui.theme.config');
