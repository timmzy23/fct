import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TypographyModule } from '../typography';
import {
  ButtonComponent,
  CreateIconButtonComponent,
  IconButtonComponent,
  } from './components';


@NgModule({
  declarations: [
    IconButtonComponent,
    CreateIconButtonComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule,
    TypographyModule
  ],
  exports: [
    IconButtonComponent,
    CreateIconButtonComponent,
    ButtonComponent
  ]
})
export class ButtonsModule { }
