export class ButtonClickEvent {

  constructor(private event: MouseEvent) {}

  public preventDefault() {
    this.event.preventDefault();
  }

  public stopPropagation() {
    this.event.stopPropagation();
  }
}