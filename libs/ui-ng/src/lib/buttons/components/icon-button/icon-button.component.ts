import {
  Component,
  ContentChild,
  TemplateRef
  } from '@angular/core';

@Component({
  selector: 'fct-icon-button',
  templateUrl: './icon-button.component.html'
})
export class IconButtonComponent {

  @ContentChild(TemplateRef, { static: true })
  public template;

  public iconState = {
    hover: false
  };

  public handleMouseEnter() {
    this.iconState.hover = true;
  }

  public handleMouseOut() {
    this.iconState.hover = false;
  }
}

@Component({
  selector: 'fct-create-account-button',
  template: '<fct-icon-button><ng-template let-hover="hover">HOVER: {{hover}}</ng-template></fct-icon-button>'
})
export class CreateIconButtonComponent {

  @ContentChild(TemplateRef, { static: true })
  public template;

  public iconState = {
    hover: false
  };

  public handleMouseEnter() {
    this.iconState.hover = true;
  }

  public handleMouseOut() {
    this.iconState.hover = false;
  }
}

