import {
  Component,
  EventEmitter,
  Input,
  Output
  } from '@angular/core';
import {
  BackgroundColor,
  ParagraphComponent,
  TextSize
  } from '../../../typography';
import { ButtonClickEvent } from '../../model';

@Component({
  selector: 'fct-button',
  templateUrl: './button.component.html',
  styleUrls: [
    './button.component.scss',
    '../../../typography/directives/background-color/background-color.directive.scss'
  ]
})
export class ButtonComponent extends ParagraphComponent {
  @Input()
  public type = 'button';

  @Input()
  public name: string; 

  @Output()
  public buttonClick = new EventEmitter<ButtonClickEvent>();

  @Input()
  public disabled = false;

  @Input()
  public fullWidth = false;

  @Input()
  public textSize = TextSize.LARGE;

  @Input()
  public backgroundColor = BackgroundColor.PRIMARY;

  public handleClick(event: MouseEvent) {
    if (this.disabled) { return; }

    if (this.buttonClick) {
      this.buttonClick.next(new ButtonClickEvent(event));
    }
  }
}
