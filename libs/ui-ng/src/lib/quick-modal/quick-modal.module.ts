import { CommonModule } from '@angular/common';
import {
  ModuleWithProviders,
  NgModule
  } from '@angular/core';
import {
  ACTION_EFFECT,
  ActionsModule
  } from '@fct/sdk-ng';
import { PortalModule } from '../portal';
import { QuickModalComponent } from './quick-modal.component';
import { QuickModalService } from './quick-modal.service';
import {
  QUICK_MODAL_SETTINGS,
  QuickModalSettings
  } from './quick-modal.tokens';

export declare namespace QuickModalModule {
  type Setttings = QuickModalSettings;
}

/**
 * QuickModal is a modal shown at the top middle of screen
 * with custom content being put in.
 */

@NgModule({
  declarations: [
    QuickModalComponent
  ],
  imports: [
    PortalModule,
    CommonModule,
    ActionsModule
  ],
  exports: [
    QuickModalComponent
  ]
})
export class QuickModalModule {
  public static forRoot(settings: QuickModalSettings): ModuleWithProviders {
    return {
      ngModule: QuickModalModule,
      providers: [
        { provide: QUICK_MODAL_SETTINGS, useValue: settings },
        QuickModalService,
      ]
    }
  }
}
 