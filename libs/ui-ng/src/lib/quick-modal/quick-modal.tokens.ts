import { InjectionToken } from '@angular/core';

export interface QuickModalSettings {
  selector: string;
}

export const QUICK_MODAL_SETTINGS = 
    new InjectionToken<string>('quick.modal.settings');
