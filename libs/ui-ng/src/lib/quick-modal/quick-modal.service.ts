import {
  ComponentRef,
  Inject,
  Injectable,
  Injector,
  OnDestroy,
  OnInit
  } from '@angular/core';
import { ErrnoError } from '@fct/sdk';
import {
  actionOf,
  CommandActionBus,
  EventActionBus,
  RootActionBus
  } from '@fct/sdk-ng';
import {
  BehaviorSubject,
  fromEvent,
  Subscription
  } from 'rxjs';
import {
  filter,
  map,
  share,
  tap
  } from 'rxjs/operators';
import { KeyboardESCEvent } from '../keyboard';
import { PortalService } from '../portal';
import {
  QuickModalCloseCommand,
  QuickModalOpenCommand
  } from './quick-modal.commands';
import { ERR_QUICK_MODAL_ALREADY_OPEN } from './quick-modal.errors';
import {
  QUICK_MODAL_SETTINGS,
  QuickModalSettings
  } from './quick-modal.tokens';

export declare namespace QuickModalService {
  interface ModalProps {
    [key: string]: any
  }
}

@Injectable()
export class QuickModalService implements OnInit, OnDestroy {

  private modalRef$ = new BehaviorSubject<ComponentRef<any>>(null);

  private subscriptions: Subscription[] = [];

  constructor(
    @Inject(QUICK_MODAL_SETTINGS)
    private settings: QuickModalSettings,
    private portalService: PortalService,
    private commands$: CommandActionBus,
    private events$: EventActionBus,
    private actions$: RootActionBus
  ) {  }

  public visible$ = this.modalRef$.pipe(
      share(),
      map((any: any) => !!any));

  public ngOnInit() {
    // Listen for quick modal open
    this.subscriptions.push(this.commands$
        .pipe(
            actionOf(QuickModalOpenCommand),
            filter(() => !this.isOpen()))
        .subscribe((command: QuickModalOpenCommand.Action) => 
            this.open(
                command.payload.componentType,
                command.payload.inputProps,
                command.payload.outputProps)));

    // Listen for "ESC" when modal is open to close it
    this.subscriptions.push(this.events$
        .pipe(
          actionOf(KeyboardESCEvent),
          filter(() => this.isOpen()))
        .subscribe({
          next: () => this.commands$.emit(QuickModalCloseCommand.create())
        }));

    this.subscriptions.push(this.commands$
          .pipe(actionOf(QuickModalCloseCommand))
          .subscribe({ next: () => this.close() }));
  }

  public open(
    modalDef, 
    inputProps: QuickModalService.ModalProps = {},
    outputProps: QuickModalService.ModalProps = {}
  ) {
    if (this.modalRef$.getValue()) {
      throw new ErrnoError(
        ERR_QUICK_MODAL_ALREADY_OPEN,
        'QuickModal already host a view!');
    }

    this.modalRef$.next(this.portalService.appendPortal(
        `#${this.settings.selector}`,
        modalDef,
        { inputs: inputProps, outputs: outputProps }));
  }

  public isOpen(): boolean {
    return !!this.modalRef$.getValue();
  }

  public close() {
    this.portalService.removePortal(this.modalRef$.getValue());

    this.modalRef$.next(null);
  }

  public ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}