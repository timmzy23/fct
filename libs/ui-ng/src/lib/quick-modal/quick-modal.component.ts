import {
  Component,
  Inject,
  OnDestroy,
  OnInit
  } from '@angular/core';
import { Subscription } from 'rxjs';
import { QuickModalService } from './quick-modal.service';
import { QUICK_MODAL_SETTINGS } from './quick-modal.tokens';

@Component({
  selector: 'fct-quick-modal',
  templateUrl: './quick-modal.component.html',
  styleUrls: ['./quick-modal.component.scss']
})
export class QuickModalComponent implements OnInit, OnDestroy {

  public className = 'quick-modal-layer';

  private subscription: Subscription;

  constructor(
    @Inject(QUICK_MODAL_SETTINGS) 
    public settings,
    private quickModalService: QuickModalService
  ) {}

  public ngOnInit() {
    this.quickModalService.visible$.subscribe(this.handleClassChange);
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private handleClassChange = (isVisible: boolean) => {
    this.className = isVisible ?
      'visible quick-modal-layer' :
      'quick-modal-layer';
  }
}
