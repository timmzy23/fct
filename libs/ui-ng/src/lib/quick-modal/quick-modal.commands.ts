import { FluxStandardAction } from '@fct/sdk-ng';
enum QuickModalCommandTypes {
  OPEN = 'cmd.quickModal.open',
  CLOSE = 'cmd.quickModal.close'
}


export declare namespace QuickModalOpenCommand {
  type ACTION_TYPE = QuickModalCommandTypes.OPEN;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    componentType: any;
    inputProps?: any;
    outputProps?: any;
  }
}

export class QuickModalOpenCommand {
  public static ACTION_TYPE: QuickModalOpenCommand.ACTION_TYPE = 
    QuickModalCommandTypes.OPEN;

  public static create(
    payload: QuickModalOpenCommand.Payload
  ): QuickModalOpenCommand.Action {
    return FluxStandardAction.create(
        QuickModalOpenCommand.ACTION_TYPE, payload);
  }
}


export declare namespace QuickModalCloseCommand {
  type ACTION_TYPE = QuickModalCommandTypes.CLOSE;
  type Action = FluxStandardAction.Action<ACTION_TYPE>;
}

export class QuickModalCloseCommand {
  public static ACTION_TYPE: QuickModalCloseCommand.ACTION_TYPE = 
    QuickModalCommandTypes.CLOSE;

  public static create(): QuickModalCloseCommand.Action {
    return FluxStandardAction.create(
        QuickModalCloseCommand.ACTION_TYPE);
  }
}