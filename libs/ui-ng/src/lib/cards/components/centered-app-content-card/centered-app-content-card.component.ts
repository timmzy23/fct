import { Component } from '@angular/core';

@Component({
  selector: 'fct-centered-app-content-card',
  templateUrl: './centered-app-content-card.component.html',
  styleUrls: ['./centered-app-content-card.component.scss']
})
export class CenteredAppContentCardComponent {
  public darkButtonContext = {
    theme: 'theme-dark', 
    label: 'Dark', 
    i18nDescription: 'Button switch to dark theme'
  }

  public lightButtonContext = {
    theme: 'theme-light', 
    label:'Light',
    i18nDescription: 'Button switch to light theme'
  }
}
