import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonsModule } from '../buttons';
import { CustomModule } from '../custom';
import { ThemesModule } from '../themes';
import { TypographyModule } from '../typography';
import { CenteredAppContentCardComponent } from './components';

@NgModule({
  declarations: [ 
    CenteredAppContentCardComponent 
  ],
  exports: [
    CenteredAppContentCardComponent
  ],
  imports: [
    CommonModule,
    CustomModule,
    TypographyModule,
    ThemesModule
  ]
})
export class CardsModule { }
