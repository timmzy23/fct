import {
  CommonModule,
  DOCUMENT
  } from '@angular/common';
import {
  HttpClient,
  HttpClientModule
  } from '@angular/common/http';
import {
  ApplicationModule,
  ErrorHandler,
  NgModule,
  RendererFactory2,
  Sanitizer
  } from '@angular/core';
import {
  PdfDocumentComponent,
  PdfTextComponent
  } from './components';
import {
  PdfComponentFactoryResolver,
  PdfRendererFactory,
  PdfViewContextFactory,
  PdfViewRegistryFactory
  } from './factories';
import { PdfFontFace } from './pdf.interfaces';
import {
  PdfRendererErrorHandler,
  PdfSanitizer
  } from './services';
import { PdfView } from './view';
  
const COMPONENTS = [
  PdfDocumentComponent,
  PdfTextComponent
]

/**
 * Module declares renderers to write into PDF file.
 */
@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    ApplicationModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [
    { provide: DOCUMENT, useValue: {} },
    { provide: ErrorHandler, useClass: PdfRendererErrorHandler, deps: [] },
    { provide: RendererFactory2, useClass: PdfRendererFactory},
    { provide: Sanitizer, useClass: PdfSanitizer, deps: [] }, 
    // { provide: ComponentFactoryResolver, useClass: PdfComponentFactoryResolver, deps: []},
    PdfViewContextFactory,
    PdfViewRegistryFactory
  ],
  exports: [
    ApplicationModule,
    CommonModule,
    ...COMPONENTS
  ]
})
export class PdfRendererModule {

  private fonts: PdfFontFace[] = [];

  constructor(
    private rendererFactory: RendererFactory2,
    private http: HttpClient,
  ) {
    console.log('PDF RENDER MODULE IS HERE!');
  }

  public getRoots(): PdfView[] {
    return (this.rendererFactory as PdfRendererFactory).getPdfRenderer().roots;
  }

  public renderPdf() {
    this.getRoots().forEach((root) => {
      root.context.registerFonts(this.fonts);
      root.render();
      root.context.getDriver().save(root.context.fileName);
    });
  }

  public downloadFonts(fontFaces: PdfFontFace[]) {
    return Promise.all(fontFaces.map((fontFace) => this.downloadFont(fontFace.url)
        .then((fontEncodedString) => 
            this.fonts.push({ ...fontFace, fontEncodedString }))
        .then(() => fontFace)));
  }

  private downloadFont(fontUrl): Promise<string> {
    return this.http
      .get(fontUrl, { 
        responseType: 'blob' as 'json',
      })
      .toPromise()
      .then((blob: Blob) => {
        const reader = new FileReader();
        reader.readAsBinaryString(blob);

        return new Promise<string>((resolve) => {
          reader.onloadend = function() {
            resolve(btoa(reader.result as string));
          }
        });
      });
  }
}
