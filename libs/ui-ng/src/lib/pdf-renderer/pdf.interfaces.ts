export interface PdfFontFace {
  fontName: string,
  fontFamily: string,
  url: string,
  fontStyle: string
  fontEncodedString?: string;
}
