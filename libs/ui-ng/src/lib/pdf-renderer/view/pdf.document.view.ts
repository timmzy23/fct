import { PdfView } from './pdf.view';

export class PdfDocumentView extends PdfView {
  
  constructor() {
    super('pdf-document');
  }

  public render() {
    this.context.addDefaultStyle('fontSize', this.getAttribute('fontSize'));
    this.context.addDefaultStyle('fontStyle', this.getAttribute('fontStyle'));
    this.context.addDefaultStyle('fontName', this.getAttribute('fontName'));
    // this.context.convertUnit = this.getAttribute('convertUnit');
    // this.context.convertFontUnit = this.getAttribute('convertFontUnit');
    
    this.context.createDriver({
      format: this.getAttribute('format'),
      orientation: this.getAttribute('orientation'),
      unit: this.getAttribute('unit')
    });
    this.context.fileName = this.getAttribute('fileName');

    this.context.fonts.forEach((font) => {
      this.context.getDriver().addFileToVFS(
          font.fontName, 
          font.fontEncodedString);
      this.context.getDriver().addFont(
          font.fontName, 
          font.fontFamily, 
          font.fontStyle);
    });

    super.render();
  }
}