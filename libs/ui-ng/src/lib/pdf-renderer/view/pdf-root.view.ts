import { PdfViewContext } from '../services';
import { PdfView } from '../view/pdf.view';

export class PdfRootView extends PdfView {
  public name = 'pdf-root'

  constructor(public context: PdfViewContext) {
    super();
  };
}