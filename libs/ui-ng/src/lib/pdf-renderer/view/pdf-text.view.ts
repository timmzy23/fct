import { PdfView } from './pdf.view';

export class PdfTextView extends PdfView {

  constructor() {
    super('pdf-text');
  }

  public render() {
    const fontStyle = this.getAttribute('fontStyle') || 
      this.context.getDefaultStyle('fontStyle');
    const fontName = this.getAttribute('fontName') || 
      this.context.getDefaultStyle('fontName');
    const fontSize = this.getFontUnitAttribute('fontSize') || 
      this.context.convertFontUnit(this.context.getDefaultStyle('fontSize'));

    if (fontName && fontStyle) {
      this.context.getDriver().setFont(fontName, fontStyle);
    } else if (!fontName && fontStyle) {
      this.context.getDriver().setFontStyle(fontStyle);
    }

    if (fontSize) {
      this.context.getDriver().setFontSize(fontSize);
    }

    this.context.getDriver().text(this.element.textContent, 0, 10);
  }
}