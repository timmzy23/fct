import { PdfViewContext } from '../services';

export class PdfView {

  private static __VIEW_KEY__ = Symbol('view');

  public element: HTMLElement;

  public context: PdfViewContext;

  private component;

  public static fetchView(element: Element): PdfView {
    return element[PdfView.__VIEW_KEY__];
  }

  constructor(public name: string = 'pdf-element') {
    this.element = document.createElement(name);

    this.element[PdfView.__VIEW_KEY__] = this;
  }

  public setComponent(component: any) {
    this.component = component;
  }

  public getComponent<C>(): C {
    return this.getComponent as any as C;
  }

  public render() {
    this.children.forEach((child) => child.render());
  }

  public get parent(): PdfView | undefined {
    return PdfView.fetchView(this.element.parentElement);
  }

  public get children(): PdfView[] {
    const childViews = [];

    for (let i = 0; i < this.element.children.length; i++) {
      const childView = PdfView.fetchView(this.element.children.item(i));
      if (childView) {
        childViews.push(childView);
      }
    }

    return childViews;
  }

  public getAttribute(name: string) {
    return this.element.getAttribute(name);
  }

  public getUnitAttribute(name: string): number {
    const value = this.getAttribute(name);

    if (!value) { return; }

    const valueNum = Number.parseFloat(value);


    return this.context.convertUnit ? 
        this.context.convertUnit(valueNum) : 
        valueNum;
  }

  public getFontUnitAttribute(name: string): number {
    const value = this.getAttribute(name);

    if (!value) { return; }

    const valueNum = Number.parseFloat(value);

    return this.context.convertFontUnit ? 
        this.context.convertFontUnit(valueNum) : 
        valueNum;
  }
}