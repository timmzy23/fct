import { PdfView } from './pdf.view';

export class PdfPageView extends PdfView {

  constructor() {
    super('pdf-page');
  }

  public render() {
    if (this.context.pageCount > 1) {
      this.context.getDriver().addPage();
    }
    this.context.pageCount += 1;

    super.render();
  }
}