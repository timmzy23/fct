import { Injectable } from '@angular/core';
import { PdfViewContext } from '../services';

@Injectable()
export class PdfViewContextFactory {
  public create(): PdfViewContext {
    return new PdfViewContext();
  }
}