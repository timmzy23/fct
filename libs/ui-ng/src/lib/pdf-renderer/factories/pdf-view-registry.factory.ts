import { Injectable } from '@angular/core';
import { PdfViewRegistry } from '../services';
import {
  PdfPageView,
  PdfRootView,
  PdfTextView
  } from '../view';
import { PdfDocumentView } from '../view/pdf.document.view';
import { PdfViewContextFactory } from './pdf-view-context.factory';

@Injectable()
export class PdfViewRegistryFactory {

  constructor(private contextFactory: PdfViewContextFactory) {}

  public create() {
    return new PdfViewRegistry({
      ['fct-pdf-root']: () => new PdfRootView(this.contextFactory.create()),
      ['fct-pdf-document']: () => new PdfDocumentView(),
      ['fct-pdf-page']: () => new PdfPageView(),
      ['fct-pdf-text']: () => new PdfTextView()
    });
  }
}