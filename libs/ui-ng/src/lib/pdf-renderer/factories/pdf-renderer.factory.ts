import {
  Injectable,
  RendererFactory2,
  RendererType2
  } from '@angular/core';
import { PdfRenderer } from '../services';
import { PdfViewContextFactory } from './pdf-view-context.factory';
import { PdfViewRegistryFactory } from './pdf-view-registry.factory';

@Injectable()
export class PdfRendererFactory implements RendererFactory2 {

  private pdfRenderer: PdfRenderer;

  constructor(
    private contextFactory: PdfViewContextFactory,
    private elementRegistryFactory: PdfViewRegistryFactory
  ) {}
  
  public createRenderer(hostElement: any, type: RendererType2): PdfRenderer {
    if (!this.pdfRenderer) {
      this.pdfRenderer = new PdfRenderer(
        this.contextFactory, 
        this.elementRegistryFactory.create());
    }
    return this.pdfRenderer;
  }

  public getPdfRenderer() {
    return this.pdfRenderer;
  }
}
