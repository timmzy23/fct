import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild
  } from '@angular/core';
import { PdfBaseComponent } from './pdf-base.component';

@Component({
  selector: 'fct-pdf-text',
  template: '<ng-content #rootElm></ng-content>'
})
export class PdfTextComponent extends PdfBaseComponent {
  @Input()
  public fontSize: number;

  @Input()
  public fontStyle: string;

  @Input()
  public fontName: string;

  @ViewChild('pdfText', { static: true, read: ElementRef })
  public pdfTextRef: ElementRef; 
}