import {
  AfterViewInit,
  ElementRef,
  ViewChild
  } from '@angular/core';
import { PdfView } from '../view';

export class PdfBaseComponent implements AfterViewInit {
  @ViewChild('rootElm', { static: true, read: ElementRef })
  public rootElmRef: ElementRef;  

  public ngAfterViewInit() {
    const view = PdfView.fetchView(this.rootElmRef.nativeElement);

    view.setComponent(this);
  }
}