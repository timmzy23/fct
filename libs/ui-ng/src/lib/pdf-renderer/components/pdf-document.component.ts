import {
  Component,
  Input,
  OnInit
  } from '@angular/core';
import { PdfBaseComponent } from './pdf-base.component';

@Component({
  selector: 'fct-pdf-document',
  template: '<ng-content #rootElm></ng-content>'
})
export class PdfDocumentComponent extends PdfBaseComponent {
  @Input()
  public orientation: string;

  @Input()
  public unit: string;

  @Input()
  public format: string;

  @Input()
  public fileName: string;

  @Input()
  public fontSize: number;

  @Input()
  public fontStyle: string;

  @Input()
  public fontName: string;

  @Input()
  public convertUnit: (unit: number) => number;

  @Input()
  public convertFontUnit: (unit: number) => number;
}