import { Renderer2 } from '@angular/core';
import { ErrnoError } from '@fct/sdk';
import { PdfViewContextFactory } from '../factories';
import { ERR_NO_ATTACHED_VIEW } from '../pdf-renderer.errors';
import { PdfRootView } from '../view';
import { PdfView } from '../view/pdf.view';
import { PdfViewRegistry } from './pdf-view-registry';

export class PdfRenderer implements Renderer2 {

  public roots: any[] = [];

  constructor(
    private contextFactory: PdfViewContextFactory,
    private viewRegistry: PdfViewRegistry
  ) {}

  data: { [key: string]: any; };  destroy(): void {
    throw new Error("Method not implemented.");
  }

  public createElement(name: string, namespace?: string): any {
    return this.viewRegistry.createView(name).element;
  }

  createComment(value: string) {
    throw new Error("Method not implemented.");
  }
  createText(value: string) {
    return document.createTextNode(value);
  }
  destroyNode: (node: any) => void;


  public appendChild(parent: Element, newChild: Element): void {
    

    if (!(newChild instanceof Text)) {
      const paretView = PdfView.fetchView(parent);
      const childView = PdfView.fetchView(newChild);

      if (!childView) {
        throw new ErrnoError(
          ERR_NO_ATTACHED_VIEW,
          `No attached view found for ${newChild.tagName}`);
      }
  
      childView.context = paretView.context;
    }

    parent.appendChild(newChild);
  }

  insertBefore(parent: any, newChild: any, refChild: any): void {
    throw new Error("Method not implemented.");
  }
  removeChild(parent: any, oldChild: any, isHostElement?: boolean): void {
    throw new Error("Method not implemented.");
  }

  public selectRootElement(selectorOrNode: any, preserveContent?: boolean) {
    const root = this.viewRegistry.createView('fct-pdf-root');

    this.roots.push(root);

    return root.element;
  }

  parentNode(node: any) {
    throw new Error("Method not implemented.");
  }
  nextSibling(node: any) {
    throw new Error("Method not implemented.");
  }
  public setAttribute(el: HTMLElement, name: string, value: string, namespace?: string): void {
    el.setAttribute(name, value);
  }
  removeAttribute(el: any, name: string, namespace?: string): void {
    el.dataset[name] = undefined;
  }
  addClass(el: any, name: string): void {
    throw new Error("Method not implemented.");
  }
  removeClass(el: any, name: string): void {
    throw new Error("Method not implemented.");
  }
  setStyle(el: any, style: string, value: any, flags?: import("@angular/core").RendererStyleFlags2): void {
    throw new Error("Method not implemented.");
  }
  removeStyle(el: any, style: string, flags?: import("@angular/core").RendererStyleFlags2): void {
    throw new Error("Method not implemented.");
  }
  setProperty(el: any, name: string, value: any): void {
    throw new Error("Method not implemented.");
  }
  setValue(node: any, value: string): void {
    throw new Error("Method not implemented.");
  }
  listen(target: any, eventName: string, callback: (event: any) => boolean | void): () => void {
    throw new Error("Method not implemented.");
  }
}