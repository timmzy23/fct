import { ErrorHandler } from '@angular/core';

export class PdfRendererErrorHandler implements ErrorHandler {
  public handleError(error: Error): void {
    console.error(error.message, error.stack);
  }
}