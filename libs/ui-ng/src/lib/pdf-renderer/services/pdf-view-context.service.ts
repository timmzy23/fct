import { ErrnoError } from '@fct/sdk';
import * as JsPdf from 'jspdf';
import { ERR_NO_PDF_DRIVER } from '../pdf-renderer.errors';
import { PdfFontFace } from '../pdf.interfaces';

export declare namespace PdfViewContext {
  interface DriverInit {
    orientation: string;
    format: string;
    unit: string;
  }
}

export class PdfViewContext {

  public pdf: JsPdf;

  public pageCount = 1;

  public fileName: string;

  public fonts: PdfFontFace[] = [];

  public defaultStyles: {[key: string]: any} = {};

  public convertUnit?: (value: number) => number;

  public convertFontUnit?: (value: number) => number;

  public createDriver(init: PdfViewContext.DriverInit) {
    this.pdf = new JsPdf(
      init.orientation,
      init.unit,
      init.format,
      1.0);
  }

  public getDriver(): JsPdf {
    if (!this.pdf) {
      throw new ErrnoError(ERR_NO_PDF_DRIVER);
    }
    return this.pdf;
  }

  public registerFonts(fonts: PdfFontFace[]) {
    this.fonts = fonts;
  }

  public addDefaultStyle(key: string, value: any) {
    this.defaultStyles[key] = value;
  }

  public getDefaultStyle(key): any {
    return this.defaultStyles[key];
  }
}