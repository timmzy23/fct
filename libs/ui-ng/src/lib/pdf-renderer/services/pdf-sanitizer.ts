import {
  Sanitizer,
  SecurityContext
  } from '@angular/core';

export class PdfSanitizer implements Sanitizer {
  public sanitize(context: SecurityContext, value: string | {}): string {
    return value as string;
  }
}