import { ErrnoError } from '@fct/sdk';
import { ERR_UNKNOWN_PDF_VIEW } from '../pdf-renderer.errors';
import { PdfView } from '../view/pdf.view';

export class PdfViewRegistry {

  constructor(
    private registry: {[name: string]: (name: string) => PdfView }
  ) {}

  public createView(name): PdfView {
    if (!Reflect.has(this.registry, name)) {
      throw new ErrnoError(
          ERR_UNKNOWN_PDF_VIEW,
          `PdfRenderer: Unknown pdf view "${name}"!`,
          { name });
    }
    return this.registry[name](name);
  }
}