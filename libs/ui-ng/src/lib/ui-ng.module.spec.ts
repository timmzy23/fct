import { async, TestBed } from '@angular/core/testing';
import { UiNgModule } from './ui-ng.module';

describe('UiNgModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiNgModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(UiNgModule).toBeDefined();
  });
});
