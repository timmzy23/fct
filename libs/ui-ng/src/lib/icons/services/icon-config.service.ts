import { Injectable } from '@angular/core';
import {
  IconSize,
  IconStrokeWidth
  } from '../directives';
import { IconRole } from '../icon-role.enum';

export declare namespace IconConfigService {
  interface Config {
    defaultIconSize?: IconSize | string;
    defaultIconStrokeColor?: IconRole | string;
    defaultIconStrokeWidth?: IconStrokeWidth | number;
    defaultIconFillColor?: IconRole | string;
    defaultIconCirleRadius?: number;
  }
}


@Injectable({
  providedIn: 'root'
})
export class IconConfigService {

  public static defaultConfig: IconConfigService.Config = {
    defaultIconSize: IconSize.MEDIUM,
    defaultIconStrokeColor: IconRole.PRIMARY,
    defaultIconStrokeWidth: IconStrokeWidth.REGULAR,
    defaultIconFillColor: 'transparent',
    defaultIconCirleRadius: 20
  }

  private config: IconConfigService.Config;

  constructor(config: IconConfigService.Config = {}) {
    this.config = {
      ...IconConfigService.defaultConfig, 
      ...config
    }
  }

  public get defaultIconSize() {
    return this.config.defaultIconSize;
  }

  public get defaultIconStrokeColor() {
    return this.config.defaultIconStrokeColor;
  }

  public get defaultIconStrokeWidth() {
    return this.config.defaultIconStrokeWidth;
  }

  public get defaultIconFillColor() {
    return this.config.defaultIconFillColor;
  }

  public get defaultIconCirleRadius() {
    return this.config.defaultIconCirleRadius;
  }
}