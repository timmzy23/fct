import {
  Component,
  Input
  } from '@angular/core';
import {
  IconSize,
  IconStrokeWidth
  } from '../../directives';
import { IconRole } from '../../icon-role.enum';

@Component({
  selector: 'fct-abstract-icon',
  templateUrl: './icon.component.svg',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent {

  @Input()
  public circleRadius = 20;

  @Input()
  public circled = false;

  @Input()
  public iconFillColor: IconRole | string = 'transparent';

  @Input()
  public fillColor?: string;

  @Input()
  public size: IconSize | string = IconSize.MEDIUM;

  @Input()
  public iconStrokeColor: IconRole | string = IconRole.PRIMARY;

  @Input()
  public iconStrokeWidth: IconStrokeWidth | number = IconStrokeWidth.REGULAR;
}
