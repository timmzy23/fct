import {
  Component,
  Input
  } from '@angular/core';
import {
  IconSize,
  IconStrokeWidth
  } from '../../directives';
import { IconDirection } from '../../directives';
import { IconRole } from '../../icon-role.enum';
import { IconConfigService } from '../../services';

@Component({
  selector: 'fct-icon-wrapper',
  templateUrl: './icon-wrapper.component.svg',
  styleUrls: ['icon.component.scss']
})
export class IconWrapperComponent {

  public iconResolutionWidth = 70;

  public iconResolutionHeight= 70;

  @Input()
  public circleRadius = this.iconConfig.defaultIconCirleRadius;

  @Input()
  public iconTitle: string;

  @Input()
  public direction: IconDirection | number;

  @Input()
  public flip: boolean;

  @Input()
  public circled = false;

  @Input()
  public iconFillColor: IconRole | string = this.iconConfig.defaultIconFillColor;

  @Input()
  public fillColor?: string;

  @Input()
  public size: IconSize | string = this.iconConfig.defaultIconSize;

  @Input()
  public iconStrokeColor: IconRole | string = this.iconConfig.defaultIconStrokeColor;

  @Input()
  public iconStrokeWidth: IconStrokeWidth | number = this.iconConfig.defaultIconStrokeWidth;

  constructor(private iconConfig: IconConfigService) {}

  public getViewBox() {
    return `0 0 ${this.iconResolutionWidth} ${this.iconResolutionHeight}`;
  }
}
