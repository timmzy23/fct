import {
  Component,
  Input
  } from '@angular/core';
import { IconDirection } from '../../directives';
import { IconComponent } from '../icon/icon.component';

@Component({
  selector: 'fct-arrow-icon',
  templateUrl: './arrow.icon.component.svg',
  styleUrls: ['../icon/icon.component.scss']
})
export class ArrowIconComponent extends IconComponent {
  @Input()
  public direction = IconDirection.RIGHT;

  @Input()
  public noLeg = false;
}