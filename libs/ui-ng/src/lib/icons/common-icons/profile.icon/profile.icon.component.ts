import {
  Component,
  Input
  } from '@angular/core';
import { IconDirection } from '../../directives';
import { IconComponent } from '../icon/icon.component';

@Component({
  selector: 'fct-profile-icon',
  templateUrl: './profile.icon.component.svg',
  styleUrls: ['../icon/icon.component.scss']
})
export class ProfileIconComponent extends IconComponent {
  @Input()
  public type: 'default' | 'Add' | 'search' | 'settings' = 'default';

  @Input()
  public direction: any;
}