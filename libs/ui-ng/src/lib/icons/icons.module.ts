import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  ConfigModule,
  ConfigService
  } from '@fct/sdk-ng';
import { CustomModule } from '../custom';
import {
  ArrowIconComponent,
  IconComponent,
  IconWrapperComponent,
  ProfileIconComponent
  } from './common-icons';
import {
  IconFillColorDirective,
  IconSizeDirective,
  IconStrokeColorDirective,
  IconStrokeWidthDirective,
  IconTransformDirective
  } from './directives';
import { IconConfigService } from './services';
import { 
  PlusIconComponent,
  FolderOpenIconComponent,
  FolderIconComponent,
} from './components/common/';

const COMPONENTS = [
  FolderOpenIconComponent,
  FolderIconComponent,
  PlusIconComponent,
  IconWrapperComponent,
  IconComponent,
  ArrowIconComponent,
  ProfileIconComponent,
];

const DIRECTIVES = [
  IconTransformDirective,
  IconStrokeWidthDirective,
  IconStrokeColorDirective,
  IconSizeDirective,
  IconFillColorDirective,
];

@NgModule({
  declarations: [
    ...DIRECTIVES,
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    CustomModule,
    ConfigModule
  ],
  exports: [
    ...DIRECTIVES,
    ...COMPONENTS
  ],
  providers: [
    {
      provide: IconConfigService,
      useFactory: (config: ConfigService) => {
        return new IconConfigService({
          defaultIconSize: config.get<string>('ui.icon.defaultIconSize'),
          defaultIconStrokeColor: config.get<string>('ui.icon.defaultIconStrokeColor'),
          defaultIconStrokeWidth: config.get<number>('ui.icon.defaultIconStrokeWidth'),
          defaultIconFillColor: config.get<string>('ui.icon.defaultIconFillColor'),
          defaultIconCirleRadius: config.get<number>('ui.icon.defaultIconCirleRadius')
        });
      },
      deps: [ConfigService]
    }
  ]
})
export class IconsModule { }
