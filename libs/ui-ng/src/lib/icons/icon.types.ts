export enum IconStrokeWidth {
  THIN = 'thin',
  LIGHT = 'light',
  REGULAR = 'regular',
  BOLD = 'bold',
}

export enum IconSize {
  TINY = 'tiny',
  EXTRA_SMALL = 'extra-small',
  SMALL = 'small',
  MEDIUM = 'medium',
}

export enum IconColor {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  INVERTED = 'inverted',
  INVERTED_HOVER = 'inverted-hover',
  ERROR = 'error',
  DISABLED = 'disabled'
}
