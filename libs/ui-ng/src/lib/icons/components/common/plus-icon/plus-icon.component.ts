import {
  Component,
  Input
  } from '@angular/core';
import { AbstractIconComponent } from '../abstract-icon/abstract-icon.component';

@Component({
  selector: 'fct-plus-icon',
  templateUrl: './plus-icon.component.svg',
  styleUrls: ['../abstract-icon/abstract-icon.component.scss']
})
export class PlusIconComponent extends AbstractIconComponent {

}
