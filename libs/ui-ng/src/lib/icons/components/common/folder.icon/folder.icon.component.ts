import {
  Component,
  Input
  } from '@angular/core';
import { IconColor } from '../../../icon.types';
import { AbstractIconComponent } from '../abstract-icon/abstract-icon.component';

@Component({
  selector: 'fct-folder-icon',
  templateUrl: './folder.icon.component.svg',
  styleUrls: ['../abstract-icon/abstract-icon.component.scss']
})
export class FolderIconComponent extends AbstractIconComponent {
  @Input()
  public separatorColor?: IconColor | string;
}