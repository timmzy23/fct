import {
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import {
  IconColor,
  IconSize,
  IconStrokeWidth
  } from '../../../icon.types';

export class AbstractIconComponent implements OnInit, OnChanges {
  @Input()
  public strokeWidth: IconStrokeWidth | number = IconStrokeWidth.THIN;

  @Input()
  public iconSize: IconSize | number = IconSize.MEDIUM;

  @Input()
  public iconColor: IconColor | string = IconColor.PRIMARY;

  @Input()
  public iconColorHover: IconColor | string;

  @Input()
  public background: IconColor | string;

  public strokeWidthComputed: number;

  public iconSizeComputed: string;

  public ngOnInit() {
    this.strokeWidthComputed = this.resolveStrokeWidth(this.strokeWidth);
    this.iconSizeComputed = this.resolveIconSize(this.iconSize);
  }

  public ngOnChanges(changes: SimpleChanges) {
    const strokeWidth = changes.strokeWidth;
    const iconSize = changes.iconSize;

    if (strokeWidth && strokeWidth.currentValue !== strokeWidth.previousValue) {
      this.strokeWidthComputed = 
          this.resolveStrokeWidth(strokeWidth.currentValue);
    }

    if (iconSize && iconSize.currentValue !== iconSize.previousValue
    ) {
      this.iconSizeComputed = this.resolveIconSize(iconSize.currentValue);
    }
  }


  private resolveStrokeWidth(input: IconStrokeWidth | number): number {
    if (ReflectionHelper.isNumber(input)) {
      return input as number;
    } else {
      switch (input) {
        case IconStrokeWidth.THIN:
          return 2;
        case IconStrokeWidth.LIGHT:
          return 3;
        case IconStrokeWidth.REGULAR:
          return 4;
        case IconStrokeWidth.BOLD:
          return 6;
      }
    }
  }

  private resolveIconSize(input: IconSize | number): string {
    if (ReflectionHelper.isNumber(input)) {
      return `${input}px`;
    } else {
      switch (input) {
        case IconSize.TINY:
          return '15px';
        case IconSize.EXTRA_SMALL:
          return '25px';
        case IconSize.SMALL:
          return '35px';
        case IconSize.MEDIUM:
          return '50px';
      }
    }
  }
}