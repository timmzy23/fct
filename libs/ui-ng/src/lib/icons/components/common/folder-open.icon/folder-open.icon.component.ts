import { Component } from '@angular/core';
import { AbstractIconComponent } from '../abstract-icon/abstract-icon.component';

@Component({
  selector: 'fct-folder-open-icon',
  templateUrl: './folder-open.icon.component.svg',
  styleUrls: ['../abstract-icon/abstract-icon.component.scss']
})
export class FolderOpenIconComponent extends AbstractIconComponent {
  
}