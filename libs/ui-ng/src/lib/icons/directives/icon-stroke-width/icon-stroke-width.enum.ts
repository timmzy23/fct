export enum IconStrokeWidth {
    EXTRA_THIN = 'extra-thin',
    THIN = 'thin',
    LIGHT = 'light',
    REGULAR = 'regular',
    SEMIBOLD = 'semibold',
    BOLD = 'bold',
    EXTRA_BOLD = 'extra-bold'
}