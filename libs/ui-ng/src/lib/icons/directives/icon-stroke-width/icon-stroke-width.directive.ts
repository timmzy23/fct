import {
  Directive,
  Input,
  OnChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ToggleClassOrAttributeDirective } from '../../../custom';
import { IconStrokeWidth } from './icon-stroke-width.enum';

@Directive({
  selector: '[fctIconStrokeWidth]'
})
export class IconStrokeWidthDirective 
  extends ToggleClassOrAttributeDirective
  implements OnChanges 
{
  public attrName = 'stroke-width';

  @Input('fctIconStrokeWidth')
  public value: IconStrokeWidth = IconStrokeWidth.EXTRA_BOLD;

  protected buildClassName(iconStroke: IconStrokeWidth): string {
    return `fct-icon-stroke-width--${iconStroke}`;
  }

  protected isClassDef(nextValue: any) {
    return !ReflectionHelper.isNumber(nextValue);
  }
}
