import {
  Directive,
  Input,
  OnChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ToggleClassOrAttributeDirective } from '../../../custom';
import { IconRole } from '../../icon-role.enum';

@Directive({
selector: '[fctIconStrokeColor]'
})
export class IconStrokeColorDirective 
  extends ToggleClassOrAttributeDirective
  implements OnChanges 
{
  public attrName = 'stroke';

  @Input('fctIconStrokeColor')
  public value: IconRole | string = IconRole.PRIMARY;

  protected buildClassName(iconStroke: IconRole): string {
    return `fct-icon-stroke-color--${iconStroke}`;
  }

  protected isClassDef(nextValue: any) {
    return ReflectionHelper.isInEnum(IconRole, nextValue);
  }
}
