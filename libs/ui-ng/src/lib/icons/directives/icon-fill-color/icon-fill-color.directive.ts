import {
  Directive,
  Input,
  OnChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ToggleClassOrAttributeDirective } from '../../../custom';
import { IconRole } from '../../icon-role.enum';

@Directive({
selector: '[fctIconFillColor]'
})
export class IconFillColorDirective 
  extends ToggleClassOrAttributeDirective
  implements OnChanges 
{
  public attrName = 'fill';

  @Input('fctIconFillColor')
  public value: IconRole | string = 'transparent';

  protected buildClassName(iconRole: IconRole): string {
    return `fct-icon-fill-color--${iconRole}`;
  }

  protected isClassDef(nextValue: any) {
    return ReflectionHelper.isInEnum(IconRole, nextValue);
  }
}
