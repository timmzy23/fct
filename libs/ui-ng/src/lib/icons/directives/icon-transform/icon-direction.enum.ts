export enum IconDirection {
  LEFT = 'left',
  RIGHT = 'right',
  UP = 'up',
  DOWN = 'down',
  LEFT_UP = 'left-up',
  LEFT_DOWN = 'left-down',
  RIGHT_UP = 'right-up',
  RIGHT_DOWN = 'right-down'
}