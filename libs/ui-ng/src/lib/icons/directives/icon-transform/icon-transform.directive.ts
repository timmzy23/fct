import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { IconDirection } from './icon-direction.enum';

@Directive({
  selector: '[fctIconTransform]'
})
export class IconTransformDirective implements OnChanges {

  @Input('fctIconTransformDirection')
  public direction: IconDirection | number;

  @Input('fctIconTransformFlip')
  public flip = false;

  @Input('fctTranslateX') 
  public translateX: number;

  @Input('fctTranslateY')
  public translateY: number;

  private directionAngleMap = {
    [IconDirection.RIGHT_DOWN]: 45,
    [IconDirection.DOWN]: 90,
    [IconDirection.LEFT_DOWN]: 135,
    [IconDirection.LEFT]: 180,
    [IconDirection.LEFT_UP]: 225,
    [IconDirection.UP]: 270,
    [IconDirection.RIGHT_UP]: 315
  };

  constructor(
    protected elm: ElementRef,
    protected renderer: Renderer2
  ) { }

  public ngOnChanges(changes: SimpleChanges) {
    const transformInstructions = this.renderIconTransformInstructions(
      changes.direction.currentValue,
      changes.flip.currentValue,
      changes.translateX.currentValue,
      changes.translateY.currentValue);

    if (transformInstructions) {
      this.renderer.setAttribute(
          this.elm.nativeElement, 
          'transform', 
          transformInstructions);
    }
  }

  private renderIconTransformInstructions(
    direction?: IconDirection | number,
    flip?: boolean,
    translateX?: string,
    translateY?: string
  ) {
    const transformRules = [];

    if (direction) {
      const angle = this.getRotateAngle(direction);
      if (angle) {
        transformRules.push(`rotate(${angle} 35 35)`);
      }
    }

    if (flip) {
      transformRules.push('scale(-1, 1)');
      transformRules.push('translate(35, 0)');
    }

    if (translateX !== undefined || translateY !== undefined) {
      const tX = translateX || 0;
      const tY = translateY || 0;
      transformRules.push(`translate(${tX}, ${tY})`);
    }

    return transformRules.length > 0 ?
      transformRules.join(',') :
      '';
  }

  private getRotateAngle(direction: IconDirection | number): number | undefined {
    if (ReflectionHelper.isNumber(direction)) {
      return direction as number;
    } else if (Reflect.has(this.directionAngleMap, direction)) {
      return this.directionAngleMap[direction];
    }
  }
}