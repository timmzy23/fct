export enum IconSize {
  EXTRA_SMALL = 'xs',
  SMALL = 's',
  MEDIUM = 'm',
  LARGE = 'l',
  EXTRA_LARGE = 'xl'
}