import {
  Directive,
  Input,
  OnChanges,
  SimpleChanges
  } from '@angular/core';
import { ReflectionHelper } from '@fct/sdk';
import { ToggleClassOrAttributeDirective } from '../../../custom';
import { IconSize } from './icon-size.enum';

@Directive({
selector: '[fctIconSize]'
})
export class IconSizeDirective 
  extends ToggleClassOrAttributeDirective
  implements OnChanges 
{
  public attrName = 'dummy';

  @Input('fctIconSize')
  public value: IconSize | string = IconSize.MEDIUM;

  protected buildClassName(iconSize: IconSize): string {
    return `fct-icon-size--${iconSize}`;
  }

  protected isClassDef(nextValue: any) {
    return ReflectionHelper.isInEnum(IconSize, nextValue);
  }

  protected getNextAttributes(
    changes: SimpleChanges
  ): ToggleClassOrAttributeDirective.AttributeDefs {
    return {
      width: changes.value.currentValue,
      height: changes.value.currentValue
    }
  }
}
