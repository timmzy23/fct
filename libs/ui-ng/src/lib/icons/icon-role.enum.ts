export enum IconRole {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  MARGINAL = 'marginal',
  ERROR = 'error'
}