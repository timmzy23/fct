import { Component } from '@angular/core';

@Component({
  selector: 'fct-asap-spinner',
  templateUrl: './asap-spinner.component.svg',
  styleUrls: ['./asap-spinner.component.scss']
})
export class AsapSpinnerComponent {}
