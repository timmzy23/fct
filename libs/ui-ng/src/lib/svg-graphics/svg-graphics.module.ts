import { NgModule } from '@angular/core';
import { AsapSpinnerComponent } from './asap-spinner/asap-spinner.component';


const COMPONENTS = [
  AsapSpinnerComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class SvgGraphicsModule {

}
