import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Renderer2
  } from '@angular/core';
import { ErrnoError } from '@fct/sdk';
import { ERR_NO_PORTAL_OUTLET } from './portal.errors';

export declare namespace PortalService {

  interface PortalProps {
    inputs?: { [key: string]: any };
    outputs: { [key: string]: any };
  }
}

@Injectable()
export class PortalService {

  constructor(
    private resolver: ComponentFactoryResolver,
    private rootInjector: Injector,
    private appRef: ApplicationRef
  ) {

  }

  /**
   * Append component instance to a specific DOM area
   */
  public appendPortal<C>(
    selector: string, 
    portalDef: any, 
    portalProps?: PortalService.PortalProps,
    lazyInjector?: Injector
  ): ComponentRef<C> {
    const portalRef: ComponentRef<C> = this.resolver
        .resolveComponentFactory<C>(portalDef)
        .create(lazyInjector || this.rootInjector);

    portalRef.instance['inputs'] = portalProps.inputs;
    portalRef.instance['outputs'] = portalProps.outputs;

    this.appRef.attachView(portalRef.hostView);

    const portalRootDomElm = (portalRef.hostView as EmbeddedViewRef<any>)
        .rootNodes[0] as HTMLElement;

    this.appendToDom(selector, portalRootDomElm);

    return portalRef
  }

  public removePortal(portalRef: ComponentRef<any>) {
    this.appRef.detachView(portalRef.hostView);
    portalRef.destroy();
  }

  private appendToDom(selector: string, child: HTMLElement) {
    const portalOutlet = document.querySelector(selector);

    if (!portalOutlet) { 
      throw new ErrnoError(
        ERR_NO_PORTAL_OUTLET,
        `No portal outlet found by "${selector}"`);
    }

    portalOutlet.appendChild(child);
  }
}