import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PortalService } from './portal.service';

@NgModule({
  providers: [
    PortalService
  ],
  imports: [
    CommonModule
  ]
})
export class PortalModule {}