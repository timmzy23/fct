import {
  Injectable,
  OnDestroy,
  OnInit
  } from '@angular/core';
import {
  actionOf,
  CommandActionBus,
  SubscriptionHandler
  } from '@fct/sdk-ng';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  OverlayHideCommand,
  OverlayShowCommand
  } from './overlay.commands';
import { OverlayStore } from './overlay.store';

@Injectable({
  providedIn: 'root'
})
export class OverlayService implements OnInit, OnDestroy {
  private subscriptons = new SubscriptionHandler();

  constructor(
    private commands$: CommandActionBus,
    private store: OverlayStore
  ) {}

  public ngOnInit() {
    this.subscriptons.add(this.commands$
      .pipe(actionOf(OverlayShowCommand))
      .subscribe(this.handleShowOverlay));

    this.subscriptons.add(this.commands$
      .pipe(actionOf(OverlayHideCommand))
      .subscribe(this.handleHideOverlay));
  }

  public ngOnDestroy() {
    this.subscriptons.unsubscribe();
  }

  private handleShowOverlay = (command: OverlayShowCommand.Action) => {
    this.store.showVisible(command.payload.layerId);
  }

  private handleHideOverlay = (command: OverlayHideCommand.Action) => {
    this.store.hideOverlay(command.payload.layerId);
  }
}
