import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AnimationModule } from '../animations';
import { OverlayComponent } from './overlay.component';

const COMPONENTS = [
  OverlayComponent
]

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    AnimationModule
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class OverlayModule {

}