import { Injectable } from '@angular/core';
import {
  ApplicationState,
  ObservableMapStore
  } from '@fct/sdk-ng';
import { Observable } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';

export declare namespace OverlayStore {
  type StateRecord = boolean;
}

@Injectable({
  providedIn: 'root'
})
export class OverlayStore extends ObservableMapStore<OverlayStore.StateRecord> {
  constructor(appState: ApplicationState) {
    super('asap.overlay.store', appState);
  }

  public showVisible(overlayId: string) {
    this.set(overlayId, true);
  }

  public isOverlayVisible(overlayId: string): boolean {
    return !!this.get(overlayId)
  }

  public hideOverlay(overlayId: string) {
    this.set(overlayId, false);
  }

  public observeOverlay(overlayId: string): Observable<OverlayStore.StateRecord> {
    return this.observe.pipe(
        filter(
          (change: ObservableMapStore.ChangeEvent<OverlayStore.StateRecord>) => 
              change.key === overlayId),
        map((change) => change.nextRecord));
  }
}
