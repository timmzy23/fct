import { FluxStandardAction } from '@fct/sdk-ng';
export enum OVERLAY_COMMAND_TYPES {
  SHOW = 'cmd.overlay.show',
  HIDE = 'cmd.overlay.hide'
}

export declare namespace OverlayShowCommand {
  type ACTION_TYPE = OVERLAY_COMMAND_TYPES.SHOW;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    layerId: string;
  }
}

export class OverlayShowCommand {
  public static ACTION_TYPE: OverlayShowCommand.ACTION_TYPE = 
    OVERLAY_COMMAND_TYPES.SHOW;

  public static create(payload: OverlayShowCommand.Payload): OverlayShowCommand.Action {
    return FluxStandardAction.create(OverlayShowCommand.ACTION_TYPE, payload);
  }
}

export declare namespace OverlayHideCommand {
  type ACTION_TYPE = OVERLAY_COMMAND_TYPES.HIDE;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    layerId: string;
  }
}

export class OverlayHideCommand {
  public static ACTION_TYPE: OverlayHideCommand.ACTION_TYPE = 
    OVERLAY_COMMAND_TYPES.HIDE;

  public static create(payload: OverlayHideCommand.Payload): OverlayHideCommand.Action {
    return FluxStandardAction.create(OverlayHideCommand.ACTION_TYPE, payload);
  }
}
