import { useEffect } from '@fct/sdk-ng';
import {
  IN_OUT_TRANSITION_DURATION_IN_MS,
  IN_OUT_TRANSITION_DURATION_OUT_MS,
  InOutTransitionState
  } from '../animations';
import { OverlayStore } from './overlay.store';
import {
  Component,
  Input,
  } from '@angular/core';

export function OverlayInOutTransitionStateFactory() {
  return new InOutTransitionState(150);
}

@Component({
  selector: 'fct-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss'],
  providers: [
    { provide: IN_OUT_TRANSITION_DURATION_IN_MS, useValue: 150 },
    { provide: IN_OUT_TRANSITION_DURATION_OUT_MS, useValue: 150 },
    InOutTransitionState
  ]
})
export class OverlayComponent {

  @Input()
  public overlayId: string;

  constructor(
    private store: OverlayStore,
    private inOutState: InOutTransitionState
  ) {}

  @useEffect([])
  private handleStore() {
    const subscription = this.store
      .observeOverlay(this.overlayId)
      .subscribe((isVisible: boolean) => isVisible ?
          this.inOutState.enter() :
          this.inOutState.leave());
    return () => subscription.unsubscribe();
  }
}