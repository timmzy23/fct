import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AsapLoaderComponent } from './custom-loaders';
import { LoaderComponent } from './loader.component';

const COMPONENTS = [
  LoaderComponent,
  AsapLoaderComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule
  ]
})
export class LoaderModule {}