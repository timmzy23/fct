import {
  Component,
  Input
  } from '@angular/core';

@Component({
  selector: 'fct-asap-loader',
  templateUrl: './asap-loader.component.html',
  styleUrls: ['./asap-loader.component.scss']
})
export class AsapLoaderComponent {
  @Input()
  public loaderId: string;
}
