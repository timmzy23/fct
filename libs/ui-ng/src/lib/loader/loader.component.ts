import {
  ChangeDetectorRef,
  Component,
  Input
  } from '@angular/core';
import {
  actionOf,
  AnyAction,
  CommandActionBus,
  useEffect
  } from '@fct/sdk-ng';
import { BehaviorSubject } from 'rxjs';
import {
  filter,
  map
  } from 'rxjs/operators';
import {
  LoaderHideCommand,
  LoaderShowCommand
  } from './loader.commands';

@Component({
  selector: 'fct-loader',
  template: `
    <div *ngIf="visible$|async" class="loader">
      <ng-content>
        <!-- Put custom overlay here -->
      </ng-content>
    </div>
  `,
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  @Input()
  public loaderId: string;

  public visible$ = new BehaviorSubject(false);

  constructor(
    private commands$: CommandActionBus,
    private changeRef: ChangeDetectorRef
  ) {}

  @useEffect([])
  private handleChange() {
    this.visible$.subscribe(() => 
        requestAnimationFrame(
            () => this.changeRef.detectChanges()));

    return () => this.visible$.complete();
  }

  @useEffect([])
  private handleLoaderShow() {
    const subscription = this.commands$
      .pipe(
          actionOf(LoaderShowCommand, LoaderHideCommand),
          filter((cmd: LoaderShowCommand.Action) => 
              cmd.payload.loaderId === this.loaderId),
          map((cmd: AnyAction) => 
              cmd.type === LoaderShowCommand.ACTION_TYPE))
      .subscribe(this.visible$ as any);
    
    return () => subscription.unsubscribe();
  }
}
