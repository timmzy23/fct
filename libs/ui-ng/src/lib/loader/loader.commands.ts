import { FluxStandardAction } from '@fct/sdk-ng';
export enum LOADER_COMMAND_TYPES {
  SHOW = 'cmd.loader.show',
  HIDE = 'cmd.loader.hide'
}


export declare namespace LoaderShowCommand {
  type ACTION_TYPE = LOADER_COMMAND_TYPES.SHOW;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    loaderId: string;
  }
}

export class LoaderShowCommand {
  public static ACTION_TYPE: LoaderShowCommand.ACTION_TYPE = 
    LOADER_COMMAND_TYPES.SHOW;

  public static create(payload: LoaderShowCommand.Payload): LoaderShowCommand.Action {
    return FluxStandardAction.create(LoaderShowCommand.ACTION_TYPE, payload);
  }

  public static main() {
    return LoaderShowCommand.create({ loaderId: 'main' });
  }
}

export declare namespace LoaderHideCommand {
  type ACTION_TYPE = LOADER_COMMAND_TYPES.HIDE;
  type Action = FluxStandardAction.Action<ACTION_TYPE, Payload>
  interface Payload {
    loaderId: string;
  }
}

export class LoaderHideCommand {
  public static ACTION_TYPE: LoaderHideCommand.ACTION_TYPE = 
    LOADER_COMMAND_TYPES.HIDE;

  public static create(payload: LoaderHideCommand.Payload): LoaderHideCommand.Action {
    return FluxStandardAction.create(LoaderHideCommand.ACTION_TYPE, payload);
  }

  public static main() {
    return LoaderHideCommand.create({ loaderId: 'main' });
  }
}
