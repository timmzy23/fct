import { DOCUMENT } from '@angular/common';
import {
  Inject,
  Injectable,
  OnDestroy,
  OnInit
  } from '@angular/core';
import { EventActionBus } from '@fct/sdk-ng';
import {
  fromEvent,
  Subscription
  } from 'rxjs';
import { filter } from 'rxjs/operators';
import {
  KeyboardArrowDownEvent,
  KeyboardArrowLeftEvent,
  KeyboardArrowRightEvent,
  KeyboardArrowUpEvent,
  KeyboardEnterEvent,
  KeyboardESCEvent,
  KeyboardOpenEvent
  } from './keyborad.events';

@Injectable()
export class KeyboardService implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];

  constructor(
    private eventBus: EventActionBus,
    @Inject(DOCUMENT)
    private document: Document
  ) {}

  public ngOnInit() {
    // Listen for "ESC"
    this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
        .pipe(filter((evt) => evt.code === 'Escape'))
        .subscribe((evt: KeyboardEvent) =>
            this.eventBus.emit(KeyboardESCEvent.create(evt))));

    // Listen for "Open"
    this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
        .pipe(filter((evt) => (evt.metaKey || evt.ctrlKey) && evt.code === 'KeyO'))
        .subscribe((evt: KeyboardEvent) =>
            this.eventBus.emit(KeyboardOpenEvent.create(evt))));

   // Listen for "ARROW_UP"
   this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
      .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowUp'))
      .subscribe((evt: KeyboardEvent) =>
          this.eventBus.emit(KeyboardArrowUpEvent.create(evt))));

  // Listen for "ARROW_UP"
  this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
      .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowDown'))
      .subscribe((evt: KeyboardEvent) =>
          this.eventBus.emit(KeyboardArrowDownEvent.create(evt))));

  // Listen for "ARROW_LEFT"
  this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowLeft'))
    .subscribe((evt: KeyboardEvent) =>
        this.eventBus.emit(KeyboardArrowLeftEvent.create(evt))));


  // Listen for "ARROW_RIGHT"
  this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'ArrowRight'))
    .subscribe((evt: KeyboardEvent) =>
        this.eventBus.emit(KeyboardArrowRightEvent.create(evt))));

  // Listen for "KEYBOARD_ENTER"
  this.subscriptions.push(fromEvent<KeyboardEvent>(this.document, 'keydown')
    .pipe(filter((evt) => (!evt.metaKey && !evt.ctrlKey && !evt.shiftKey) && evt.code === 'Enter'))
    .subscribe((evt: KeyboardEvent) =>
        this.eventBus.emit(KeyboardEnterEvent.create(evt))));
  }

  public ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}