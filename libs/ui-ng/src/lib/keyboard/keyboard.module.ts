import { NgModule } from '@angular/core';
import { ActionsModule } from '@fct/sdk-ng';
import { KeyboardService } from './keyboard.service';

@NgModule({
  imports: [
    ActionsModule
  ],
  providers: [
    KeyboardService
  ]
})
export class KeyboardModule {

}