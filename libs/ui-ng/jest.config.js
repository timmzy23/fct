module.exports = {
  name: 'ui-ng',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/ui-ng',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
